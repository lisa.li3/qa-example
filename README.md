# Test Automation

This is an example project for structure view only. It has not been configured with the right environment and will not run locally.

---

Test Status ( Latest Runs )

[<img src="http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/aws-account-id/badge/run.svg"/>](http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/aws-account-id/report.html)

[<img src="http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/aws-account-id/badge/run.svg"/>](http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/aws-account-id/report.html)

---

BDD test files written that are performed by a TypeScript backend, with Product and Config seeding.

## Prerequisites to setup

If not already provided you will need:

- Data required for the env file
- Access to AWS, with MFA key set up

Contact the test team if you do not have any of these.

## Requirements & Installation

- Node version 14 or higher ( nvmrc file present )

Copy Secret / Project Files - Contact Automation Team if you require any credentials

```shell
$ cp .env.example .env
$ cp .npmrc.example .npmrc
```

Install Dependencies:

```shell
$ yarn install
```

---

#Coding standards/Best practise:
##Naming conventions
Generally pascal case or camel case are preferred, I have mentioned in the folder structure if one is preferred over the other for this file type. Folders are created camel case in most cases.
##Basic folder structure

```
| Folder              | Notes                             | Case   |
|---------------------|-----------------------------------|--------|
| src/features        | Cucumber feature files            | Pascal |
|                     |                                   |        |
| src/pageobjects     | Page object files, note the page  | Pascal |
|                     | files are expected to only contain|        |
|                     | a list of selectors               |        |
|                     |                                   |        |
| src/seedingPayloads | Data required for seeding.        | Pascal |
|                     | Includes both:                    |        |
|                     |  - Data payloads (*.data.seed.ts) |        |
|                     |    for delivering content,        |        |
|                     |    product, inventory, or user    |        |
|                     |    seed data                      |        |
|                     |  - Config payloads                |        |
|                     |    (*.config.seed.ts) for         |        |
|                     |    delivering site configuration  |        |
|                     |    changes                        |        |
|                     |                                   |        |
| src/steps           | Cucumber step definition files    | Camel  |
|                     |                                   |        |
| src/support         | Functions to enable all of the    | Camel  |
|                     | above, along with type            |        |
|                     | definitions                       |        |
```

##Formatting and indentation
ES lint and prettier are enabled and should standardise this.

---

#Feature files:

## How to write a test

Tests are written in [Gherkin syntax](https://cucumber.io/docs/gherkin/)
that means that you write down what's supposed to happen in a real language. All test files are located in
`./src/features/*` and have the file ending `.feature`. You will already find some test files in that
directory. They should demonstrate, how tests could look like. Just create a new file and write your first
test.

**User.feature**

```gherkin
Feature:
    In order to keep people having access to the best clothing products
    As a user of the website
    I want to make sure that I am able to use all the great features

Scenario: Buy Some Products
    Given I open the site "/"
    When I pick a product that I want to buy
    And I complete the checkout process with "Credit and Debit Card"
    Then I expect that my order is confirmed

Scenario: Another test
    Given ...

```

## How to run the test

```sh
$ yarn test --help
```

_please note_ This will give you the options to pass into `yarn test`

## Running tests on Saucelabs or Locally

### Saucelabs:

This command will run the tests on saucelabs. This is the closest approximation of the final pipeline test run, however it is the slowest way to run tests.

```sh
$ yarn test
```

### Locally:

This will run your tests locally, without communicating to saucelabs, and will therefore run faster

```sh
$ yarn test local
```

This will run your tests locally, in a headless browser, which is the fastest way to run tests.

```sh
$ yarn test local headless
```

## Running single feature / Subset of features

Sometimes it's useful to only execute a single feature file, or just a small amount of them, to do so use the following command:

```sh
$ yarn test local desktop @TAG_TO_TEST
```

## Using tags

If you want to run only specific tests you can mark your features with tags. These tags will be placed before each feature like so:

```gherkin
@Tag
Feature: ...
```

###Tags currently in use:

- **@PENDING** - See below for further details
- **@HAPPY** - Denotes table sections to be used in a 'happy path' run. See the scenario outlines section for further detail
- **@MOBILE** - Scenario to only be executed on this platform
- **@TABLET** - Scenario to only be executed on this platform
- **@DESKTOP** - Scenario to only be executed on this platform
- **@B2C2-####** - Originating ticket that theAC's used to build this test came from

## Pending test

If you have failing or unimplemented tests you can mark them as "Pending" so they will get skipped.

```gherkin
// skip whole feature file
@Pending
Feature: ...

// only skip a single scenario
@Pending
Scenario: ...
```

## Comments

You can add additional descriptive comments in your feature files.

```gherkin
###
  This is a
  block comment
###
Feature: Some Feature
    I can do some things
    So that I end up completing the jobs

# This is a single line comment
Scenario: check if username is present
    Given I login as "superuser" with password "superS3cure"
    Then the username "Super User" should be present in the header
```

## Scenario outlines

Copying and pasting scenarios to use different values can quickly become tedious and repetitive:

```gherkin
Scenario: Eat 5 out of 12
  Given there are 12 cucumbers
  When I eat 5 cucumbers
  Then I should have 7 cucumbers

Scenario: Eat 5 out of 20
  Given there are 20 cucumbers
  When I eat 5 cucumbers
  Then I should have 15 cucumbers
```

Scenario Outlines allow us to more concisely express these examples through the use of a template with placeholders:

```gherkin
Scenario Outline: Eating
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

Examples:
  | start | eat | left |
  |  12   |  5  |  7   |
  |  20   |  5  |  15  |
```

The Scenario outline steps provide a template which is never directly run. A Scenario Outline is run once for each row in the Examples section beneath it (not counting the first row of column headers).

The Scenario Outline uses placeholders, which are contained within < > in the Scenario Outline’s steps. For example:

`Given <I'm a placeholder and I'm ok>`
Think of a placeholder like a variable. It is replaced with a real value from the Examples: table row, where the text between the placeholder angle brackets matches that of the table column header. The value substituted for the placeholder changes with each subsequent run of the Scenario Outline, until the end of the Examples table is reached.

### Tags in Examples:

You can tag a table like below:

```gherkin
@B2C2-771 @DANTEST
Scenario Outline:
    Given I open the page "/signup"
    When I set "<emailFieldInput>" to the inputfield "NewsletterSignup.emailAddress"
    And I click on the element "CookieConsent.accept" if displayed
    And I click on the element "Subscribe"
    Then I expect that element "<resultMessage>" becomes displayed
  @HAPPY
    Examples:
        | emailFieldInput   | resultMessage                 |
        | plainaddress      | NewsletterSignup.errorMessage |
        | email@example.com | NewsletterSignup.thankYou     |

    Examples:
        | emailFieldInput                | resultMessage                 |
        | #@%^%#$@#$@#.com               | NewsletterSignup.errorMessage |
        | email@example..com             | NewsletterSignup.errorMessage |
        | Abc..123@example.com           | NewsletterSignup.errorMessage |
        | firstname.lastname@example.com | NewsletterSignup.thankYou     |
        | email@subdomain.example.com    | NewsletterSignup.thankYou     |
        | firstname+lastname@example.com | NewsletterSignup.thankYou     |
```

Now if I run:

```shell
yarn run wdio --cucumberOpts.tagExpression='@DANTEST'
```

...it runs them all. If I run:

```shell
yarn run wdio --cucumberOpts.tagExpression='@DANTEST and @HAPPY'
```

...it only runs the two in the ‘short’ examples table.

## Selectors

Selectors are usually stored in page files. They can be referenced using `fileName.selectorName` for example:

```gherkin
Scenario: Clicking a selector
  Given I open the site "/login"
  Then I click on the element "Login.invalidEmailWarning"
```

The third line access a selector by referencing `Login.page.ts` followed by a selector from within that file.

For more information on pageFiles see the section below.

## Updating the pipeline image

To update the image that is used when running the pipelines (eg node modules have been modified), run the following command:

```shell
aws-mfa 123456

sh ./build-and-push.sh
```

### Fallback methods

If no full stop is present in the selector provided the framework will fall back to trying to access the element using the text provided to try and select by text visible on the page, or failing that will pass the text through as a $ selector assuming it is a css selector.

## List of predefined steps

Check out all predefined snippets.

### Given steps

- `I open the (url|site) "([^"]*)?"` <br>Open a site in the current browser window/tab
- `the element "([^"]*)?" is( not)* displayed` <br>Check the (in)visibility of an element
- `the element "([^"]*)?" is( not)* enabled` <br>Check if an element is (not) enabled
- `the element "([^"]*)?" is( not)* selected` <br>Check if an element is (not) selected
- `the checkbox "([^"]*)?" is( not)* checked` <br>Check if a checkbox is (not) checked
- `there is (an|no) element "([^"]*)?" on the page` <br>Check if an element (does not) exist
- `the title is( not)* "([^"]*)?"` <br>Check the title of the current browser window/tab
- `the element "([^"]*)?" contains( not)* the same text as element "([^"]*)?"` <br>Compare the text of two elements
- `the (button|element) "([^"]*)?"( not)* contains the text "([^"]*)?"` <br>Check if an element contains the given text
- `the (button|element) "([^"]*)?"( not)* contains any text` <br>Check if an element does not contain any text
- `the (button|element) "([^"]*)?" is( not)* empty` <br>Check if an element is empty
- `the page url is( not)* "([^"]*)?"` <br>Check the url of the current browser window/tab
- `the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"` <br>Check the value of an element's (css) attribute
- `the cookie "([^"]*)?" contains( not)* the value "([^"]*)?"` <br>Check the value of a cookie
- `the cookie "([^"]*)?" does( not)* exist` <br>Check the existence of a cookie
- `the element "([^"]*)?" is( not)* ([\d]+)px (broad|tall)` <br>Check the width/height of an element
- `the element "([^"]*)?" is( not)* positioned at ([\d]+)px on the (x|y) axis` <br>Check the position of an element
- `I have a screen that is ([\d]+) by ([\d]+) pixels` <br>Set the browser size to a given size
- `I have closed all but the first (window|tab)` <br>Close all but the first browser window/tab
- `a (alertbox|confirmbox|prompt) is( not)* opened` <br>Check if a modal is opened

### Then steps

- `I expect that the title is( not)* "([^"]*)?"` <br>Check the title of the current browser window/tab
- `I expect that element "([^"]*)?" does( not)* appear exactly "([^"]*)?" times` <br>Checks that the element is on the page a specific number of times
- `I expect that element "([^"]*)?" is( not)* visible` <br>Check if a certain element is visible
- `I expect that element "([^"]*)?" becomes( not)* visible` <br>Check if a certain element becomes visible
- `I expect that element "([^"]*)?" is( not)* within the viewport` <br>Check if a certain element is within the current viewport
- `I expect that element "([^"]*)?" does( not)* exist` <br>Check if a certain element exists
- `I expect that element "([^"]*)?"( not)* contains the same text as element "([^"]*)?"` <br>Compare the text of two elements
- `I expect that (button|element) "([^"]*)?"( not)* contains the text "([^"]*)?"` <br>Check if an element or input field contains the given text
- `I expect that (button|element) "([^"]*)?"( not)* contains any text` <br>Check if an element or input field contains any text
- `I expect that (button|elementelement) "([^"]*)?" is( not)* empty` <br>Check if an element or input field is empty
- `I expect that the url is( not)* "([^"]*)?"` <br>Check if the the URL of the current browser window/tab is a certain string
- `I expect that the path is( not)* "([^"]*)?"` <br>Check if the path of the URL of the current browser window/tab is a certain string
- `I expect the url to( not)* contain "([^"]*)?"` <br>Check if the URL of the current browser window/tab contains a certain string
- `I expect that the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"` <br>Check the value of an element's (css) attribute
- `I expect that checkbox "([^"]*)?" is( not)* checked` <br>Check if a check-box is (not) checked
- `I expect that element "([^"]*)?" is( not)* selected` <br>Check if an element is (not) selected
- `I expect that the ((\d+)?(st |nd |rd |th )?)?element "([^"]*)?" is( not)* enabled` <br>Check if an element or an item from an array of elements is (not) enabled
- `I expect that cookie "([^"]*)?"( not)* contains "([^"]*)?"` <br>Check if a cookie with a certain name contains a certain value
- `I expect that cookie "([^"]*)?"( not)* exists` <br>Check if a cookie with a certain name exist
- `I expect that element "([^"]*)?" is( not)* ([\d]+)px (broad|tall)` <br>Check the width/height of an element
- `I expect that element "([^"]*)?" is( not)* positioned at ([\d]+)px on the (x|y) axis` <br>Check the position of an element
- `I expect that element "([^"]*)?" (has|does not have) the class "([^"]*)?"` <br>Check if an element has a certain class
- `I expect a new (window|tab) has( not)* been opened` <br>Check if a new window/tab has been opened
- `I expect the url "([^"]*)?" is opened in a new (tab|window)` <br>Check if a URL is opened in a new browser window/tab
- `I expect that element "([^"]*)?" is( not)* focused` <br>Check if an element has the focus
- `I wait on element "([^"]*)?"( for (\d+)ms)*( to( not)* (be checked|be enabled|be selected|be visible|contain a text|contain a value|exist))*` <br>Wait for an element to be checked, enabled, selected, visible, contain a certain value or text or to exist
- `I expect that a (alertbox|confirmbox|prompt) is( not)* opened` <br>Check if a modal is opened
- `I expect that a (alertbox|confirmbox|prompt)( not)* contains the text "$text"` <br>Check the text of a modal
- `I expect that the values returned by "([^"]*)?" to be in the following order` <br>Checks the ordering of the string returned from the selector with the provided Doc strings
- `I expect the element in "([^"]*)" containing text "([^"]*)" to (also|not) contain text (.*)` <br>Checks that an element found using 1 string also contains another

### When steps

- `I (click|doubleclick) on the (link|button|element) "([^"]*)?"( if displayed)*` <br>(Double)click a link, button or element, with an optional check to only click the element if displayed
- `I (add|set) "([^"]*)?" to the inputfield "([^"]*)?"` <br>Add or set the content of an input field
- `I clear the inputfield "([^"]*)?"` <br>Clear an input field
- `I drag element "([^"]*)?" to element "([^"]*)?"` <br>Drag an element to another element
- `I submit the form "([^"]*)?"` <br>Submit a form
- `I pause for (\d+)ms` <br>Pause for a certain number of milliseconds
- `I set a cookie "([^"]*)?" with the content "([^"]*)?"` <br>Set the content of a cookie with the given name to the given string
- `I delete the cookie "([^"]*)?"` <br>Delete the cookie with the given name
- `I press "([^"]*)?"` <br>Press a given key. Youll find all supported characters [here](https://w3c.github.io/webdriver/webdriver-spec.html#keyboard-actions). To do that, the value has to correspond to a key from the table.
- `I (accept|dismiss) the (alertbox|confirmbox|prompt)` <br>Accept or dismiss a modal window
- `I enter "([^"]*)?" into the prompt` <br>Enter a given text into a modal prompt
- `I scroll to element "([^"]*)?"` <br>Scroll to a given element
- `I close the last opened (window|tab)` <br>Close the last opened browser window/tab
- `I focus the last opened (window|tab)` <br>Focus the last opened browser window/tab
- `I log in to site with username "([^"]*)?" and password "([^"]*)?"` <br>Login to a site with the given username and password
- `I select the (\d+)(st|nd|rd|th) option for element "([^"]*)?"` <br>Select an option based on it's index either from a 'select' element or an array of elements
- `I select the option with the (name|value|text) "([^"]*)?" for element "([^"]*)?"` <br>Select an option based on its name, value or visible text
- `I move to element "([^"]*)?"( with an offset of (\d+),(\d+))` <br>Move the mouse by an (optional) offset of the specified element
- `I enter debug mode` <br> Enters the 'browser.debug' mode and pauses the running tests

---

# Page files:

Page files contain the predefined selectors that are available for the test to use. The primary method of selecting an element shoud be testing library.

[https://testing-library.com/](https://testing-library.com/)

The general format of the pagefiles should follow the below template:

```typescript
import Page from './page';
import { within } from '@util/testing-library-webdriverio';

export class GlobalBanner extends Page {
  get bannerMain() {
    return (async () => {
      return await browser.findByTestId('global-banner', {}, { timeout: 10000 });
    })();
  }
  get link() {
    return (async () => {
      // @ts-ignore
      return await within(await this.bannerMain).findByRole('link', {}, { timeout: 10000 });
    })();
  }
}

export default new GlobalBanner();
```

**Notes:**

- Page is imported - Anything defined within page will be accessible universaly. You may want to do this with a selector that is present in many places. EG: password as a field may be defined on the login page, but the same selector might be relevant on the my account page or the checkout gateway. Placing it in the page file would allow it to be used as both `Login.passwordField` and `MyAccount.passwordField` and `CheckoutGateway.passwordField`

- Within - It is good practice to nest your selector so that the scope of where testing library looks for it in the DOM is reduced. In our example you can see this done on the `link()` getter

- Get, Find, and Query - These are further detailed on the testing library site, but as a general rule, if you are expecting something to be immediately available use Get. Both find and query can be used to automatically wait for an element.

- Timeout - A default timeout is applied to the find and query selectors, but you can set it implicitly as an option.

- All - You can return multiple elements by adding the word all. For example `browser.findAllByRole()` Note that if you are waiting for a specific number of elements to be available you may want to wrap this in a waitUntil and check the number returned.
