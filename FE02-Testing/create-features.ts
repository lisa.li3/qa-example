/**
 *   This file creates all of the scenarios for Promotion FE02 Testing, including stacking
 */
import fs from 'fs';
import * as Path from 'path';
import { IStackedPromotionItem, IPromotionItem, ITestCase } from './support/types';
import { toLowerCaseWithoutSpaces, isDuplicatePromoStack } from './support/helpers';
import mergeChecks from './support/merge-checknodes';
import { featureBoilerplate, generateScenario } from './support/generate-cucumber';
import { generateSeed } from './support/generate-seed';

/**
 * Promotion Configuration
 */
const promotions: IPromotionItem[] = [
  {
    name: 'basket.XforY',
    checkNodes: [
      {
        name: '//oms:LineItem',
        checkAttributes: [
          {
            name: 'promotionName',
            value: 'XforYamount',
          },
          {
            name: 'promotionID',
            value: '2123124',
          },
        ],
      },
    ],
  },
  {
    name: 'voucher.XforY',
    checkNodes: [],
  },
  {
    name: 'basket.XforYAmount',
    checkNodes: [],
  },
  {
    name: 'voucher.XforYAmount',
    checkNodes: [],
  },
  {
    name: 'basket.XPercent',
    checkNodes: [],
  },
  {
    name: 'voucher.XPercent',
    checkNodes: [],
  },
  {
    name: 'basket.XAmount',
    checkNodes: [],
  },
  {
    name: 'voucher.XAmount',
    checkNodes: [],
  },
  {
    name: 'basket.SpendXSaveY',
    checkNodes: [
      {
        name: '//oms:LineItem',
        checkAttributes: [
          {
            name: 'promotionName',
            value: 'SpendXSaveY',
          },
        ],
      },
      {
        name: '//oms:A',
        checkAttributes: [
          {
            name: 'test',
            value: 'abc',
          },
        ],
      },
    ],
  },
  {
    name: 'voucher.SpendXSaveY',
    checkNodes: [],
  },
  {
    name: 'basket.SpendXSaveYAmount',
    checkNodes: [],
  },
  {
    name: 'voucher.SpendXSaveYAmount',
    checkNodes: [],
  },
];

/**
 * Stacking Matrix
 */
const stackingMatrix = [
  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

/**
 * Common Complexities
 */
const commonComplexities = {
  loggedInStates: [
    'Guest',
    // 'Logged in',
    // 'Staff Discount',
    // 'Logged in with Customer Credit',
    // 'Staff Discount with Customer Credit',
  ],
  paymentTypes: ['CREDIT_CARD'],
  lineTypes: ['Single Line', 'Multi Line', 'Split'],
  quantitiesPerLine: ['Single', 'Multi'],
  taxIncluded: [true, false],
  locales: ['en-GB'],
};

/**
 * Stack the promos
 * Create an array of all of the possible promos (with stacking too)
 */
const shouldStackList: IStackedPromotionItem[] = [];
const shouldNotStackList: IStackedPromotionItem[] = [];

for (const primaryPromoIndex of Object.keys(promotions)) {
  const primaryPromo = promotions[primaryPromoIndex];
  if (primaryPromo) {
    if (!isDuplicatePromoStack(shouldStackList, primaryPromo.name)) {
      // Add primary promo
      shouldStackList.push({
        name: primaryPromo.name,
        stackedNames: [primaryPromo.name],
        checkNodes: primaryPromo.checkNodes,
      });
    }

    for (const [secondaryPromoIndex, matrixValue] of stackingMatrix[primaryPromoIndex].entries()) {
      const secondaryPromo = promotions[secondaryPromoIndex];
      const doesStack = matrixValue === 1;
      if (secondaryPromo) {
        if (doesStack) {
          if (!isDuplicatePromoStack(shouldStackList, primaryPromo.name, secondaryPromo.name)) {
            // Add stacked promo
            shouldStackList.push({
              name: `${primaryPromo.name} stacked with ${secondaryPromo.name}`,
              stackedNames: [primaryPromo.name, secondaryPromo.name],
              checkNodes: mergeChecks(primaryPromo.checkNodes, secondaryPromo.checkNodes, true),
            });
          }
        } else {
          if (!isDuplicatePromoStack(shouldNotStackList, primaryPromo.name, secondaryPromo.name)) {
            // Add not stacked promo
            shouldNotStackList.push({
              name: `${primaryPromo.name} stacked with ${secondaryPromo.name}`,
              stackedNames: [primaryPromo.name, secondaryPromo.name],
              checkNodes: mergeChecks(primaryPromo.checkNodes, secondaryPromo.checkNodes, true),
            });
          }
        }
      }
    }
  }
}

console.table({
  'Individual Promotions': promotions.length,
  'Stacked Promotions': shouldStackList.length - promotions.length,
  'Total Promotions': shouldStackList.length,
  'Does not stack': shouldNotStackList.length,
  'Number of complexities': Object.values(commonComplexities).reduce(
    (previousValue, complexity) => previousValue + complexity.length,
    0
  ),
});

const keyedTestCases: { [key: string]: ITestCase } = {};

// for (const testCase of testCases) {
for (const loggedInState of commonComplexities.loggedInStates) {
  for (const paymentType of commonComplexities.paymentTypes) {
    for (const lineType of commonComplexities.lineTypes) {
      for (const quantityPerLine of commonComplexities.quantitiesPerLine) {
        for (const taxInclude of commonComplexities.taxIncluded) {
          for (const locale of commonComplexities.locales) {
            for (const promoStack of shouldStackList) {
              const stackName = promoStack.stackedNames.join('.');

              // Retrieve the current stored testCase if there is one, create a blank if one isn't returned
              const testCase: ITestCase = keyedTestCases[stackName] || {
                feature: stackName,
                scenarios: [],
              };

              const nameItem = `${loggedInState} User performing a ${lineType}/${quantityPerLine} Quantity ${
                taxInclude ? 'Tax Included' : 'Tax Excluded'
              } ${promoStack.name}`;

              testCase.scenarios.push({
                name: `${nameItem}`,
                basketPromos: promoStack.stackedNames.filter((promo) => {
                  return promo.includes('basket');
                }),
                voucherPromos: promoStack.stackedNames.filter((promo) => {
                  return promo.includes('voucher');
                }),
                loggedInState,
                paymentType,
                lineType,
                quantityPerLine,
                taxInclude,
                locale,
                shouldFailToStack: loggedInState.includes('Staff'),
                checkNodes: promoStack.checkNodes,
              });

              keyedTestCases[stackName] = testCase;
            }

            // Uncomment when we are ready to add failure cases
            // for (const failureCase of dedupeNoStack) {
            //   const nameItem = `${loggedInState} User performing a ${lineType}/${quantityPerLine} Quantity ${
            //     taxInclude ? 'Tax Included' : 'Tax Excluded'
            //   } ${failureCase.join(' stacked with ')} should not be possible`;
            //   testCases.push({
            //     name: `${nameItem}`,
            //     basketPromos: failureCase.filter((promo) => {
            //       return promo.includes('basket');
            //     }),
            //     voucherPromos: failureCase.filter((promo) => {
            //       return promo.includes('voucher');
            //     }),
            //     loggedInState,
            //     paymentType,
            //     lineType,
            //     quantityPerLine,
            //     taxInclude,
            //     locale,
            //     shouldFailToStack: true,
            //   });
            // }
          }
        }
      }
    }
  }
}
// }

const seeds = [];

// Convert to standard array
const testCases: ITestCase[] = [];
for (const [key, testCase] of Object.entries(keyedTestCases)) {
  testCases.push(testCase);
}
console.table(testCases);

// Step over testCases
for (const testCase of testCases) {
  let fileInformation = featureBoilerplate(testCase);

  if (testCase.scenarios) {
    for (const scenario of testCase.scenarios) {
      fileInformation += generateScenario(testCase, scenario);

      // Create a seed file
      const seedInformation = generateSeed();
      fs.writeFileSync(
        Path.join(
          __dirname,
          `../src/seedingPayloads/${toLowerCaseWithoutSpaces(
            testCase.feature
          )}.${toLowerCaseWithoutSpaces(scenario.lineType)}.${toLowerCaseWithoutSpaces(
            scenario.quantityPerLine
          )}.data.seed.ts`
        ),
        seedInformation
      );
    }
  }

  // Create a scenario set inside the features
  fs.writeFileSync(
    Path.join(__dirname, `../src/features/PromosAndFE02/${testCase.feature}.promo.FE02.feature`),
    fileInformation
  );
}

// console.log(dedupeNoStack.length, 'Cases suppressed (for now)');
console.log(testCases.length, 'Features Created under /src/features/PromosAndFE02/');
console.log(seeds.length, 'Seeding files created under /src/seedingPayloads/');

export default testCases;

/**
 *
 * COMMENTS FOR PAUL AND ADI
 *
 * 1) Convert to a standalone module for clarity (move to multiple files) - JS docs?
 *
 * 2) Be able to export a sample size for Manual Test, with a cucumber report (ts-node ./FE02-Testing/create-features.ts --sample-size 10)
 *
 * 3) Seed the orders for all of the non-happy paths and add the orderID into the state (https://bitbucket.org/projectteam/order-service/src/master/src/order/models/order.interface.ts)
 *
 * 4) Extend the "download the FE02" step to be able to pass in an orderID, or add it to the state and get it from there. (?? Does the order-service return the id when seeded)
 *
 * 5) Re-enable non-stacking checks when we have all the stacking ones working
 *
 * 6) Allow for future changes in payments to check the payment stacking ( GiftCard + Customer Credit + [Adyen Payment] )
 *
 * 7) Add in assertions for the Order Summary on Checkout and Order Confirmation screen ( for #1 )
 *
 * 8) Only run the browser for the first instance of each promo, and not for all of the permutations. - Dont even want to start a browser for all others
 *
 * 9) Test the limits for the order exporter, can we seed all of them at the start, then export once, and then check each one?
 *

 Example non-browser feature

 @CHECKOUT
 @CONFIRMATION
 @EXAMPLE
 @FE02

 Feature: Checkout Confirmation
 Scenario Outline: And a user completes a <orderType> order with a customerCredit card we should see an FE02 produced
 Given I seed using the file "<orderType>.checkout"
 And I seed an order using the file ""
 And I download the FE02 file from S3 <- Needs extending
 Then The FE02 path "//oms:PaymentMethod" has attribute "PaymentType" with value "<paymentType>"
 And The FE02 path "//oms:PaymentDetails" has attribute "ChargeType" with value "<chargeType>"

 Examples:
 | orderType  | paymentType | chargeType |
 | singleLine | CREDIT_CARD | AUTHORIZATION |


 */
