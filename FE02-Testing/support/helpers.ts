import { IStackedPromotionItem } from './types';

/**
 * Removes spaces and makes lowercase
 * @param str
 */
export const toLowerCaseWithoutSpaces = (str: string): string => {
  return str.replace(/\s/g, '').toLowerCase();
};

/**
 * Returns true if Promo Stack would be a duplicate
 * @param list - Current list of promotions
 * @param item1 - Promo 1
 * @param item2 - Promo 2
 */
export const isDuplicatePromoStack = (
  list: IStackedPromotionItem[],
  item1: string,
  item2?: string
) => {
  if (item2) {
    return (
      list.find(
        (item) =>
          item.stackedNames.includes(item1) &&
          item.stackedNames.includes(item2) &&
          item.stackedNames.length === 2
      ) !== undefined
    );
  } else {
    return (
      list.find((item) => item.stackedNames.includes(item1) && item.stackedNames.length === 1) !==
      undefined
    );
  }
};
