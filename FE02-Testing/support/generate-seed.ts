import { IScenario, ITestCase } from './types';

export const generateSeed = (testCase?: ITestCase, scenario?: IScenario) => {
  return `
/**
 * WARNING - AUTO GENERATED FILE - DO NOT TOUCH
 */
/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../support/types/IProduct';
import { IContentSeeder } from '../support/types/IContent';
import { IInventorySeeder } from '../support/types/IInventory';
import IUser from '../support/types/IUser';

export default async () => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [{}];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [{}];
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [];
  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
`;
};
