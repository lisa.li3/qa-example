import { ICheckNode } from './types';

/**
 *
 * @param primaryCheckNodes - Primary Check Nodes
 * @param secondaryCheckNodes - Secondary Check Nodes
 * @param mergeNodes - If TRUE nodes and their attributes will be merged, if there's not clashes. If FALSE the the nodes will be stacked
 */
export const mergeChecks = (
  primaryCheckNodes: ICheckNode[],
  secondaryCheckNodes: ICheckNode[],
  mergeNodes = true
): ICheckNode[] => {
  let combinedChecks: ICheckNode[] = [];

  if (!mergeNodes) {
    // Don't merge check node, attributes can have multiple different values
    combinedChecks = [...primaryCheckNodes, ...secondaryCheckNodes];
  } else {
    // Merge Nodes, attributes cannot be tested with multiple different values

    // Build list of all unique nodes
    const allNodeNames: string[] = [];
    for (const node of [...primaryCheckNodes, ...secondaryCheckNodes]) {
      if (allNodeNames.indexOf(node.name) === -1) {
        allNodeNames.push(node.name);
      }
    }
    // Step over each node
    for (const name of allNodeNames) {
      const mergedCheckNode: ICheckNode = {
        name,
        checkAttributes: [],
      };

      // Step over Primary Check Nodes
      for (const primaryCheckNode of primaryCheckNodes) {
        if (primaryCheckNode.name === name) {
          mergedCheckNode.checkAttributes = primaryCheckNode.checkAttributes;
        }
      }

      // Step over Secondary Check Nodes
      for (const secondaryCheckNode of secondaryCheckNodes) {
        if (secondaryCheckNode.name === name) {
          // Has primary already provided checks for node?
          if (mergedCheckNode.checkAttributes.length > 0) {
            // Step of attributes to merge and provide warnings if needed
            for (const newAttributeCheck of secondaryCheckNode.checkAttributes) {
              const previouslySetAttribute = mergedCheckNode.checkAttributes.find(
                (attribute) => attribute.name === newAttributeCheck.name
              );

              if (
                previouslySetAttribute &&
                previouslySetAttribute.value !== newAttributeCheck.value
              ) {
                // Attribute cannot have 2 different values
                console.warn(
                  `!!! Attribute "${newAttributeCheck.name}" is expected to have both values "${previouslySetAttribute.value}" and "${newAttributeCheck.value}"`
                );
              } else {
                // No potential issues
                mergedCheckNode.checkAttributes = [
                  ...mergedCheckNode.checkAttributes,
                  ...secondaryCheckNode.checkAttributes,
                ];
              }
            }
          } else {
            // No merge needed, take secondary
            mergedCheckNode.checkAttributes = secondaryCheckNode.checkAttributes;
          }
        }
      }
      combinedChecks.push(mergedCheckNode);
    }
  }
  // console.log('combinedChecks', combinedChecks);
  return combinedChecks;
};

export default mergeChecks;
