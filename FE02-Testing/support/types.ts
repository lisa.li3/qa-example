export interface ICheckAttribute {
  name: string;
  value: string;
}
export interface ICheckNode {
  name: string;
  checkAttributes: ICheckAttribute[];
}
export interface IScenario {
  name: string;
  basketPromos: string[];
  voucherPromos: string[];
  loggedInState: string;
  paymentType: string;
  lineType: string;
  quantityPerLine: string;
  taxInclude: boolean;
  locale: string;
  shouldFailToStack: boolean;
  checkNodes: ICheckNode[];
}

export interface ITestCase {
  feature: string;
  scenarios: IScenario[];
}
export interface IPromotionItem {
  name: string;
  checkNodes: ICheckNode[];
}
export interface IStackedPromotionItem {
  name: string;
  stackedNames: string[];
  checkNodes: ICheckNode[];
}
