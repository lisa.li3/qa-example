require('dotenv').config();

import { loadAwsAuth } from './src/support/hooks';
import { v5 } from 'uuid';
import { makeBadge } from 'badge-maker';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';

console.log('Creating PR based on changes...');
const { exec } = require('child_process');
let branch = '';
exec('git branch --show-current', (error, stdout, stderr) => {
  branch = stdout.substring(0, stdout.length - 1);

  if (!process.env.JIRA_USERNAME || !process.env.JIRA_TOKEN) {
    console.error(`You need to have JIRA_USERNAME and JIRA_TOKEN set in your .env`);
  }

  console.log('Generating PR...');

  const runID = v5(branch, '228761fe-d29a-4b25-8693-fb63b286da46');

  console.log(runID);

  const s3Client = new S3Client(loadAwsAuth() || {});
  const format = {
    label: 'Test Run',
    message: 'Pending',
    color: 'lightgrey',
  };

  const svg = makeBadge(format);

  (async () =>
    await s3Client.send(
      new PutObjectCommand({
        Bucket: 'test-automation-reporting',
        Key: `${runID}/badge/desktop.svg`,
        Body: svg,
        ContentType: 'image/svg+xml',
      })
    ))();

  const description = `
${branch.toUpperCase()}

[![Desktop](http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/${runID}/badge/desktop.svg)](http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/${runID}/report.html) Desktop Regression Run

`;

  const data = {
    title: 'Merge some branches',
    description: description,
    source: {
      branch: { name: branch },
      repository: { full_name: 'team/wdio-b2c-automation' },
    },
    destination: { branch: { name: 'develop' } },
    reviewers: [{ username: 'Prometheus AT Approver' }],
    close_source_branch: true,
  };
  const url = `https://api.bitbucket.org/2.0/repositories/team/wdio-b2c-automation/pullrequests`;

  console.log(process.env.JIRA_USERNAME, process.env.JIRA_TOKEN);
  const plain = `${process.env.JIRA_USERNAME}:${process.env.JIRA_TOKEN}`;
  const auth = Buffer.from(plain).toString('base64');
  console.log(auth);

  exec(
    `curl -v -X POST -H "Content-Type: application/json" -H "Authorization: Basic ${auth}" ${url} -d '${JSON.stringify(
      data
    )}'`,
    (error, stdout, stderr) => {
      console.log(error);
      console.log(stdout);
      console.log(stderr);
    }
  );
});

// const result = await fetch(url, {
//   method: 'POST',
//   headers: {
//     Authorization: `Basic ${credentialString}`,
//   },
// });

//curl -X POST -H "Content-Type: application/json" -u s3m3n:bbpassword https://bitbucket.org/api/2.0/repositories/s3m3n/reponame/pullrequests -d '{ "title": "Merge some branches", "description": "stackoverflow example", "source": { "branch": { "name": "choose branch to merge with" }, "repository": { "full_name": "s3m3n/reponame" } }, "destination": { "branch": { "name": "choose branch that is getting changes" } }, "reviewers": [ { "username": "some other user needed to review changes" } ], "close_source_branch": false }'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
