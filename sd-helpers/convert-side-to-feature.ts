import * as path from 'path';
import * as fs from 'fs';
import moment from 'moment';
import os from 'os';

// Get the command line arguments
const myArgs = process.argv.slice(2);

if (!myArgs[0]) {
  throw Error(`You need to pass the path to the .SIDE file as the first argument`);
}

// Parse them into the correct areas / variables

const sideLocation = myArgs[0];

/*
 * Read in the SIDE file from the local filesystem
 */
const sideFileRaw = fs.readFileSync(path.resolve(__dirname, sideLocation));
const sideFile = JSON.parse(sideFileRaw.toString());

if (sideFile.name.includes('.') === false) {
  throw Error(`The name of the test MUST have a "." to determine the folder, got ${sideFile.name}`);
}

/*
 * Start Stripping down the JSON, and assigning to our own object
 */
const dataToGenerate: IConvertSideToFeature = {
  feature: sideFile.name,
  acceptanceCriteria: sideFile.comment,
  scenarios: sideFile.tests,
};

let featureFile: string = '';

/*
 * Tagging
 */

featureFile += `@Pending\n@Imported\n@${moment().format('DD-MM-YYYY')}\n@${
  os.userInfo().username
}\n`;

featureFile += `Feature: ${sideFile.name.split('.')[1]}\n`;

/*
 * Acceptance Criteria (added as a comment to the test in the IDE)
 */

featureFile += dataToGenerate.acceptanceCriteria ? `${dataToGenerate.acceptanceCriteria}\n` : '';

for (const scenario of dataToGenerate.scenarios) {
  /*
   * Scenarios
   */

  featureFile += `Scenario: ${scenario.name}\n`;

  /*
   * Steps organisation
   */

  let firstOfType = {
    Given: true,
    When: true,
    Then: true,
  };

  let andUsed = {
    Given: false,
    When: false,
    Then: false,
  };

  const addType = (type, isFirst = true) => {
    if (type === 'Given') {
      if (!isFirst || andUsed.Given) {
        andUsed.Given = true;
        return `  And`;
      } else {
        return 'Given';
      }
    } else if (type === 'When') {
      if (andUsed.Given) {
        if (!isFirst || andUsed.When) {
          andUsed.When = true;
          return `      And`;
        } else {
          return `    When`;
        }
      } else {
        if (!isFirst || andUsed.When) {
          andUsed.When = true;
          return `    And`;
        } else {
          return `  When`;
        }
      }
    } else {
      if ((andUsed.Given && !andUsed.When) || (!andUsed.Given && andUsed.When)) {
        if (!isFirst || andUsed.Then) {
          andUsed.Then = true;
          return `        And`;
        } else {
          return `      Then`;
        }
      } else if (andUsed.Given && andUsed.When) {
        if (!isFirst || andUsed.Then) {
          andUsed.Then = true;
          return `          And`;
        } else {
          return `        Then`;
        }
      } else {
        if (!isFirst || andUsed.Then) {
          andUsed.Then = true;
          return `      And`;
        } else {
          return `    Then`;
        }
      }
    }
  };

  let lastStep = false;

  const addToLog = (type: string, step: string) => {
    if (lastStep) {
      type = 'Then';
    }
    if (firstOfType[type]) {
      firstOfType[type] = false;
      featureFile += `${addType(type)} ${step}\n`;
    } else {
      featureFile += `${addType(type, false)} ${step}\n`;
    }
  };

  /*
   * Steps that can be made ( subject to change as we get better at using cucumber )
   *
   * [type] I navigate to {url}
   * [type] I {action} on {target} (NO VALUE)
   * [type] I {action} {value} in {target}
   * [type] I execute the script {script}
   * [type] I expect {target}'s {attribute} {assertion} {value}
   *
   */

  for (const commandObject of scenario.commands) {
    const ignoredActions = ['setWindowSize', 'runScript', 'selectFrame'];
    if (ignoredActions.includes(commandObject.command)) {
      console.log(
        `Not adding command, as we detected an ignored action of ${commandObject.command}`
      );
      continue;
    }

    const getTarget = (targets, otherTarget) => {
      let selector = otherTarget.replace('id=', '').replace('css=', '');
      for (const target of targets) {
        switch (target[1]) {
          case 'xpath:idRelative':
            selector = target[0];
            break;
        }
      }
      return selector;
    };

    if (scenario.commands.indexOf(commandObject) + 1 === scenario.commands.length) {
      lastStep = true;
    }
    if (commandObject.command === 'open') {
      addToLog(firstOfType.Given ? 'Given' : 'When', `I navigate to "${commandObject.target}"`);
    } else {
      if (!commandObject.value && commandObject.command !== 'runScript') {
        addToLog(
          firstOfType.Given ? 'Given' : 'When',
          `I ${commandObject.command} on "${getTarget(
            commandObject.targets,
            commandObject.target
          ).replace('xpath=', '')}"`
        );
      } else if (commandObject.command !== 'assert' && commandObject.command !== 'runScript') {
        addToLog(
          firstOfType.Given ? 'Given' : 'When',
          `I ${commandObject.command} ${commandObject.value} in "${getTarget(
            commandObject.targets,
            commandObject.target
          ).replace('xpath=', '')}"`
        );
      } else if (commandObject.command === 'runScript') {
        addToLog(
          firstOfType.Given ? 'Given' : 'When',
          `I execute the script\n """${commandObject.target}"""`
        );
      } else {
        // assertion
        addToLog(
          'Then',
          `I expect "${commandObject.target}"'s ${commandObject.command} ${commandObject.value}`
        );
      }
    }
  }

  console.log(`../features/${sideFile.name.split('.')[0]}`);

  if (!fs.existsSync(path.resolve(__dirname, `../features/${sideFile.name.split('.')[0]}`))) {
    fs.mkdirSync(path.resolve(__dirname, `../features/${sideFile.name.split('.')[0]}`));
  }

  fs.writeFileSync(
    path.resolve(
      __dirname,
      `../features/${sideFile.name.split('.')[0]}/${sideFile.name
        .split('.')[1]
        .replace(/ /g, '-')}.feature`
    ),
    featureFile
  );
}

interface IConvertSideToFeature {
  feature: string;
  acceptanceCriteria: string;
  scenarios: ITestCase[];
}

interface ITestCase {
  id: string;
  name: string;
  commands: ICommand[];
}

interface ICommand {
  id: string;
  comment: string;
  command: string;
  target: string;
  targets: string[];
  value: string;
}
