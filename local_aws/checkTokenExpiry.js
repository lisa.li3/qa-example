const fs = require('fs');
const path = require('path');
const moment = require('moment');

try {
  const TokenString = fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
  const Token = JSON.parse(TokenString);
  if (moment(Token.Expiration).unix() > moment().add(1, 'hour').unix()) {
    console.log('Token Valid! Expires', moment(Token.Expiration).fromNow());
    process.exit(0);
  } else {
    console.log('Token close to or is already expired... Removing!');
    fs.unlinkSync(path.join(__dirname, '../aws-mfa-cache'));
    process.exit(1);
  }
} catch (err) {
  console.log('Error when checking AWS Creds - ', err);
  process.exit(1);
}
