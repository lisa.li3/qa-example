echo "Checking for AWS Token";
FILE=./aws-mfa-cache;
if [ -f "$FILE" ]; then
    echo "$FILE exists! Checking if its valid and does not expire...";
    if node ./local_aws/checkTokenExpiry; then
      echo "MFA Valid! Continuing..."
    else
      echo "AWS User ID (First.Last normally): ";
      	read userid;
      	echo "AWS MFA Code: ";
      	read mfa;
      	userID=$userid mfa=$mfa node ./local_aws/generateToken;
         fi;
  else
      echo "$FILE does not exist. We will need to create a token";
      echo "AWS User ID (First.Last normally): ";\
    	read userid;
    	echo "AWS MFA Code: ";
    	read mfa;
    	userID=$userid mfa=$mfa node ./local_aws/generateToken;
  fi;