const fs = require('fs');
const path = require('path');

try {
  const TokenString = fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
  if (TokenString !== 'NONE') {
    console.log('File Exists! And has data in it');
    process.exit(0);
  } else {
    process.exit(1);
  }
} catch (err) {
  process.exit(1);
}
