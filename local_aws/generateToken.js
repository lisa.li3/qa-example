const fs = require('fs');
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
const AWS = require('aws-sdk');

(async function () {
  if (!process.env.MFA_AWS_ACCESS_KEY_ID || !process.env.MFA_AWS_SECRET_ACCESS_KEY) {
    console.log(`ERROR: Please add your access key ID and secret access key to your .env file!`);
  }

  let retry = 0;

  const aws_account_id = '942418423207';

  const AWS_MFA_ARN = `arn:aws:iam::${aws_account_id}:mfa/${process.env.userID}`;

  const awsCredentials = new AWS.Credentials({
    accessKeyId: process.env.MFA_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.MFA_AWS_SECRET_ACCESS_KEY,
  });

  AWS.config.credentials = awsCredentials;

  AWS.config.update({
    region: process.env.AWS_REGION || 'eu-west-1',
  });

  const readMfaToken = async function () {
    try {
      return fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
    } catch (err) {
      return false;
    }
  };

  const getMfaToken = async function () {
    return new Promise(async function (resolve, reject) {
      const params = {
        DurationSeconds: 129600,
        SerialNumber: AWS_MFA_ARN,
        TokenCode: process.env.mfa,
      };
      console.log('Hitting AWS API');
      console.log(params);
      var sts = new AWS.STS();
      return sts.getSessionToken(params, async function (err, data) {
        if (err) {
          console.log('Error Hitting AWS API:');
          console.log(err, err.stack); // an error occurred
          return reject(err);
        } else {
          console.log(`Success! Writing to disk...`);
          fs.writeFileSync(
            path.join(__dirname, '../aws-mfa-cache'),
            JSON.stringify(data.Credentials)
          );
          return resolve(data); // successful response
        }
      });
    });
  };

  if (process.env.mfa) {
    const existingToken = await readMfaToken();
    if (!existingToken) {
      console.log('Creating New Token...');
      await getMfaToken();
      console.log('Created Token!');
      const TkStr = fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
      const Token = JSON.parse(TkStr);
      console.log(`
                Successfully created a token
        
                [TEMP]
                Access Key: ${Token.AccessKeyId}
                Secret Key: ${Token.SecretAccessKey}
                Session Token: ${Token.SessionToken}
        
                [NOTE]
                Session Expires at: ${Token.Expiration}  
            `);

      process.exit(0);
    } else {
      console.log('MFA Passed, But will attempt to use the cached MFA token first...');
      const TokenString = fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
      const Token = JSON.parse(TokenString);
      console.log('Found Token!');
      console.log(`
                Successfully found a token
        
                [TEMP]
                Access Key: ${Token.AccessKeyId}
                Secret Key: ${Token.SecretAccessKey}
                Session Token: ${Token.SessionToken}
        
                [NOTE]
                Session Expires at: ${Token.Expiration}  
            `);
      process.exit(0);
    }
  } else {
    console.log('No MFA Passed... Will attempt to use cached MFA token...');
    const TokenString = fs.readFileSync(path.join(__dirname, '../aws-mfa-cache'));
    const Token = JSON.parse(TokenString);
    console.log('Found Token!');
    console.log(`
            Successfully found a token
    
            [TEMP]
            Access Key: ${Token.AccessKeyId}
            Secret Key: ${Token.SecretAccessKey}
            Session Token: ${Token.SessionToken}
    
            [NOTE]
            Session Expires at: ${Token.Expiration}  
        `);
    process.env.AWS_ACCESS_KEY_ID = Token.AccessKeyId;
    process.env.AWS_SECRET_ACCESS_KEY = Token.SecretAccessKey;
    process.env.AWS_SESSION_TOKEN = Token.SessionToken;
    process.exit(0);
  }
})();
