/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import fetch from 'node-fetch';
import path from 'path';
import { filter, isEmpty } from 'lodash';
import { config } from 'dotenv';

config({ path: path.join(__dirname, '../.env') });

const team = 'projectteam';
const defaultBranch = 'develop';

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

interface ExtendedPullRequest extends Schema.Pullrequest {
  repo_slug: string;
  workspace: string;
}

export const repositories = [
  // and the test framework
  { defaultBranch, team, name: 'wdio-b2c-automation' },
];

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

/**
 * Gets a list of open pull requests for the specified component
 *
 * @param componentConfig
 */
export async function getOpenPullRequests(
  componentConfig: ComponentConfig
): Promise<ExtendedPullRequest[]> {
  const openPullRequests = await bbClient.repositories.listPullRequests({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    sort: '-created_on',
    pagelen: 50,
    q: `state = "OPEN"`,
  });

  return (
    openPullRequests.data.values!.map((value) => ({
      ...value,
      repo_slug: componentConfig.name,
      workspace: componentConfig.team,
    })) || []
  );
}

/**
 * get open PRs matching branch prefix
 * @param branchPrefix
 */
export async function getPullRequestsForNextRuns(): Promise<ExtendedPullRequest[]> {
  const matchedPullRequests: ExtendedPullRequest[] = [];
  for (const repository of repositories) {
    try {
      const pullRequests = await getOpenPullRequests(repository);
      console.log(`There are ${pullRequests.length} open Pull Requests`);
      for (const pullRequest of pullRequests) {
        const credentialString = getBasicAuthString(
          process.env.BB_USERNAME!,
          process.env.BB_PASSWORD!
        );
        const url = `https://api.bitbucket.org/2.0/repositories/${pullRequest.workspace}/${pullRequest.repo_slug}/pullrequests/${pullRequest.id}/tasks`;
        const tasksResponse = await fetch(url, {
          method: 'GET',
          headers: {
            Authorization: `Basic ${credentialString}`,
          },
        });

        for (const task of (await tasksResponse.json()).values) {
          if (task.content.raw === 'RUN' && task.state === 'UNRESOLVED') {
            matchedPullRequests.push(pullRequest);
            break;
          }
        }
      }
      return matchedPullRequests;
    } catch (e) {
      console.log(e);
    }
  }
  return matchedPullRequests;
}

function getBasicAuthString(username: string, password: string): string {
  const plain = `${username}:${password}`;
  return Buffer.from(plain).toString('base64');
}

/**
 * CLI entrypoint
 */
export const runOnSchedule = async (): Promise<void> => {
  console.log('Running Scheduled Task');
  for (const envVar of ['BB_USERNAME', 'BB_PASSWORD']) {
    if (!process.env[envVar]) {
      console.error(`${envVar} environment variable is not defined.`);
      process.exit(1);
    }
  }

  // 1 - Get a list of all open PRs and Check each PR for the task of RUN that is UNRESOLVED (X)
  const prsToRun = await getPullRequestsForNextRuns();

  if (isEmpty(prsToRun)) {
    console.log(`No tests to trigger right now`);
    return;
  }

  console.log(
    `${prsToRun.length} PR's are Requiring a RUN${prsToRun.map((pr) => {
      return '\nhttps://bitbucket.org/projectteam/wdio-b2c-automation/pull-requests/' + pr.id;
    })}`
  );

  const PR_NAME = prsToRun[prsToRun.length - 1].source?.branch?.name;
  if (!PR_NAME) {
    console.log('Unexpected object format returned from Bitbucket API');
    process.exit(1);
  }
  let envUrl =
    process.env.DEFUALT_TEST_ENVIRONMENT || 'https://com.regression.dynamic.prometheus.sd.co.uk';
  if (PR_NAME && PR_NAME.includes('/B2C2-')) {
    const [, branch] = PR_NAME.split('/');
    const [b2c2, ticket] = branch.split('-');
    const dynamicEnvName = `${b2c2}${ticket}`.toLowerCase();
    envUrl = `https://com.${dynamicEnvName}.dynamic.prometheus.sd.co.uk/`;
  }

  // 2 - Get any running pipelines (Y)
  const pipelines = await bbClient.repositories.listPipelines({
    repo_slug: prsToRun[prsToRun.length - 1].repo_slug,
    workspace: prsToRun[prsToRun.length - 1].workspace,
    sort: '-created_on',
  });

  const runningPipelines = filter(pipelines.data.values, {
    state: {
      name: 'IN_PROGRESS',
    },
    target: {
      selector: {
        pattern: 'test-pipeline',
      },
    },
  });

  // if zero pipelines running, start one from (X)
  if (isEmpty(runningPipelines)) {
    return startPipeline({
      branch: PR_NAME,
      url: envUrl,
      repo_slug: prsToRun[prsToRun.length - 1].repo_slug,
      workspace: prsToRun[prsToRun.length - 1].workspace,
    });
  }
  console.log('Pipelines are already running, so not starting anything...');
};

export const startPipeline = async ({
  branch,
  url,
  repo_slug,
  workspace,
}: {
  branch: string;
  url: string;
  repo_slug: string;
  workspace: string;
}): Promise<void> => {
  // Trigger the 1st run
  await bbClient.repositories.createPipeline({
    _body: {
      type: 'pipeline_ref_target',
      target: {
        type: 'pipeline_ref_target',
        ref_type: 'branch',
        ref_name: branch,
        selector: {
          type: 'custom',
          pattern: 'test-pipeline',
        },
      },
      variables: [
        {
          key: 'TEST_TAG',
          value: process.env.DEFUALT_TEST_TAG || 'REGRESSION',
        },
        {
          key: 'ENV_URL',
          value: url,
        },
        {
          key: 'FEATURE_BRANCH_PREFIX',
          value: branch,
        },
      ],
    },
    repo_slug,
    workspace,
  });
  console.log(`Started Pipeline on branch ${branch}`);
};

runOnSchedule();
