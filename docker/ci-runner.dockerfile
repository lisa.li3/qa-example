FROM node:fermium-alpine3.14
#ENV buildpackages "python3 cairo-dev jpeg-dev pango-dev giflib-dev g++ make"
ENV buildpackages "python3 aws-cli openjdk8 bash ncurses make chromium-chromedriver"
WORKDIR /usr/src/app

# Installs OS packages
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories \
    && apk upgrade -U -a \
    && apk add \
    curl \
    git \
    libstdc++ \
    chromium \
    harfbuzz \
    nss \
    freetype \
    ttf-freefont \
    font-noto-emoji \
    wqy-zenhei \
    ${buildpackages} \
    && rm -rf /var/cache/* \
    && mkdir /var/cache/apk

ADD docker/local.conf /etc/fonts/local.conf
#
#ENV CHROME_BIN=/usr/bin/chromium-browser \
#    CHROME_PATH=/usr/lib/chromium/


RUN chmod 0755 /usr/lib/chromium/

## ------ TESTRUNNER ------

# Create expected directories
RUN mkdir localTranslations reports

# Testrunner dependencies
COPY ["package.json", ".npmrc", "yarn.lock*", "npm-shrinkwrap.json*", "./"]
RUN rm -rf node_modules
RUN yarn install

# Local framework files
ADD run.sh .
ADD local_aws ./local_aws
ADD config ./config
ADD .env.build .env
ADD wdio.conf.ts .
ADD tsconfig.json .
ADD docker/approve-reject-prs.ts scripts/approve-reject-prs.ts
ADD docker/pr-tasks.ts scripts/pr-tasks.ts
ADD docker/get-next-to-run.ts scripts/get-next-to-run.ts
ADD docker/on-push-check-runs.ts scripts/on-push-check-runs.ts
ADD docker/get-latest-changes.ts scripts/get-latest-changes.ts
ADD docker/comment-and-transition.ts scripts/comment-and-transition.ts
ADD docker/comment-and-transition-cli.ts scripts/comment-and-transition-cli.ts
ADD docker/merge-to-develop.ts scripts/merge-to-develop.ts
ADD docker/slack-notifications.ts scripts/slack-notifications.ts
ADD docker/constants.ts scripts/constants.ts

# Override selenium-standalone Chromedriver with Alpine-compatible one
RUN mkdir -p node_modules/@wdio/selenium-standalone-service/node_modules/selenium-standalone/.selenium/chromedriver/latest-x64/ \
    && ln -s /usr/bin/chromedriver node_modules/@wdio/selenium-standalone-service/node_modules/selenium-standalone/.selenium/chromedriver/latest-x64/
ADD docker/selenium-server ./node_modules/@wdio/selenium-standalone-service/node_modules/selenium-standalone/.selenium/selenium-server

CMD ["/bin/sh"]