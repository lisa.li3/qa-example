/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import PaginatedRefs = Schema.PaginatedRefs;
import * as child_process from 'child_process';
import fs from 'fs';
import Path from 'path';
import { commentAndTransition } from './comment-and-transition';
import { get, isEmpty } from 'lodash';
import { repositories } from './constants';

require('dotenv').config();

const team = 'projectteam';
const defaultBranch = 'develop';
let wdioBranch = 'master';

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

/**
 * Gets a list of matching commit hashes for a named ref
 *
 * @param componentConfig
 * @param ref
 */
async function getMatchingHashesForRef(
  componentConfig: { defaultBranch: string; name: string; team: string },
  ref: string
): Promise<Schema.PaginatedRefs[]> {
  const allRefs: PaginatedRefs[] = [];

  const matchingHashes = await bbClient.repositories.listRefs({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    sort: 'name',
    pagelen: 100,
    q: `name ~ "${ref}"`,
  });

  const hashValues = get(matchingHashes, 'data.values', []);

  if (componentConfig.name === 'wdio-b2c-automation' && !isEmpty(hashValues)) {
    wdioBranch = hashValues[0].name || 'master';
  }

  allRefs.push(matchingHashes.data);

  return allRefs;
}

/**
 * Gets a list of open pull requests for the specified component
 *
 * @param string
 */
async function checkForOpenPullRequests(): Promise<any> {
  const [branchPrefix] = process.argv.slice(2);

  const branches: string[] = [];

  for (const repository of repositories) {
    for (const { values } of await getMatchingHashesForRef(repository, branchPrefix)) {
      if (!values) {
        continue;
      }

      for (const { name } of values) {
        if (name) {
          const matches = name.match(/[a-z]+\/B2C2-\d+/i);
          if (matches) {
            branches.push(`${repository.name} :: ${name}`);
          }
        }
      }
    }
  }
  console.log(`Total branches: ${branches.length}`);

  if (isEmpty(branches)) {
    await commentAndTransition({
      jiraReference: branchPrefix,
      message: 'Branch Issue: You have not got any branches.',
    });
    process.exit(2);
  }
}

/**
 * Force failure if there are new commits in develop branch that are not in this branch
 * Context: So that we can make sure all code going to prod-like environments has been regression tested - nothing slips through
 *
 * @param string
 * @param ref
 * @param string
 */
async function failWhenNewCommitsInDevelop(repositoryName: string, ref: any, branchPrefix: string) {
  console.log(`Checking if branch is up to date with develop...`);
  try {
    if (!fs.existsSync('temp_repos')) {
      fs.mkdirSync('temp_repos');
    }

    child_process.execSync(
      ` 
      cd temp_repos
      git config --global user.email "prometheus-ci-testuser@prometheus.sd.co.uk"
      git config --global user.name "Prometheus CI Test User"
      git clone https://${process.env.BB_USERNAME}:${process.env.BB_PASSWORD}@bitbucket.org/projectteam/${repositoryName}.git
      cd ${repositoryName}
      git checkout ${ref?.name}
      git merge origin/develop
      if ! git diff --quiet
      then
          thiswillmakeitfail
      fi
      `
    );

    console.log(`All Good! Branch is ready to be merged`);
    fs.rmdirSync('temp_repos', { recursive: true });
  } catch (e) {
    await commentAndTransition({
      jiraReference: branchPrefix,
      message: `Develop branch has new commits`,
    });
    fs.rmdirSync('temp_repos', { recursive: true });
    process.exit(2);
  }
}

async function mergeDevelopChangesIntoBranch() {
  const [branchPrefix, failIfDifferent] = process.argv.slice(2);

  for (const repository of repositories) {
    for (const { values } of await getMatchingHashesForRef(repository, branchPrefix)) {
      if (values && values.length > 0) {
        // Update this branch
        const ref = values[0];

        if (failIfDifferent) {
          failWhenNewCommitsInDevelop(repository.name, ref, branchPrefix);
        }

        try {
          if (fs.existsSync('temp_repos')) {
            fs.rmdirSync('temp_repos');
          }

          fs.mkdirSync('temp_repos');
          console.log(`Cloning ${repository.name}...`);
          child_process.execSync(
            `
            cd temp_repos &&
            git config --global user.email "prometheus-ci-testuser@prometheus.sd.co.uk"
            git config --global user.name "Prometheus CI Test User"
            git clone https://${process.env.BB_USERNAME}:${process.env.BB_PASSWORD}@bitbucket.org/projectteam/${repository.name}.git
            `
          );
          console.log(`Done! ${repository.name} Cloned`);
          console.log(`Merging in develop...`);

          child_process.execSync(`
            cd temp_repos &&
            cd ${repository.name} &&
            git checkout ${ref?.name} &&
            git merge origin/develop &&
            git push
          `);
          console.log(`Done! ${repository.name} Updated`);

          fs.rmdirSync('temp_repos', { recursive: true });
          // Successfully updated the branch, and removed the repo
        } catch (e) {
          // Failed to update branch, fall back
          fs.rmdirSync('temp_repos', { recursive: true });
          console.log(`Error! ${e}`);

          await commentAndTransition({
            jiraReference: branchPrefix,
            message: `FAIL: Unable to update branch (${ref?.name}) on ${repository}, error: ${e}`,
          });
          process.exit(2);
        }
      }
    }
  }
}

/**
 * CLI entrypoint
 */
const run = async () => {
  const [branchPrefix] = process.argv.slice(2);

  if (!branchPrefix) {
    console.error(`Usage: \`${__filename} {branch} {checkForDevelopChanges}\``);
    process.exit(1);
  }

  for (const envVar of ['BB_USERNAME', 'BB_PASSWORD']) {
    if (!process.env[envVar]) {
      console.error(`${envVar} environment variable is not defined.`);
      process.exit(1);
    }
  }

  await checkForOpenPullRequests();
  await mergeDevelopChangesIntoBranch();

  // output the wdioBranch to 'AT-Branch' for future pipelines
  fs.writeFileSync(Path.join(__dirname, `../AT-Branch.txt`), wdioBranch);
};

run();
