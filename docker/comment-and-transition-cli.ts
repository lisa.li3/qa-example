import { commentAndTransition } from './comment-and-transition';

const [jiraReference, message, transition] = process.argv.slice(2);

(async (): Promise<void> => {
  await commentAndTransition({
    jiraReference,
    message,
    transition,
  });
})();
