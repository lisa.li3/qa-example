/* eslint-disable @typescript-eslint/no-non-null-assertion */
import fetch from 'node-fetch';
require('dotenv').config();

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

function getBasicAuthString(username: string, password: string) {
  const plain = `${username}:${password}`;
  return Buffer.from(plain).toString('base64');
}

export const commentAndTransition = async ({
  jiraReference,
  message,
  transition,
}: {
  jiraReference: string;
  message: string;
  transition?: string;
}) => {
  // Remove any slashes from the jiraReference
  jiraReference = jiraReference.replace(/\//gm, '');

  let transitionId;

  switch (transition) {
    case 'In Development':
      transitionId = 111;
      break;
    case 'Code Review': 
      transitionId = 41;
      break;
    case 'Acceptance Testing':
      transitionId = 121;
      break;
    case 'Ready For Release':
      transitionId = 71; 
      break;
    case 'Done':
      transitionId = 81;
      break;
    case 'Blocked':
      transitionId = 21;
      break;
    default:
      transitionId = 21;
      break;
  }

  // Something happened, so we need to comment and transition the issue
  const bodyData = {
    update: {
      comment: [
        {
          add: {
            body: {
              type: 'doc',
              version: 1,
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Pipeline Link\n',
                      marks: [
                        {
                          type: 'link',
                          attrs: {
                            href: `https://bitbucket.org/${process.env.BITBUCKET_WORKSPACE}/${process.env.BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${process.env.BITBUCKET_BUILD_NUMBER}/steps/${process.env.BITBUCKET_STEP_UUID}`,
                            title: 'Bitbucket Pipeline',
                          },
                        },
                      ],
                    },
                    {
                      text: `Review process: ${message}`,
                      type: 'text',
                    },
                  ],
                },
              ],
            },
          },
        },
      ],
    },
    transition: {
      id: transitionId,
    },
  };

  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `Basic ${getBasicAuthString(
      process.env.JIRA_USERNAME || '',
      process.env.JIRA_TOKEN || ''
    )}`,
  };

  console.log(
    `Commenting on Ticket ${jiraReference}, with Message ${message}, `,
    transition ? `starting transition to ${transition}` : 'with no transition'
  );

  await fetch(`https://supergroupbt.atlassian.net/rest/api/3/issue/${jiraReference}/comment`, {
    method: 'POST',
    headers,
    body: JSON.stringify(bodyData.update.comment[0].add),
  })
    .then((response) => {
      console.log(`Comment Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .catch((err) => console.error(err));

  if (transition) {
    await fetch(
      `https://supergroupbt.atlassian.net/rest/api/3/issue/${jiraReference}/transitions`,
      {
        method: 'POST',
        headers,
        body: JSON.stringify(bodyData),
      }
    )
      .then((response) => {
        console.log(`Transition Response: ${response.status} ${response.statusText}`);
        if (response.status > 299) {
          // Error From Transition
          throw Error(`Transition returned code ${response.status}, ${response.error}`);
        }
        return response.text();
      })
      .catch((err) => {
        throw err;
      });
  }
};
