import axios from 'axios';
import moment from 'moment';
import { Bitbucket, Schema } from 'bitbucket';
import { config } from 'dotenv';
import path from 'path';

config({ path: path.join(__dirname, '../.env') });

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

const getPassStatus = (name: string | undefined): string => {
  switch (name) {
    case 'PASSED':
      return 'Passed   :white_check_mark:';
    case 'FAILED':
      return 'Failed   :x:';
    default:
      return 'Unknown status   :confused:';
  }
};

const postSlackMessage = async ({
  id,
  url,
  custom_message,
}: {
  id: string;
  url: string;
  custom_message: string;
}): Promise<void> => {
  const pipeline = await bbClient.repositories.getPipeline({
    pipeline_uuid: id,
    repo_slug: 'wdio-b2c-automation',
    workspace: 'projectteam',
  });

  console.log('PIPELINE: ', pipeline.data);

  const { target, creator, completed_on, created_on, state } = pipeline.data;

  const { result }: any = state;

  const blocks = [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: '*Bitbucket Test Pipeline*',
      },
    },
    {
      type: 'header',
      text: {
        type: 'plain_text',
        text: getPassStatus(result.name),
        emoji: true,
      },
    },
    {
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: `*Branch:*\n<https://bitbucket.org/projectteam/wdio-b2c-automation/branch/${target?.ref_name}|${target?.ref_name}>`,
        },
        {
          type: 'mrkdwn',
          text: `*Created by:*\n${creator ? creator.display_name : ''}`,
        },
      ],
    },
    {
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: `*When:*\n${moment(completed_on).format('MMM Do, h:mm a')}`,
        },
        {
          type: 'mrkdwn',
          text: `*Duration:*\n${moment(completed_on).diff(moment(created_on), 'minutes')}`,
        },
      ],
    },
    custom_message && {
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: custom_message,
        },
      ],
    },
  ];

  console.log('BLOCKS: ', blocks);

  axios.post(url, blocks);
};

postSlackMessage({
  id: process.argv[3],
  url: process.argv[2],
  custom_message: process.argv[4],
});
