/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import PaginatedRefs = Schema.PaginatedRefs;
import fetch from 'node-fetch';

const team = 'projectteam';
const defaultBranch = 'develop';

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

interface ExtendedPullRequest extends Schema.Pullrequest {
  repo_slug: string;
  workspace: string;
}

const repositories = [
  // // frontend
  // { defaultBranch, team, name: 'react-frontend' },
  // // backend
  // { defaultBranch, team, name: 'basket-service' },
  // { defaultBranch, team, name: 'content-service' },
  // { defaultBranch, team, name: 'email-service' },
  // { defaultBranch, team, name: 'giftcard-service' },
  // { defaultBranch, team, name: 'inventory-service' },
  // { defaultBranch, team, name: 'newsletter-service' },
  // { defaultBranch, team, name: 'order-service' },
  // { defaultBranch, team, name: 'payment-service' },
  // { defaultBranch, team, name: 'product-service' },
  // { defaultBranch, team, name: 'review-service' },
  // { defaultBranch, team, name: 'shipping-service' },
  // { defaultBranch, team, name: 'user-service' },
  // { defaultBranch, team, name: 'wishlist-service' },
  // and the test framework
  { defaultBranch, team, name: 'wdio-b2c-automation' },
];

const passFailValues = {
  PASS: 'pass',
  FAIL: 'fail',
};

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

/**
 * Gets a list of matching commit hashes for a named ref
 *
 * @param componentConfig
 * @param ref
 */
async function getMatchingHashesForRef(
  componentConfig: ComponentConfig,
  ref: string
): Promise<PaginatedRefs[]> {
  const allRefs: PaginatedRefs[] = [];

  const matchingHashes = await bbClient.repositories.listRefs({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    sort: 'name',
    pagelen: 100,
    q: `name ~ "${ref}"`,
  });
  allRefs.push(matchingHashes.data);

  return allRefs;
}

/**
 * Gets a list of open pull requests for the specified component
 *
 * @param componentConfig
 */
async function getOpenPullRequests(
  componentConfig: ComponentConfig
): Promise<ExtendedPullRequest[]> {
  const openPullRequests = await bbClient.repositories.listPullRequests({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    q: `state = "OPEN"`,
  });

  return (
    openPullRequests.data.values!.map((value) => ({
      ...value,
      repo_slug: componentConfig.name,
      workspace: componentConfig.team,
    })) || []
  );
}

/**
 * get open PRs matching branch prefix
 * @param branchPrefix
 */
async function getPullRequestsForApprovals(branchPrefix: string): Promise<ExtendedPullRequest[]> {
  const matchedPullRequests: ExtendedPullRequest[] = [];
  for (const repository of repositories) {
    const matchingRefs = await getMatchingHashesForRef(repository, branchPrefix);

    for (const matchingRef of matchingRefs) {
      if (matchingRef.values?.length) {
        console.log(`Found ref for component: ${repository.name} with pattern ${branchPrefix}`);
      } else {
        console.log(
          `Did not find ref for component: ${repository.name} with pattern ${branchPrefix}`
        );
      }
      for (const ref of matchingRef.values!) {
        if (ref.name !== repository.defaultBranch) {
          console.log(`Checking PRs for component: ${repository.name} with ref: ${ref.name}`);
          try {
            const pullRequests = await getOpenPullRequests(repository);
            for (const pullRequest of pullRequests) {
              if (pullRequest.source?.branch?.name === ref!.name) {
                matchedPullRequests.push(pullRequest);
              }
            }
          } catch (e) {
            console.log(e);
          }
        }
      }
    }
  }
  return matchedPullRequests;
}

function getBasicAuthString(username: string, password: string) {
  const plain = `${username}:${password}`;
  return Buffer.from(plain).toString('base64');
}

/**
 * CLI entrypoint
 */
const run = async () => {
  const [branchPrefix, passFail] = process.argv.slice(2);

  if (!branchPrefix || !Object.values(passFailValues).includes(passFail)) {
    console.error(`Usage: \`${__filename} {branch} {pass|fail}\``);
    process.exit(1);
  }
  for (const envVar of ['BB_USERNAME', 'BB_PASSWORD']) {
    if (!process.env[envVar]) {
      console.error(`${envVar} environment variable is not defined.`);
      process.exit(1);
    }
  }

  const prsToUpdate = await getPullRequestsForApprovals(branchPrefix);
  const credentialString = getBasicAuthString(process.env.BB_USERNAME!, process.env.BB_PASSWORD!);

  for (const pullRequest of prsToUpdate) {
    const prAttributes = {
      repo_slug: pullRequest.repo_slug,
      workspace: pullRequest.workspace,
      pull_request_id: pullRequest.id || 0,
    };
    if (passFail === passFailValues.PASS) {
      try {
        await bbClient.repositories.createPullRequestApproval(prAttributes);
        console.log(`PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug} approved`);
      } catch (e) {
        if (e.status === 409) {
          // PR already approved
          console.log(
            `PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug} already approved`
          );
          return;
        }
        throw e;
      }
    } else {
      // Request Changes
      const url = `https://api.bitbucket.org/2.0/repositories/${prAttributes.workspace}/${prAttributes.repo_slug}/pullrequests/${prAttributes.pull_request_id}/request-changes`;
      const result = await fetch(url, {
        method: 'POST',
        headers: {
          Authorization: `Basic ${credentialString}`,
        },
      });
      if (result.status === 409) {
        // PR already approved
        console.log(
          `PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug} has already has changes requested`
        );
        return;
      }
      if (result.status >= 299) {
        throw new Error(`Unexpected HTTP response code on ${url}: ${result.status}`);
      }
      console.log(
        `PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug} approval retracted, changes requested`
      );
    }
  }
};

run();
