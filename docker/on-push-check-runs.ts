/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import path from 'path';
import { filter, find, isEmpty, startsWith } from 'lodash';
import {
  startPipeline,
  repositories,
  getPullRequestsForNextRuns,
  runOnSchedule,
} from './get-next-to-run';
import moment from 'moment';

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const team = 'projectteam';
const defaultBranch = 'develop';

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

interface ExtendedPullRequest extends Schema.Pullrequest {
  repo_slug: string;
  workspace: string;
}

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

const onPush = async () => {
  // 2 - Get any running pipelines (Y)
  const pipelines = await bbClient.repositories.listPipelines({
    repo_slug: repositories[0].name,
    workspace: repositories[0].team,
    sort: '-created_on',
  });

  const runningPipelines = filter(pipelines.data.values, {
    state: {
      name: 'IN_PROGRESS',
    },
    target: {
      selector: {
        pattern: 'test-pipeline',
      },
    },
  });

  // Search (Y) for matching branch name in (X)
  const runningOnThisBranch = filter(runningPipelines, {
    target: {
      ref_name: process.env.BITBUCKET_BRANCH || repositories[0].defaultBranch,
    },
  });

  // If none found, exit
  if (!isEmpty(runningPipelines) && isEmpty(runningOnThisBranch)) {
    console.log(`Other pipelines are running on other PR's, exiting`);
    process.exit(0);
  }

  if (moment().isBetween(moment().startOf('day'), moment().startOf('day').add(6, 'hours'))) {
    console.log(`Within Rebuild Window... Exiting`);
    process.exit(0);
  }

  // If >0 found, remove them
  for (const runningPipeline of runningOnThisBranch) {
    await bbClient.repositories.stopPipeline({
      // TODO: Type Guard (if we cba)
      // @ts-ignore-next-line
      pipeline_uuid: runningPipeline.uuid,
      repo_slug: process.env.BITBUCKET_REPO_SLUG || repositories[0].name,
      workspace: process.env.BITBUCKET_WORKSPACE || repositories[0].team,
    });
    // @ts-ignore-next-line
    console.log(`Stopped Pipeline with UUID: ${runningPipeline.uuid}`);
  }

  return runOnSchedule;
};

onPush();
