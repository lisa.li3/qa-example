const team = 'projectteam';
const defaultBranch = 'develop';

export const repositories = [
  // frontend
  { defaultBranch, team, name: 'react-frontend' },
  { defaultBranch, team, name: 'react-infrastructure' },
  // backend
  { defaultBranch, team, name: 'sitemap-generator' },
  { defaultBranch, team, name: 'ms-site-config' },
  { defaultBranch, team, name: 'basket-service' },
  { defaultBranch, team, name: 'content-service' },
  { defaultBranch, team, name: 'email-service' },
  { defaultBranch, team, name: 'giftcard-service' },
  { defaultBranch, team, name: 'inventory-service' },
  { defaultBranch, team, name: 'newsletter-service' },
  { defaultBranch, team, name: 'order-service' },
  { defaultBranch, team, name: 'order-exporter' },
  { defaultBranch, team, name: 'payment-service' },
  { defaultBranch, team, name: 'product-service' },
  { defaultBranch, team, name: 'review-service' },
  { defaultBranch, team, name: 'shipping-service' },
  { defaultBranch, team, name: 'user-service' },
  { defaultBranch, team, name: 'test-mock-service' },
  { defaultBranch, team, name: 'wishlist-service' },
  // and the test framework
  { defaultBranch: 'master', team, name: 'wdio-b2c-automation' },
];
