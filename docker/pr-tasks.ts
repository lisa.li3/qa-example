/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import PaginatedRefs = Schema.PaginatedRefs;
import fetch from 'node-fetch';
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });

const team = 'projectteam';
const defaultBranch = 'develop';

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

interface ExtendedPullRequest extends Schema.Pullrequest {
  repo_slug: string;
  workspace: string;
}

const repositories = [
  // and the test framework
  { defaultBranch, team, name: 'wdio-b2c-automation' },
];

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

/**
 * Gets a list of matching commit hashes for a named ref
 *
 * @param componentConfig
 * @param ref
 */
async function getMatchingHashesForRef(
  componentConfig: ComponentConfig,
  ref: string
): Promise<PaginatedRefs[]> {
  const allRefs: PaginatedRefs[] = [];

  const matchingHashes = await bbClient.repositories.listRefs({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    sort: 'name',
    pagelen: 100,
    q: `name ~ "${ref}"`,
  });
  allRefs.push(matchingHashes.data);

  return allRefs;
}

/**
 * Gets a list of open pull requests for the specified component
 *
 * @param componentConfig
 */
async function getOpenPullRequests(
  componentConfig: ComponentConfig
): Promise<ExtendedPullRequest[]> {
  const openPullRequests = await bbClient.repositories.listPullRequests({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    q: `state = "OPEN"`,
  });

  return (
    openPullRequests.data.values!.map((value) => ({
      ...value,
      repo_slug: componentConfig.name,
      workspace: componentConfig.team,
    })) || []
  );
}

/**
 * get open PRs matching branch prefix
 * @param branchPrefix
 */
async function getPullRequestsForTasks(branchPrefix: string): Promise<ExtendedPullRequest[]> {
  const matchedPullRequests: ExtendedPullRequest[] = [];
  for (const repository of repositories) {
    const matchingRefs = await getMatchingHashesForRef(repository, branchPrefix);

    for (const matchingRef of matchingRefs) {
      if (matchingRef.values?.length) {
        console.log(`Found ref for component: ${repository.name} with pattern ${branchPrefix}`);
      } else {
        console.log(
          `Did not find ref for component: ${repository.name} with pattern ${branchPrefix}`
        );
      }
      for (const ref of matchingRef.values!) {
        if (ref.name !== repository.defaultBranch) {
          console.log(`Checking PRs for component: ${repository.name} with ref: ${ref.name}`);
          try {
            const pullRequests = await getOpenPullRequests(repository);
            for (const pullRequest of pullRequests) {
              if (pullRequest.source?.branch?.name === ref!.name) {
                matchedPullRequests.push(pullRequest);
              }
            }
          } catch (e) {
            console.log(e);
          }
        }
      }
    }
  }
  return matchedPullRequests;
}

function getBasicAuthString(username: string, password: string) {
  const plain = `${username}:${password}`;
  return Buffer.from(plain).toString('base64');
}

/**
 * CLI entrypoint
 */
const run = async () => {
  const [command, path, branchPrefix] = process.argv;
  let taskName = process.env.TASK_OVERRIDE;

  if (!process.env.TASK_OVERRIDE) {
    taskName = 'RUN';
  }

  for (const envVar of ['BB_USERNAME', 'BB_PASSWORD']) {
    if (!process.env[envVar]) {
      console.error(`${envVar} environment variable is not defined.`);
      process.exit(1);
    }
  }

  const prsToUpdate = await getPullRequestsForTasks(branchPrefix);
  const credentialString = getBasicAuthString(process.env.BB_USERNAME!, process.env.BB_PASSWORD!);

  for (const pullRequest of prsToUpdate) {
    const prAttributes = {
      repo_slug: pullRequest.repo_slug,
      workspace: pullRequest.workspace,
      pull_request_id: pullRequest.id || 0,
    };

    const url = `https://api.bitbucket.org/2.0/repositories/${prAttributes.workspace}/${prAttributes.repo_slug}/pullrequests/${prAttributes.pull_request_id}/tasks`;
    const tasksResponse = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: `Basic ${credentialString}`,
      },
    });
    if (tasksResponse.status >= 299) {
      throw new Error(
        `Unexpected HTTP response code on ${url}: ${
          tasksResponse.status
        }\n\nResponse: ${JSON.stringify(tasksResponse)}`
      );
    }
    const currentTasks = (await tasksResponse.json()).values;

    let tagUrl;
    let taskFound;

    for (const task of currentTasks) {
      if (task.content.raw === taskName) {
        console.log(`${taskName} Task already present!, with state: ${task.state}`);
        if (
          process.env.UPDATE_TASK === 'true' &&
          process.env.UPDATE_MESSAGE === 'UNRESOLVED' &&
          task.state !== 'UNRESOLVED'
        ) {
          // Mark the task as unresolved
          await fetch(task.links.self.href, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Basic ${credentialString}`,
            },
            body: JSON.stringify({ state: 'UNRESOLVED' }),
          });
          console.log(`${taskName}: Updated Task to be UNRESOLVED`);
          taskFound = true;
          break;
        } else if (
          process.env.UPDATE_TASK === 'true' &&
          process.env.UPDATE_MESSAGE === 'RESOLVED' &&
          task.state !== 'RESOLVED'
        ) {
          // Resolve Task
          await fetch(task.links.self.href, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Basic ${credentialString}`,
            },
            body: JSON.stringify({ state: 'RESOLVED' }),
          });
          console.log(`${taskName}: Updated Task to be RESOLVED`);
          taskFound = true;
          break;
        }
        taskFound = true;
        tagUrl = task.links.self.href;
      }
    }

    if (!taskFound) {
      const result = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${credentialString}`,
        },
        body: JSON.stringify({
          content: { raw: taskName },
        }),
      });
      if (result.status === 409) {
        console.log(
          `PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug} has a task called RUN`
        );
        return;
      }
      if (result.status >= 299) {
        throw new Error(
          `Unexpected HTTP response code on ${url}: ${result.status}\n\nResponse: ${JSON.stringify(
            result
          )}`
        );
      }

      console.log(
        `PR ${prAttributes.pull_request_id} for ${prAttributes.repo_slug}: Task "${taskName}" added`
      );

      if (process.env.UPDATE_MESSAGE === 'RESOLVED') {
        await fetch((await result.json()).links.self.href, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Basic ${credentialString}`,
          },
          body: JSON.stringify({ state: 'RESOLVED' }),
        });
        console.log(`${taskName}: Updated Task to be RESOLVED`);
      }
    }
  }
};

run();
