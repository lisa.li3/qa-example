/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Bitbucket, Schema } from 'bitbucket';
import PaginatedRefs = Schema.PaginatedRefs;
import * as child_process from 'child_process';
import fs from 'fs';
import { commentAndTransition } from './comment-and-transition';
import { repositories } from './constants';
require('dotenv').config();

const team = 'projectteam';
const defaultBranch = 'develop';
const inProgressTransition = 'In Development';
const wdioBranch = defaultBranch;

interface ComponentConfig {
  defaultBranch: string;
  team: string;
  name: string;
}

interface ExtendedPullRequest extends Schema.Pullrequest {
  repo_slug: string;
  workspace: string;
}

process.on('unhandledRejection', (e: Error) => {
  console.log(e);
  console.error(`\nError: ${e.message}`);
  // @ts-ignore
  const s = e.statusCode || false;
  if (s) {
    console.error(`Status code: ${s}`);
  }
  process.exit(1);
});

const bbClient = new Bitbucket({
  auth: {
    username: process.env.BB_USERNAME!,
    password: process.env.BB_PASSWORD!,
  },
  notice: false,
});

/**
 * Gets a list of matching commit hashes for a named ref
 *
 * @param componentConfig
 * @param ref
 */
async function getMatchingHashesForRef(
  componentConfig: ComponentConfig,
  ref: string
): Promise<PaginatedRefs[]> {
  const allRefs: PaginatedRefs[] = [];

  const matchingHashes = await bbClient.repositories.listRefs({
    repo_slug: componentConfig.name,
    workspace: componentConfig.team,
    sort: 'name',
    pagelen: 100,
    q: `name ~ "${ref}"`,
  });

  console.log(`Looking in ${componentConfig.name} for ${ref}`);
  if (matchingHashes.data?.values?.length === 0) {
    console.log(`No matches for ${ref} in ${componentConfig.name} `);
  }

  allRefs.push(matchingHashes.data);

  return allRefs;
}

/**
 * CLI entrypoint
 */
const run = async () => {
  const [branchPrefix] = process.argv.slice(2);

  if (!branchPrefix) {
    console.error(`Usage: \`${__filename} {branch} {checkForDevelopChanges}\``);
    process.exit(1);
  }
  for (const envVar of ['BB_USERNAME', 'BB_PASSWORD']) {
    if (!process.env[envVar]) {
      console.error(`${envVar} environment variable is not defined.`);
      process.exit(1);
    }
  }

  // Sync the references with Develop again
  for (const repository of repositories) {
    const matchingRefs = await getMatchingHashesForRef(repository, branchPrefix);
    console.log(matchingRefs);
    for (const { values } of matchingRefs) {
      if (values && values.length > 0) {
        // Update this branch
        const ref = values[0];
        console.log(`Merging ${repository.name}'s code... (branch: ${ref.name})`);

        try {
          if (!fs.existsSync('temp_repos')) {
            fs.mkdirSync('temp_repos');
          }

          child_process.execSync(
            `
            cd temp_repos &&
            git config --global user.email "prometheus-ci-testuser@prometheus.sd.co.uk"
            git config --global user.name "Prometheus CI Test User"
            git clone https://${process.env.BB_USERNAME}:${process.env.BB_PASSWORD}@bitbucket.org/projectteam/${repository.name}.git &&
            cd ${repository.name} &&
            git merge origin/${ref.name} --commit --no-edit &&
            git push &&
            git push origin --delete ${ref.name}`
          );
          fs.rmdirSync('temp_repos', { recursive: true });
          // Successfully updated the branch, and removed the repo
        } catch (e) {
          // Failed to update branch, fall back
          await commentAndTransition({
            jiraReference: branchPrefix,
            message: `FATAL: Unable to Merge (${ref?.name}) to develop on ${repository}, error: ${e}`,
            transition: inProgressTransition,
          });
          fs.rmdirSync('temp_repos', { recursive: true });
          process.exit(2);
        }
      }
    }
  }
};

run();
