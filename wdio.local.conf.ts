import * as base from './wdio.conf';

// @ts-ignore
export const config: IConfig = {
  ...base.config,

  waitforTimeout_get: 500,
  waitforTimeout_find: 1000,
  waitforTimeout_clientMutation: 2000,
  waitforTimeout_serverMutation: 4000,
  waitforTimeout: 7000,

  cucumberOpts: {
    ...base.config.cucumberOpts,
    timeout: 30000,
  } as WebdriverIO.CucumberOpts,
};
