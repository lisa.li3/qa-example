import * as base from '../../wdio.conf';
import * as acid from '../acid.common.conf';
import { BUILD_ID } from '../../src/support/hooks';
import { isUndefined } from 'lodash';
import { v4 as uuidv4 } from 'uuid';

const TAG = process.env.TAG_EXPRESSION || 'unknown';
const buildID = process.env.buildID || BUILD_ID;

export const sauceBuildID = `B2C: ${TAG} - ${buildID} - ${uuidv4()}`;

require('dotenv').config();

export const config = {
  ...base.config, // Keep First
  services: [['sauce']],
  reporters: ['cucumberjs-json'],
  logLevel: 'error',
  maxInstances: process.env.isCI ? 15 : 10,
  region: 'eu',
  user: process.env.SAUCE_USERNAME,
  key: process.env.SAUCE_ACCESS_KEY,
  ...(!isUndefined(process.env.IS_ACID) ? acid.config : {}), // Keep Last
};
