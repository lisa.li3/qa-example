import * as base from './saucelabs.common.conf';

require('dotenv').config();

export const config = {
  ...base.config,
  capabilities: [
    {
      browserName: 'chrome',
      browserVersion: 'latest',
      platformName: 'Windows 10',
      'sauce:options': {
        parentTunnel: 'Test-Sam',
        tunnelIdentifier: 'Super-Automation',
        screenResolution: '1920x1080',
        build: base.sauceBuildID,
        region: 'eu',
        seleniumVersion: '4.0.0',
        maxDuration: 3600,
        // extendedDebugging: true,
        // capturePerformance: true,
      },
    },
  ],
  'framework:meta': {
    windowSize: '1920x1080',
    browserType: 'Desktop',
  },
};
