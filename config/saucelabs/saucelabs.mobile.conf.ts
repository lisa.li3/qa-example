import * as base from './saucelabs.common.conf';

require('dotenv').config();

export const config = {
  ...base.config,
  capabilities: [
    {
      browserName: 'chrome',
      browserVersion: 'latest',
      platformName: 'Windows 10',
      'goog:chromeOptions': {
        mobileEmulation: {
          deviceName: 'iPhone X',
        },
      },
      'sauce:options': {
        parentTunnel: 'Test-Sam',
        tunnelIdentifier: 'Super-Automation',
        screenResolution: '1920x1080',
        build: base.sauceBuildID,
        region: 'eu',
        seleniumVersion: '4.0.0',
        maxDuration: 3600,
      },
    },
  ],
  'framework:meta': {
    windowSize: '400x1080',
    browserType: 'Mobile',
  },
};
