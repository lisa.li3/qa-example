import * as base from './local.common.conf';

require('dotenv').config();

const googleArgs: string[] = ['--window-size=768,1080'];

process.env.HEADLESS && googleArgs.push('headless');
process.env.HEADLESS && googleArgs.push('disable-gpu');

export const config = {
  ...base.config,
  capabilities: [
    {
      browserName: 'chrome',
      'meta:targetName': `chrome (iPad emulation, 768x1080)`,
      'goog:chromeOptions': {
        mobileEmulation: {
          deviceName: 'iPad',
        },
        args: googleArgs,
      },
    },
  ],
  'framework:meta': {
    windowSize: '768x1080',
    browserType: 'Tablet',
  },
};
