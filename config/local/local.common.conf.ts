import * as base from '../../wdio.local.conf';
import * as acid from '../acid.common.conf';
import { isUndefined } from 'lodash';
require('dotenv').config();

export const config = {
  ...base.config, // Keep First
  services: ['chromedriver'],
  logLevel: 'error',
  maxInstances: process.env.HEADLESS ? 10 : 15,
  ...(!isUndefined(process.env.IS_ACID) ? acid.config : {}), // Keep Last
};
