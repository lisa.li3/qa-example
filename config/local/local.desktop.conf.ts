import * as base from './local.common.conf';

require('dotenv').config();

const googleArgs: string[] = ['--window-size=1920,1080'];

process.env.HEADLESS && googleArgs.push('headless');
process.env.HEADLESS && googleArgs.push('disable-gpu');

export const config = {
  ...base.config,
  capabilities: [
    {
      browserName: 'chrome',
      'meta:targetName': `chrome (Desktop, 1980x100)`,
      'goog:chromeOptions': {
        args: googleArgs,
      },
    },
  ],
  'framework:meta': {
    windowSize: '1920x1080',
    browserType: 'Desktop',
  },
};
