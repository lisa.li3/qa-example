import * as base from './local.common.conf';

require('dotenv').config();

const googleArgs: string[] = ['--window-size=375,812'];

process.env.HEADLESS && googleArgs.push('headless');
process.env.HEADLESS && googleArgs.push('disable-gpu');

export const config = {
  ...base.config,
  capabilities: [
    {
      browserName: 'chrome',
      'meta:targetName': `chrome (iPhone X emulation, 375x812)`,
      'goog:chromeOptions': {
        mobileEmulation: {
          deviceName: 'iPhone X',
        },
        args: googleArgs,
      },
    },
  ],
  'framework:meta': {
    windowSize: '812x375',
    browserType: 'Mobile',
  },
};
