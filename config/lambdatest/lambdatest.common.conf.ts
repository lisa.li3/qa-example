import * as base from '../../wdio.conf';
import * as acid from '../acid.common.conf';
import { isUndefined } from 'lodash';
require('dotenv').config();

export const config = {
  ...base.config, // Keep First
  bail: process.env.isCI ? 1 : 0,
  maxInstances: 10,
  services: [['lambdatest']],
  user: process.env.LT_USERNAME,
  key: process.env.LT_ACCESS_KEY,
  hostname: 'hub.lambdatest.com',
  port: 80,
  path: '/wd/hub',
  ...(!isUndefined(process.env.IS_ACID) ? acid.config : {}), // Keep Last
};
