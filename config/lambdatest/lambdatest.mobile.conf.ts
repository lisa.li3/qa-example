import * as base from './lambdatest.common.conf';
import { BUILD_ID } from '../../src/support/hooks';
import { v4 as uuidv4 } from 'uuid';

require('dotenv').config();

const TAG = process.env.TAG_EXPRESSION || 'unknown';
const buildID = process.env.buildID || BUILD_ID;
const BRANCH = process.env.BRANCH || 'develop';

export const sauceBuildID = `${BRANCH} - AT_ID: ${buildID} - rev. ${uuidv4()}`;

export const config = {
  ...base.config,

  capabilities: [
    {
      browserName: 'chrome',
      'LT:Options': {
        build: sauceBuildID,
        platformName: 'Windows 11',
        selenium_version: '4.1.0',
        geoLocation: 'GB',
        resolution: '1920x1080',
        network: true,
      },
      'goog:chromeOptions': {
        mobileEmulation: {
          deviceName: 'iPhone X',
        },
      },
    },
  ],
};
