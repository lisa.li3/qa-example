@PLP
@PLP_PAGE
@REGRESSION

Feature: Product List Page (PLP) - Products
  As a user of the Product List Page, I can see and order products

  #
  # CATEGORY PLP
  #
  @B2C2-537 @B2C2-3890
  Scenario: Seed the data
    Given I open the page "/"
    And I seed config using the file "ProductListPage_StaffDiscountWithProducts"
    And I open the siteId "staffdiscount-plp"
    And I seed using the file "ProductListPage_StaffDiscountUserAndProducts"
    And I open the page "/main/framework/b2c2_537_plp_staffdiscount_1"

  @B2C2-3890
  Scenario: All products on a page should display product images
    Then the element "ProductListPage.firstImage" is displayed
    And the element "ProductListPage.secondImage" is displayed
    And the element "ProductListPage.thirdImage" is displayed

  @B2C2-537
  Scenario Outline: For a 'GUEST' user on a 'CATEGORY PLP' page, The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | not       | 88.80   |
      | Product 1 - Staff discount set to true               | not       | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | also      | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 75.00   |
      | Product 2 - Markdown with Staff discount set to true | not       | 40.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: For a 'GUEST' user on a 'CATEGORY PLP' page, the you save amount should be displayed for dicounted product
    Then the element "ProductListPage.youSaveOnProductOne" is not displayed
    Then the element "ProductListPage.youSaveOnProductTwo" is displayed
    Then the element "ProductListPage.youSaveOnProductThree" is not displayed

  @B2C2-537
  Scenario: Log in with a 'STAFF' user and navigate to a 'CATEGORY PLP'
    Given I open the page "/login"
    And I set "plpuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "StaffUser WithDiscount"
    And I open the page "/main/framework/b2c2_537_plp_staffdiscount_1"

  @B2C2-537
  Scenario Outline: For a 'STAFF' user a 'CATEGORY PLP', The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | also      | 88.80   |
      | Product 1 - Staff discount set to true               | also      | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | not       | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | not       | 75.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 40.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 60.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: For a 'STAFF' user a 'CATEGORY PLP', the you save amount should be displayed for dicounted product
    Then the element "ProductListPage.youSaveOnProductOne" is displayed
    Then the element "ProductListPage.youSaveOnProductTwo" is displayed
    Then the element "ProductListPage.youSaveOnProductThree" is not displayed

  @B2C2-537
  Scenario: Log out the 'STAFF' user, and log in with a 'NON STAFF' user and navigate to a 'CATEGORY PLP'
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    When I open the page "/login"
    And I set "plpuser.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "NonStaffUser WithoutStaffDiscount"
    And I open the page "/main/framework/b2c2_537_plp_staffdiscount_1"
    And I wait on element "Product 1 - Staff discount set to true"

  @B2C2-537
  Scenario Outline: For a 'NON STAFF' user on a 'CATEGORY PLP', The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | not       | 88.80   |
      | Product 1 - Staff discount set to true               | not       | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | also      | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | not       | 40.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 75.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: For a 'NON STAFF' user on a 'CATEGORY PLP', the you save amount should be displayed for dicounted product
    Then the element "ProductListPage.youSaveOnProductOne" is not displayed
    Then the element "ProductListPage.youSaveOnProductTwo" is displayed
    Then the element "ProductListPage.youSaveOnProductThree" is not displayed
  # ---
  #
  # Display Average Product Ratings on a Search PLP page
  # https://supergroupbt.atlassian.net/browse/B2C2-2260
  #
  # ---
  @B2C2-537
  Scenario: Go to a 'SEARCH PLP' Page
    Given I open the page "/search/plp00666p1,plp00666p2,plp00666p3"

  @B2C2-537
  Scenario Outline: For a 'NON STAFF' user on a 'SEARCH PLP' Page, The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | not       | 88.80   |
      | Product 1 - Staff discount set to true               | not       | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | also      | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 75.00   |
      | Product 2 - Markdown with Staff discount set to true | not       | 40.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: For a 'NON STAFF' user on a 'SEARCH PLP' Page, the you save amount should be displayed for dicounted product
    Then the element "ProductListPage.youSaveOnProductOne" is not displayed
    Then the element "ProductListPage.youSaveOnProductTwo" is displayed
    Then the element "ProductListPage.youSaveOnProductThree" is not displayed

  @B2C2-537
  Scenario: Lout out previous user and Log in with a 'STAFF' user and navigate to a 'Search PLP'
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    When I open the page "/login"
    And I set "plpuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "StaffUser WithDiscount"
    And I open the page "/search/plp00666p1,plp00666p2,plp00666p3"

  @B2C2-537
  Scenario Outline: For a 'STAFF' user a 'SEARCH PLP', The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | also      | 88.80   |
      | Product 1 - Staff discount set to true               | also      | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | not       | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 40.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 60.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: Lout out previous user and go to a 'SEARCH PLP'
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    And  I open the page "/search/plp00666p1,plp00666p2,plp00666p3"

  @B2C2-537
  Scenario Outline: For a 'GUEST' user on a 'SEARCH PLP' page, The product details for <productName> should <condition> contain <details>
    Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<details>"

    Examples:
      | productName                                          | condition | details |
      | Product 1 - Staff discount set to true               | also      | 222.00  |
      | Product 1 - Staff discount set to true               | not       | 88.80   |
      | Product 1 - Staff discount set to true               | not       | 133.20  |
      | Product 2 - Markdown with Staff discount set to true | also      | 100.00  |
      | Product 2 - Markdown with Staff discount set to true | also      | 25.00   |
      | Product 2 - Markdown with Staff discount set to true | not       | 40.00   |
      | Product 2 - Markdown with Staff discount set to true | also      | 75.00   |
      | Product 3 - Does not qualify                         | also      | 12.00   |
      | Product 3 - Does not qualify                         | not       | 4.81    |
      | Product 3 - Does not qualify                         | not       | 9.00    |

  @B2C2-537
  Scenario: For a 'GUEST' user on a 'SEARCH PLP' page, the you save amount should be displayed for dicounted product
    Then the element "ProductListPage.youSaveOnProductOne" is not displayed
    Then the element "ProductListPage.youSaveOnProductTwo" is displayed
    Then the element "ProductListPage.youSaveOnProductThree" is not displayed

  @B2C2-537
  Scenario: Tidy up - Switch back to uk site for next set of tests
    And I open the siteId "com"
    Then I expect the url to contain "com."