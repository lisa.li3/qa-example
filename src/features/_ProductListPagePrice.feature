@PLP
@PLP_PRICE
@REGRESSION

Feature: Product List Page (PLP) - Price
  As a user of the Product List Page, I can view and sort by ratings

  # ---
  #
  # Sort By Price
  # https://supergroupbt.atlassian.net/browse/B2C2-296
  #
  # ---

  Rule: Products on the PLP can be sorted by Price - Highest and Lowest (also checks Relevance)

    @B2C2-296
    Scenario: Seed products with defined prices on the plp
      Given I open the domain "com"
      And I seed using the file "ProductListPage_ProductsWithDefinedPrices"
      And I open the page "/main/framework/sort-by-price"
      And I refresh the page

    @B2C2-296 @SKIP_TABLET @SKIP_MOBILE
    Scenario Outline: On Desktop, Check the sorting on the PLP when the sort order is <sortAttribute>
      When I select the option with the value "<sortAttribute>" for element "ProductListPage.sortDropdown"
      Then I expect that the values returned by "ProductListPage.productsNames" to be in the following order
        """
        <productName1>
        <productName2>
        <productName3>
        <productName4>
        <productName5>
        """

      Examples:
        | sortAttribute | productName1              | productName2              | productName3              | productName4              | productName5              |
        | relevance     | Medium Priced Product - 1 | Highest Priced Product    | Lowest Priced Product     | Medium Priced Product - 2 | Medium Priced Product - 3 |
        | price~desc    | Highest Priced Product    | Medium Priced Product - 1 | Medium Priced Product - 2 | Medium Priced Product - 3 | Lowest Priced Product     |
        | price~asc     | Lowest Priced Product     | Medium Priced Product - 3 | Medium Priced Product - 1 | Medium Priced Product - 2 | Highest Priced Product    |

    @B2C2-296 @SKIP_DESKTOP
    Scenario Outline: On Mobile or Tablet, Check the sorting on the PLP when the sort order is <mobileSort>
      Given I click on the element "ProductListPage.sortMobile"
      And I click on the element "ProductListPage.<mobileSort>"
      And I wait on element "ProductListPage.<mobileSort>" to not be displayed
      Then I expect that the values returned by "ProductListPage.productsNames" to be in the following order
        """
        <productName1>
        <productName2>
        <productName3>
        <productName4>
        <productName5>
        """

      Examples:
        | mobileSort             | productName1              | productName2              | productName3              | productName4              | productName5              |
        | mobileSortMostRelevant | Medium Priced Product - 1 | Highest Priced Product    | Lowest Priced Product     | Medium Priced Product - 2 | Medium Priced Product - 3 |
        | mobileSortHighestPrice | Highest Priced Product    | Medium Priced Product - 1 | Medium Priced Product - 2 | Medium Priced Product - 3 | Lowest Priced Product     |
        | mobileSortLowestPrice  | Lowest Priced Product     | Medium Priced Product - 3 | Medium Priced Product - 1 | Medium Priced Product - 2 | Highest Priced Product    |

