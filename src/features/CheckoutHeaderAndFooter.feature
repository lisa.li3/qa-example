@CHECKOUTHEADERANDFOOTER
@CHECKOUT
@DELIVERY
@DELIVERY_ADDRESS
@REGRESSION

Feature: Checkout Delivery Options

  @B2C2-3609
  Scenario: Setup environment with products and site id
    Given I seed config using the file "CheckoutHeaderAndFooter"
    When I open the siteId "b2c23609"
    And I seed using the file "CheckoutHeaderAndFooter" and add the items into my "basket" local storage

  @B2C2-3609
  Scenario: Should check header/footer selectors are correct
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "Navigation.projectLogo" is displayed
    Then I expect that element "Header.bannerMain" is not displayed
    And I expect that element "Footer.customerServicesColumn" is not displayed
    And I expect that element "Footer.copyright" is displayed

  @B2C2-3609
  Scenario: Customer is presented with home page after clicking project logo
    Given I click on the element "Navigation.projectLogo"
    Then the page url is "/"

  @B2C2-3609
  Scenario: Customer is presented with shopping bag page after clicking basket icon from checkout
    Given I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    When I click on the element "Header.miniBagIcon"
    Then I expect the url to contain "/shopping-bag"

  @B2C2-3609
  Scenario: Checks the country selector in the footer can be opened
    Given I am on the page "/checkout"
    When I click on the element "Footer.siteSelectorButton"
    Then I expect that element "Footer.sitePanelClose" is within the viewport

  @B2C2-3609
  Scenario: Checks the country selector in the footer can be closed
    When I click on the element "Footer.sitePanelClose"
    Then I expect that element "Footer.siteSelectorButton" becomes displayed

