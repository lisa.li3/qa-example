@REDEEMING_GIFT_CARDS
@REGRESSION
@B2C2-2724

Feature: Checkout Redeem Gift Card
  Scenario: Navigating to Checkout with a basket
    Given I am on the page "/"
    And I seed using the file "CheckoutRedeemGiftCard" and add the items into my "basket" local storage
    And I open the page "/checkout"

  Scenario: I can access the Need Help popup modal and I can see the gift card popup text
    When I wait on element "CheckoutRedeemGiftCard.RedeemGiftCardButton"
    And I click on the element "CheckoutRedeemGiftCard.RedeemGiftCardButton"
    And I wait on element "CheckoutRedeemGiftCard.NeedHelpButton"
    And I click on the element "CheckoutRedeemGiftCard.NeedHelpButton"
    And I wait on element "CheckoutRedeemGiftCard.NeedHelpPopupText" to be displayed
    Then I expect that element "CheckoutRedeemGiftCard.NeedHelpPopupText" is displayed

  Scenario: I can close the gift card popup modal
    Given I wait on element "CheckoutRedeemGiftCard.NeedHelpPopupText"
    When I click on the element "CheckoutRedeemGiftCard.NeedHelpPopupCloseButton"
    Then I wait on element "CheckoutRedeemGiftCard.NeedHelpPopupText" to not be displayed
    And I expect that element "CheckoutRedeemGiftCard.NeedHelpPopupText" is not displayed