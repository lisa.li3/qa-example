@ACCOUNT
@REGRESSION

Feature: My Account - Customer Credit
  B2C2-2724 - View customer credit in my account

  @B2C2-2822 @B2C2-1639 @B2C2-2317
  Scenario: Seed for my account section
    Given I seed using the file "MyAccountCustomerCredit"
    When I open the page "/login"
    And I wait on element "Login.emailAddress" to be displayed

  @B2C2-2317
  Scenario: I seed a user without credit and assert the credit history button is not displayed
    When I set "customercreditNoCredit@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    Then I expect that element "MyAccount.creditHistoryButton" is not displayed
    And I click on the element "MyAccount.logOutButton"

  @B2C2-2317
  Scenario: I seed an account with customer credit and navigate to my account page
    And I am on the page "/login"
    When I set "customercredit@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"

  @B2C2-2317
  Scenario: Ensure the credit history button is displayed and clickable
    Given I click on the element "MyAccount.creditHistoryButton"

  @B2C2-2317
  Scenario Outline: Ensure user can see <ElementsToBeDisplayed> on the page
    Then I expect that element "CustomerCredits.<ElementsToBeDisplayed>" is displayed
    Examples:
      | ElementsToBeDisplayed  |
      | issueDateHeader        |
      | creditValueHeader      |
      | remainingBalanceHeader |
      | expiredStatus          |
      | activeStatus           |
      | pendingStatus          |
      | disabledStatus         |

  @B2C2-2317
  Scenario: Ensures when clicking the 'Back' button the user returns to the my-account page
    Given I click on the element "CustomerCredits.backButton"
    Then I expect the url to contain "/my-account"
    And I click on the element "MyAccount.logOutButton"