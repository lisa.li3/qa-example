@PLP
@REGRESSION

Feature: Product List Page (PLP)
  As a user of the Product List Page, I want to be able to view and select products for purchase

  @B2C2-275 @B2C2-3327 @B2C2-2177 @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @B2C2-3634 @FACETS @B2C2-4761 @B2C2-4762 @B2C2-4987
  Scenario: Seed products on the PLP page for the associated tests
    Given I open the page "/"
    And I seed using the file "ProductListPage"

  @B2C2-4761 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Check that the page scrolls to top when selecting a second filter
    Given I open the page "/main/framework/b2c2-275/100-products"
    When I click on the element "ProductListPage.sizeFilter"
    And I select the 1st option for element "ProductListPage.facetOptions"
    And I scroll to element "ProductListPage.productListItem"
    And I click on the element "ProductListPage.genderFilter"
    And I select the 1st option for element "ProductListPage.facetOptions"
    Then I expect that element "NewsletterSignupBanner.emailField" is not within the viewport
    And I expect that element "Stylish Products" is within the viewport

  @B2C2-4761 @SKIP_DESKTOP
  Scenario: Check that the page scrolls to top when selecting a second filter
    Given I open the page "/main/framework/b2c2-275/100-products"
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.sizeFilterMobile"
    And I click on the element "ProductListPage.mobile11"
    And I click on the button "ProductListPage.closeButton"
    And I scroll to element "ProductListPage.productListItem"
    And I scroll to element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.genderFilterMobile"
    And I click on the element "ProductListPage.mobileWomens"
    And I click on the button "ProductListPage.closeButton"
    Then I expect that element "NewsletterSignupBanner.emailField" is not within the viewport
    And I expect that element "Stylish Products" is within the viewport

  @B2C2-4761 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Clicking Clear All Removes Facets to test scrolling after clicking on the "LOAD MORE" button
    When I click on the element "ProductListPage.genderFilter"
    And I click on the element "ProductListPage.clearAllButton"
    Then I expect that element "ProductListPage.hundredItems" is displayed

  @B2C2-4761 @SKIP_DESKTOP
  Scenario: Clicking Clear All Removes Facets to test scrolling after clicking on the "LOAD MORE" button
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.clearAllButton"
    And I click on the button "ProductListPage.closeButton"
    Then I expect that element "ProductListPage.hundredItems" is displayed

  @B2C2-4761 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Check that the page scrolls to top when selecting a second filter after loading more products
    Given I refresh the page
    When I click on the element "ProductListPage.sizeFilter"
    And I select the 1st option for element "ProductListPage.facetOptions"
    And I scroll to element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.genderFilter"
    And I select the 1st option for element "ProductListPage.facetOptions"
    Then I expect that element "NewsletterSignupBanner.emailField" is not within the viewport
    And I expect that element "Stylish Products" is within the viewport

  @B2C2-4761 @SKIP_DESKTOP
  Scenario: Check that the page scrolls to top when selecting a second filter after loading more products
    Given I refresh the page
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.sizeFilterMobile"
    And I click on the element "ProductListPage.mobile11"
    And I click on the button "ProductListPage.closeButton"
    And I scroll to element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.genderFilterMobile"
    And I click on the element "ProductListPage.mobileWomens"
    And I click on the button "ProductListPage.closeButton"
    Then I expect that element "NewsletterSignupBanner.emailField" is not within the viewport
    And I expect that element "Stylish Products" is within the viewport  
  
  @B2C2-4761 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Clicking Clear All Removes Facets for the next tests
    When I click on the element "ProductListPage.genderFilter"
    And I click on the element "ProductListPage.clearAllButton"
    Then I expect that element "ProductListPage.hundredItems" is displayed

  @B2C2-4761 @SKIP_DESKTOP
  Scenario: Clicking Clear All Removes Facets for the next tests
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.clearAllButton"
    And I click on the button "ProductListPage.closeButton"
    Then I expect that element "ProductListPage.hundredItems" is displayed

  @B2C2-275
  Scenario: If there are 48 results or less, 'Load More' Not Displayed
    Given I open the page "/main/framework/b2c2-275/48-products"
    Then the element "ProductListPage.loadMore" is not displayed

  @B2C2-275
  Scenario: If there are more than 48 results, 'Load More' Displayed
    Given I open the page "/main/framework/b2c2-275/100-products"
    Then the element "ProductListPage.loadMore" is displayed
  
  @B2C2-2177 @SKIP_MOBILE @SKIP_TABLET @FACETS 
  Scenario: Multiple Facets Stay Selected After Clicking LOAD MORE - DESKTOP
    Given I open the page "/main/framework/b2c2-275/100-products"
    And I click on the element "ProductListPage.sizeFilter"
    And I select the 1st option for element "ProductListPage.facetOptions"
    And I select the 2nd option for element "ProductListPage.facetOptions"
    When I scroll to element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.loadMore"
    And I refresh the page 
    Then I expect that element "ProductListPage.sizeFilter" contains the text "2"

  @B2C2-3634 @SKIP_DESKTOP
  Scenario: Filter Drawer Stays Open After Selection - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-275/100-products"
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.sizeFilterMobile"
    And I select the 1st option for element "ProductListPage.facetOptions"
    And I pause for 2000ms
    Then the element "ProductListPage.filtersDrawer" is displayed

  @B2C2-3634 @SKIP_DESKTOP
  Scenario: Clear State for Next Test
    When I select the 1st option for element "ProductListPage.facetOptions"

  @B2C2-2177 @SKIP_DESKTOP @FACETS 
  Scenario: Multiple Facets Stay Selected After Clicking LOAD MORE - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-275/100-products"
    And I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.sizeFilterMobile"
    And I select the 1st option for element "ProductListPage.facetOptions"
    And I select the 2nd option for element "ProductListPage.facetOptions"
    And I refresh the page
    When I click on the element "ProductListPage.loadMore"
    And I refresh the page
    And I scroll to element "ProductListPage.refineBy" 
    And I pause for 2000ms
    And I click on the element "ProductListPage.refineBy" 
    Then I expect that element "ProductListPage.sizeFilterMobile" contains the text "2"

  @B2C2-275
  Scenario: If I click on 'Load More', More Results Displayed
    Given I open the page "/main/framework/b2c2-275/100-products"
    And I scroll to element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.loadMore"
    And I scroll to element "ProductListPage.loadMore"
    Then I expect that element "ProductListPage.loadedMoreItems" is displayed

  @B2C2-275
  Scenario: If I click on the last 'Load More', All Items Displayed so 'Load More' No Longer Displayed
    Given I refresh the page
    When I click on the element "ProductListPage.loadMore"
    Then the element "ProductListPage.loadMore" is not displayed

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_MOBILE @SKIP_TABLET @FACETS
  Scenario: All Filters Displayed - DESKTOP
    Given I open the page "/main/framework/b2c2-300/3-products"
    Then the element "ProductListPage.productTypeFilter" is displayed
    And the element "ProductListPage.colourFilter" is displayed
    Then the element "ProductListPage.sizeFilter" is displayed
    And the element "ProductListPage.genderFilter" is displayed

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_DESKTOP @FACETS
  Scenario: All Filters Displayed - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    When I click on the element "ProductListPage.refineBy"
    Then the element "ProductListPage.productTypeFilterMobile" is displayed
    And the element "ProductListPage.colourFilterMobile" is displayed
    And the element "ProductListPage.sizeFilterMobile" is displayed
    And the element "ProductListPage.genderFilterMobile" is displayed

  #--
  # TO-DO: ADD MAVERICK TESTS FOR ALL FACETS AFTER BUG FIXED, AS DETAILED IN B2C2-3539
  # https://supergroupbt.atlassian.net/browse/B2C2-3539
  #--
  # @B2C2-300
  # Scenario: Product Type Facet Order Determined By Maverick 
  #   Given I open the page "/main/framework/b2c2-300/3-products"
  #   When I click on the element "ProductListPage.productTypeFilter"
  #   Then the element "ProductListPage.facetOptionsContainer" is displayed
    # And I expect that the values returned by "ProductListPage.facetOptionsList" to be in the following order
    # """
    # BOMBER JACKET
    # CASUAL JACKET
    # DENIM JACKET
  # ---

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_MOBILE @SKIP_TABLET @FACETS
  Scenario Outline: Selectable Facets Should Always Provide At Least One Result - DESKTOP
    Given I click on the element <filterName>
    When I select the <index> option for element "ProductListPage.facetOptions"
    Then I expect that element <noOfItems> is displayed
    When I select the <index> option for element "ProductListPage.facetOptions"
    Then I expect that element "ProductListPage.threeItems" is displayed
    And I click on the element <filterName>

    Examples:
      | filterName                          | index | noOfItems                  |
      | "ProductListPage.productTypeFilter" | 1st   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilter" | 2nd   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilter" | 3rd   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilter"      | 1st   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilter"      | 2nd   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilter"      | 3rd   | "ProductListPage.oneItem"  |
      | "ProductListPage.sizeFilter"        | 1st   | "ProductListPage.twoItems" |
      | "ProductListPage.sizeFilter"        | 2nd   | "ProductListPage.oneItem"  |
      | "ProductListPage.genderFilter"      | 1st   | "ProductListPage.twoItems" |
      | "ProductListPage.genderFilter"      | 2nd   | "ProductListPage.oneItem"  |


  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_DESKTOP @FACETS
  Scenario Outline: Selectable Facets Should Always Provide At Least One Result - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    And I click on the element "ProductListPage.refineBy"
    When I click on the element <filterName>
    And I click on the element <facet>
    Then I expect that element <noOfItems> is displayed
   
    Examples:
      | filterName                                  | facet                             | noOfItems                  |
      | "ProductListPage.productTypeFilterMobile"   | "CASUAL JACKET"                   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilterMobile"   | "DENIM JACKET"                    | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilterMobile"   | "BOMBER JACKET"                   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilterMobile"        | "DARK GREEN"                      | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilterMobile"        | "DARK BLUE"                       | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilterMobile"        | "DARK RED"                        | "ProductListPage.oneItem"  |
      | "ProductListPage.sizeFilterMobile"          | "11"                              | "ProductListPage.twoItems" |
      | "ProductListPage.sizeFilterMobile"          | "14"                              | "ProductListPage.oneItem"  |
      | "ProductListPage.genderFilterMobile"        | "WOMEN"                           | "ProductListPage.twoItems" |
      | "ProductListPage.genderFilterMobile"        | "ProductListPage.mensFacetOption" | "ProductListPage.oneItem"  |

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_MOBILE @SKIP_TABLET @FACETS
  Scenario Outline: Facet Selection Filters Out Products - DESKTOP
    Given I open the page "/main/framework/b2c2-300/3-products"
    And I expect that element "ProductListPage.threeItems" is displayed
    When I click on the element <filterName>
    And I select the 2nd option for element "ProductListPage.facetOptions"
    Then I expect that element "ProductListPage.oneItem" is displayed
    And I expect that element "GREEN HOODIE" is <greenHoodieStatus>
    And I expect that element "BLUE HOODIE" is <blueHoodieStatus>
    And I expect that element "BLACK HOODIE" is <blackHoodieStatus>

    Examples:
      | filterName                          | greenHoodieStatus | blueHoodieStatus | blackHoodieStatus |
      | "ProductListPage.genderFilter"      | not displayed     | displayed        | not displayed     |
      | "ProductListPage.colourFilter"      | displayed         | not displayed    | not displayed     |
      | "ProductListPage.sizeFilter"        | not displayed     | displayed        | not displayed     |
      | "ProductListPage.productTypeFilter" | not displayed     | not displayed    | displayed         |


  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_DESKTOP @FACETS
  Scenario Outline: Facet Selection Filters Out Products - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    And I expect that element "ProductListPage.threeItems" is displayed
    When I click on the element "ProductListPage.refineBy"
    And I click on the element <filterName>
    And I click on the element <facetName>
    Then I expect that element "ProductListPage.oneItem" is displayed
    And I expect that element "GREEN HOODIE" is <greenHoodieStatus>
    And I expect that element "BLUE HOODIE" is <blueHoodieStatus>
    And I expect that element "BLACK HOODIE" is <blackHoodieStatus>

    Examples:
      | filterName                                    | facetName                         | greenHoodieStatus | blueHoodieStatus | blackHoodieStatus |
      | "ProductListPage.genderFilterMobile"          | "ProductListPage.mensFacetOption" | not displayed     | displayed        | not displayed     |
      | "ProductListPage.colourFilterMobile"          | "DARK GREEN"                      | displayed         | not displayed    | not displayed     |
      | "ProductListPage.sizeFilterMobile"            | "14"                              | not displayed     | displayed        | not displayed     |
      | "ProductListPage.productTypeFilterMobile"     | "CASUAL JACKET"                   | displayed         | not displayed    | not displayed     |

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_MOBILE @SKIP_TABLET @FACETS
  Scenario: Set-up scenario; Select an items which will disable some of the other facet options - DESKTOP
    Given I open the page "/main/framework/b2c2-300/3-products"
    And I click on the element "ProductListPage.productTypeFilter"
    And I click on the element "CASUAL JACKET"

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_MOBILE @SKIP_TABLET @FACETS
  Scenario Outline: Facet Selection Filters are Greyed Out If Not Applicable to Current Selection - DESKTOP
    Given I click on the element <filterName>
    Then I expect that the 1st element "ProductListPage.facetOptions" is <elementOneStatus>
    And I expect that the 2nd element "ProductListPage.facetOptions" is <elementTwoStatus>
    And I select the <enabled> option for element "ProductListPage.facetOptions"

    Examples:
      | filterName                     | elementOneStatus | elementTwoStatus | enabled |
      | "ProductListPage.colourFilter" | not enabled      | enabled          | 2nd     |
      | "ProductListPage.sizeFilter"   | enabled          | not enabled      | 1st     |
      | "ProductListPage.genderFilter" | enabled          | not enabled      | 1st     |

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_DESKTOP @FACETS @TEST
  Scenario: Set-up scenario; Select an items which will disable some of the other facet options - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    When I click on the element "ProductListPage.refineBy"
    And I click on the element "ProductListPage.productTypeFilterMobile"
    And I click on the element "CASUAL JACKET"

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @SKIP_DESKTOP @FACETS @TEST
  Scenario Outline: Facet Selection Filters are Greyed Out If Not Applicable to Current Selection - MOBILE & TABLET
    Given I click on the element <filterName>
    Then I expect that element <elementOne> is not enabled
    And I expect that element <elementTwo> is enabled

    Examples:
      | filterName                            | elementOne                        | elementTwo                         | 
      | "ProductListPage.colourFilterMobile"  | "ProductListPage.mobileDarkRed"   | "ProductListPage.mobileDarkGreen"  | 
      | "ProductListPage.genderFilterMobile"  | "ProductListPage.mobileMens"      | "ProductListPage.mobileWomens"     | 
      | "ProductListPage.sizeFilterMobile"    | "ProductListPage.mobile14"        | "ProductListPage.mobile11"         | 

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS @DEFECT
  Scenario: Clicking Clear All Removes Facets and Shows All Results 
    Given I click on the element "CLEAR ALL"
    Then I expect that element "ProductListPage.threeItems" is displayed

    @SKIP_DESKTOP @SKIP_TABLET @B2C2-4987
    Scenario: Search bar opens if you have scrolled half way though the PLP results
      Given I open the page "/main/framework/b2c2-275/100-products"
      And I scroll to element "Load more"
      And I scroll to element "This is the description"
      And I click on the element "Navigation.searchIcon"
      Then I expect that element "NavigationDrawer.searchField" is displayed


  @SKIP_MOBILE @SKIP_TABLET @B2C2-4762
  Scenario: On the PLP, hovering over the main nav should list categories for user to select from
    Given I open the page "/main/framework/b2c2-275/100-products"
    And I scroll to element "Load more"
    And I scroll to element "This is the description"
    And I move to "Mens"
    Then I expect that element "Stylish hoodies" is displayed
    And I expect that element "Stylish trainers" is displayed

  # Scenario below needs to be removed once the above scenario defect is resolved
  # Used only to reset the state for following tests
  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS
  Scenario: Reset State [temporary]
    Given I open the page "/main/framework/b2c2-300/3-products"

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Clicking 'X' on any Facet Type, Removes It and Updates the Page with Results - DESKTOP
    Given I click on the element <filterName>
    And I select the <index> option for element "ProductListPage.facetOptions"
    And I expect that element <filteredNoItems> is displayed
    And I refresh the page
    When I click on the element "ProductListPage.removeFacet"
    Then I expect that element "ProductListPage.threeItems" is displayed

    Examples:
      | filterName                          | index | filteredNoItems            |
      | "ProductListPage.colourFilter"      | 1st   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilter"      | 2nd   | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilter"      | 3rd   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilter" | 1st   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilter" | 2nd   | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilter" | 3rd   | "ProductListPage.oneItem"  |
      | "ProductListPage.sizeFilter"        | 1st   | "ProductListPage.twoItems" |
      | "ProductListPage.sizeFilter"        | 2nd   | "ProductListPage.oneItem"  |

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS @SKIP_DESKTOP
  Scenario Outline: Clicking 'X' on any Facet Type, Removes It and Updates the Page with Results - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    And I click on the element "ProductListPage.refineBy"
    And I click on the element <filterName>
    And I click on the element <removeFacet>
    And I expect that element <filteredNoItems> is displayed
    When I click on the element <removeFacet>
    Then I expect that element "ProductListPage.threeItems" is displayed

    Examples:
      | filterName                                 | removeFacet     | filteredNoItems            |
      | "ProductListPage.colourFilterMobile"       | "DARK RED"      | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilterMobile"  | "CASUAL JACKET" | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilterMobile"       | "DARK GREEN"    | "ProductListPage.oneItem"  |
      | "ProductListPage.sizeFilterMobile"         | "14"            | "ProductListPage.oneItem"  |
      | "ProductListPage.colourFilterMobile"       | "DARK BLUE"     | "ProductListPage.oneItem"  |
      | "ProductListPage.productTypeFilterMobile"  | "DENIM JACKET"  | "ProductListPage.oneItem"  |
      | "ProductListPage.sizeFilterMobile"         | "11"            | "ProductListPage.twoItems" |
      | "ProductListPage.productTypeFilterMobile"  | "BOMBER JACKET" | "ProductListPage.oneItem"  |



  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Can View Number of Facets Selected - DESKTOP
    Given I click on the element <filterName>
    When I select the <index> option for element "ProductListPage.facetOptions"
    Then I expect that element <filterName> contains the text <noOfFacetsSelected>
    And I click on the element <filterName>

    Examples:
      | filterName                          | index | noOfFacetsSelected |
      | "ProductListPage.productTypeFilter" | 1st   | "1"                |
      | "ProductListPage.productTypeFilter" | 2nd   | "2"                |
      | "ProductListPage.productTypeFilter" | 3rd   | "3"                |
      | "ProductListPage.colourFilter"      | 1st   | "1"                |
      | "ProductListPage.colourFilter"      | 2nd   | "2"                |
      | "ProductListPage.colourFilter"      | 3rd   | "3"                |
      | "ProductListPage.sizeFilter"        | 1st   | "1"                |
      | "ProductListPage.sizeFilter"        | 2nd   | "2"                |
      | "ProductListPage.genderFilter"      | 1st   | "1"                |
      | "ProductListPage.genderFilter"      | 2nd   | "2"                |

  @B2C2-277 @B2C2-278 @B2C2-279 @B2C2-280 @FACETS @SKIP_DESKTOP
  Scenario Outline: Can View Number of Facets Selected - MOBILE & TABLET
    Given I click on the element <filterName>
    And I pause for 2000ms
    When I click on the element <facetName>
    And I click on the element <filterName>
    Then I expect that element <filterName> contains the text <noOfFacetsSelected>


    Examples:
      | filterName                                 | facetName                         | noOfFacetsSelected |
      | "ProductListPage.colourFilterMobile"       | "DARK RED"                        | "1"                |
      | "ProductListPage.colourFilterMobile"       | "DARK GREEN"                      | "2"                | 
      | "ProductListPage.colourFilterMobile"       | "DARK BLUE"                       | "3"                |
      | "ProductListPage.productTypeFilterMobile"  | "BOMBER JACKET"                   | "1"                |
      | "ProductListPage.productTypeFilterMobile"  | "DENIM JACKET"                    | "2"                |
      | "ProductListPage.productTypeFilterMobile"  | "CASUAL JACKET"                   | "3"                |
      | "ProductListPage.sizeFilterMobile"         | "14"                              | "1"                |
      | "ProductListPage.sizeFilterMobile"         | "11"                              | "2"                |
      | "ProductListPage.genderFilterMobile"       | "ProductListPage.mensFacetOption" | "1"                |
      | "ProductListPage.genderFilterMobile"       | "WOMEN"                           | "2"                |

  @B2C2-277 @B2C2-3327 @FACETS @SKIP_MOBILE @SKIP_TABLET
  Scenario: If Facets Applied So No Results, Shows Error Message - DESKTOP
    Given I open the page "/main/framework/b2c2-300/3-products"
    When I click on the element "ProductListPage.colourFilter"
    And I click on the element "DARK GREEN"
    And I click on the element "DARK RED"
    And I click on the element "ProductListPage.productTypeFilter"
    And I click on the element "BOMBER JACKET"
    And I click on the element "ProductListPage.colourFilter"
    And I click on the element "DARK RED"
    Then the element "ProductListPage.noResults" is displayed
    And I expect that element "ProductListPage.zeroItems" is displayed

  @B2C2-277 @B2C2-3327 @FACETS @SKIP_DESKTOP
  Scenario: If Facets Applied So No Results, Shows Error Message - MOBILE & TABLET
    Given I open the page "/main/framework/b2c2-300/3-products"
    When I click on the element "ProductListPage.refineBy"
    When I click on the element "ProductListPage.colourFilterMobile"
    And I click on the element "DARK GREEN"
    And I click on the element "DARK RED"
    And I click on the element "ProductListPage.productTypeFilterMobile"
    And I click on the element "BOMBER JACKET"
    And I click on the element "ProductListPage.colourFilterMobile"
    And I click on the element "DARK RED"
    Then the element "ProductListPage.noResults" is displayed
    And I expect that element "ProductListPage.zeroItems" is displayed