@CHECKOUT
@CONFIRMATION
@REGRESSION
#  Only fails when run via the pipeline

Feature: Checkout Confirmation

  #
  # Display of Staff Discount on Checkout Confirmation (Order Summary)
  # https://supergroupbt.atlassian.net/browse/B2C2-537
  #

  Rule: I WANT to be shown the staff discounted prices in the Confirmation Page Order Summary

    @B2C2-537 @B2C2-2608 @TEST
    Scenario: Setup environment with CheckoutOrderSummary
      Given I seed config using the file "CheckoutOrderConfirmation_StaffDiscount"
      When I open the siteId "staffdiscount-confir"
      And I seed using the file "CheckoutOrderConfirmation_StaffDiscount" and add one of each item into my "basket" local storage

    @B2C2-2608 @TEST
    Scenario: Should have the dedicated website helpline displayed
      Given I am on the page "/checkout"
      Then I wait on element "CheckoutAddress.helplineNumber" to be displayed

    @B2C2-537
    Scenario: For a 'GUEST' user, go to checkout and fill in delivery details
      Given I am on the page "/checkout"
      Then I expect the url to contain "/checkout"
      When I set "Guest" to the inputfield "CheckoutAddress.firstNameTextbox"
      And I set "lastName" to the inputfield "CheckoutAddress.lastNameTextbox"
      And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
      And I set "projecttesting+1234@gmail.com" to the inputfield "CheckoutAddress.emailAddressTextbox"
      And I set "projecttesting+1234@gmail.com" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
      And I set "address1" to the inputfield "CheckoutAddress.address1Textbox"
      And I set "city" to the inputfield "CheckoutAddress.cityTextbox"
      And I set "GL51 9NW" to the inputfield "CheckoutAddress.postcodeTextbox"

    @B2C2-537
    Scenario: For a 'GUEST' user, fill in card payment details
      When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
      And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I press "4444 3333 2222 1111"
      And I switch back to the parent frame
      And I pause for 2000ms
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "0330" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "737" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"

    @B2C2-537
    Scenario: For a 'GUEST' user, place the order and check the order confirmation page appears
      When I click on the button "CheckoutPayment.buyNowButton"
      Then the element "Confirmation.orderConfirmationText" is displayed
      And the element "Confirmation.orderReferenceText" is displayed
      And the element "Confirmation.orderNumber" is displayed

    @B2C2-537
    Scenario: For a 'Guest' user within the 'CONFIRMATION PAGE', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      Then I expect that the values returned by "Confirmation.informationInConfirmationOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-537
    Scenario: Reseed the data and add items to the basket (the basket items will be promoted when the 'STAFF' user logs in)
      Given I seed using the file "CheckoutOrderConfirmation_StaffDiscount" and add one of each item into my "basket" local storage

    @B2C2-537
    Scenario: Log in with 'STAFF' user and open the basket (basket should be promoted from previous test)
      Given I open the page "/login"
      And I set "confirmationpage.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithDiscount"

    @B2C2-537
    Scenario: For a 'STAFF' user, go to checkout and fill in card payment details
      Given I open the page "/checkout"
      When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
      And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I press "4444 3333 2222 1111"
      And I switch back to the parent frame
      And I pause for 3000ms
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "0330" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "737" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"

    @B2C2-537
    Scenario: For a 'STAFF' user, place the order and check the order confirmation page appears
      When I click on the button "CheckoutPayment.buyNowButton"
      And I click on the element "CheckoutStaffDiscountTerms.checkbox"
      When I click on the element "CheckoutStaffDiscountTerms.proceedButton"
      Then the element "Confirmation.orderConfirmationText" is displayed
      And the element "Confirmation.orderReferenceText" is displayed
      And the element "Confirmation.orderNumber" is displayed

    @B2C2-537
    Scenario: For a 'STAFF' user within the 'CONFIRMATION PAGE'', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      When I scroll to element "Confirmation.confirmationOrderSummary"
      Then I expect that the values returned by "Confirmation.informationInConfirmationOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Staff Discount
        -£161.00
        Total
        £173.00
        """

    @B2C2-537
    Scenario: Go to the order history page for the logged in user and Click on the first order displayed
      Given I am on the page "/my-account/order-history"
      And I click on the element "OrderHistory.firstOrder"
      Then I expect the url to contain "/my-account/order-history/order-details/"

    @B2C2-537
    Scenario: For a 'STAFF' user within the 'ORDER HISTORY PAGE'', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      When I scroll to element "OrderHistory.orderHistoryOrderSummary"
      Then I expect that the values returned by "OrderHistory.informationInOrderHistoryOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Staff Discount
        -£161.00
        Total
        £173.00
        """

    @B2C2-537
    Scenario: Log out the 'STAFF' user
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"

    @B2C2-537
    Scenario: Reseed the data and add items to the basket (the basket items will be promoted when the 'NON-STAFF' user logs in)
      Given I seed using the file "CheckoutOrderConfirmation_StaffDiscount" and add one of each item into my "basket" local storage

    @B2C2-537
    Scenario: Log in with 'NON-STAFF' user and open the basket (basket should be promoted from previous test)
      Given I open the page "/login"
      And I set "confirmationpage.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithoutStaffDiscount"

    @B2C2-537
    Scenario: For a 'NON-STAFF' user, go to checkout and fill in card payment details
      Given I am on the page "/checkout"
      When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
      And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I press "4444 3333 2222 1111"
      And I switch back to the parent frame
      And I pause for 3000ms
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "0330" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
      And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
      And I set "737" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
      And I switch back to the parent frame
      And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"

    @B2C2-537
    Scenario: For a 'NON-STAFF' user, place the order and check the order confirmation page appears
      When I click on the button "CheckoutPayment.buyNowButton"
      Then the element "Confirmation.orderConfirmationText" is displayed
      And the element "Confirmation.orderReferenceText" is displayed
      And the element "Confirmation.orderNumber" is displayed

    @B2C2-537
    Scenario: For a 'NON-STAFF' user within the 'CONFIRMATION PAGE', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      Then I expect that the values returned by "Confirmation.informationInConfirmationOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-537
    Scenario: Go to the order history page for the logged in user and Click on the first order displayed
      Given I am on the page "/my-account/order-history"
      And I click on the element "OrderHistory.firstOrder"
      Then I expect the url to contain "/my-account/order-history/order-details/"

    @B2C2-537
    Scenario: For a 'NON STAFF' user within the 'ORDER HISTORY PAGE'', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      When I scroll to element "OrderHistory.orderHistoryOrderSummary"
      Then I expect that the values returned by "OrderHistory.informationInOrderHistoryOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-537
    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

    @B2C2-3993
    Scenario: User is not presented with confirmation but is scrolled to first missing fields
      Given I am on the page "/"
      And I seed using the file "CheckoutProductScrollToMissingFields" and add one of each item into my "basket" local storage
      And I am on the page "/checkout"
      And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
      When I click on the button "CheckoutPayment.buyNowButton"
      Then I expect that element "CheckoutAddress.firstNameTextbox" is within the viewport