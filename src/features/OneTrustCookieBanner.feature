@ONETRUSTCOOKIEBANNER
@REGRESSION

Feature: One trust cookie banner

  @B2C2-3603
  Scenario: Customer is not shown cookie banner by default
    Given I open the page "/"
    Then I expect that element "OneTrustCookieBanner.cookieBanner" is not displayed

  @B2C2-3603
  Scenario: Customer is shown cookie banner when it is enabled
    Given I seed config using the file "OneTrustCookieBannerEnabled"
    When I open the siteId "b2c23603"
    Then I expect that element "OneTrustCookieBanner.cookieBanner" is displayed