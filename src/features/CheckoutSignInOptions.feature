@CHECKOUT
@CHECKOUTSIGNINOPTIONS
@REGRESSION

Feature: Checkout - Sign In Options

  @B2C2-175 @B2C2-2608
  Scenario: Customer is presented with checkout as guest option
    Given I am on the page "/"
    And I seed using the file "CheckoutProduct" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    When I click on the element "CheckoutLogin.continueAsGuest"
    Then I expect that element "CheckoutProduct" is displayed

  @B2C2-2608
  Scenario: Should have the dedicated website helpline displayed
    Given I am on the page "/login"
    Then I wait on element "Login.helplineNumber" to be displayed

  # @B2C2-175
    # TODO Paypal button not loading on sauce labs
    #TODO enable when saucelabs tunnel issue is resolved
  #Scenario: Customer is presented with checkout with paypal option
    #Then I expect that element "CheckoutLogin.paypalCheckoutBtn" is displayed

  @B2C2-175
  Scenario: Customer is presented with forgot password option
    Given I am on the page "/checkout/login"
    When I click on the element "Login.forgottenPassword"
    Then I expect that element "Forgotten your Password?" is displayed

  @B2C2-175
  Scenario: Customer is presented with errors when form is not completed
    Given I am on the page "/checkout/login"
    And I set "abc" to the inputfield "Login.emailAddress"
    And I set " " to the inputfield "Login.password"
    When I click on the element "Login.signInButton"
    Then I expect that element "Login.invalidEmailWarning" is displayed
    And I expect that element "Login.valueRequiredWarning" is displayed

  @B2C2-175
  Scenario: Customer is presented with login not recognised error
    Given I am on the page "/checkout/login"
    And I set "abc@abc.com" to the inputfield "Login.emailAddress"
    And I set "abc" to the inputfield "Login.password"
    When I click on the element "Login.signInButton"
    Then I expect that element "Login.loginNotRecognisedWarning" is displayed

  @B2C2-175
  Scenario: Customer is presented with register option
    Given I click on the element "Login.register"
    Then I expect that element "Registration.emailAddressTextbox" is displayed
    And I expect the url to contain "/register"

  @B2C2-175
  Scenario: Customer is presented with checkout after logging in
    Given I am on the page "/"
    And I seed using the file "CheckoutProduct" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "checkoutproduct@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    When I click on the element "Login.signInButton"
    Then I expect that element "CheckoutProduct" is displayed

  @B2C2-505
  Scenario: Customer is redirected to checkout page
    Given I am on the page "/checkout/login"
    Then I expect that element "CheckoutProduct" is displayed