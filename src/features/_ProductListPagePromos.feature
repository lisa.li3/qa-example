@PLP
@PLP_PROMOS
@REGRESSION

Feature: Product List Page (PLP) - Promos
  As a user of the Product List Page, I can see and order promotions

    #
    # Promo slugs
    # https://supergroupbt.atlassian.net/browse/B2C2-540
    # https://supergroupbt.atlassian.net/browse/B2C2-2501
    #
  Rule: I WANT to be able to browse & purchase products that are part of a promotions

    @B2C2-540 @B2C2-2501
    Scenario: Prepare environment
      Given I open the page "/"
      And I spawn the site "plp-promos" from the base config "default"
      And I seed using the file "ProductListPage_PromosWithRatings"
      And I am on the page "/main/framework/promos-with-ratings"

    # AC1
    # Given a customer does not have a Staff Discount linked to their account
    # and the customer is browsing a PLP
    # and one or more products on the PLP are included in an active promotion
    # when the customer views the the product on the PLP
    # and the QATB (quick add to bag) is not being shown on the product image
    # then a “promo slug” shall be displayed that shows the product is included within the promotion.
    Scenario Outline: The product <productName> should <condition> contain <promotion>
      When I scroll to element "<productName>"
      Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<promotion>"

      @B2C2-540
      Examples:
      | productName | condition | promotion |
      | Product 1   | not       | 3 FOR £20 |
      | Product 2   | also      | 3 FOR £20 |
      | Product 3   | also      | 3 FOR £20 |
      | Product 4   | not       | 3 FOR £20 |
      | Product 5   | not       | 3 FOR £20 |
      | Product 6   | also      | 3 FOR £20 |
      | Product 7   | not       | 3 FOR £20 |
      | Product 8   | also      | 3 FOR £20 |

      @B2C2-2501
      Examples:
      | productName | condition | promotion |
      | Product 1   | not       | 3 FOR 2   |
      | Product 2   | not       | 3 FOR 2   |
      | Product 3   | not       | 3 FOR 2   |
      | Product 4   | also      | 3 FOR 2   |
      | Product 5   | also      | 3 FOR 2   |
      | Product 6   | not       | 3 FOR 2   |
      | Product 7   | also      | 3 FOR 2   |
      | Product 8   | not       | 3 FOR 2   |

# ToDo Paul will have a look at the following to see why the move to fails
#    @SKIP_TABLET @SKIP_MOBILE
#    Scenario Outline: The promo slugs <promotion> should not appear on <productName> when QATB is displayed
#      Given I move to "<productName>"
#      Then I expect the element in "ProductListPage.productList" containing text "<productName>" to not contain text "<promotion>"
#
#      @B2C2-540
#      Examples:
#        | productName | promotion |
#        | Product 2   | 3 FOR £20 |
#        | Product 3   | 3 FOR £20 |
#        | Product 6   | 3 FOR £20 |
#        | Product 8   | 3 FOR £20 |
#
#      @B2C2-2501
#      Examples:
#        | productName | promotion |
#        | Product 4   | 3 FOR £20 |
#        | Product 5   | 3 FOR £20 |
#        | Product 7   | 3 FOR £20 |

    @B2C2-540 @B2C2-2501
    Scenario: Prepare for Scenario Outline by logging in and navigating to PLP
      Given I am on the page "/login"
      And I set "plp.staffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "User WithDiscount"
      And I am on the page "/main/framework/promos-with-ratings"

    Scenario Outline: The promo slug <promotion> should not appear on <productName> when user with staff discount is logged in
      Then I expect the element in "ProductListPage.productList" containing text "<productName>" to not contain text "<promotion>"

      @B2C2-540
      Examples:
      | productName | promotion |
      | Product 2   | 3 FOR £20 |
      | Product 3   | 3 FOR £20 |
      | Product 6   | 3 FOR £20 |
      | Product 8   | 3 FOR £20 |

      @B2C2-2501
      Examples:
      | productName | promotion |
      | Product 4   | 3 FOR 2   |
      | Product 5   | 3 FOR 2   |
      | Product 7   | 3 FOR 2   |

    # AC3
    # Given a customer is browsing the project site
    # when the customer selects a Promo Slug for a promotion either from the PLP, PDP or mini-bag
    # then the customer is directed to the merchandised PLP for that promotion.
    @B2C2-540 @B2C2-2501
    Scenario: Prepare for Scenario Outline by logging out and navigating to PLP
      Given I am on the page "/my-account"
      And I click on the button "MyAccount.logOutButton"
      And I pause for 1000ms
      And I am on the page "/main/framework/promos-with-ratings"

#    TODO: Test is blocked by bug: https://supergroupbt.atlassian.net/browse/B2C2-3484
#    @B2C2-540 @B2C2-2501 @SKIP_DESKTOP
#    Scenario Outline: The promo slug <promotion> should navigate to the correct promotion page
#      Given I am on the page "/main/framework/promos-with-ratings"
#      When I click on the element "<promotion>"
#      Then I expect the url to contain "/promotions/"
#      And I expect that element "ProductListPage.firstHeading" matches the text "<promotion>"
#      And I expect that element "ProductListPage.productList" is not empty

#      Examples:
#        | promotion |
#        | 3 FOR £20 |
#        | 3 FOR 2   |



#
# Display PROMO Slugs on Search PLP page
# https://supergroupbt.atlassian.net/browse/B2C2-2259
#
  Rule: I WANT PROMO slugs to be displayed on selected products on a SEARCH PLP Page

    @B2C2-2259
    Scenario: Seed a New site and a PLP with promos with have defined product Ids
      Given I seed config using the file "ProductListPage_SearchPromoPLPSite"
      And I open the siteId "searchplp-promo"
      And I seed using the file "ProductListPage_SearchPromoPLP"
      And I open the page "/search/promo666p1,promo666p2,promo666p3,promo666p4,promo666p5"

    @B2C2-2259
    Scenario Outline: The promo slug <promotion> should appear on <productName> on the SEARCH PLP page
      Then I expect the element in "ProductListPage.productList" containing text "<productName>" to <condition> contain text "<promotion>"

      @B2C2-2259
      Examples:
        | productName | condition | promotion |
        | Product 1   | also      | 4 FOR 2   |
        | Product 2   | also      | 4 FOR £20 |
        | Product 3   | not       | 4 FOR £20 |
        | Product 3   | not       | 4 FOR 2   |
        | Product 4   | also      | 4 FOR £20 |
        | Product 5   | also      | 5 FOR 3   |

    @B2C2-2259 @SKIP_MOBILE @SKIP_TABLET
    Scenario: The promo slug should not appear on a product when it is hovered over on the SEARCH PLP page
      When I move to "Product 5"
      Then I expect that element "5 FOR 3" is not displayed
      Then I expect that element "ProductListPage.quickAddToBagButton" is displayed
      And I expect that element "ProductListPage.quickAddToBagSizeDropdown" is displayed

    @B2C2-2259
    Scenario: Clicking on a promo product on the search results PLP, going to the PDP and clicking on the promo slug takes the user to the respective PROMOTION PLP page
      Given I move to "Product 2"
      When I click on the element "Product 2"
      Then I expect the url to contain "/searchpromo_plp/details/promo666p2/"
      When I click on the element "4 FOR £20"
      Then I expect the url to contain "/promotions/"
      And I expect that element "4 FOR £20" is displayed
      And I expect that element "Product 2" is displayed
      And I expect that element "Product 4" is displayed

  #
  # Display of Staff Discount on PLP
  # https://supergroupbt.atlassian.net/browse/B2C2-537
  #
  Rule: I WANT to be shown the staff discounted prices on a CATEGORY PLP and a Search PLP

    @B2C2-537
    Scenario: Seed products and a staff discount user on a different site
      Given I seed config using the file "ProductListPage_StaffDiscountWithProducts"
      And I open the siteId "staffdiscount-plp"
      And I seed using the file "ProductListPage_StaffDiscountUserAndProducts"
      And  I open the page "/main/framework/b2c2_537_plp_staffdiscount_1"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed
