@NAVIGATION
@REGRESSION

Feature: Main navigation
  As a user
  I want to be able to access key site features from the homepage

  @B2C2-57 @B2C2-64 @B2C2-261 @B2C2-262 @B2C2-263 @B2C2-2903 
  Scenario:Seeding the environment
    Given I open the page "/"
    And I seed using the file "Navigation"
    And I refresh the page

  @B2C2-57 @B2C2-64
  # TODO: SPLIT
  Scenario: User can scroll to the footer, menu hides when scrolled down and reappears when scrolled up
    Given I am on the page "/"
    When I scroll to element "Footer.footerScroll"
    And I pause for 5000ms
    Then I expect that element "Navigation.basketButton" is not within the viewport
    When I scroll to element "Navigation.projectLogo"
    Then I expect that element "Navigation.basketButton" is within the viewport

  @SKIP_TABLET @SKIP_MOBILE @B2C2-57
  Scenario: Key navigation elements are displayed for desktop
    Given I am on the page "/"
    Then the element "Navigation.projectLogo" is displayed
    And the element "Navigation.searchBar" is displayed
    And the element "Navigation.accountButton" is displayed
    And the element "Navigation.wishlistButton" is displayed
    And the element "Navigation.basketButton" is displayed

  @SKIP_DESKTOP @B2C2-59
	Scenario: Mobile burger navigation
        Given I am on the page "/"
		Then the element "Navigation.projectLogo" is displayed
		And the element "Navigation.searchIcon" is displayed
		And the element "Navigation.basketButton" is displayed
		When I click on the button "Navigation.burgerButton"
		Then I expect that element "NavigationDrawer.searchField" is within the viewport
		And I expect that element "NavigationDrawer.myAccount" is within the viewport
		And I expect that element "NavigationDrawer.wishList" is within the viewport
		And I expect that element "NavigationDrawer.storeLocator" is within the viewport

  @B2C2-60
  Scenario: When user clicks SD logo user is redirected to homepage
    Given I open the site "/anywhere"
    When I click on the element "Navigation.projectLogo"
    Then the page url is "/"

  @B2C2-2499
  Scenario: Visiting the /help-center link we are not shown a 404
    Given I am on the page "/help-centre"
    When I switch to the child iFrame
    And I wait on element "HelpCentre.Title" for 10000ms to exist
    And I switch back to the parent frame
    #Then I expect that the title contains "Help Centre"
    And I expect the url to not contain "/404"

  @B2C2-3443 @SKIP_MOBILE @SKIP_TABLET @DEFECT
  Scenario Outline: <footerText> link in the footer should open the help center
    Given I am on the page "/"
    When I click on the element "<footerText>" within "Footer.customerServicesColumn"
    Then I expect the url to not contain "/404"
    And I expect the url to contain "/help-centre"
    #And I expect that the title contains "Help Centre"
    Examples:
      | footerText   |
      | Help & FAQ's |
      | Returns      |
      | Delivery     |

  @B2C2-3443 @SKIP_DESKTOP
  Scenario Outline: <footerText> link in the footer should open the help center (on devices)
    Given I am on the page "/"
    When I click on the element "Footer.customerServicesMobileButton"
    When I click on the element "<footerText>" within "Footer.customerServicesMobile"
    Then I expect the url to not contain "/404"
    And I expect the url to contain "/help-centre"
    #And I expect that the title contains "Help Centre"
    Examples:
      | footerText   |
      | Help & FAQ's |
      | Returns      |
      | Delivery     |

  @B2C2-61 @SKIP_MOBILE
  Scenario: Global banner is displayed and has a close icon on tablet and desktop
    Given I am on the page "/"
    Then I expect that element "FREE DELIVERY & RETURNS" is within the viewport
    When I click on the element "GlobalBanner.close"
    Then I expect that element "GlobalBanner.close" is not within the viewport

	@B2C2-61 @SKIP_TABLET @SKIP_DESKTOP
	Scenario: Global banner is displayed
      Given I am on the page "/"
      Then I expect that element "FREE DELIVERY & RETURNS" is within the viewport

  @B2C2-1382
  Scenario: Checks the country selector in the footer can be opened
    Given I am on the page "/"
    When I click on the element "Footer.siteSelectorButton"
    Then I expect that element "Footer.sitePanelClose" is within the viewport

  @B2C2-1382
  Scenario: Checks the country selector in the footer can be closed
    When I click on the element "Footer.sitePanelClose"
    Then I expect that element "Footer.siteSelectorButton" becomes displayed

  @B2C2-261 @B2C2-262 @B2C2-263
  Scenario Outline: Checks that when a short URL is used for "<type>" then the relevant page is landed on
    Given I am on the page "<url>"
    Then I expect that element "Navigation.shortURLsProductHeading" is displayed
    Then I expect that element "<sizeTextVisible>" is displayed
    Examples:
      | type       | url                                | sizeTextVisible |
      | sku        | /products/?sku=vyf81i5abc-8        | size8Label      |
      | plu        | /products/?plu=vyf81i5abc-plu      | select size     |
      | product_id | /products/?product_id=vyf81i5abc-8 | select size     |

  @B2C2-3400 @B2C2-4363
  Scenario: Setup
    Given I open the page "/" 
    And I seed using the file "BasketAndMiniBag" and add one of each item into my "basket" local storage

  @B2C2-3400 @B2C2-4363 @DEFECT
  Scenario Outline: Checks that tab shows the correct title, correctly formatted when page loaded
    Given I am on the page "/<url>"
    Then I expect that the title contains "<expectedTitle>"
    And I expect that the title does not begin with "'"
    Examples: 
      | url                      | expectedTitle                                                |
      | sdsdsdsd                 | OOPS! Sorry, we couldn't find what you were looking for ...  |
      | gobbledygook             | OOPS! Sorry, we couldn't find what you were looking for ...  |
      | category-mens-uk         | Men's                                                        | 
      | my-account/wish-list     | Wish List                                                    | 
      
  @B2C2-261 @B2C2-262 @B2C2-263
  Scenario Outline: Checks that when an invalid short URL is used for "<type>" then the 404 page is landed on
    Given I am on the page "<url>"
    When I wait on element "Navigation.pageNotFound" to be displayed
    Then I expect that element "Navigation.pageNotFound" is displayed
    Examples:
      | type       | url                       |
      | sku        | /products/?sku=000        |
      | plu        | /products/?plu=000        |
      | product_id | /products/?product_id=000 |

  @B2C2-2903 @SKIP_DESKTOP 
  Scenario: Nested Menu Options Have Cancel and Back Arrow Option Displayed
    Given I am on the page "/"
    When I click on the element "Navigation.burgerButton"
    And I click on the element "NavigationDrawer.sale"
    And I click on the element "NavigationDrawer.saleLimited"
    Then the element "NavigationDrawer.saleLimited" is not displayed
    And the element "NavigationDrawer.backDepth2" is displayed
    And the element "NavigationDrawer.closeDepth2" is displayed

  @B2C2-2902 @SKIP_DESKTOP
  Scenario: Seed a large category to test scrolling works on small devices
    Given I am on the page "/"
    And I seed using the file "Navigation_BigCategory"
    And I refresh the page
    When I click on the element "Navigation.burgerButton"
    And I click on the element "NavigationDrawer.bigCategory"
    Then I expect that element "Big Category Item 001" is within the viewport
    And I expect that element "Big Category Item 050" is not within the viewport

  @B2C2-2902 @SKIP_DESKTOP
  Scenario: Scroll to bottom to make sure scrolling works on small devices
    When I scroll to element "Big Category Item 050"
    Then I expect that element "Big Category Item 001" is not within the viewport
    And I expect that element "Big Category Item 050" is within the viewport

  @B2C2-2902 @SKIP_DESKTOP
  Scenario: Scroll back to top to make sure scrolling works on small devices
    When I scroll to element "Big Category Item 001"
    Then I expect that element "Big Category Item 001" is within the viewport
    And I expect that element "Big Category Item 050" is not within the viewport



