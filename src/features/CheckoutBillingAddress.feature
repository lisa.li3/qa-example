@CHECKOUT
@BILLING
@BILLING_ADDRESS
@REGRESSION

Feature: Checkout Billing Address

  @B2C2-193 @B2C2-2608
  Scenario: Setup environment with CheckoutBillingAddress
    Given I am on the page "/"
    And I seed using the file "CheckoutBillingAddress" and add the items into my "basket" local storage

  @B2C2-193
  Scenario: Should have the billing address checkbox preselected
    Given I open the page "/checkout"
    Then I expect that element "CheckoutBillingAddress.billingAddressSameTick" is displayed
    And I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is checked

  @B2C2-2608
  Scenario: Should have the dedicated website helpline displayed
    Given I am on the page "/checkout"
    Then I expect that element "CheckoutBillingAddress.helplineNumber" is displayed

  @B2C2-193 @SKIP_TABLET
  Scenario: Should uncheck the billing address checkbox and check that first and last name fields exist
    Given I am on the page "/checkout"
    When I scroll to element "CheckoutBillingAddress.billingAddress"
    And I click on the element "CheckoutBillingAddress.billingAddressCheckbox"
    Then I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is not checked
    And I wait on element "CheckoutBillingAddress.firstName" to be displayed
    And I wait on element "CheckoutBillingAddress.lastName" to be displayed

  @B2C2-193 @SKIP_DESKTOP @SKIP_MOBILE
  Scenario: Should uncheck the billing address checkbox and check that first and last name fields exist
    Given I am on the page "/checkout"
    When I click on the element "CheckoutBillingAddress.billingAddressSameTick"
    Then I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is not checked
    And I wait on element "CheckoutBillingAddress.firstName" to be displayed
    And I wait on element "CheckoutBillingAddress.lastName" to be displayed

  @B2C2-193
  Scenario: Should check that the correct fields are displayed
    Then I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is not checked
    And I wait on element "CheckoutBillingAddress.addressLine1" to be displayed
    And I wait on element "CheckoutBillingAddress.addressLine2" to be displayed
    And I wait on element "CheckoutBillingAddress.city" to be displayed
    And I wait on element "CheckoutBillingAddress.postcode" to be displayed
    And I wait on element "CheckoutBillingAddress.county" to be displayed

  @B2C2-193 @SKIP_TABLET
  Scenario: Should show and hide the billing address form based on user selection
    When I scroll to element "CheckoutBillingAddress.billingAddress"
    And I click on the element "CheckoutBillingAddress.billingAddressSameTick"
    Then I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is checked
    And I expect that element "CheckoutBillingAddress.firstName" becomes not displayed
    And I expect that element "CheckoutBillingAddress.lastName" becomes not displayed

  @B2C2-193 @SKIP_DESKTOP @SKIP_MOBILE
  Scenario: Should show and hide the billing address form based on user selection
    When I scroll to element "CheckoutBillingAddress.navigationMain"
    And I click on the element "CheckoutBillingAddress.billingAddressSameTick"
    Then I expect that checkbox "CheckoutBillingAddress.billingAddressCheckbox" is checked
    And I expect that element "CheckoutBillingAddress.firstName" becomes not displayed
    And I expect that element "CheckoutBillingAddress.lastName" becomes not displayed

  Rule: I WANT to be able to look up my address in the billing address section in the checkout

    @B2C2-192
    Scenario: Seed a site with ADDRESS LOOKUP ENABLED and open the site
      Given I seed config using the file "CheckoutLoqateBillingAddressLookupOn"
      And I open the siteId "billingloqate-on"

    @B2C2-192
    Scenario: Seed the product data for the GUEST user and go to checkout
      Given I seed using the file "CheckoutLoqateBillingAddressLookup" and add one of each item into my "basket" local storage
      When I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" becomes displayed

    @B2C2-192
    Scenario: Upload the 'Address-Lookup' mock files to S3
      Given I upload the "mocks/loqate/text/a.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/a.json"
      And I upload the "mocks/loqate/text/ab.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/ab.json"
      And I upload the "mocks/loqate/text/abc.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/abc.json"
      And I upload the "mocks/loqate/text/g.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/g.json"
      And I upload the "mocks/loqate/text/gl.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl.json"
      And I upload the "mocks/loqate/text/gl2.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl2.json"
      And I upload the "mocks/loqate/text/gl20.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20.json"
      And I upload the "mocks/loqate/text/gl20a.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20a.json"
      And I upload the "mocks/loqate/text/gl20aa.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa.json"
      And I upload the "mocks/loqate/text/gl20aa7.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa7.json"
      And I upload the "mocks/loqate/text/gl20aa70.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa70.json"
      And I upload the "mocks/loqate/Id/GB|RM|B|1612492.json" file to the S3 bucket "address-lookup-service" with path "Id/GB|RM|B|1612492.json"
      And I upload the "mocks/loqate/container/GB|RM|ENG|0AA-GL2.json" file to the S3 bucket "address-lookup-service" with path "GBR/Container/GB|RM|ENG|0AA-GL2.json"

    @B2C2-192
    Scenario: Check that autocomplete is switched off for the 'Loqate Delivery Address Search' field
      Given I click on the element "CheckoutAddress.billingAddressSameTick"
      Then I expect that the attribute "autocomplete" from element "CheckoutAddress.billingAddressFinderTextInput" is "off"
      And I expect that element "CheckoutAddress.billingAddressFinderTextInput" is displayed

    @B2C2-192
    Scenario:  Check that clicking the 'Buy now button' without entering anything into the address finder triggers an error on the address finder field
      When I click on the button "CheckoutPayment.buyNowButton"
      Then I expect that element "CheckoutAddress.billingAddressFinderError" is displayed

    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline: Start typing an address in the billing address finder field
      When I add "<inputLetter>" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      Then the element "CheckoutAddress.billingAddressFinderTextInput" contains the text "<typedLetters>"
      And I pause for 500ms

      Examples:
        | inputLetter | typedLetters |
        | g           | g            |
        | l           | gl           |
        | 2           | gl2          |
        | 0           | gl20         |
        | a           | gl20a        |

    @B2C2-192
    Scenario: Check the number of addresses returned in the dropdown is 10 in descending order of relevance
      Given the element "CheckoutAddress.billingManualAddressEntryButton" is displayed
      Then I expect that the values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
          """
          GL2 0AA Church Road, Gloucester - 18 Addresses
          GL2 0AB Church Road, Gloucester - 20 Addresses
          GL2 0AD Wedgwood Drive, Gloucester - 46 Addresses
          GL2 0AE Church Road, Gloucester - 7 Addresses
          GL2 0AF Allendale Close, Gloucester - 11 Addresses
          GL2 0AG College Fields, Gloucester - 24 Addresses
          GL2 0AH Church Road, Gloucester - 15 Addresses
          GL2 0AJ Church Road, Gloucester - 8 Addresses
          Longlevens Junior School, Church Road Longlevens, Gloucester, GL2 0AL
          GL2 0AN Old Cheltenham Road, Gloucester - 13 Addresses
          """

    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline:  Start typing an address in the address finder field and check that the suggested addresses update
      Given  I add "<typedValue>" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      Then the element "CheckoutAddress.billingAddressFinderTextInput" contains the text "<typedValue>"
      And I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        <firstAddressInList>
        <secondAddressInList>
        """

      Examples:
        | typedValue | firstAddressInList                                     | secondAddressInList                                     |
        | a          | A, Selborne Mansions, Selborne Mount Bradford, BD9 4NP | A, Dalemoor, Mytholmes Lane Haworth, Keighley, BD22 8EZ |
        | b          | A-B, Sneyd Lodge, 80 Olive Road London, NW2 6UL        | AB10 1AB Broad Street, Aberdeen - 6 Addresses           |
        | c          | A B C, 618 Lordship Lane London, N22 5JH               | A B C, 5 Hutton Street Boldon Colliery, NE35 9LW        |


    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline: Start typing an address in the billing address finder field
      When I add "<inputLetter>" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      Then the element "CheckoutAddress.billingAddressFinderTextInput" contains the text "<typedLetters>"
      And I pause for 500ms

      Examples:
        | inputLetter | typedLetters |
        | g           | g            |
        | l           | gl           |
        | 2           | gl2          |
        | 0           | gl20         |
        | a           | gl20a        |
        | a           | gl20aa       |


      #TODO: Below test skipped due to an issue with Loqate (address lookup flashes with correct results and then displays incorrect options): https://supergroupbt.atlassian.net/browse/B2C2-4488
    @B2C2-192 @DEFECT
    Scenario:  Select an address option with multiple variations (container address)
      When I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
      Then I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        70A, Church Road Longlevens, Gloucester, GL2 0AA
        76A, Church Road Longlevens, Gloucester, GL2 0AA
        """
      When I add "7" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      And I add "0" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      Then the element "CheckoutAddress.billingAddressFinderTextInput" contains the text "gl20aa70"
      And I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        70 Church Road Longlevens, Gloucester, GL2 0AA
        70A, Church Road Longlevens, Gloucester, GL2 0AA
        """

    @B2C2-192
    Scenario:  Use the Billing Address finder, select an address and check it is input into the form correctly and the Billing 'FIND YOUR ADDRESS' button is displayed
      When I refresh the page
      And I set "a" to the inputfield "CheckoutAddress.billingAddressFinderTextInput"
      And I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
      Then I expect that element "CheckoutAddress.billingFindYourAddressButton" is displayed
      And the element "ViewSelectBillingAddress.addressLine1" contains the text "A, Selborne Mansions"
      And the element "ViewSelectBillingAddress.addressLine2" contains the text "Selborne Mount"
      And the element "ViewSelectBillingAddress.city" contains the text "Bradford"
      And the element "ViewSelectBillingAddress.county" contains the text "West Yorkshire"
      And the element "ViewSelectBillingAddress.postcode" contains the text "BD9 4NP"

    @B2C2-192
    Scenario: Seed a site with ADDRESS LOOKUP Disabled and open the site
      Given I seed config using the file "CheckoutLoqateBillingAddressLookupOff"
      And I open the siteId "billingloqate-off"

    @B2C2-192
    Scenario: Seed the product data for the GUEST user and go to checkout
      Given I seed using the file "CheckoutLoqateBillingAddressLookup" and add one of each item into my "basket" local storage
      When I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" becomes displayed

    @B2C2-192
    Scenario: When ADDRESS LOOKUP is Disabled, Check that the Loqate Billing Address finder is not displayed and the Billing manual address entry form is displayed
      When I scroll to element "CheckoutAddress.country"
      And I click on the element "CheckoutAddress.billingAddressSameTick"
      Then I expect that element "ViewSelectBillingAddress.addressLine1" is displayed
      And I expect that element "ViewSelectBillingAddress.addressLine2" is displayed
      And I expect that element "ViewSelectBillingAddress.county" is displayed
      And I expect that element "ViewSelectBillingAddress.postcode" is displayed
      And I expect that element "CheckoutAddress.billingManualAddressEntryButton" is not displayed
      And I expect that element "CheckoutAddress.billingFindYourAddressButton" is not displayed

    @B2C2-192
    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

  @B2C2-3665
  Scenario: Setup environment with CheckoutDeliveryAddress
    Given I seed config using the file "CheckoutDeliveryAddress"
    When I open the siteId "deliveryaddress"
    And I seed using the file "CheckoutDeliveryAddress" and add the items into my "basket" local storage

  @B2C2-3665 @DEFECT
  Scenario: Should validate that when USA is the selected delivery country, then the STATE/PROVINCE/REGION field is a single-select drop-down that lists all USA states in alphabetical order
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the button "CheckoutBillingAddress.billingAddressSameTick"
    And I click on the element "CheckoutBillingAddress.country"
    And I click on the element "United States of America"
    And I scroll to element "CheckoutBillingAddress.city"
    And I click on the button "CheckoutBillingAddress.stateSelect"
    Then I expect that the values returned by "CheckoutBillingAddress.states" to be in the following order
            """
            Select
            Alabama
            Alaska
            Arizona
            Arkansas
            California
            Colorado
            Connecticut
            Delaware
            District Of Columbia
            Florida
            Georgia
            Hawaii
            Idaho
            Illinois
            Indiana
            Iowa
            Kansas
            Kentucky
            Louisiana
            Maine
            Maryland
            Massachusetts
            Michigan
            Minnesota
            Mississippi
            Missouri
            Montana
            Nebraska
            Nevada
            New Hampshire
            New Jersey
            New Mexico
            New York
            North Carolina
            North Dakota
            Ohio
            Oklahoma
            Oregon
            Pennsylvania
            Rhode Island
            South Carolina
            South Dakota
            Tennessee
            Texas
            Utah
            Vermont
            Virginia
            Washington
            West Virginia
            Wisconsin
            Wyoming
            """
    And I click on the element "California"

  # Bug for no role: https://supergroupbt.atlassian.net/browse/B2C2-4516
  @B2C2-3665 @DEFECT
  Scenario: Should validate that when USA is NOT the selected country, then the STATE/PROVINCE/REGION field is a non mandatory textbox and US state selection should not be preserved
    When I click on the element "CheckoutBillingAddress.country"
    And I click on the element "CheckoutBillingAddress.UKbillingAddress"
    And I expect that element "CheckoutBillingAddress.stateTextbox" is displayed
    And I expect that element "CheckoutBillingAddress.stateTextbox" not contains any text
    And I set "36characterTestInputIsTooLongXXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "Order Summary"
    And I expect that element "CheckoutBillingAddress.stateErrorAlert" is displayed
    And I set "36characterTestInputIsTooLongXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "Order Summary"
    And I expect that element "CheckoutBillingAddress.stateErrorAlert" is not displayed

  Rule: I WANT to be able to change my billing address at checkout when logged in

  @B2C2-2900
  Scenario: Prepare environment
    Given I am on the page "/"
    And I seed config using the file "CheckoutLoqateBillingAddressChange"
    And I open the siteId "billingloqate-change"
    And I upload the "mocks/loqate/text/gl20a.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa.json"
    And I upload the "mocks/loqate/text/gl20aa.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa.json"
    And I upload the "mocks/loqate/Id/GB|RM|B|51070459.json" file to the S3 bucket "address-lookup-service" with path "Id/GB|RM|B|51070459.json"
    And I upload the "mocks/loqate/container/GB|RM|ENG|0AA-GL2.json" file to the S3 bucket "address-lookup-service" with path "GBR/Container/GB|RM|ENG|0AA-GL2.json"
    And I seed using the file "CheckoutBillingAddressChange" and add one of each item into my "basket" local storage
    And I pause for 10000ms

  @B2C2-2900
  Scenario: Should allow logged in user to add a new billing address at checkout
    Given I open the page "/checkout"
    And I login with the defined customer
    And I am on the page "/checkout"
    When I scroll to element "Checkout/BillingAddress.main"
    And I expect that element "Checkout/BillingAddress.first-address" contains the text "definedCustomer.1.Address1"
    And I click on the element "Checkout/BillingAddress.editAddress"
    And I click on the element "Checkout/BillingAddress.findYourAddressButton"
    And I set "gl20a" to the inputfield "Checkout/BillingAddress.addressTextbox"
    And I pause for 1000ms
    And I add "a" to the inputfield "Checkout/BillingAddress.addressTextbox"
    And I pause for 6000ms
    And I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
    And I pause for 6000ms
    And I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
    Then I expect that element "Checkout/BillingAddress.saveButton" becomes displayed

  @B2C2-2900
  Scenario: Save should redirect back to checkout with new address
    When I pause for 6000ms
    And I click on the element "Checkout/BillingAddress.saveButton"
    And I scroll to element "Checkout/BillingAddress.main"
    Then I expect the url to contain "/checkout"
    And I expect that element "Checkout/BillingAddress.first-address" contains the text "GL2 0AA"

  @B2C2-2900
  Scenario: Switch back to com for next tests
    Given I am on the page "/"
    And I open the siteId "com"

  @B2C2-4313 @B2C2-4366
  Scenario: Logging In to User Without An Associated Phone Number
    Given I am on the page "/"
    And I seed using the file "CheckoutWithoutAssociatedPhoneNumber" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "checkoutNoAssociatedPhoneNumber@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I pause for 2000ms

  @B2C2-4366
  Scenario: User Without An Associated Phone Number Gets Expected Error State If No Fields Entered
    Given I refresh the page
    And I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    When I scroll to element "CheckoutPayment.buyNowButton"
    And I click on the button "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutDeliveryAddress.orderNotSavedModal" is not displayed
    And I expect the count of elements in "Registration.allRequiredAlerts" to equal 1
    And I expect the count of elements in "CheckoutPayment.incompleteFields" to equal 3
    And I expect that element "CheckoutPayment.invalidCardholderName" is displayed
    And I expect that element "CheckoutDelivery.phoneTextbox" is within the viewport

  @B2C2-4313 @SKIP_MOBILE @SKIP_TABLET
  Scenario: User Without An Associated Phone Number Can Order Successfully
    Given I refresh the page
    And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
    When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4444 3333 2222 1111"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    When I click on the button "CheckoutPayment.buyNowButton"
    Then I wait on element "Confirmation.orderConfirmationText" to be displayed
    And the element "Confirmation.orderReferenceText" is displayed
    And the element "Confirmation.orderNumber" is displayed
    