@GEO_REDIRECT
@GEO_LOCATION
@REGRESSION

# https://supergroupbt.atlassian.net/browse/B2C2-814

Feature: Geo Redirect
		As a customer visiting a project site
		I want to be directed to the authoritative site for my location
		So that I am shown relevant products throughout my journey
		And so I am not disappointed late in my shopping journey when I cannot ship to my location.

	# AC1
	# when the customer has not previously chosen to remain on the site
	# and the customer has been geo-located,
	# and the site visited is the Authoritative Site for the customer’s location,
	# then no prompts shall be shown to the customer that notify them to switch sites.
	@B2C2-814
	Scenario: Setup environment with GeoRedirectFRWithGBR
		Given I seed config using the file "GeoRedirectFRWithGBR"
		And I open the siteId "site-814fr"
		And I seed using the file "GeoRedirect"
		And I delete the local storage keys "userSelectedCountry"

	@B2C2-814
	Scenario Outline: Geo Redirect modal shouldn't appear on <pageUrl> when geo location is an Authoritative Site
		Given I open the page "<pageUrl>"
		Then I expect the url to contain "site-814fr"
		And I expect that the path is "<pageUrl>"
		And I wait on element "Common.geoRedirect" for 5000ms to not be displayed

		Examples:
			| pageUrl                                                               |
			| /                                                                     |
			| /main/framework/geo-redirect                                          |
			| /main/framework/geo-redirect/details/jps69yphlj/simple-product-salmon |

	# AC1 - Check against false positives
	# when the customer has not previously chosen to remain on the site
	# and the customer has been geo-located,
	# and the site visited is NOT the Authoritative Site for the customer’s location,
	# then THE PROMPT SHALL be shown to the customer that notify them to switch sites.
	@B2C2-814
	Scenario: Setup environment with GeoRedirectFRWithoutGBR
		Given I seed config using the file "GeoRedirectFRWithoutGBR"
		And I open the siteId "site-814fr"
		And I seed using the file "GeoRedirect" with the locale "fr"
		And I delete the local storage keys "userSelectedCountry"

	@B2C2-814
	Scenario Outline: Geo Redirect modal should appear on <pageUrl> when geo location isn't an Authoritative Site
		And I open the page "<pageUrl>"
		Then I expect the url to contain "site-814fr"
		And I expect that the path is "<pageUrl>"
		And I wait on element "Common.geoRedirect" for 5000ms to be displayed

		Examples:
			| pageUrl                                                               |
			| /                                                                     |
			| /main/framework/geo-redirect                                          |
			| /main/framework/geo-redirect/details/jps69yphlj/simple-product-salmon |

	# AC2
	# when the customer has previously chosen to remain on the site,
	# then no prompts shall be shown to the customer that notify them to switch sites.
	@B2C2-814
	Scenario: Setup environment with GeoRedirectFRWithoutGBR ready for local storage values
		Given I seed config using the file "GeoRedirectFRWithoutGBR"
		And I open the siteId "site-814fr"
		And I seed using the file "GeoRedirect" with the locale "fr"

	@B2C2-814
	Scenario Outline: Geo Redirect modal shouldn't appear on <pageUrl> when local storage value <userSelectedCountry> is present
		And I set the local storage key "userSelectedCountry" with the content "<userSelectedCountry>"
		And I open the page "<pageUrl>"
		Then I expect the url to contain "site-814fr"
		And I expect that the path is "<pageUrl>"
		And I wait on element "Common.geoRedirect" for 5000ms to not be displayed

		Examples:
		  | pageUrl                                                               | userSelectedCountry |
		  | /                                                                     | GBR                 |
		  | /main/framework/geo-redirect                                          | GBR                 |
		  | /main/framework/geo-redirect/details/jps69yphlj/simple-product-salmon | GBR                 |
		  | /                                                                     | FRA                 |
		  | /main/framework/geo-redirect                                          | FRA                 |
		  | /main/framework/geo-redirect/details/jps69yphlj/simple-product-salmon | FRA                 |

# AC3
# when the customer has not previously chosen to remain on the site
# and the customer’s IP address could not be resolved to a location,
#     ^ Not sure how this could be tested with cloudfront doing the IP lookup
# then no prompts shall be shown to the customer that notify them to switch sites.

# AC4
# when the customer has not previously chosen to remain on the site
# and the customer has been geo-located,
# and the site visited is not the Authoritative Site for the customer’s location,
#     ^  This would require seeding the environment so GBR doesn't appear as an Authoritative Site for all sites
# and there is not an Authoritative Site for the customers location,
# then no prompts shall be shown to the customer that notify them to switch sites.
