@CHECKOUT
@ORDER
@ORDER_SUMMARY

Feature: Checkout Order Summary
  
  @B2C2-188 @B2C2-189 @B2C2-2087
  Scenario: Setup environment with CheckoutOrderSummary
    Given I seed config using the file "CheckoutOrderSummary"
    When I open the siteId "b2c2189"
    And I seed using the file "CheckoutOrderSummary" and add one of each item into my "basket" local storage

  @B2C2-189 @B2C2-2087
  Scenario: Should check that order summary contains the right information
    Given I open the page "/checkout"
    When I scroll to element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.expectedDelivery" is displayed
    And I expect that element "CheckoutOrderSummary.subtotal" is displayed
    And I expect that element "CheckoutOrderSummary.shippingFee" is displayed
    And I expect that element "CheckoutOrderSummary.total" is displayed
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£69.98"

  @B2C2-189 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Should check the order summary is sticky when scrolling on <title>
    When I scroll to element "CheckoutOrderSummary.<headings>"
    Then I expect that element "CheckoutOrderSummary.orderSummary" is within the viewport
    
    Examples:
      | headings        | title            |
      | deliveryAddress | Delivery Address |
      | billingAddress  | Billing Address  |
      | paymentDetails  | Payment Details  |

  @B2C2-188 @B2C2-189
  Scenario: Should check that when the remove button is clicked a modal is displayed
    Given I am on the page "/checkout"
    When I scroll to element "CheckoutOrderSummary.deliveryOptions"
    And I click on the button "CheckoutOrderSummary.remove"
    Then I expect that element "CheckoutOrderSummary.removeModal" becomes displayed

  @B2C2-188 @B2C2-189
  Scenario: Should remove an item when modal is confirmed
    When I click on the button "CheckoutOrderSummary.confirmRemove"
    And I scroll to element "CheckoutOrderSummary.deliveryOptions"
    Then I expect that element "Brown Jeans" becomes not displayed
    And I expect that element "Blue Jeans" is displayed

  # Below scenarios with comments are marked as defects
  # FLAKY ADDED, BUG RAISED: https://supergroupbt.atlassian.net/browse/B2C2-4475

  @B2C2-189 @DEFECT
  Scenario: Should check if removing an item from checkout is reflected in the order summary
    When I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "CheckoutOrderSummary.orderSummary"
    # TODO: This pause is important to give the UI time for the basket to recalculate, this should probably wait for the price we expect instead
    And I pause for 3000ms
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"

  @B2C2-189 @DEFECT
  Scenario: Should check if changing to delivery option "UK delivery3" is reflected in the order summary
    When I scroll to element "UK delivery2"
    And I click on the button "UK delivery3"
        # TODO: SAM - Create a waitForTextToNotBe step and waitForTextToBe one
    And I pause for 2000ms
    And I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£2.50"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£42.49"

  @B2C2-189 @DEFECT
  Scenario: Should check if changing to delivery option "UK delivery1" is reflected in the order summary
    When I scroll to element "UK delivery2"
    And I click on the button "UK delivery1"
    # TODO: SAM - Create a waitForTextToNotBe step and waitForTextToBe one
    And I pause for 2000ms
    And I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£3.95"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£43.94"

  @B2C2-189 
  Scenario: Should check if changing delivery country is reflected in the order summary
    Given I am on the page "/checkout"
    And I wait on element "CheckoutOrderSummary.homeDeliveryRadio"
    When I click on the button "CheckoutOrderSummary.countrySelect"
    And I click on the button "France"
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I wait on element "FRA delivery2" to be displayed
    And I scroll to element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£1.50"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£41.49"
    And I click on the button "CheckoutOrderSummary.countrySelect"
    And I click on the button "United Kingdom"
    And I click on the button "CheckoutOrderSummary.modalOk"

  @B2C2-188
  Scenario: Should remove the second item when modal is confirmed and redirect the user to /shopping-bag
    Given I am on the page "/checkout"
    When I scroll to element "CheckoutOrderSummary.deliveryOptions"
    And I click on the button "CheckoutOrderSummary.remove"
    And I click on the button "CheckoutOrderSummary.confirmRemove"
    Then I expect that element "Brown Jeans" is not displayed
    And I expect that the path is "/shopping-bag"

  @B2C2-189
  Scenario: Should add multiple items to the basket of the logged in User and navigate to Checkout
    When I login with "checkoutordersummary@project.local" and "its-a-secret"
    And I seed using the file "CheckoutOrderSummary" and add the items into my "basket" local storage
    And I open the page "/checkout"

  @B2C2-189
  Scenario: Should check that order summary contains the right information
    When I scroll to element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.expectedDelivery" is displayed
    And I expect that element "CheckoutOrderSummary.subtotal" is displayed
    And I expect that element "CheckoutOrderSummary.shippingFee" is displayed
    And I expect that element "CheckoutOrderSummary.total" is displayed
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£69.98"

  @B2C2-189 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Should check the order summary is sticky when scrolling on <title>
    When I scroll to element "CheckoutOrderSummary.<headings>"
    Then I expect that element "CheckoutOrderSummary.orderSummary" is within the viewport

    Examples:
      | headings       | title           |
      | billingAddress | Billing Address |
      | paymentDetails | Payment Details |

  @B2C2-189
  Scenario: Should check that when the remove button is clicked a modal is displayed
    When I scroll to element "CheckoutOrderSummary.deliveryOptions"
    And I click on the button "CheckoutOrderSummary.remove"
    Then I expect that element "CheckoutOrderSummary.removeModal" becomes displayed

  @B2C2-189
  Scenario: Should remove an item when modal is confirmed
    When I click on the button "CheckoutOrderSummary.confirmRemove"
    And I scroll to element "CheckoutOrderSummary.deliveryOptions"
    Then I expect that element "Brown Jeans" becomes not displayed
    And I expect that element "Blue Jeans" is displayed

  @B2C2-189
  Scenario: Should check if removing an item from checkout is reflected in the order summary
    When I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"

  @B2C2-189 @DEFECT
  Scenario: Should check if changing to delivery option "UK delivery3" is reflected in the order summary
    When I scroll to element "UK delivery2"
    And I click on the button "UK delivery3"
        # TODO: SAM - Create a waitForTextToNotBe step and waitForTextToBe one
    And I pause for 2000ms
    And I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "CheckoutOrderSummary.orderSummary"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£2.50"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£42.49"

  @B2C2-189 @DEFECT
  Scenario: Should check if changing to delivery option "UK delivery1" is reflected in the order summary
    When I scroll to element "UK delivery2"
    And I click on the button "UK delivery1"
    And I pause for 2000ms
        # TODO: SAM - Create a waitForTextToNotBe step and waitForTextToBe one
    And I scroll to element "CheckoutOrderSummary.orderSummary"
    And I wait on element "£3.95"
    Then I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£39.99"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£3.95"
    And I expect that element "CheckoutOrderSummary.orderSummary" contains the text "£43.94"

  #
  # Display of Staff Discount on Checkout (Order Summary)
  # https://supergroupbt.atlassian.net/browse/B2C2-537
  #

  Rule: I WANT to be shown the staff discounted prices in the Checkout Order Summary

    @B2C2-537
    Scenario: Setup environment with CheckoutOrderSummary
      Given I seed config using the file "CheckoutOrderSummary_StaffDiscount"
      When I open the siteId "staffdiscount-checkout"
      And I seed using the file "CheckoutOrderSummary_StaffDiscount" and add one of each item into my "basket" local storage

    @B2C2-537
    Scenario: For a 'Guest' user within the 'CHECKOUT', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      Given I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" becomes displayed
      When I scroll to element "CheckoutOrderSummary.orderSummary"
      Then I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-537
    Scenario: Log in with 'STAFF' user and open the basket (basket should be promoted from previous test)
      Given I open the page "/login"
      And I set "checkoutuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithDiscount"

    @B2C2-537
    Scenario: For a 'STAFF' user within the 'CHECKOUT', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      Given I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      When I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Staff Discount
        -£161.00
        Total
        £173.00
        """

    @B2C2-537
    Scenario: Log out the 'STAFF' user
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"

    @B2C2-537
    Scenario: Reseed the data and add items to the basket (the basket items will be promoted when the 'NON STAFF' user logs in)
      Given I seed using the file "CheckoutOrderSummary_StaffDiscount" and add one of each item into my "basket" local storage

    @B2C2-537
    Scenario: Log in with a 'NON STAFF' user
      When I open the page "/login"
      And I set "checkoutuser.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "NonStaffUser WithoutStaffDiscount"

    @B2C2-537
    Scenario: For a 'NON-STAFF' user within the 'CHECKOUT', The 'ORDER SUMMARY SECTION' should have the correct information including any discount (items ordered left to right and down)
      Given I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      When I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £334.00
        Shipping fee
        £0.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-2087 @SKIP_MOBILE @SKIP_TABLET
    # https://supergroupbt.atlassian.net/browse/B2C2-2087 -- AC3
    Scenario: Split delivery displayed in order summary
        Given I seed config using the file "CheckoutOrderSummary_BasketSplit"
        And I open the siteId "b2c22087"
        And I seed using the file "CheckoutOrderSummary_BasketSplit" and add one of each item into my "basket" local storage
        When I open the page "/checkout"
        And I scroll to element "Checkout/OrderSummary.heading"
        Then I expect that element "Checkout/OrderSummary.heading" contains the text "Order Summary"
        And I expect that element "Checkout/OrderSummary.first-delivery" contains the text "Delivery 1 of 4"
        And I expect that element "Checkout/OrderSummary.second-delivery" contains the text "Delivery 2 of 4"
        And I expect that element "Checkout/OrderSummary.third-delivery" contains the text "Delivery 3 of 4"
        And I expect that element "Checkout/OrderSummary.last-delivery" contains the text "Delivery 4 of 4"

    @B2C2-3343 
    Scenario: If the same SKU causes a split delivery, removing one consignment does not remove all
        Given I seed config using the file "CheckoutOrderSummary_BasketSplitSameSKU"
        And I open the siteId "b2c23343"
        And I seed using the file "CheckoutOrderSummary_BasketSplitSameSKU" and add the items into my "basket" local storage
        And I open the page "/checkout"
        And I expect that element "Checkout/OrderSummary.first-delivery" contains the text "Delivery 1 of 2"
        And I expect that element "Checkout/OrderSummary.second-delivery" contains the text "Delivery 2 of 2"
        When I click on the button "CheckoutOrderSummary.remove"
        And I click on the button "CheckoutOrderSummary.confirmRemove"
        Then I expect that element "ShoppingBag.noItemsPresentText" is not displayed
        And I expect that element "CheckoutOrderSummary.quantity" contains the text "3"

  #
  # Display of Promo's
  # https://supergroupbt.atlassian.net/browse/B2C2-3203
  #


  Rule: Promo's should show correct calculations

    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

    @B2C2-3203 @B2C2-3216
    Scenario: 3 for 2 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the element "PopinBasket.closeButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      And I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      And I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £59.97
        Shipping fee
        £0.00
        3 for 2
        -£19.99
        Total
        £39.98
        """

      
    @B2C2-3231
    Scenario: 3 for 2 with 6 items shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the element "PopinBasket.closeButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      And I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £119.94
        Shipping fee
        £0.00
        3 for 2
        -£39.98
        Total
        £79.96
        """
      
    @B2C2-3203 @B2C2-3216
    Scenario: 3 for £19.99 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf3-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the element "PopinBasket.closeButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I wait on element "PopinBasket.miniBag3for1999" to be displayed
      And I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      And I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £26.97
        Shipping fee
        £0.00
        3 for £19.99
        -£6.98
        Total
        £19.99
        """

    @B2C2-3231
    Scenario: 3 for £19.99 with 6 items shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf3-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the element "PopinBasket.closeButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I click on the button "ShoppingBag.incrementButton"
      And I wait on element "PopinBasket.miniBag3for1999" to be displayed
      And I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      And I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £53.94
        Shipping fee
        £0.00
        3 for £19.99
        -£13.96
        Total
        £39.98
        """

    @B2C2-3217
    Scenario: 2 for £14.99 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf4-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the element "PopinBasket.closeButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "ShoppingBag.incrementButton"
      And I wait on element "PopinBasket.miniBag2for1499" to be displayed
      And I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" is displayed
      And I scroll to element "CheckoutOrderSummary.orderSummary"
      And I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £15.98
        Shipping fee
        £0.00
        2 for £14.99
        -£0.99
        Total
        £14.99
        """