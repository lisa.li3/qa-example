@REGRESSION
@SITEMAPS
@ROBOTS

Feature: Site maps

  @B2C2-1155
  Scenario: Robots.txt should disallow everything on nonprod environments
    When I open the page "/robots.txt"
    Then I expect that element "User-agent: * Disallow: /" is displayed

