@PDP
@PDP_MAIN
@REGRESSION
#  Only fails when run via the pipeline

Feature: Product Description Page (PDP)
  # ---
  #
  # Common Setup
  #
  # ---
  @B2C2-441 @B2C2-442 @B2C2-446 @B2C2-168 @B2C2-443 @B2C2-444 @B2C2-445 @B2C2-643 @B2C2-2360 @B2C2-540 @B2C2-2501 @B2C2-2312 @B2C2-4227
  Scenario: Seed the data for the tests
    Given I open the site "/"
    And I seed using the file "ProductDescriptionPage"
    And I refresh the page

  # ---
  #
  # PDP Breadcrumbs
  # https://supergroupbt.atlassian.net/browse/B2C2-441
  #
  # ---
  @B2C2-441 @B2C2-4227 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Breadcrumbs shall be displayed and hyperlinked to the appropriate categories on the relevant site
    Given I am on the page "/category/details/vyf81i5s8e/testproduct"
    Then I expect that element "ProductDescription.homeBreadcrumb" is within the viewport
    And I expect that element "ProductDescription.categoryBreadcrumb" is within the viewport
    And I expect that element "ProductDescription.VYF81I5S8EBreadcrumb" is within the viewport
    And I expect that element "ProductDescription.TESTPRODUCTBreadcrumb" is within the viewport
    And I expect that the attribute "href" from element "ProductDescription.homeBreadcrumb" is "/"

#TODO: Below test is skipped until @B2C2-4561 is in master
@B2C2-4227 @SKIP_TABLET @SKIP_MOBILE @DEFECT
  Scenario: Page should contain a title with gender, product name and colour and a correct meta description
    Given I am on the page "/main/framework/B2C2-441/details/hover0col1/poolside-pique-polo-shirt-mint-green"
    And I expect that the title is "Mens - Poolside Pique Polo Shirt in Red Thing | project"
    And the attribute "content" from element "ProductDescription.descriptionMeta" is "Shop project Mens Poolside Pique Polo Shirt in Red Thing. Buy now with free delivery from the Official project Store."
    And I am on the page "/main/framework/B2C2-441/details/hover0col2/second-product-name-orange"
    And I expect that the title is "Womens - Second product name in Blue Thing | project"
    And the attribute "content" from element "ProductDescription.descriptionMeta" is "Shop project Womens Second product name in Blue Thing. Buy now with free delivery from the Official project Store."

  # ---
  #
  # Product Information - Details & Care
  # https://supergroupbt.atlassian.net/browse/B2C2-446
  #
  # ---
  @B2C2-446
  Scenario: Product description and details panel is displayed
    Given I am on the page "/category/details/vyf81i5s8e/testproduct"
    When I scroll to element "ProductDescription.detailsCareButton"
    And I click on the element "ProductDescription.detailsCareButton"
    Then I expect that element "ProductDescription.detailsCarePanel" is displayed
    And I expect that element "ProductDescription.descriptionHeading" is displayed
    And I expect that element "ProductDescription.itemCodeHeading" is displayed
    And I expect that element "ProductDescription.compositionHeading" is displayed
    And I click on the element "ProductDescription.detailsCarePanelClose"


  @B2C2-446 @B2C2-2312
  Scenario: Product description and details panel is displayed
    Given I am on the page "/category/details/vyf81i5s8e/testproduct"
    When I scroll to element "ProductDescription.detailsCareButton"
    And I click on the element "This is my description text and it needs to be very long so that the text in the paragraph gets truncated"
    Then I expect that element "ProductDescription.detailsCarePanel" is displayed
    And I expect that element "ProductDescription.descriptionHeading" is displayed
    And I expect that element "ProductDescription.itemCodeHeading" is displayed
    And I expect that element "ProductDescription.compositionHeading" is displayed


  @B2C2-446
  Scenario: Product description and details panel closes when close symbol is clicked
    When I click on the element "ProductDescription.detailsCarePanelClose"
    And I wait on element "ProductDescription.detailsCarePanel" to not be displayed
    Then I expect that element "ProductDescription.detailsCarePanel" is not displayed

  # ---
  #
  # Out of Stock Products' PDP
  # https://supergroupbt.atlassian.net/browse/B2C2-168
  #
  # ---
  @B2C2-168
  Scenario: Out of stock products are marked appropriately on the PDP
    Given I am on the page "/category/details/oos168oos1o/testproduct"
    Then I expect that element "ProductDescription.outOfStockMessage" is displayed
    And I expect that element "ProductDescription.addToBagButton" is not displayed

  # ---
  #
  # Basic Product Information
  # https://supergroupbt.atlassian.net/browse/B2C2-442
  #
  # ---
  @B2C2-442 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Colour families are displayed on products that have them along with model information
    Given I am on the page "/category/details/hover0col1/testproduct"
    Then I expect that element "ProductDescription.colourFamilyRedImage" is displayed
    And I expect that element "ProductDescription.colourFamilyBlueImage" is displayed
    And I expect that element "Red Thing Desc" is displayed
    And I expect that element "17'10" is displayed
    And I expect that element "XXXL" is displayed

  @B2C2-442 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Correct colour name is displayed when hovering over colour families
    Given I am on the page "/category/details/hover0col1/testproduct"
    When I move to "ProductDescription.colourFamilyBlueImage"
    Then I expect that element "Blue Thing Desc" is displayed

  # ---
  #
  # Product Size Selection
  # https://supergroupbt.atlassian.net/browse/B2C2-443
  #
  # ---
  @B2C2-443
  Scenario: Single size products do not present size selection option
    Given I am on the page "/category/details/onesize443/testproduct"
    Then I expect that element "ProductDescription.sizeDropdown" is not displayed

  @B2C2-443
  Scenario: Products with a size and inventory have an add to bag cta
    Given I am on the page "/category/details/longsiz443/testproduct"
    Then I expect that element "ProductDescription.addToBagButton" is displayed

  @B2C2-443
  Scenario Outline: Products with a <sizeType> size are displayed correctly in the pdp size options
    Given I am on the page "/category/details/<productID>/testproduct"
    When I click on the element "ProductDescription.sizeDropdown"
    Then I expect that element "ProductDescription.<enabledElement>" is displayed
    And I expect that element "ProductDescription.<disabledElement>" is not displayed
    Examples:
      | sizeType | productID  | enabledElement     | disabledElement     |
      | long     | longsiz443 | longSizeEnabled    | longSizeDisabled    |
      | short    | shortsz443 | shortSizeEnabled   | shortSizeDisabled   |
      | integer  | intsize443 | integerSizeEnabled | integerSizeDisabled |
      | split    | splitsz443 | splitSizeEnabled   | splitSizeDisabled   |

  # ---
  #
  # Add to Bag & Select Size Pop-Up
  # https://supergroupbt.atlassian.net/browse/B2C2-444
  #
  # Develop wishlist size popup
  # https://supergroupbt.atlassian.net/browse/B2C2-445
  #
  # ---
  @B2C2-444 @B2C2-445
  Scenario: Prepare for Size selector scenario outline
    Given I open the page "/category/details/modal444n5/testproduct"

  @B2C2-444 @B2C2-445
  Scenario Outline: Size selector modal shows appropriate stock options for <ctaType>
    When I click on the element "<buttonType>"
    And I click on the element "ProductDescription.modalSizeDropdown"
    Then I expect that element "ProductDescription.<enabledElement>" is enabled
    And I expect that element "ProductDescription.<disabledElement>" is not enabled
    And I click on the element "ProductDescription.sizeModalClose"

    Examples:
      | ctaType    | buttonType                        | enabledElement   | disabledElement   |
      | add to bag | ProductDescription.addToBagButton | modalSizeEnabled | modalSizeDisabled |
      | wish list  | ProductDescription.wishListButton | modalSizeEnabled | modalSizeDisabled |

  @B2C2-444 @B2C2-445
  Scenario Outline: Size selector modal <ctaType> shows appropriate success message
    Given I am on the page "/category/details/modal444n5/testproduct"
    And I delete the local storage
    And I click on the element "<buttonType>"
    And I select the option with the text "size_modal_1" for element "ProductDescription.modalSizeDropdown"
    And I click on the element "<modalButton>"
    And I am on the page "<goTo>"
    And I wait on element "<waitOn>"
    Then I expect that element "size_modal_1" becomes displayed

    Examples:
      | ctaType    | buttonType                        | modalButton                            | goTo                  | waitOn       |
      | add to bag | ProductDescription.addToBagButton | ProductDescription.modalAddToBagButton | /shopping-bag         | Wish List    |
      | wish list  | ProductDescription.wishListButton | ProductDescription.modalWishListButton | /my-account/wish-list | Shopping Bag |

  @B2C2-444 @B2C2-445
  Scenario Outline: Size selector modal <ctaType> transfers selected size to PDP when modal is closed
    Given I am on the page "/category/details/modal444n5/testproduct"
    And I delete the local storage
    When I click on the element "<buttonType>"
    And I select the option with the text "size_modal_1" for element "ProductDescription.modalSizeDropdown"
    And I click on the element "ProductDescription.closeModal"
    Then I expect that element "size_modal_1" is displayed

    Examples:
      | ctaType    | buttonType                        |
      | add to bag | ProductDescription.addToBagButton |
      | wish list  | ProductDescription.wishListButton |

  # ---
  #
  # Display Trustpilot Average Product Rating on PDP
  # https://supergroupbt.atlassian.net/browse/B2C2-643
  #
  # ---
  @B2C2-643
  Scenario Outline: Products with a <ratingType> rating are <display> with an average rating
    Given I am on the page "/category/details/<productID>/testproduct"
    Then I expect that element "ProductDescription.<element>" is <display>
    Examples:
      | ratingType | display       | productID  | element                |
      | undefined  | not displayed | trust643no | undefinedAverageRating |
      | zero       | not displayed | trust643r0 | zeroAverageRating      |
      | set        | displayed     | trust643l6 | setAverageRating       |

  # ---
  #
  # Display Trustpilot Product Reviews on PDP
  # https://supergroupbt.atlassian.net/browse/B2C2-2360
  #
  # ---
  @B2C2-2360
  Scenario Outline: Products with <numberOfReviews> reviews are displayed as expected
    Given I am on the page "/category/details/<productID>/testproduct"
    And I scroll to element "ProductDescription.reviewsPanelOpenButton"
    When I click on the element "ProductDescription.reviewsPanelOpenButton"
    Then I expect that element "ProductDescription.reviewsPanelCloseButton" is displayed
    And I expect that element "ProductDescription.reviewsPanelNextButton" is <display>

    Examples:
      | numberOfReviews | display       | productID  |
      | less than 6     | not displayed | trust643l6 |
      | more than 6     | displayed     | trust643o6 |

  # ---
  #
  # Promotions "Promo Slug" Display
  # https://supergroupbt.atlassian.net/browse/B2C2-540
  # https://supergroupbt.atlassian.net/browse/B2C2-2501
  #
  # ---
  @B2C2-540 @B2C2-2501
  Scenario Outline: The promo slug <promotion> on <productID> should be <condition>
    Given I am on the page "/category/details/<productID>/testproduct"
    Then I expect that element "<promotion>" is <condition>

    Examples:
      | productID  | condition     | promotion |
      | promo540o7 | displayed     | 3 FOR £20 |
      | promo540o7 | not displayed | 3 FOR 2   |
      | promo540o8 | not displayed | 3 FOR £20 |
      | promo540o8 | displayed     | 3 FOR 2   |

  @B2C2-540 @B2C2-2501
  Scenario: Prepare for Scenario Outline by logging in with staff discount
    Given I am on the page "/login"
    And I set "pdp.staffdiscount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "User WithDiscount"

  @B2C2-540 @B2C2-2501
  Scenario Outline: The promo slug <promotion> on <productID> should be not displayed when logged in with staff discount
    Given I am on the page "/category/details/<productID>/testproduct"
    Then I expect that element "<promotion>" is not displayed

    Examples:
      | productID  | promotion |
      | promo540o7 | 3 FOR £20 |
      | promo540o8 | 3 FOR 2   |

  @B2C2-540 @B2C2-2501
  Scenario: Prepare for Scenario Outline by logging out
    Given I am on the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    And I pause for 1000ms

  @B2C2-540 @B2C2-2501
  Scenario Outline: The promo slug <promotion> on <productID> should navigate to the correct promotion page
    Given I am on the page "/category/details/<productID>/testproduct"
    When I click on the element "<promotion>"
    Then I expect the url to contain "/promotions/"
    And I expect that element "ProductListPage.firstHeading" matches the text "<promotion>"

    Examples:
      | productID  | promotion |
      | promo540o7 | 3 FOR £20 |
      | promo540o8 | 3 FOR 2   |

  # ---
  #
  # Product Information - Delivery & Returns
  # https://supergroupbt.atlassian.net/browse/B2C2-2044
  # NOTE: Seeding changes!
  #
  # ---
  @B2C2-2044
  Scenario: Seed config and data for delivery panel tests
    Given I seed config using the file "PDP"
    And I open the siteId "site-pdp"
    And I seed using the file "ProductDescriptionPageDeliveryPanel"
    And I refresh the page

  @B2C2-2044
  Scenario: Configured delivery countries are displayed
    Given I am on the page "/main/framework/B2C2-2044/details/delivery44/fantastic-soft-shirt-orange"
    And I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    And I click on the element "ProductDescription.deliveryPanelCountrySelector"
    Then I expect that element "ProductDescription.deliveryPanelCloseButton" is displayed
    And I expect that element "United Kingdom" is displayed
    And I expect that element "France" is displayed

  @B2C2-2044
  Scenario: Closing the delivery countries
    When I click on the element "ProductDescription.deliveryPanelCloseButton"
    Then I wait on element "ProductDescription.deliveryPanelCloseButton" to not be displayed

  @B2C2-2044
  Scenario: Delivery options are displayed for the selected delivery country
    Given I am on the page "/main/framework/B2C2-2044/details/delivery44/fantastic-soft-shirt-orange"
    And I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    And I click on the element "ProductDescription.deliveryPanelCountrySelector"
    And I click on the element "France"
    Then I expect that element "FRAVery slow delivery" is displayed
    And I expect that element "FREE" is displayed
    And I expect that element "FRAVery slow delivery description" is displayed
    And I expect that element "FRAStandard delivery" is displayed
    And I expect that element "£29" is displayed
    And I expect that element "FRAStandard delivery description" is displayed
    And I expect that element "FRAVery fast delivery" is displayed
    And I expect that element "£399" is displayed
    And I expect that element "FRAVery fast delivery description" is displayed

  @B2C2-2044
  Scenario: Closing the delivery options
    When I click on the element "ProductDescription.deliveryPanelCloseButton"
    Then I wait on element "ProductDescription.deliveryPanelCloseButton" to not be displayed

  @B2C2-2044
  Scenario: Delivery options are carried through to checkout when selected on the PDP
    Given I am on the page "/main/framework/B2C2-2044/details/delivery44/fantastic-soft-shirt-orange"
    And I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    And I click on the element "ProductDescription.deliveryPanelCountrySelector"
    And I click on the element "France"
    And I click on the element "ProductDescription.deliveryPanelCloseButton"
    And I select the option with the text "size_modal_1" for element "ProductDescription.sizeDropdown"
    And I click on the element "ProductDescription.addToBagButton"
    And I am on the page "/checkout/login"
    And I click on the button "CheckoutLogin.continueAsGuest"
    Then I expect that element "France" is displayed

  @B2C2-2044
  Scenario: Country selector is not displayed for logged in users
    Given I am on the page "/login"
    And I set "PDP@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I expect that element "PDP test" is displayed
    And I am on the page "/main/framework/B2C2-2044/details/delivery44/fantastic-soft-shirt-orange"
    And I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    Then I expect that element "ProductDescription.deliveryPanelCountrySelector" is not displayed

  #
  # Product Information - Size Guide
  # https://supergroupbt.atlassian.net/browse/B2C2-2043
  #

  @B2C2-2043
  Scenario: Seed the data for the 'Product Information - Size Guide' tests and open the product page

    Given I seed config using the file "ProductDescriptionPage_SizeGuide"
    When I open the siteId "sizeguide"
    And I seed using the file "ProductDescriptionPage_SizeGuide"
    And I open the page "/category/details/szguide0p1/testproduct"
    And I scroll to element "ProductDescription.sizeGuideButton"
    Then the element "Product with Size Guide" is displayed

    @B2C2-2043 @SKIP_MOBILE @SKIP_TABLET
    Scenario: On DESKTOP, clicking the size guide button opens the size guide dialog
      Given I click on the element "ProductDescription.sizeGuideButton"
      Then I expect that element "ProductDescription.sizeGuideDialog" is displayed

    @B2C2-2043 @SKIP_MOBILE @SKIP_TABLET
    Scenario: On DESKTOP, clicking the size guide close button closes the size guide
      Given I click on the element "ProductDescription.sizeGuideDialogClose"
      Then I expect that element "ProductDescription.sizeGuideDialog" is not displayed

    @B2C2-2043 @SKIP_DESKTOP
    Scenario: On MOBILE or TABLET, clicking the size guide button opens the size guide dialog
      Given I click on the element "ProductDescription.sizeGuideButton"
      Then I expect that element "ProductDescription.sizeGuideImage" is displayed

    @B2C2-2043 @SKIP_DESKTOP
    Scenario: On MOBILE or TABLET, clicking the size guide button closes the size guide
      Given I click on the element "ProductDescription.sizeGuideButton"
      Then I expect that element "ProductDescription.sizeGuideImage" is not displayed

    @B2C2-2043
    Scenario: A product with no size guides should not display the size guide button
      Given I am on the page "/category/details/noguide0p2/testproduct"
      Then I expect that element "ProductDescription.sizeGuideButton" is not displayed

    @B2C2-2043
    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

  @B2C2-3605
  Scenario: Customer is not presented with the klarna and clearpay installment options
    Given I seed config using the file "KlarnaAndClearPayNonSupportedDeliveryCountriesInstalmentOptions"
    And I open the siteId "b2c23605non"
    And I seed using the file "KlarnaAndClearPayProduct"
    When I am on the page "/category/details/B2C23605P01/testproduct"
    Then I expect that element "ProductDescription.payInThreeKlarna" is not displayed
    And I expect that element "ProductDescription.payInFourClearPay" is not displayed
    And I expect that element "ProductDescription.klarnaLogo" is not displayed
    And I expect that element "ProductDescription.clearpayLogo" is not displayed

  @B2C2-3605
  Scenario: Customer is presented with the klarna and clearpay when switching to a supported country
    Given I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    And I click on the element "ProductDescription.deliveryPanelCountrySelector"
    And I click on the element "United States"
    And I click on the button "ProductDescription.deliveryPanelCloseButton"
    Then I expect that element "ProductDescription.payInThreeKlarna" is displayed
    And I expect that element "ProductDescription.payInFourClearPay" is displayed
    And I expect that element "ProductDescription.klarnaLogo" is displayed
    And I expect that element "ProductDescription.clearpayLogo" is displayed

  @B2C2-3605
  Scenario: Customer is presented with deferred payment options for klarna and clearpay
    Given I seed config using the file "KlarnaAndClearPaySupportedDeliveryCountriesDeferredOptions"
    And I open the siteId "b2c23605def"
    And I seed using the file "KlarnaAndClearPayProduct"
    When I am on the page "/category/details/B2C23605P01/testproduct"
    Then I expect that element "ProductDescription.klarnaDeferredPaymentOptionText" is displayed
    And I expect that element "ProductDescription.clearpayDeferredPaymentOptionText" is displayed

  @B2C2-3605
  Scenario: Customer is presented with the klarna and clearpay installment options
    Given I seed config using the file "KlarnaAndClearPaySupportedDeliveryCountriesInstalmentOptions"
    And I open the siteId "b2c23605ins"
    And I seed using the file "KlarnaAndClearPayProduct"
    When I am on the page "/category/details/B2C23605P01/testproduct"
    Then I expect that element "ProductDescription.payInThreeKlarna" is displayed
    And I expect that element "ProductDescription.payInFourClearPay" is displayed
    And I expect that element "ProductDescription.klarnaLogo" is displayed
    And I expect that element "ProductDescription.clearpayLogo" is displayed

  @B2C2-3605
  Scenario: Customer is not presented with the klarna and clearpay when switching to a non supported country
    Given I scroll to element "ProductDescription.deliveryPanelOpenButton"
    When I click on the element "ProductDescription.deliveryPanelOpenButton"
    And I click on the element "ProductDescription.deliveryPanelCountrySelector"
    And I click on the element "Australia"
    And I click on the button "ProductDescription.deliveryPanelCloseButton"
    Then I expect that element "ProductDescription.payInThreeKlarna" is not displayed
    And I expect that element "ProductDescription.payInFourClearPay" is not displayed
    And I expect that element "ProductDescription.klarnaLogo" is not displayed
    And I expect that element "ProductDescription.clearpayLogo" is not displayed