@VIEW_BILLING_ADDRESS
@SELECT_BILLING_ADDRESS
@REGRESSION

Feature: View/Select Billing Address

  @B2C2-195
  Scenario: Seed the environment
    Given I am on the page "/"
      And I seed using the file "ViewSelectBillingAddress1address" and add the items into my "basket" local storage
      And I refresh the page
      And I login with "viewselectbillingaddress@project.local" and "its-a-secret"

  @B2C2-195
  Scenario: Should check if seeded address is displayed as default delivery address in the checkout
    Given I am on the page "/checkout"
    When I wait on element "ViewSelectBillingAddress.homeDeliveryRadio"
    Then I expect that element "ViewSelectBillingAddress.seededNameDeliveryAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.seededAddressDeliveryAddress" is displayed
  
  @B2C2-195
  Scenario: Should check if seeded address is displayed as default billing address in the checkout
    When I scroll to element "ViewSelectBillingAddress.billingAddressHeading"
    Then I expect that element "ViewSelectBillingAddress.seededNameBillingAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.seededAddressBillingAddress" is displayed
    
  @B2C2-195
  Scenario: Should check that the right heading is displayed after clicking the Edit Address button
    When I click on the button "ViewSelectBillingAddress.editAddressButton"
    And I wait on element "ViewSelectBillingAddress.firstName" to be displayed
    Then I expect that element "ViewSelectBillingAddress.accountInformationHeading" is displayed

  @B2C2-195
  Scenario: Should fill out the fields to manually enter address
#    When I click on the button "ViewSelectBillingAddress.manuallyEnterYourAddressButton"
    Then I set "Name" to the inputfield "ViewSelectBillingAddress.firstName"
      And I set "Lastname" to the inputfield "ViewSelectBillingAddress.lastName"
      And I set "address1" to the inputfield "ViewSelectBillingAddress.addressLine1"
      And I set "address2" to the inputfield "ViewSelectBillingAddress.addressLine2"
      And I set "city" to the inputfield "ViewSelectBillingAddress.city"
      And I set "county" to the inputfield "ViewSelectBillingAddress.county"
      And I set "GL51 9NH" to the inputfield "ViewSelectBillingAddress.postcode"
      And I scroll to element "ViewSelectBillingAddress.postcode"
      And I click on the button "ViewSelectBillingAddress.country"
      And I wait on element "ViewSelectBillingAddress.countryUK" to be displayed
      And I click on the button "ViewSelectBillingAddress.countryUK"
      And I click on the button "ViewSelectBillingAddress.saveButton"

  @B2C2-195
  Scenario: User's updated address is shown as the default delivery address
    When I wait on element "ViewSelectBillingAddress.homeDeliveryRadio"
    Then I expect that element "ViewSelectBillingAddress.manualNameDeliveryAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.manualAddressDeliveryAddress" is displayed

  @B2C2-195
  Scenario: User's updated address is shown as the default billing address
    When I scroll to element "ViewSelectBillingAddress.billingAddressHeading"
    Then I expect that element "ViewSelectBillingAddress.manualNameBillingAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.manualAddressBillingAddress" is displayed

  @B2C2-195
  Scenario: Setup environment with ViewSelectBillingAddress
    Given I seed config using the file "ViewSelectBillingAddress"
    When I open the siteId "b2c2195"
      And I seed using the file "ViewSelectBillingAddress2addresses" and add the items into my "basket" local storage
      And I refresh the page
      And I login with "viewselectbillingaddress2addresses@project.local" and "its-a-secret"

  @B2C2-195
  Scenario: Should check if seeded address is displayed as default delivery address in the checkout
    Given I am on the page "/checkout"
    When I wait on element "ViewSelectBillingAddress.homeDeliveryRadio"
    Then I expect that element "ViewSelectBillingAddress.seededNameDeliveryAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.seededAddressDeliveryAddress" is displayed
  
  @B2C2-195
  Scenario: Should check if seeded address is displayed as default billing address in the checkout
    When I scroll to element "ViewSelectBillingAddress.billingAddressHeading"
    Then I expect that element "ViewSelectBillingAddress.seededNameBillingAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.seededAddressBillingAddress" is displayed

  @B2C2-195
  Scenario: Should select a premium delivery option
    When I scroll to element "ViewSelectBillingAddress.deliveryOptionsHeading"
    Then I click on the button "Standard delivery"

  @B2C2-195
  Scenario: Should select non default delivery address
    When I scroll to element "ViewSelectBillingAddress.deliveryAddressHeading"
    Then I click on the element "Foo Bar, Flat 2, Bar Road, City, London, W11 3BB, United Kingdom"
  
  # Skipping the tests from this point because of this bug "https://supergroupbt.atlassian.net/browse/B2C2-3664"

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: Should check that the right heading is displayed after clicking the Edit Address button
    When I scroll to element "ViewSelectBillingAddress.billingAddressHeading"
      And I click on the button "ViewSelectBillingAddress.editAddressButton"
      And I wait on element "ViewSelectBillingAddress.firstName" to be displayed
    Then I expect that element "ViewSelectBillingAddress.accountInformationHeading" is displayed

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: Should fill out the fields to manually enter address
    When I set "Name" to the inputfield "ViewSelectBillingAddress.firstName"
      And I set "Lastname" to the inputfield "ViewSelectBillingAddress.lastName"
      And I set "address1" to the inputfield "ViewSelectBillingAddress.addressLine1"
      And I set "address2" to the inputfield "ViewSelectBillingAddress.addressLine2"
      And I set "city" to the inputfield "ViewSelectBillingAddress.city"
      And I set "county" to the inputfield "ViewSelectBillingAddress.county"
      And I set "GL51 9NH" to the inputfield "ViewSelectBillingAddress.postcode"
      And I scroll to element "ViewSelectBillingAddress.postcode"
      And I click on the button "ViewSelectBillingAddress.country"
      And I wait on element "ViewSelectBillingAddress.countryUK" to be displayed
      And I click on the button "ViewSelectBillingAddress.countryUK"
      And I click on the button "ViewSelectBillingAddress.saveButton"

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: User's updated address is shown as the default delivery address
    When I wait on element "ViewSelectBillingAddress.homeDeliveryRadio"
    Then I expect that element "ViewSelectBillingAddress.manualNameDeliveryAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.manualAddressDeliveryAddress" is displayed

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: User's updated address is shown as the default billing address
    When I scroll to element "ViewSelectBillingAddress.billingAddressHeading"
    Then I expect that element "ViewSelectBillingAddress.manualNameBillingAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.manualAddressBillingAddress" is displayed

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: Should check that the previously selected delivery option is preserved
    When I scroll to element "ViewSelectBillingAddress.deliveryOptionsHeading"
    Then I expect that element "ViewSelectBillingAddress.standardDeliveryButton" is displayed
      And I expect that the attribute "aria-pressed" from element "ViewSelectBillingAddress.standardDeliveryButton" is "true"

  @B2C2-195 @SKIP_DESKTOP @SKIP_TABLET @SKIP_MOBILE
  Scenario: Should check that the previously selected address option should be preserved
    When I scroll to element "ViewSelectBillingAddress.deliveryAddressHeading"
    Then I expect that element "ViewSelectBillingAddress.secondAddress" is displayed
      And I expect that element "ViewSelectBillingAddress.secondAddress" has the class "selected"
