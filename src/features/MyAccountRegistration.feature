@ACCOUNT
@REGISTER
@REGRESSION

Feature: My Account - Registration
  B2C2-1014 - Registration Page
  B2C2-2255 - Loqate Address Lookup for My Account Pages

  @B2C2-1014 @B2C2-2717 @B2C2-2608
  Scenario: Prepare for Scenario Outlines by opening register page
    Given I am on the page "/register"

  @B2C2-2608
  Scenario: Should have the dedicated website helpline displayed
    Then I expect that element "Registration.helplineNumber" is displayed

  @B2C2-1014
  Scenario Outline: Ensure whilst on the registration page the following fields are displayed - <expectedValue>
    Then I expect that element "Registration.<expectedValue>" is displayed

    Examples:
      | expectedValue              |
      | emailAddressTextbox        |
      | passwordTextbox            |
      | confirmPasswordTextbox     |
      | phoneTextbox               |
      | mobilPhoneTextbox          |
      | gender                     |
      | firstNameTextbox           |
      | lastNameTextbox            |
      | lastNameTextbox            |
      | address1Textbox            |
      | address2Textbox            |
      | cityTextbox                |
      | postcodeTextbox            |
      | countryComboBox            |
      | subscribeCheckbox          |
      | registerButton             |

  @B2C2-1014
  Scenario Outline: When entering <value> into the registration email textbox I am displayed with a message for invalid email address
    When I set "<value>" to the inputfield "Registration.emailAddressTextbox"
    And I click on the element "Registration.passwordTextbox"
    Then I expect that element "Registration.invalidEmailMessage" is displayed
    And I clear the inputfield "Registration.emailAddressTextbox"

    Examples:
      | value        |
      | abc          |
      | abc@abc      |
      | abc.com      |
      | .abc@abc.com |
      | abc.@abc.com |

  @B2C2-1014
  Scenario Outline: User is displayed with registration password rules - <values>
    Then I expect that element "<values>" is displayed

    Examples:
      | values                                  |
      | A password must contain the following   |
      | Minimum of 8 characters                 |
      | One or more uppercase letter            |
      | One or more lowercase letter            |
      | One or more number or special character |

  @B2C2-1014
  Scenario Outline: Ensure when user enters <values> into the registration password textbox, user is displayed password error <textDisplayed>
    Given I set "<values>" to the inputfield "Registration.passwordTextbox"
    When I click on the element "Registration.emailAddressTextbox"
    Then I expect that element "Registration.passwordErrors" matches the text "<textDisplayed>"
    And I clear the inputfield "Registration.passwordTextbox"

    Examples:
      | values    | textDisplayed                           |
      | abcdefg   | MINIMUM OF 8 CHARACTERS                 |
      | ABCDEFGH1 | ONE OR MORE LOWERCASE LETTER            |
      | abcdefgh  | ONE OR MORE UPPERCASE LETTER            |
      | Abcdefgh  | ONE OR MORE NUMBER OR SPECIAL CHARACTER |

  @B2C2-1014
  Scenario: User is not shown registration password validation error
    When I set "1Th15IsaVal1dPassw*rd" to the inputfield "Registration.passwordTextbox"
    And I click on the element "Registration.emailAddressTextbox"
    Then I expect that element "Registration.passwordErrors" is not displayed
    And I clear the inputfield "Registration.passwordTextbox"

  @B2C2-1014
  Scenario: User sets password that does not match with confirm password
    When I set "ThisIsNotTheSame" to the inputfield "Registration.confirmPasswordTextbox"
    And I set "1Th15IsaVal1dPassw*rd" to the inputfield "Registration.passwordTextbox"
    And I click on the element "Registration.registerButton"
    Then I expect that element "Registration.confirmPasswordWarning" matches the text "PASSWORDS MUST MATCH"
    And I clear the inputfield "Registration.confirmPasswordTextbox"
    And I clear the inputfield "Registration.passwordTextbox"

  @B2C2-2717
  Scenario Outline: Confirm Password Field Matches Password Field when Password is >50 Characters
    When I set <password> to the inputfield "Registration.confirmPasswordTextbox"
    And I set <password> to the inputfield "Registration.passwordTextbox"
    And I click on the element "Registration.registerButton"
    Then I expect that element "Registration.confirmPasswordWarning" is not displayed
    And I expect the value length of element "Registration.confirmPasswordTextbox" to equal <length>
    And I clear the inputfield "Registration.confirmPasswordTextbox"
    And I clear the inputfield "Registration.passwordTextbox"

    Examples:
      | password                                                                                               | length |
      | "51Characters-popeosdpoposvpojrbifr0[[0-51Characters"                                                  | 51     |
      | "75Characters-opopeosdpoposvpojrbifr0[[0sfnjsnvsoksvookegpjoioj-75Characters"                          | 75     |
      | "100Characters-popeosdpoposvpojrbifr0[[0sfnjsnvsoksvookegpjoiojqopopeosdpoposvpojrbifr0-100Characters" | 100    |
 

  @B2C2-1014
  Scenario Outline: Ensures correct registration validation when entering <value> into <field>
    When I set "<value>" to the inputfield "Registration.<field>"
    And I click on the element "Registration.registerButton"
    Then I expect that element "Registration.inputTooLongWarning" matches the text "TOO LONG"
    And I clear the inputfield "Registration.<field>"

    Examples:
      | value                                                             | field             |
      | 64-1111111111111111111111111111111111111111111111111111111111111  | phoneTextbox      |
      | 64-1111111111111111111111111111111111111111111111111111111111111  | mobilPhoneTextbox |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                                 | firstNameTextbox  |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                                 | lastNameTextbox   |

  @B2C2-1014
  Scenario Outline: Ensure when entering <value> into the registration <field>
    When I set "<value>" to the inputfield "Registration.<field>"
    Then I expect that element "Registration.inputTooLongWarning" is not displayed
    And I clear the inputfield "Registration.<field>"

    Examples:
      | value                          | field             |
      | 0[222]+123-32123               | phoneTextbox      |
      | 0[222]+123-32123               | mobilPhoneTextbox |
      | abc123!£&aaaaaaaaaaaaaaaaaaaaa | firstNameTextbox  |
      | abc123!£&aaaaaaaaaaaaaaaaaaaaa | lastNameTextbox   |

  @B2C2-1014
  Scenario Outline: User is alerted when entering registration values <field>
    When I set "<value>" to the inputfield "Registration.<field>"
    And I click on the element "Registration.registerButton"
    Then I expect that element "Registration.phoneNumberValueTypeWarning" matches the text "MUST BE A PHONE NUMBER"
    And I clear the inputfield "Registration.<field>"

    Examples:
      | value | field             |
      | a     | phoneTextbox      |
      | a     | mobilPhoneTextbox |

  @B2C2-1014
  Scenario Outline: User is not alerted when entering values of the correct registration type - <field>
    When I set "<value>" to the inputfield "Registration.<field>"
    Then I expect that element "Registration.phoneNumberValueTypeWarning" is not displayed
    And I clear the inputfield "Registration.<field>"

    Examples:
      | value            | field             |
      | 0[222]+123-32123 | phoneTextbox      |
      | 0[222]+123-32123 | mobilPhoneTextbox |

  @B2C2-1014
  Scenario Outline: User is able to select from registration gender options - <gender>
    Then I select the option with the text "<gender>" for element "Registration.gender"

    Examples:
      | gender            |
      | Male              |
      | Female            |
      | Prefer not to say |

  @B2C2-1014
  Scenario: Customer is notified of required fields
    Given I open the page "/register"
    When I click on the element "Registration.registerButton"
    Then I expect the count of elements in "Registration.allRequiredAlerts" to equal 11

  @B2C2-1014
  Scenario: User is presented with option to subscribe to electronic marketing on registration page
    Then I expect that the attribute "name" from element "Registration.subscriptionApproval" is "subscriptionApproval"
    And I expect that the attribute "type" from element "Registration.subscriptionApproval" is "checkbox"
    And I expect that element "Registration.subscriptionApprovalLabel" matches the text "I subscribe to electronic marketing"

  @B2C2-1014
  Scenario: User is presented with option to accept terms and conditions
    Then I expect that the attribute "name" from element "Registration.termsAndConditions" is "termsApproval"
    And I expect that the attribute "type" from element "Registration.termsAndConditions" is "checkbox"
    And I expect that the attribute "href" from element "Registration.termsAndConditionsLink" is "terms-and-conditions"

  @B2C2-1014
  Scenario: User is not shown newsletter sign up on registration page
    Then I expect that element "NewsletterSignupBanner.complementary" is not displayed

  @B2C2-1014
  Scenario Outline: When entering <value> into the registration email textbox I am displayed with a message for invalid email address
    When I set "<value>" to the inputfield "Registration.emailAddressTextbox"
    And I click on the element "Registration.passwordTextbox"
    Then I expect that element "Registration.invalidEmailMessage" is displayed
    And I clear the inputfield "Registration.emailAddressTextbox"

    Examples:
      | value           |
      | abc             |
      | abc@abc         |
      | abc.com         |
      | .abc@abc.com    |
      | abc.@abc.com    |
      | abc @ abc . com |

  @B2C2-1014 @B2C2-2255 @DEFECT @B2C2-4488
  Scenario: When a customer registers on the /register page with loqate the account is created
    Given I spawn the site "register" from the base config "loqate-on"
    And I am on the page "/register"
    When I set "definedCustomer.Email" to the inputfield "Registration.emailAddressTextbox"
    And I set "definedCustomer.Password" to the inputfield "Registration.passwordTextbox"
    And I set "definedCustomer.Password" to the inputfield "Registration.confirmPasswordTextbox"
    And I set "definedCustomer.Phone" to the inputfield "Registration.phoneTextbox"
    And I set "definedCustomer.Mobile" to the inputfield "Registration.mobilPhoneTextbox"
    And I select the option with the text "Male" for element "Registration.gender"
    And I set "definedCustomer.FirstName" to the inputfield "Registration.firstNameTextbox"
    And I set "definedCustomer.LastName" to the inputfield "Registration.lastNameTextbox"
    And I set "definedCustomer.Address1" to the inputfield "Registration.addressTextbox"
    # Pause here to give the loqate address search box time to calm down
    And I pause for 3000ms
    And I click on the element "Registration.firstAddressSuggestion"
    And I pause for 3000ms
    And I set "definedCustomer.LastName" to the inputfield "Registration.lastNameTextbox"
    And I click on the element "Registration.subscribeCheckbox"
    And I click on the element "Registration.termsAndConditionsParent"
    And I click on the element "Registration.registerButton"
    And I click on the element "MyAccount.accountInformationButton"
    Then I expect that element "Registration.emailAddressTextbox" contains the text "definedCustomer.Email"
    And I expect that element "Registration.phoneTextbox" contains the text "definedCustomer.Phone"
    And I expect that element "Registration.mobilPhoneTextbox" contains the text "definedCustomer.Mobile"
    And I expect that element "Registration.firstNameTextbox" contains the text "definedCustomer.FirstName"
    And I expect that element "Registration.lastNameTextbox" contains the text "definedCustomer.LastName"

  @B2C2-1014 @B2C2-2255
  Scenario: When a customer registers on the /register page with manual address the account is created
    Given I delete the local storage
    And I open the siteId "com"
    And I am on the page "/register"
    When I set "definedCustomer.2.Email" to the inputfield "Registration.emailAddressTextbox"
    And I set "definedCustomer.2.Password" to the inputfield "Registration.passwordTextbox"
    And I set "definedCustomer.2.Password" to the inputfield "Registration.confirmPasswordTextbox"
    And I set "definedCustomer.2.Phone" to the inputfield "Registration.phoneTextbox"
    And I set "definedCustomer.2.Mobile" to the inputfield "Registration.mobilPhoneTextbox"
    And I select the option with the text "Male" for element "Registration.gender"
    And I set "definedCustomer.2.FirstName" to the inputfield "Registration.firstNameTextbox"
    And I set "definedCustomer.2.LastName" to the inputfield "Registration.lastNameTextbox"
    And I set "definedCustomer.2.Address1" to the inputfield "Registration.address1Textbox"
    And I set "definedCustomer.2.Address2" to the inputfield "Registration.address2Textbox"
    And I set "definedCustomer.2.City" to the inputfield "Registration.cityTextbox"
    And I set "definedCustomer.2.Postcode" to the inputfield "Registration.postcodeTextbox"
    And I select the option with the text "United Kingdom" for element "Registration.countryComboBox"
    And I click on the element "Registration.termsAndConditionsParent"
    And I click on the element "Registration.registerButton"
    And I click on the element "MyAccount.accountInformationButton"
    Then I expect that element "Registration.emailAddressTextbox" contains the text "definedCustomer.2.Email"
    And I expect that element "Registration.phoneTextbox" contains the text "definedCustomer.2.Phone"
    And I expect that element "Registration.mobilPhoneTextbox" contains the text "definedCustomer.2.Mobile"
    And I expect that element "Registration.firstNameTextbox" contains the text "definedCustomer.2.FirstName"
    And I expect that element "Registration.lastNameTextbox" contains the text "definedCustomer.2.LastName"

  @B2C2-1014 @B2C2-2255
  Scenario: When a customer attempts to register on the /register page with an email address that is already in use they are alerted
    Given I delete the local storage
    And I am on the page "/register"
    When I set "definedCustomer.2.Email" to the inputfield "Registration.emailAddressTextbox"
    And I set "definedCustomer.2.Password" to the inputfield "Registration.passwordTextbox"
    And I set "definedCustomer.2.Password" to the inputfield "Registration.confirmPasswordTextbox"
    And I set "definedCustomer.2.Phone" to the inputfield "Registration.phoneTextbox"
    And I set "definedCustomer.2.Mobile" to the inputfield "Registration.mobilPhoneTextbox"
    And I select the option with the text "Male" for element "Registration.gender"
    And I set "definedCustomer.2.FirstName" to the inputfield "Registration.firstNameTextbox"
    And I set "definedCustomer.2.LastName" to the inputfield "Registration.lastNameTextbox"
    And I set "definedCustomer.2.Address1" to the inputfield "Registration.address1Textbox"
    And I set "definedCustomer.2.Address2" to the inputfield "Registration.address2Textbox"
    And I set "definedCustomer.2.City" to the inputfield "Registration.cityTextbox"
    And I set "definedCustomer.2.Postcode" to the inputfield "Registration.postcodeTextbox"
    And I select the option with the text "United Kingdom" for element "Registration.countryComboBox"
    And I click on the element "Registration.termsAndConditionsParent"
    And I click on the element "Registration.registerButton"
    Then I expect that element "Registration.duplicateEmailError" is displayed