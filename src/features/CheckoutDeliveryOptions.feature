@CHECKOUT
@DELIVERY
@DELIVERY_OPTIONS
@REGRESSION_FLAKY
# NOTE: Failed locally

Feature: Checkout Delivery Options

  @B2C2-128
  Scenario: Setup environment with CheckoutDeliveryOptions
    Given I seed config using the file "CheckoutDeliveryOptionsBasketSplit"
    When I open the siteId "b2c2128"
    And I seed using the file "CheckoutDeliveryOptionsBasketSplit" and add the items into my "basket" local storage

  @B2C2-128
  Scenario: Split deliveries should be displayed in the checkout
    Given I open the page "/checkout"
    Then I expect that element "CheckoutDelivery.firstDelivery" is displayed
    And I expect that element "CheckoutDelivery.secondDelivery" is displayed
    And I expect that element "CheckoutDelivery.thirdDelivery" is displayed
    And I expect that element "CheckoutDelivery.fourthDelivery" is displayed

  @B2C2-128
  Scenario: Each split delivery should display associated delivery options and cheapest delivery option should be pre-selected
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.firstDeliveryFC1delivery2" is "true"
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.firstDeliveryFC1delivery1" is "false"
    And I expect that element "FC1 delivery 1" is displayed
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.secondDeliveryFC2delivery2" is "true"
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.secondDeliveryFC2delivery1" is "false"
    And I expect that element "FC2 delivery 1" is displayed
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.thirdDeliveryFC3delivery2" is "true"
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.thirdDeliveryFC3delivery1" is "false"
    And I expect that element "FC3 delivery 1" is displayed
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.fourthDeliveryFC4delivery1" is "true"

  @B2C2-128
  Scenario: User can select a specific day for timed delivery option
    And I expect that element "CheckoutDelivery.FC3delivery2TimedDelivery" is displayed
    And I click on the button "CheckoutDelivery.FC3delivery2TimedDelivery"
    And I wait on element "CheckoutDelivery.FC3delivery2TimedDeliveryMonday" to be displayed
    And I click on the button "CheckoutDelivery.FC3delivery2TimedDeliveryMonday"
    And I expect that element "CheckoutDelivery.FC4delivery1TimedDelivery" is displayed
    And I scroll to element "CheckoutDelivery.FC4delivery1TimedDelivery"
    And I click on the button "CheckoutDelivery.FC4delivery1TimedDelivery"
    And I click on the button "CheckoutDelivery.FC4delivery1TimedDeliveryFriday"

  @B2C2-131 @B2C2-134 @B2C2-181 @B2C2-135
  Scenario: Setup environment with CheckoutDeliveryOptions
    Given I seed config using the file "CheckoutDeliveryOptions"
    When I open the siteId "b2c2134"
    And I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage

  @B2C2-131 @B2C2-134 @B2C2-181 @B2C2-135
  Scenario: Home delivery option should be displayed and selected by default
  and correct delivery options should be displayed
  and seeded helpline number should be displayed
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "true"
    And I expect that element "CheckoutDelivery.storeCollectionRadio" is enabled
    And I expect that element "UK delivery1" is displayed
    And I expect that element "UK delivery2" is displayed
    And I expect that element "UK delivery3" is displayed
    Then I expect that element "CheckoutDelivery.callHelplineLink" is displayed

  @B2C2-135
  Scenario: Select a store functionality should be shown after user selects a Collect From Store button
  and home delivery options should be shown if user clicks home delivery button
    Given I scroll to element "CheckoutDelivery.storeCollectionRadio"
    When I click on the button "CheckoutDelivery.storeCollectionRadio"
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.storeCollectionRadio" is "true"
    And I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "false"
    When I scroll to element "CheckoutDelivery.countrySelect"
    And I click on the button "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.storeCollectionRadio" is "false"
    And I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "true"
    And I expect that element "CheckoutDelivery.storeCollectionRadio" is enabled
    And I expect that element "UK delivery1" is displayed
    And I expect that element "UK delivery2" is displayed
    And I expect that element "UK delivery3" is displayed

  @B2C2-135
  Scenario: Setup environment with CheckoutDeliveryOptionsNoStoreCollection data seed
    Given I seed using the file "CheckoutDeliveryOptionsNoStoreCollection" and add the items into my "basket" local storage

  @B2C2-135
  Scenario: Home delivery option should be displayed and selected by default
  and store collection button should be disabled if 2 products in basket and 1 is not available for store collection
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "true"
    And I expect that the attribute "aria-checked" from element "CheckoutDelivery.storeCollectionRadio" is "false"

  @B2C2-135
  Scenario: Store collection button should be disabled if the only product in basket is not available to be collected from store
    When I click on the button "CheckoutDelivery.removeFirstItemButton"
    And I click on the button "CheckoutDelivery.popUpYes"
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.storeCollectionRadio" is "false"
    And I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "true"


  @B2C2-131
  Scenario: Cheapest delivery method should be preselected
    Given I wait on element "UK delivery1" to be displayed
    When I scroll to element "UK delivery2"
    Then I expect that the attribute "aria-pressed" from element "CheckoutDelivery.cheapestDeliveryOptionButton" is "true"

  @B2C2-131
  Scenario: Cheapest delivery method should be preselected
    Given I wait on element "UK delivery1" to be displayed
    When I scroll to element "UK delivery2"
    Then I expect that the attribute "aria-pressed" from element "CheckoutDelivery.cheapestDeliveryOptionButton" is "true"

  @B2C2-131
  Scenario: Delivery options should be displayed in descending order
    Given I wait on element "UK delivery1" to be displayed
    When I scroll to element "UK delivery2"
    Then I expect that the values returned by "CheckoutDelivery.deliveryOptionsButtonText" to be in the following order
        """
        UK delivery2
        FREE
        Delivery in 5 days
        UK delivery3
        £2.50
        Delivery in 3 days
        UK delivery1
        £3.95
        Delivery in 2 days
        """

  @B2C2-131
  Scenario: Delivery option should be highlighted after user clicks on it
    Given I wait on element "UK delivery1" to be displayed
    When I click on the button "UK delivery1"
    Then I expect that the attribute "aria-pressed" from element "CheckoutDelivery.mostExpensiveDeliveryOptionButton" is "true"

  @B2C2-131 @B2C2-134
  Scenario: Home delivery method should be displayed and selected by default
  and delivery options should update
  and cheapest delivery option should be selected by default
  and delivery options should be sorted by price
  after delivery country is changed
    Given I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage
    When I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the element "CheckoutDelivery.countrySelect"
    And I click on the button "France"
    And I click on the button "CheckoutDelivery.popUpOk"
    And I wait on element "FRA delivery2" to be displayed
    Then I expect that the attribute "aria-checked" from element "CheckoutDelivery.homeDeliveryRadio" is "true"
    And I expect that the attribute "aria-pressed" from element "CheckoutDelivery.cheapestDeliveryOptionButton" is "true"
    And I scroll to element "FRA delivery2"
    And I expect that element "FRA delivery1" is displayed
    And I expect that element "FRA delivery2" is displayed
    And I expect that element "FRA delivery3" is displayed
    And I expect that the values returned by "CheckoutDelivery.deliveryOptionsButtonText" to be in the following order
            """
            FRA delivery2
            FREE
            FRADelivery in 5 days
            FRA delivery3
            £2.50
            FRADelivery in 3 days
            FRA delivery1
            £3.95
            FRADelivery in 2 days
            """

  @B2C2-181
  Scenario: The user should log in with seeded data and directed to checkout
  and seeded helpline number should be displayed
    Given I am on the page "/checkout/login"
    When I set "checkout@deliveryOptions.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "CheckoutDelivery.callHelplineLink" is displayed


  @B2C2-182
  Scenario: Setup environment with CheckoutDeliveryOptions1country
    Given I seed config using the file "CheckoutDeliveryOptions1country"
    When I open the siteId "1country"
    And I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage


  @B2C2-182
  Scenario: Default delivery country is displayed when there is 1 delivery country in site config
  and correct delivery options should be displayed
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "CheckoutDelivery.defaultDeliveryCountry1country" is displayed

  @B2C2-182 @B2C2-187 @B2C2-1012
  Scenario: Setup environment with CheckoutDeliveryOptions5countries
    Given I seed config using the file "CheckoutDeliveryOptions5countries"
    When I open the siteId "5countries"
    And I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage

  @B2C2-182 @B2C2-187 @B2C2-1012
  Scenario: Delivery countries dropdown doesn't show a search bar for 5 or less delivery countries
  and correct delivery options should be displayed
    and default delivery country should be preselected
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I expect that element "United Kingdom" is displayed
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    Then I expect that element "United Kingdom" is displayed
    And I expect that element "Germany" is displayed
    And I expect that element "France" is displayed
    And I expect that element "Italy" is displayed
    And I expect that element "Poland" is displayed
    #TODO: Below test skipped due to an issue: https://supergroupbt.atlassian.net/browse/B2C2-3503
#    And I expect that element "CheckoutDelivery.countrySearchTextbox" is not displayed

  @B2C2-182 @B2C2-187
  Scenario: Setup environment with CheckoutDeliveryOptions20countries
    Given I seed config using the file "CheckoutDeliveryOptions20countries"
    When I open the siteId "20countries"
    And I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage


  @B2C2-182 @B2C2-187
  Scenario: Delivery countries shows a search bar for 6 or more delivery countries
  and correct delivery options should be displayed
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    Then I wait on element "CheckoutDelivery.countrySearchTextbox" to be displayed
    And I click on the button "United Kingdom"

  @B2C2-182 @B2C2-187
  Scenario Outline: Search functionality works as expected in a delivery country dropdown
  and delivery countries shows a search bar for 6 or more delivery countries
    #Then I set "searchTerm" to the inputfield "CheckoutDelivery.countrySearchTextbox"
    When I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    Then I wait on element "CheckoutDelivery.countrySearchTextbox" to be displayed
    And I click on the button "CheckoutDelivery.countrySearchTextbox"
    And I clear the inputfield "CheckoutDelivery.countrySearchTextbox"
    And I press "<searchTerm>"
    And I expect that element "<expectedResult1>" is <displayedState1>
    And I expect that element "<expectedResult2>" is <displayedState2>
    And I expect that element "<expectedResult3>" is <displayedState3>
    And I click on the button "<countryToClick>"
    And I expect that element "CheckoutDelivery.importantNoticeHeading" is <popUpState>
    And I expect that element "CheckoutDelivery.importantNoticeBody" is <popUpState>
    And I click on the button "CheckoutDelivery.popUpOk"
    And I expect that element "<countryToClick>" is displayed

    Examples:
      | searchTerm | expectedResult1                   | displayedState1 | expectedResult2 | displayedState2 | expectedResult3 | displayedState3 | countryToClick | popUpState    |
      | Germany    | Germany                           | displayed       | n/a             | not displayed   | n/a             | not displayed   | Germany        | displayed     |
      | Italy      | Italy                             | displayed       | n/a             | not displayed   | n/a             | not displayed   | Italy          | displayed     |
      | l          | Latvia                            | displayed       | Lithuania       | displayed       | Luxembourg      | displayed       | Lithuania      | displayed     |
      | C          | Croatia                           | displayed       | Cyprus          | displayed       | Czech Republic  | displayed       | Cyprus         | displayed     |
      | f          | France                            | displayed       | Finland         | displayed       | n/a             | not displayed   | France         | displayed     |
      | Po         | Poland                            | displayed       | n/a             | not displayed   | n/a             | not displayed   | Poland         | displayed     |
      | Bul        | Bulgaria                          | displayed       | n/a             | not displayed   | n/a             | not displayed   | Bulgaria       | displayed     |
      #| xyz        | CheckoutDelivery.noResultsMatched | displayed       | n/a             | not displayed   | n/a             | not displayed   | Bulgaria       | not displayed |

  @B2C2-182 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Setup environment with CheckoutDeliveryOptions6countries
  and validate search box is displayed for a site with 6 countries
    Given I seed config using the file "CheckoutDeliveryOptions6countries"
    When I open the siteId "6countries"
    And I seed using the file "CheckoutDeliveryOptions" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    Then I wait on element "CheckoutDelivery.countrySearchTextbox" to be displayed


  @B2C2-182 @SKIP_TABLET @SKIP_MOBILE
  Scenario Outline: Tabbing through delivery countries dropdown focuses on correct fields
    When I press "Tab"
    Then I expect that element "<elementInFocus>" is focused

    Examples:
      | elementInFocus                        |
      | CheckoutDelivery.countrySearchTextbox |
      | CheckoutDelivery.unitedKingdomButton  |
      | CheckoutDelivery.germanyButton        |
      | CheckoutDelivery.franceButton         |
      | CheckoutDelivery.italyButton          |
      | CheckoutDelivery.polandButton         |
      | CheckoutDelivery.austriaButton        |
      | CheckoutDelivery.homeDeliveryRadio    |


  @B2C2-186
  Scenario Outline: Seed data for <dataSeeding> and change delivery country and expect messages informing that items can't be delivered to specified location

    Given I seed config using the file "CheckoutDeliveryOptionsUnableToDeliver"
    When I open the siteId "b2c2186"
    And I seed using the file "<dataSeeding>" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    And I click on the button "Germany"
    And I click on the button "CheckoutDelivery.popUpOk"
    And I wait on element "CheckoutDelivery.modalHeader"
    Then I expect that element "CheckoutDelivery.modalHeader" is displayed
    And I expect that element "CheckoutDelivery.modalLine1" is displayed
    And I expect that element "CheckoutDelivery.modalLine2" is displayed
    And I click on the button "CheckoutDelivery.popUpOkSorry"
    And I expect that element "CheckoutDelivery.cantDeliverProductFirstMessage" is <firstMessageState>
    And I expect that element "CheckoutDelivery.cantDeliverProductSecondMessage" is <secondMessageState>
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage1" is displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage2" is displayed
    And I expect that element "CheckoutPayment.buyNowButton" is not enabled
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    And I click on the button "United Kingdom"
    And I click on the button "CheckoutDelivery.popUpOk"
    Examples:
      | dataSeeding                                                   | firstMessageState | secondMessageState |
      | CheckoutDeliveryOptionsUnableToDeliverSingleLine              | displayed         | not displayed      |
      | CheckoutDeliveryOptionsUnableToDeliverSingleLineMultiQuantity | displayed         | not displayed      |
      | CheckoutDeliveryOptionsUnableToDeliverSplitAllError           | displayed         | displayed          |
      | CheckoutDeliveryOptionsUnableToDeliverSplitPartialError       | displayed         | not displayed      |

  @B2C2-186
  Scenario: Setup environment with CheckoutDeliveryOptionsUnableToDeliverSplitPartialError and change the delivery country to trigger a fulfilment error

    Given I seed config using the file "CheckoutDeliveryOptionsUnableToDeliver"
    When I open the siteId "b2c2186"
    And I seed using the file "CheckoutDeliveryOptionsUnableToDeliverSplitPartialError" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    And I click on the button "Germany"
    And I click on the button "CheckoutDelivery.popUpOk"
    And I wait on element "CheckoutDelivery.modalHeader"
    And I click on the button "CheckoutDelivery.popUpOkSorry"

  @B2C2-186
  Scenario: Remove the product that can't be delivered to a specified location and Error messages should no longer be displayed and BUY NOW button should be enabled
    When I click on the button "CheckoutDelivery.removeFirstItemButton"
    And I click on the button "CheckoutDelivery.popUpYes"
    And I wait on element "CheckoutDelivery.popUpYes" to not be displayed
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "CheckoutDelivery.cantDeliverProductFirstMessage" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverProductSecondMessage" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage1" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage2" is not displayed
    And I expect that element "CheckoutPayment.buyNowButton" is enabled

  @B2C2-186
  Scenario: Switch back to the United Kingdom as the default country
    Given I refresh the page
    And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
    And I click on the button "United Kingdom"
    And I click on the button "CheckoutDelivery.popUpOk"

  @B2C2-186 @B2C2-3392
  Scenario: User should log in with seeded data and be directed to checkout
    Given I seed config using the file "CheckoutDeliveryOptionsUnableToDeliver"
    When I open the siteId "b2c2186"
    And I seed using the file "CheckoutDeliveryOptionsUnableToDeliverSplitPartialError" and add the items into my "basket" local storage
    And I am on the page "/checkout/login"
    And I set "unableToDeliver3@deliveryOptions.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"

  @B2C2-186 @B2C2-3392
  Scenario Outline: As a logged in user seed data for <dataSeeding> and change delivery country and expect messages informing that items can't be delivered to specified location
    Given I seed config using the file "CheckoutDeliveryOptionsUnableToDeliver"
    When I open the siteId "b2c2186"
    And I seed using the file "<dataSeeding>" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I click on the button "DE home"
    And I click on the button "CheckoutDelivery.popUpOk"
    And I wait on element "CheckoutDelivery.modalHeader"
    Then I expect that element "CheckoutDelivery.modalHeader" is displayed
    And I expect that element "CheckoutDelivery.modalLine1" is displayed
    And I expect that element "CheckoutDelivery.modalLine2" is displayed
    And I click on the button "CheckoutDelivery.popUpOkSorry"
    And I expect that element "CheckoutDelivery.cantDeliverProductFirstMessage" is <firstMessageState>
    And I expect that element "CheckoutDelivery.cantDeliverProductSecondMessage" is <secondMessageState>
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage1" is displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage2" is displayed
    And I expect that element "CheckoutPayment.buyNowButton" is not enabled
    And I click on the button "UK home"
    And I click on the button "CheckoutDelivery.popUpOk"
    Then I expect that element "CheckoutDelivery.modalHeader" is not displayed
    And I expect that element "CheckoutDelivery.modalLine1" is not displayed
    And I expect that element "CheckoutDelivery.modalLine2" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverProductFirstMessage" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverProductSecondMessage" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage1" is not displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage2" is not displayed
    And I expect that element "CheckoutPayment.buyNowButton" is enabled

    Examples:
      | dataSeeding                                                   | firstMessageState | secondMessageState |
      | CheckoutDeliveryOptionsUnableToDeliverSingleLine              | displayed         | not displayed      |
      | CheckoutDeliveryOptionsUnableToDeliverSingleLineMultiQuantity | displayed         | not displayed      |
      | CheckoutDeliveryOptionsUnableToDeliverSplitAllError           | displayed         | displayed          |
      | CheckoutDeliveryOptionsUnableToDeliverSplitPartialError       | displayed         | not displayed      |
      | CheckoutDeliveryOptionsUnableToDeliverMultiLine               | displayed         | displayed          |

  ## ToDo - There is a defect https://supergroupbt.atlassian.net/browse/B2C2-4023 which causes the adding of an address to fail because the refresh token times out on Saucelabs
  ## ToDo - The following scenario is in place to allow these scenarios to pass by forcing a refresh and can be removed when the defect is fixed
  ## https://supergroupbt.atlassian.net/browse/B2C2-4023

  @B2C2-186
  Scenario: User should log out and then back in (refresh session)
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    And I seed using the file "CheckoutDeliveryOptionsUnableToDeliverSplitPartialError" and add the items into my "basket" local storage
    And I refresh the page
    And I am on the page "/login"
    When I set "unableToDeliver3@deliveryOptions.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I seed using the file "CheckoutDeliveryOptionsUnableToDeliverMultiLine" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"

  @B2C2-186
  Scenario: Should add a German delivery address for a logged in user
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryAddressHeading"
    And I click on the button "ViewSelectDeliveryAddresses.addAddressButton"
    And I wait on element "ViewSelectDeliveryAddresses.addressLine1" to be displayed
    And I set "DE added" to the inputfield "ViewSelectDeliveryAddresses.addressTitle"
    And I set "Name" to the inputfield "ViewSelectDeliveryAddresses.firstName"
    And I set "Lastname" to the inputfield "ViewSelectDeliveryAddresses.lastName"
    And I set "Knesebeckstraße 28" to the inputfield "ViewSelectDeliveryAddresses.addressLine1"
    And I set "Düren" to the inputfield "ViewSelectDeliveryAddresses.city"
    And I set "Nordrhein-Westfalen" to the inputfield "ViewSelectDeliveryAddresses.county"
    And I set "52355" to the inputfield "ViewSelectDeliveryAddresses.postcode"
    And I scroll to element "ViewSelectDeliveryAddresses.postcode"
    And I click on the button "ViewSelectDeliveryAddresses.countryDropdown"
    And I wait on element "ViewSelectDeliveryAddresses.countryDE" to be displayed
    And I click on the button "ViewSelectDeliveryAddresses.countryDE"
    And I set "010101010101" to the inputfield "ViewSelectDeliveryAddresses.phone"
    And I set "020202020202" to the inputfield "ViewSelectDeliveryAddresses.mobilePhone"
    And I click on the button "ViewSelectDeliveryAddresses.saveButton"
    Then I wait on element "ViewSelectDeliveryAddresses.checkoutHeading" to be displayed

  @B2C2-186
  Scenario: Expect fulfilment error messages after adding a German delivery address
    When I wait on element "CheckoutDelivery.modalHeader"
    Then I expect that element "CheckoutDelivery.modalHeader" is displayed
    And I expect that element "CheckoutDelivery.modalLine1" is displayed
    And I expect that element "CheckoutDelivery.modalLine2" is displayed
    And I click on the button "CheckoutDelivery.popUpOkSorry"
    And I expect that element "CheckoutDelivery.cantDeliverProductFirstMessage" is displayed
    And I expect that element "CheckoutDelivery.cantDeliverProductSecondMessage" is displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage1" is displayed
    And I expect that element "CheckoutDelivery.cantDeliverBuyNowMessage2" is displayed
    And I expect that element "CheckoutPayment.buyNowButton" is not enabled
  
  @B2C2-132
  Scenario: Setup environment with CheckoutDeliveryOptionsHomeSpecificDate
    Given I seed config using the file "CheckoutDeliveryOptionsHomeSpecificDate"
    When I open the siteId "b2c2132"
      And I seed using the file "CheckoutDeliveryOptionsHomeSpecificDate" and add the items into my "basket" local storage

  @B2C2-132
  Scenario: Navigate to checkout and check that there are no dates shown after 7 days of today and that the first available options should be today + transit time
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the 1st element of "CheckoutDelivery.deliveryDays" matches the text "date+1"
      And I expect that the 7th element of "CheckoutDelivery.deliveryDays" matches the text "date+7"

  @B2C2-132
  Scenario: Setup environment with CheckoutDeliveryOptionsHomeSpecificDate2
    Given I seed config using the file "CheckoutDeliveryOptionsHomeSpecificDate"
    When I seed using the file "CheckoutDeliveryOptionsHomeSpecificDate2"

  @B2C2-132
  Scenario: Should check for order placed before cutoff - first available options should be today + transit time
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the 1st element of "CheckoutDelivery.deliveryDays" matches the text "date+2"

  @B2C2-132
  Scenario: Setup environment with CheckoutDeliveryOptionsHomeSpecificDate3
    Given I seed config using the file "CheckoutDeliveryOptionsHomeSpecificDate"
    When I seed using the file "CheckoutDeliveryOptionsHomeSpecificDate3"

  @B2C2-132
  Scenario: Should not show unavailable day in select options
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that the 1st element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 2nd element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 3rd element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 4th element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 5th element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 6th element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
      And I expect that the 7th element of "CheckoutDelivery.deliveryDays" not matches the text "date-1"
    
  @B2C2-132
  Scenario: Should allow the user to select specific day delivery and update order summary
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryOptionsHeading"
      And I click on the button "CheckoutDelivery.specificDayDeliveryButton"
      And I scroll to element "CheckoutDelivery.orderSummary"
      And I wait on element "CheckoutDelivery.orderSummary"
    Then I expect that the attribute "aria-pressed" from element "CheckoutDelivery.specificDayDeliveryButton" is "true"
      And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£7.99"

  @B2C2-132
  Scenario: Should allow the user to select evening delivery and update order summary
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryOptionsHeading"
      And I click on the button "CheckoutDelivery.eveningDeliveryButton"
      And I scroll to element "CheckoutDelivery.orderSummary"
      And I wait on element "CheckoutDelivery.orderSummary"
    Then I expect that the attribute "aria-pressed" from element "CheckoutDelivery.eveningDeliveryButton" is "true"
      And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£10.99"
