@GIFT_CARD
@REGRESSION

Feature: Gift Cards

  Scenario Outline: Verifies <giftCardText> is present on the gift cards page
    Given I am on the page "/giftcards"
    Then I wait on element "GiftCards.<giftCardText>"

    Examples:
      | giftCardText         |
      | shopNowButtonText    |
      | viewBalanceText      |
      | registerYourCardText |
      | findNearestStoreText |
      | lostCardText         |

  @B2C2-918 @B2C2-2727 @B2C2-1707
  Scenario: I seed a page with gift cards
    Given I seed using the file "GiftCards"

  @B2C2-918
  Scenario Outline: Ensures when <GiftCardColours> is selected, <GiftCardText> is displayed
    Given I open the page "/giftcards/buy"
    When I click on the element "GiftCards.<GiftCardColours>"
    And I wait on element "GiftCards.<GiftCardText>" to be displayed
    Then I expect that element "GiftCards.<GiftCardText>" is displayed

    Examples:
      | GiftCardColours      | GiftCardText       |
      | giftCardBlackColour  | blackGiftcardText  |
      | giftCardPinkColour   | pinkGiftcardText   |
      | giftCardOrangeColour | orangeGiftcardText |

  Scenario: Ensures the dedicated website helpline is not displayed on the page
    Then I expect that element "GiftCards.dedicatedHelpline" is not displayed

  #  TODO: Clearing the field does not work at the moment, need to revisit
  #  Scenario Outline: Ensures manual entry validation for <HappyManualAmounts> <UnhappyManualAmounts>
  #    Given I set "<UnhappyManualAmountsBelowTen>" to the inputfield "GiftCards.manualEntryField"
  #    And I click on the element "ProductDescription.addToBagButton"
  #    And I pause for 6000ms
  #    Then I expect that element "GiftCards.errorMessage" is displayed
  #    When I enter "<HappyManualAmounts>" into the prompt
  #
  #    Examples:
  #      | HappyManualAmounts | UnhappyManualAmountsBelowTen | UnhappyAmountsAboveTen |
  #      | 10                 |                              |                        |
  #      |                    | 9                            |                        |
  #      |                    |                              | 501                    |
  #      |                    | -11                          |                        |

  Scenario Outline: Ensures button amounts are clickable
    Then I click on the element "GiftCards.<ButtonAmounts>"
    Examples:
      | ButtonAmounts            |
      | buttonAmountsTen         |
      | buttonAmountsTwenty      |
      | buttonAmountsFifty       |
      | buttonAmountsOneHundred  |
      | buttonAmountsFiveHundred |

  @B2C2-3353
  Scenario Outline: Gift card manual entry validation should be <expectedValidation> for <customPrice>
    Given I am on the page "/giftcards/buy"
    When I set "<customPrice>" to the inputfield "GiftCards.manualEntryField"
    And I click on the element "GiftCards.heading"
    Then I expect that element "GiftCards.manualEntryValidation" is <expectedValidation>
    Examples:
      | customPrice | expectedValidation |
      | 10          | not displayed      |
      | 20.0        | not displayed      |
      | 30.01       | displayed          |
      | 40.1        | displayed          |
      | 444.4444    | displayed          |

  @B2C2-2727
  Scenario: Unable to redeem a giftcard with a giftcard in the basket
    Given I open the page "/giftcards/buy"
    And I set "10" to the inputfield "GiftCards.manualEntryField"
    And I click on the element "ProductDescription.addToBagButton"
    When I open the page "/checkout"
    Then I expect that element "CheckoutRedeemGiftCard.RedeemGiftCardButton" is not displayed

# ---
#
# N.B. UPDATE B2C2-1707 TESTS BELOW
# Currently cannot seed/mock gift cards in order to test 
# e.g. Marking a registered card as lost, no valid giftcards
# This is currently being worked on by Team C
# Been given the ETA of this being done by the 17/12/2021
# 
# This will allow us to implicitly creates a giftcard where:
#   The first digit determines whether or not the card should be created:
#   1 to simulate 404 card not found scenarios,
#   9 to create or recreate a card - useful for tests that need to reset a card back to its original balance and wipe out any transactions against it
#   2-8 for regular gift cards that can only be created registered once against one account.
# And
#   The last five digits of the number represent the balance on the card in pounds and pence, so it goes up to £999.99
# 
# 2000000000000050000 would create a card with a £500 balance.
# ---

@B2C2-1707
Scenario: Logged-In User Has Option to Register Gift Cards and Report Lost Gift Cards
Given I login with the defined customer
Then the element "GiftCards.registerOrLostCardText" is displayed

@B2C2-1707
Scenario: Shows No Gift Cards Registered If Logged-In Customer has No Registered Gift Cards
Given I click on the element "GiftCards.registerOrLostCardText"
Then I expect the url to contain "/my-account/gift-cards"
And the element "GiftCards.noRegisteredGiftCards" is displayed

@B2C2-1707
Scenario: Logged-In User Can Register Gift Cards, View Current Cards and Balances and Report Lost Gift Cards
Given I open the page "/my-account"
When I click on the element "GiftCards.registerOrLostCardText"
Then I expect the url to contain "/my-account/gift-cards"
And the element "GiftCards.registerCardText" is displayed
And the element "GiftCards.registeredCardsTable" is displayed
And the element "GiftCards.cardNumber" is displayed
And the element "GiftCards.balance" is displayed
# Add check to Mark as Lost a Registered Gift Card (Cannot currently because {SEE TO-DO ABOVE})

@B2C2-1707
Scenario: If Invalid Gift Card Registered Displays Invalid Error Message
Given I set "1234567890987654321" to the inputfield "GiftCards.giftCardInput"
When I click on the element "GiftCards.registerCardText"
Then the element "GiftCards.invalidGiftCardInput" is displayed

@B2C2-1707
Scenario: GiftCards Can't Be Registered if On A Site With GiftCards Disabled 
Given I seed config using the file "GiftCards" 
And I open the siteId "b2c21707"
And I seed using the file "GiftCards"
And I refresh the page
And I login with the defined customer
Then the element "GiftCards.registerOrLostCardText" is not displayed

@B2C2-1707
Scenario: GiftCard Page Can't Be Accessed if On A Site With GiftCards Disabled 
Given I open the page "/my-account/gift-cards"
Then I expect the url to contain "/404"


#----------------------------------------------------------------------------------------------------------------
# TO-DO (SEE NOTE PRECEDING B2C2-1707 SCENARIOS)
# https://supergroupbt.atlassian.net/jira/software/c/projects/B2C2/boards/165?modal=detail&selectedIssue=B2C2-1707

# AC: 5
# Given a customer is visiting the /my-account/gift-cards page on a site that has giftcards enabled
# when the customer enters a valid Gift Card Number (including for a Gift Card with zero balance)
# then the customer’s Gift Card is registered to their account
# and the customer is notified of the successful Gift Card registration
# and the customer’s registered Gift Card shall appear listed on the /my-account/gift-cards page.

# AC: 6
# Given a project customer has previously registered a Gift Card to their account
# when the customer visits their /my-account/gift-cards page
# then the customer is presented with a list of their registered Gift Cards
# and the following information is shown for each registered Gift Card:

# Gift Card Number
# Gift Card Balance

# AC: 7
# Given a project customer has previously registered a Gift Card to their account
# and the customer is visiting their /my-account/gift-cards page
# when the customer selects the Lost this card? option for one of their Gift Cards
# then the customer is shown a modal asking them if they are sure

# Should be the same style of notification as the  successful add to wishlist functionality. <== 

# AC: 8
# Given the are you sure modal is being shown
# when the customer selects Yes
# then the modal is closed
# and customer is shown a modal letting them know that a customer service agent shall be in touch
# and the card is marked as Card Reported as Lost on their /my-account/gift-cards page.

# Should be the same style of notification as the  successful add to wishlist functionality.

# AC: 9
# Given the are you sure modal is being shown
# when the customer selects No
# then the modal is closed
# and the card is NOT marked as lost

# AC: 12
# Given a customer is visiting the /my-account/gift-cards page on a site that has giftcards enabled
# and the customer  has previously registered a gift card to their account
# and the customer has subsequently spent all the balance of the registered gift card
# when the customer visits the /my-account/gift-cards page
# then the customer’s spent gift card is presented with a zero balance.
