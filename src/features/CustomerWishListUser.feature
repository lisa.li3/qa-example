@WISH_LIST
@WISH_LIST_USER
@REGRESSION

Feature: Wish List (User)

    @B2C2-1359 @B2C2-1361
    Scenario: Prep (User)
      Given I open the page "/my-account/wish-list"
      And I delete the local storage
      And I seed using the file "CustomerWishList"
      And I refresh the page
      And I login with the defined customer
      And I clear the shopping basket
      And I refresh the page

    @B2C2-1359 @SKIP_MOBILE @SKIP_TABLET
    Scenario: Wishlist navigation icon opens the wishlist
      When I click on the element "Navigation.wishlistButton"
      Then I expect the url to contain "/my-account/wish-list"

    @B2C2-1359 @SKIP_DESKTOP
    Scenario: Wishlist Navigation Opens Wishlist (User)
      And I click on the element "Navigation.burgerButton"
      When I click on the element "NavigationDrawer.wishList"
      Then I expect the url to contain "/my-account/wish-list"
      And I expect that element "Wishlist.Header" becomes displayed

    @B2C2-1361
    Scenario: Out of Stock items (User)
      When I am on the page "/my-account/wish-list"
      And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
      When I refresh the page
      Then I expect the element in "Wishlist.WishlistRows" containing text "Hoodie Four - OutOfStock" to also contain text "Sorry, this item is currently out of stock."

    ## Product Detail Page
    @B2C2-1361
    Scenario: Add one Item to wishlist (User)
      Given I clear the wishlist
      And I open the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I wait on element "ProductDescription.addToWishlistButton" to be displayed
      And I scroll to element "ProductDescription.addToWishlistButton"
      And I click on the element "ProductDescription.addToWishlistButton"
      When I open the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed

    @B2C2-1361
    Scenario: Items added to wishlist show remove button (User)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I wait on element "ProductDescription.removeFromWishlistButton" to be displayed
      Then I expect that element "ProductDescription.removeFromWishlistButton" is displayed

    @B2C2-1361
    Scenario: Clicking remove button removes the item from the wishlist (User)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      When I click on the element "ProductDescription.removeFromWishlistButton"
      And I wait on element "ProductDescription.removeFromWishlistButton" to not be displayed
      And I open the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is not displayed

    @B2C2-1361
    Scenario: Adding an item shows the popup (User)
      Given I open the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.addToWishlistButton"
      When I click on the element "ProductDescription.addToWishlistButton"
      Then I wait on element "Wishlist.AddConfirmed" to be displayed
      And I expect that element "Wishlist.AddConfirmed" is displayed

    @B2C2-1361
    Scenario: Removing an item shows the popup (User)
      Given I open the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I wait on element "ProductDescription.removeFromWishlistButton" to be displayed
      And I scroll to element "ProductDescription.removeFromWishlistButton"
      When I click on the element "ProductDescription.removeFromWishlistButton"
      Then I wait on element "Wishlist.RemoveConfirmed" for 3000ms to be displayed
      And I expect that element "Wishlist.RemoveConfirmed" is displayed

    @B2C2-1359
    Scenario: Clicking Wishlist Item Added Popup Opens Wishlist (User)
      Given I open the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.addToWishlistButton"
      And I click on the element "ProductDescription.addToWishlistButton"
      And I wait on element "ProductDescription.removeFromWishlistButton" to be displayed
      When I click on the element "Wishlist.AddConfirmed"
      Then I expect the url to contain "/my-account/wish-list"
      And I expect that element "Wishlist.Header" becomes displayed

    @B2C2-1361
    Scenario: Adding an item with multiple sizes shows modal (User)
      Given I clear the wishlist
      And I wait for the API call to return
      And I expect the count of elements in "Wishlist.WishlistRows" to equal 0
      And I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL001/wishlist-hoodie-one-multiple-sizes-white"
      And I wait on element "ProductDescription.addToWishlistButton" to be displayed
      And I scroll to element "ProductDescription.addToWishlistButton"
      When I click on the element "ProductDescription.addToWishlistButton"
      Then I expect that element "ProductDescription.sizeModal" is displayed

    @B2C2-1361
    Scenario: Adding an sized item shows correct size in wish list (User)
      Given I wait on element "ProductDescription.sizeModal" to be displayed
      And I wait on element "ProductDescription.modalSizeDropdown" to be displayed
      When I select the option with the text "m" for element "ProductDescription.modalSizeDropdown"
      And I click on the element "ProductDescription.modalWishListButton"
      And I wait on element "ProductDescription.sizeModal" to not be displayed
      And I open the page "/my-account/wish-list"
      And I expect the element in "Wishlist.WishlistRows" containing text "Hoodie One - Multiple Sizes" to also contain text "Size: M"

    @B2C2-1361
    Scenario: Removing items from the wishlist removes the correct items  - prep (User)
      Given I am on the page "/my-account/wish-list"
      And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
      When I refresh the page
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed
      And I expect the count of elements in "Wishlist.WishlistRows" to equal 6

      #    @B2C2-1361
#    Scenario: Wishlist Items link to Product Page
#      Given I delete the local storage
#      And  I login with "wishlistuser1@project.local" and "its-a-secret"
#      Given I am on the page "/my-account/wish-list"
#      When I click on the element "Wishlist.RowFour"
#      Then I pause for 1000ms
#      Then I expect the url to contain "CWL004"

    @B2C2-1361
    Scenario: Removing items from the wishlist removes the correct items (User)
      Given I am on the page "/my-account/wish-list"
      And I wait on element "Wishlist.SecondSeededProduct" to be displayed
      When I scroll to element "Wishlist.SecondSeededProductRemoveButton"
      And I click on the element "Wishlist.SecondSeededProductRemoveButton"
      And I wait on element "Wishlist.SecondSeededProduct" to not be displayed
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 5
      And I expect that element "Wishlist.SecondSeededProduct" is not displayed

    @B2C2-1361
    Scenario: Signin/Register Prompt not visible (User)
      Given I am on the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SignInOrRegister" is not displayed

    @B2C2-1361
    Scenario: Remove Item from Wishlist displays the Popup (User)
      Given I open the page "/my-account/wish-list"
      When I click on the element "Wishlist.RemoveFromWishlist"
      Then I wait on element "Wishlist.RemoveConfirmed" to be displayed
      And I expect that element "Wishlist.RemoveConfirmed" is displayed

    @B2C2-1361
    Scenario: Shows Move to Shopping Basket Button (User)
      Given I am on the page "/my-account/wish-list"
      And I clear the wishlist
      And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
      When I refresh the page
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 6
      And I expect that element "Wishlist.SecondSeededProduct" is displayed

    @B2C2-1361
    Scenario: Move Wishlist Item to Shopping Bag (User)
      Given I am on the page "/my-account/wish-list"
      When I click on the element "Wishlist.SecondSeededProductMoveToShoppingButton"
      And I wait on element "Wishlist.SecondSeededProduct" to not be displayed
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 5
      And I wait for the API call to return
      And I expect that element "Navigation.basketButtonIconText" contains the text "1"

    @B2C2-1361
    Scenario: Move All Wishlist Items to Shopping Bag (User)
      Given I am on the page "/my-account/wish-list"
      And I scroll to element "Wishlist.MoveAllToShoppingBag"
      When I click on the element "Wishlist.MoveAllToShoppingBag"
      And I wait on element "Wishlist.MoveAllToShoppingBag" to not be displayed
       # All items except the one out of stock item is moved over
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 1
      And I wait for the API call to return
      And I expect that element "Navigation.basketButtonIconText" contains the text "5"

    @B2C2-1361
    Scenario: Move All Wishlist Items to Shopping Bag doesn't move out of stock items (User)
      Given I am on the page "/my-account/wish-list"
      Then I expect the element in "Wishlist.WishlistRows" containing text "Hoodie Four - OutOfStock" to also contain text "Sorry, this item is currently out of stock."

    @B2C2-1361
    Scenario: Logged In Wishlist is initially Empty
      And I am on the page "/my-account/wish-list"
      When I clear the wishlist
      Then I expect that element "Wishlist.WishlistEmpty" is displayed

    @B2C2-1367
    Scenario: Adding a wishlist item works whilst logged in
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I wait on element "ProductDescription.addToWishlistButton" to be displayed
      And I scroll to element "ProductDescription.addToWishlistButton"
      When I click on the element "ProductDescription.addToWishlistButton"
      And I am on the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed

    @B2C2-1367Y
    Scenario: Wishlist items persists across sessions
      Given  I am on the page "/my-account"
      When I click on the element "MyAccount.logOutButton"
      And I delete the local storage
      And I login with the defined customer
      And I am on the page "/my-account/wish-list"
      Then I wait on element "Wishlist.SecondSeededProduct" to be displayed
      And I expect that element "Wishlist.SecondSeededProduct" is displayed

    @B2C2-1367
    Scenario: Wishlist items are removed on logout
      And  I am on the page "/my-account"
      When I click on the element "MyAccount.logOutButton"
      And I am on the page "/my-account/wish-list"
      Then I expect that element "Wishlist.WishlistEmpty" is displayed

    @B2C2-1366 @B2C2-1367
    Scenario: Wishlist Populated By Guest Wishlist when Logged In
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I wait on element "ProductDescription.addToWishlistButton" to be displayed
      And I scroll to element "ProductDescription.addToWishlistButton"
      And I click on the element "ProductDescription.addToWishlistButton"
      And I am on the page "/my-account/wish-list"
      And I wait on element "Wishlist.SecondSeededProduct" to be displayed
      And I login with the defined customer
      And I am on the page "/my-account/wish-list"
      Then I wait on element "Wishlist.SecondSeededProduct" to be displayed

    @B2C2-1367Y
    Scenario: Once I have logged in with a wishlist it should persist
      Given  I am on the page "/my-account"
      When I click on the element "MyAccount.logOutButton"
      And I delete the local storage
      And I login with the defined customer
      And I am on the page "/my-account/wish-list"
      Then I wait on element "Wishlist.SecondSeededProduct" to be displayed
      And I expect that element "Wishlist.SecondSeededProduct" is displayed