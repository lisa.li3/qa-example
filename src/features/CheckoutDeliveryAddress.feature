@CHECKOUT
@DELIVERY
@DELIVERY_ADDRESS
@REGRESSION_FLAKY

Feature: Checkout Delivery Address

  @B2C2-2723
  Scenario: Set-Up for Validation Tests
    Given I open the page "/"
    And I seed using the file "CheckoutProduct" and add the items into my "basket" local storage

  @B2C2-2723
  Scenario Outline: Validate delivery phone number length error message
      Given I am on the page "/checkout"
      When I click on the element "CheckoutDelivery.phoneTextbox"
      And I set "<phoneFieldInput>" to the inputfield "CheckoutDelivery.phoneTextbox"
      Then I click on the element "CheckoutDelivery.emailAddressTextbox"
      And I expect that element "AccountInformation.phoneNumberLongWarning" is <validationErrorStatus>

    Examples:
      | phoneFieldInput                                   | validationErrorStatus |
      | 11-12323345                                       | not displayed         |
      | 44-87647283748593748593849583748594837462731      | not displayed         |
      | 50-87647283748593748593849583748594837462731724354| displayed             |

  @B2C2-2723
  Scenario Outline: Validate delivery phone number characters
      Given I click on the element "CheckoutDelivery.phoneTextbox"
      When I set "<phoneFieldInput>" to the inputfield "CheckoutDelivery.phoneTextbox"
      And I click on the element "CheckoutDelivery.emailAddressTextbox"
      Then I expect that element "CheckoutDelivery.phoneNumberValueTypeWarning" is <expectedResult>
      And I clear the inputfield "CheckoutDelivery.phoneTextbox"

    Examples:
      | phoneFieldInput    | expectedResult |
      | 07555666777        | not displayed  |
      | +447555666777      | not displayed  |
      | #78483             | not displayed  |
      | [989               | not displayed  |
      | (0985()            | not displayed  |
      | []098765678        | not displayed  |
      | *3429              | not displayed  |
      | notAPhoneNumber    | displayed      |
      | /78975745          | displayed      |
      | <07555666777>      | displayed      |
      | :0755685           | displayed      |
      | $0767767           | displayed      |
      | %456               | displayed      |
      | -07456             | displayed      |
    
  @B2C2-191
  Scenario: Setup environment with CheckoutDeliveryAddress1country
    Given I seed config using the file "CheckoutDeliveryAddress1country"
    When I open the siteId "b2c2191"
      And I seed using the file "CheckoutDeliveryAddress" and add the items into my "basket" local storage

  @B2C2-191
  Scenario: Should validate mandatory fields
    Given I open the page "/checkout"
    Then I expect that element "CheckoutDeliveryAddress.firstNameTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.lastNameTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.phoneTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.emailAddressTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.confirmEmailAddressTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.countryComboBox" is displayed

  @B2C2-191
  Scenario: Should validate that the right fields appear when customer selects to manually enter their address
    Then I expect that element "CheckoutDeliveryAddress.address1Textbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.address2Textbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.cityTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.stateProvinceRegionTextbox" is displayed
      And I expect that element "CheckoutDeliveryAddress.postcodeTextbox" is displayed
      And I expect the count of elements in "CheckoutDeliveryAddress.allRequiredAlerts" to equal 8

  @B2C2-191
  Scenario: Should check if the user enters an invalid email address
    When I set "abc" to the inputfield "CheckoutDeliveryAddress.emailAddressTextbox"
      And I click on the element "CheckoutDeliveryAddress.confirmEmailAddressTextbox"
    Then I expect that element "CheckoutDeliveryAddress.invalidEmailMessage" is displayed

  @B2C2-191
  Scenario: Should display a message when the confirmation email does not match the email address
    When I set "checkoutdeliveryaddress@project.local" to the inputfield "CheckoutAddress.emailAddressTextbox"
      And I set "checkoutdeliveryaddress@project.loca" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
    Then I expect that element "CheckoutDeliveryAddress.emailNotMatch" is displayed

  @B2C2-191
  Scenario: The message should disappear when the confirmation email matches the email address
    When I click on the element "CheckoutAddress.confirmEmailAddressTextbox"
      And I set "checkoutdeliveryaddress@project.local" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
    Then I expect that element "CheckoutDeliveryAddress.emailNotMatch" becomes not displayed

  @B2C2-191
  Scenario: Should show GDPR message next to the phone number
    When I click on the element "CheckoutAddress.phoneTextbox"
    Then I expect that element "CheckoutDeliveryAddress.phoneGDPR" is displayed

  @B2C2-191 @B2C2-3665
  Scenario: Setup environment with CheckoutDeliveryAddress5countries
    Given I seed config using the file "CheckoutDeliveryAddress5countries"
    When I open the siteId "b2c2191"
      And I seed using the file "CheckoutDeliveryAddress" and add the items into my "basket" local storage

  @B2C2-191 @B2C2-3665
  Scenario: Should validate country display as UK default
    Given I open the page "/checkout"
    Then I expect that element "United Kingdom" is displayed

  @B2C2-191
  Scenario: Should validate that the selected delivery country is France
    When I click on the button "CheckoutDeliveryAddress.countrySelect"
      And I click on the button "France"
      And I click on the button "CheckoutDeliveryAddress.popUpOk"
    Then I expect that element "France" is displayed

  @B2C2-191
  Scenario: Should validate that the selected delivery country is Germany
    When I click on the button "CheckoutDeliveryAddress.countrySelect"
      And I click on the button "Germany"
      And I click on the button "CheckoutDeliveryAddress.popUpOk"
    Then I expect that element "Germany" is displayed

  @B2C2-191
  Scenario: Should validate that the selected delivery country is Spain
    When I click on the button "CheckoutDeliveryAddress.countrySelect"
      And I click on the button "Spain"
      And I click on the button "CheckoutDeliveryAddress.popUpOk"
    Then I expect that element "Spain" is displayed

   @B2C2-3665 @B2C2-191
   Scenario: Should validate that the selected delivery country is United States
     When I click on the button "CheckoutDeliveryAddress.countrySelect"
       And I click on the button "United States"
       And I click on the button "CheckoutDeliveryAddress.popUpOk"
     Then I expect that element "United States" is displayed

   @B2C2-3665 @B2C2-191 @REGRESSION_FLAKY @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
   Scenario: Should validate that when USA is the selected delivery country, then the STATE/PROVINCE/REGION field is a single-select drop-down that lists all USA states in alphabetical order
       When I scroll to element "CheckoutDeliveryAddress.cityTextbox"
       And I click on the button "CheckoutDeliveryAddress.stateSelect"
       Then I wait on element "CheckoutDeliveryAddress.getStates" to be displayed
       And I expect that the values returned by "CheckoutDeliveryAddress.states" to be in the following order
             """
             Select
             Alabama
             Alaska
             Arizona
             Arkansas
             California
             Colorado
             Connecticut
             Delaware
             District Of Columbia
             Florida
             Georgia
             Hawaii
             Idaho
             Illinois
             Indiana
             Iowa
             Kansas
             Kentucky
             Louisiana
             Maine
             Maryland
             Massachusetts
             Michigan
             Minnesota
             Mississippi
             Missouri
             Montana
             Nebraska
             Nevada
             New Hampshire
             New Jersey
             New Mexico
             New York
             North Carolina
             North Dakota
             Ohio
             Oklahoma
             Oregon
             Pennsylvania
             Rhode Island
             South Carolina
             South Dakota
             Tennessee
             Texas
             Utah
             Vermont
             Virginia
             Washington
             West Virginia
             Wisconsin
             Wyoming
             """
     And I click on the element "California"

  @B2C2-3665
  Scenario: Should validate that when USA is NOT the selected country, then the STATE/PROVINCE/REGION field is a non mandatory textbox and US state selection should not be preserved
     When I click on the button "CheckoutDeliveryAddress.countrySelect"
     And I click on the button "United Kingdom"
     And I click on the button "CheckoutDeliveryAddress.popUpOk"
     Then I expect that element "United Kingdom" is displayed
     And I scroll to element "CheckoutDeliveryAddress.cityTextbox"
     And I expect that element "ViewSelectDeliveryAddresses.county" is displayed
     And I expect that element "ViewSelectDeliveryAddresses.county" not contains any text
     And I set "36characterTestInputIsTooLongXXXXXXX" to the inputfield "ViewSelectDeliveryAddresses.county"
     And I click on the element "Order Summary"
     And I expect that element "CheckoutBillingAddress.stateErrorAlert" is displayed
     And I set "36characterTestInputIsTooLongXXXXXX" to the inputfield "ViewSelectDeliveryAddresses.county"
     And I click on the element "Order Summary"
     And I expect that element "CheckoutBillingAddress.stateErrorAlert" is not displayed

  Rule: I WANT to be able to look up my address in the delivery address section in the checkout

    @B2C2-192
    Scenario: Seed a site with ADDRESS LOOKUP ENABLED and open the site
      Given I seed config using the file "CheckoutLoqateDeliveryAddressLookupOn"
      And I open the siteId "deliveryloqate-on"

    @B2C2-192
    Scenario: Seed the product data for the GUEST user and go to checkout
      Given I seed using the file "CheckoutLoqateDeliveryAddressLookup" and add one of each item into my "basket" local storage
      When I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" becomes displayed

    @B2C2-192
    Scenario: Upload the 'Address-Lookup' mock files to S3
      Given I upload the "mocks/loqate/text/a.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/a.json"
      And I upload the "mocks/loqate/text/ab.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/ab.json"
      And I upload the "mocks/loqate/text/abc.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/abc.json"
      And I upload the "mocks/loqate/text/g.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/g.json"
      And I upload the "mocks/loqate/text/gl.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl.json"
      And I upload the "mocks/loqate/text/gl2.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl2.json"
      And I upload the "mocks/loqate/text/gl20.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20.json"
      And I upload the "mocks/loqate/text/gl20a.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20a.json"
      And I upload the "mocks/loqate/text/gl20aa.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa.json"
      And I upload the "mocks/loqate/text/gl20aa7.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa7.json"
      And I upload the "mocks/loqate/text/gl20aa70.json" file to the S3 bucket "address-lookup-service" with path "GBR/Text/gl20aa70.json"
      And I upload the "mocks/loqate/Id/GB|RM|B|1612492.json" file to the S3 bucket "address-lookup-service" with path "Id/GB|RM|B|1612492.json"
      And I upload the "mocks/loqate/container/GB|RM|ENG|0AA-GL2.json" file to the S3 bucket "address-lookup-service" with path "GBR/Container/GB|RM|ENG|0AA-GL2.json"

    @B2C2-192
    Scenario: Check that autocomplete is switched off for the 'Loqate Delivery Address Search' field
      When I scroll to element "CheckoutAddress.addressTextbox"
      Then I expect that the attribute "autocomplete" from element "CheckoutAddress.addressTextbox" is "off"

    @B2C2-192
    Scenario:  Check that clicking the 'Buy now button' without entering anything into the address finder triggers an error on the address finder field
      Given I click on the button "CheckoutPayment.buyNowButton"
      Then I expect that element "CheckoutAddress.deliveryAddressFinderError" is displayed

    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline: Start typing an address in the delivery address finder field
      When I add "<inputLetter>" to the inputfield "CheckoutAddress.addressTextbox"
      Then the element "CheckoutAddress.addressTextbox" contains the text "<typedLetters>"
      And I pause for 500ms

      Examples:
        | inputLetter | typedLetters |
        | g           | g            |
        | l           | gl           |
        | 2           | gl2          |
        | 0           | gl20         |
        | a           | gl20a        |

    @B2C2-192
    Scenario:  Check the number of addresses returned in the delivery address finder dropdown is 10 in descending order of relevance
      Given the element "CheckoutAddress.deliveryManualAddressEntryButton" is displayed
      Then I expect that the values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        GL2 0AA Church Road, Gloucester - 18 Addresses
        GL2 0AB Church Road, Gloucester - 20 Addresses
        GL2 0AD Wedgwood Drive, Gloucester - 46 Addresses
        GL2 0AE Church Road, Gloucester - 7 Addresses
        GL2 0AF Allendale Close, Gloucester - 11 Addresses
        GL2 0AG College Fields, Gloucester - 24 Addresses
        GL2 0AH Church Road, Gloucester - 15 Addresses
        GL2 0AJ Church Road, Gloucester - 8 Addresses
        Longlevens Junior School, Church Road Longlevens, Gloucester, GL2 0AL
        GL2 0AN Old Cheltenham Road, Gloucester - 13 Addresses
        """

    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline:  Start typing an address in the delivery address finder field and check that the suggested addresses update
      Given  I add "<typedValue>" to the inputfield "CheckoutAddress.addressTextbox"
      Then the element "CheckoutAddress.addressTextbox" contains the text "<typedValue>"
      And I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        <firstAddressInList>
        <secondAddressInList>
        """

      Examples:
        | typedValue | firstAddressInList                                     | secondAddressInList                                     |
        | a          | A, Selborne Mansions, Selborne Mount Bradford, BD9 4NP | A, Dalemoor, Mytholmes Lane Haworth, Keighley, BD22 8EZ |
        | b          | A-B, Sneyd Lodge, 80 Olive Road London, NW2 6UL        | AB10 1AB Broad Street, Aberdeen - 6 Addresses           |
        | c          | A B C, 618 Lordship Lane London, N22 5JH               | A B C, 5 Hutton Street Boldon Colliery, NE35 9LW        |

    @B2C2-192
    Scenario: Refresh the page
      When I refresh the page

    @B2C2-192
    Scenario Outline: Start typing an address in the delivery address finder field
      When I add "<inputLetter>" to the inputfield "CheckoutAddress.addressTextbox"
      Then the element "CheckoutAddress.addressTextbox" contains the text "<typedLetters>"
      And I pause for 500ms

      Examples:
        | inputLetter | typedLetters |
        | g           | g            |
        | l           | gl           |
        | 2           | gl2          |
        | 0           | gl20         |
        | a           | gl20a        |
        | a           | gl20aa       |

    @B2C2-192
    Scenario:  Start typing an address in the address finder field and select an address option with multiple variations
      When I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
      Then I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        70A, Church Road Longlevens, Gloucester, GL2 0AA
        76A, Church Road Longlevens, Gloucester, GL2 0AA
        """
      When I add "7" to the inputfield "CheckoutAddress.addressTextbox"
      And I add "0" to the inputfield "CheckoutAddress.addressTextbox"
      Then the element "CheckoutAddress.addressTextbox" contains the text "gl20aa70"
      And I expect that the first 2 values returned by "CheckoutAddress.suggestedAddressListItemsText" to be in the following order
        """
        70 Church Road Longlevens, Gloucester, GL2 0AA
        70A, Church Road Longlevens, Gloucester, GL2 0AA
        """

    @B2C2-192
    Scenario:  Select a delivery country other than the default and check that the 'DELIVERY ADDRESS FINDER' is not displayed (Delivery manual address entry only)
      When I refresh the page
      And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
      And I click on the button "France"
      Then the element "CheckoutDelivery.importantNoticeHeading" is displayed
      And I click on the button "CheckoutDelivery.popUpOk"
      And I scroll to element "CheckoutAddress.country"
      Then I expect that element "ViewSelectDeliveryAddresses.addressLine1" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addressLine2" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.county" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.postcode" is displayed
      And I expect that element "CheckoutAddress.deliveryManualAddressEntryButton" is not displayed
      And I expect that element "CheckoutAddress.deliveryFindYourAddressButton" is not displayed

    @B2C2-192
    Scenario:  Select the default delivery country and check that the 'ADDRESS FINDER' is displayed (manual address entry button is shown)
      When I refresh the page
      And I click on the button "CheckoutDelivery.defaultDeliveryCountry"
      And I click on the button "United Kingdom"
      And I click on the button "CheckoutDelivery.popUpOk"
      And I scroll to element "CheckoutAddress.country"
      Then I expect that element "CheckoutAddress.deliveryAddressFinder" is displayed
      And I expect that element "ViewSelectDeliveryAddress.postcode" is not displayed
      And I expect that element "CheckoutAddress.deliveryManualAddressEntryButton" is displayed

    @B2C2-192
    Scenario:  Use the Delivery Address finder, select an address and check it is input into the form correctly and the Delivery 'FIND YOUR ADDRESS' button is displayed
      When I refresh the page
      And I set "a" to the inputfield "CheckoutAddress.addressTextbox"
      And I select the 1st option for element "CheckoutAddress.suggestedAddressListItems"
      Then I expect that element "CheckoutAddress.deliveryFindYourAddressButton" is displayed
      And the element "ViewSelectDeliveryAddresses.addressLine1" contains the text "A, Selborne Mansions"
      And the element "ViewSelectDeliveryAddresses.addressLine2" contains the text "Selborne Mount"
      And the element "ViewSelectDeliveryAddresses.city" contains the text "Bradford"
      And the element "ViewSelectDeliveryAddresses.county" contains the text "West Yorkshire"
      And the element "ViewSelectDeliveryAddresses.postcode" contains the text "BD9 4NP"

    @B2C2-192
    Scenario: Seed a site with ADDRESS LOOKUP Disabled and open the site
      Given I seed config using the file "CheckoutLoqateDeliveryAddressLookupOff"
      And I open the siteId "deliveryloqate-off"

    @B2C2-192
    Scenario: Seed the product data for the GUEST user and go to checkout
      Given I seed using the file "CheckoutLoqateDeliveryAddressLookup" and add one of each item into my "basket" local storage
      When I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.total" becomes displayed

    @B2C2-192
    Scenario: When ADDRESS LOOKUP is Disabled, Check that the Loqate Delivery Address finder is not displayed and the Delivery manual address entry form is displayed
      When I scroll to element "CheckoutAddress.country"
      Then I expect that element "ViewSelectDeliveryAddresses.addressLine1" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addressLine2" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.county" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.postcode" is displayed
      And I expect that element "CheckoutAddress.deliveryManualAddressEntryButton" is not displayed
      And I expect that element "CheckoutAddress.deliveryFindYourAddressButton" is not displayed

    @B2C2-192
    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

    @B2C2-515
    Scenario: Setup environment with CheckoutDeliveryAddressUnsupportedPartial
      Given I seed config using the file "CheckoutDeliveryAddress1country"
      When I open the siteId "b2c2191"
      And I seed using the file "CheckoutDeliveryAddressUnsupportedPartial" and add the items into my "basket" local storage


    @B2C2-515
    Scenario: Error messages should be displayed when user has some some delivery addresses which are unsupported by a shopper group
      Given I am on the page "/checkout/login"
      When I set "checkoutdeliveryaddress@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"

      #TODO: Below line is a workaround for an issue: https://supergroupbt.atlassian.net/browse/B2C2-4357
      And I click on the element "CheckoutRedeemGiftCard.NeedHelpPopupCloseButton"

      And I wait on element "CheckoutDelivery.homeDeliveryRadio"
      Then I expect that element "CheckoutDeliveryAddress.cantDeliver2Addresses" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addAddressButton" is displayed
      And I expect that element "CheckoutDeliveryAddress.validUKaddress" is displayed
      And the element "CheckoutDeliveryAddress.validUKaddressRadio" is enabled
      And I expect that element "Show" is displayed
      And I expect that element "Hide" is not displayed
      And I click on the element "CheckoutDeliveryAddress.cantDeliver2Addresses"
      And I expect that element "Show" is not displayed
      And I expect that element "Hide" is displayed
      And I expect that element "CheckoutDeliveryAddress.cantDeliverContentOpen" is displayed
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddress" contains the text "Foo Bar"
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddress" contains the text "Flat 1, Bar Road, Foobar, county, 1233321, France"
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddressRadio" is not enabled
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddress" contains the text "Foo Bar"
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddress" contains the text "Flat 1, Bar Road, Foobar, county, 85535, Poland"
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddressRadio" is not enabled
      And I expect that element "CheckoutDeliveryAddress.cantDeliverErrorMessage" does appear exactly "2" times
      And I click on the element "CheckoutDeliveryAddress.cantDeliver2Addresses"
      And I expect that element "Show" is displayed
      And I expect that element "Hide" is not displayed

    @B2C2-515
    Scenario: Log the user out
      Given I am on the page "/my-account"
      When I click on the element "MyAccount.logOutButton"

    @B2C2-515
    Scenario: Setup environment with CheckoutDeliveryAddressUnsupportedPartial
      Given I seed config using the file "CheckoutDeliveryAddress1country"
      When I open the siteId "b2c2191"
      And I seed using the file "CheckoutDeliveryAddressUnsupportedAll" and add the items into my "basket" local storage


    @B2C2-515
    Scenario: Error messages should be displayed if all user's delivery addresses are unsupported by a shopper group
      Given I am on the page "/checkout/login"
      When I set "checkoutdeliveryaddress@unsupported.address" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"

      #TODO: Below line is a workaround for an issue: https://supergroupbt.atlassian.net/browse/B2C2-4357
      And I click on the element "CheckoutRedeemGiftCard.NeedHelpPopupCloseButton"

      And I wait on element "CheckoutDelivery.homeDeliveryRadio"
      Then I expect that element "CheckoutDeliveryAddress.cantDeliver3Addresses" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addAddressButton" is displayed
      And I expect that element "Show" is displayed
      And I expect that element "Hide" is not displayed
      And I click on the element "CheckoutDeliveryAddress.cantDeliver3Addresses"
      And I expect that element "Show" is not displayed
      And I expect that element "Hide" is displayed
      And I expect that element "CheckoutDeliveryAddress.cantDeliverContentOpen" is displayed
      And the element "CheckoutDeliveryAddress.cantDeliverITaddress" contains the text "Foo Bar"
      And the element "CheckoutDeliveryAddress.cantDeliverITaddress" contains the text "Flat 1, Bar Road, Foobar, county, 111111, Italy"
      And the element "CheckoutDeliveryAddress.cantDeliverITaddressRadio" is not enabled
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddress" contains the text "Foo Bar"
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddress" contains the text "Flat 1, Bar Road, Foobar, county, 1233321, France"
      And the element "CheckoutDeliveryAddress.cantDeliverFRAaddressRadio" is not enabled
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddress" contains the text "Foo Bar"
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddress" contains the text "Flat 1, Bar Road, Foobar, county, 85535, Poland"
      And the element "CheckoutDeliveryAddress.cantDeliverPOLaddressRadio" is not enabled
      And I expect that element "CheckoutDeliveryAddress.cantDeliverErrorMessage" does appear exactly "3" times
      And I click on the element "CheckoutDeliveryAddress.cantDeliver3Addresses"
      And I expect that element "Show" is displayed
      And I expect that element "Hide" is not displayed

          #TODO: Below test is skipped due to an issue: https://supergroupbt.atlassian.net/browse/B2C2-4360
    @B2C2-515 @DEFECT @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
    Scenario: Clicking BUY NOW with no valid delivery address scrolls the page up to delivery address section
      and error message is displayed
      When I scroll to element "CheckoutPayment.buyNowButton"
      And I click on the element "CheckoutPayment.buyNowButton"
      And I expect that element "CheckoutDeliveryAddress.cantDeliver3Addresses" is within the viewport
      And I expect that element "Please provide a valid delivery address" is displayed

