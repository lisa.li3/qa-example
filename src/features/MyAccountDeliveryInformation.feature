@ACCOUNT
@REGRESSION

Feature: My Account - Delivery Information
  B2C2-1223- Add, Update and remove delivery address

  @B2C2-1223 @B2C2-2929 @B2C2-3862
  Scenario: Setup for my account section
    Given I seed using the file "MyAccount"
    And I open the page "/login"
    When I set "myaccount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"

  @B2C2-1223 @B2C2-2929
  Scenario: Customer is directed to manage delivery address page
    Given I wait on element "MyAccount.deliveryInformationButton"
    When I click on the element "MyAccount.deliveryInformationButton"
    Then the element "DeliveryInformation.addNewAddressButton" is displayed

  @B2C2-3862
  Scenario Outline: Address Nickname Validation
    Given I am on the page "/my-account/delivery-addresses/account-shipto/"
    When I set "<addressTitle>" to the inputfield "CheckoutAddress.addressTitle"
    And I click on the element "CheckoutAddress.firstNameTextbox"
    Then I expect that element "CheckoutAddress.inputTooLongWarning" is <displayedStatus>


    Examples:
      | addressTitle                      | displayedStatus |
      | 32---------max_cap_of_characters  | not displayed   |
      | 33------------too_many_characters | displayed       |


  @B2C2-1223
  Scenario: Customer can add delivery information
    Given I am on the page "/my-account/delivery-addresses/account-shipto/"
    And I set "Test1223 Nickname" to the inputfield "CheckoutAddress.addressTitle"
    And I set "Bob" to the inputfield "CheckoutAddress.firstNameTextbox"
    And I set "Dylan" to the inputfield "CheckoutAddress.lastNameTextbox"
    And I set "Flat 4, Copper court" to the inputfield "CheckoutAddress.address1Textbox"
    And I set "23 Kings Road" to the inputfield "CheckoutAddress.address2Textbox"
    And I set "London" to the inputfield "CheckoutAddress.cityTextbox"
    And I set "London" to the inputfield "CheckoutAddress.stateProvinceRegionTextbox"
    And I set "W14 9YA" to the inputfield "CheckoutAddress.postcodeTextbox"
    And I select the option with the text "United Kingdom" for element "Registration.countryComboBox"
    And I set "2342342342234" to the inputfield "CheckoutAddress.phoneTextbox"
    And I set "123124123123" to the inputfield "CheckoutAddress.mobilPhoneTextbox"
    And I click on the element "CheckoutAddress.saveButton"
    Then I expect that element "Test1223 Nickname" is displayed

  @B2C2-1223 @B2C2-2929
  Scenario: Customer can update delivery information
    Given I am on the page "/my-account/delivery-addresses"
    When I click on the element "DeliveryInformation.addressTestNickname"
    And I set "New Nickname" to the inputfield "CheckoutAddress.addressTitle"
    And I set "GL51 9Na" to the inputfield "DeliveryInformation.postcodeTextbox"
    When I click on the element "CheckoutAddress.saveButton"
    Then I expect that element "New Nickname" is displayed

  @B2C2-1223
  Scenario: Customer can delete delivery information
    Given I seed using the file "MyAccount"
    And I open the page "/my-account/delivery-addresses"
    And I click on the element "DeliveryInformation.addressTestNickname"
    When I click on the element "CheckoutAddress.deleteButton"
    Then I expect that element "New Nickname" is not displayed

  @B2C2-1223
  Scenario: Prepare for Scenario Outline by navigating to page and clicking to manually enter address and clicking save
    Given I open the page "/my-account/delivery-addresses/account-shipto/"
    When I click on the element "CheckoutAddress.saveButton"

  @B2C2-1223
  Scenario Outline: Customer is notified of delivery address required fields - <error>
    Then I expect that element "CheckoutAddress.<error>" matches the text "REQUIRED"

    Examples:
      | error             |
      | addressTitleError |
      | firstNameErrors   |
      | lastNameErrors    |
      | address1Error     |
      | cityError         |
      | postcodeError     |
      | countryError      |
      | phoneError        |

  @B2C2-1223
  #Todo enable poscode error test once B2C2-3596 is fixed
  Scenario Outline: Customer is presented with <displayedError> upon entering  <invalidInputValue>
    And I set "<invalidInputValue>" to the inputfield "<InputField>"
    When I click on the element "CheckoutAddress.saveButton"
    Then I expect that element "CheckoutAddress.<elementError>" matches the text "<displayedError>"

    Examples:
      | invalidInputValue                    | InputField                        | elementError      | displayedError         |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa    | CheckoutAddress.addressTitle      | addressTitleError | TOO LONG               |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa    | CheckoutAddress.firstNameTextbox  | firstNameErrors   | TOO LONG               |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa    | CheckoutAddress.lastNameTextbox   | lastNameErrors    | TOO LONG               |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | CheckoutAddress.address1Textbox   | address1Error     | TOO LONG               |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | CheckoutAddress.cityTextbox       | cityError         | TOO LONG               |
      #| aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa    | CheckoutAddress.postcodeTextbox   |postcodeError     | TOO LONG              |
      | abc£$                                | CheckoutAddress.phoneTextbox      | phoneError        | MUST BE A PHONE NUMBER |
      | %$^abc                               | CheckoutAddress.mobilPhoneTextbox | mobileError       | MUST BE A PHONE NUMBER |

  @B2C2-3665
  Scenario: Setup environment with CheckoutDeliveryAddress
    Given I seed config using the file "CheckoutDeliveryAddress"
    When I open the siteId "deliveryaddress"
    And I seed using the file "CheckoutDeliveryAddress" and add the items into my "basket" local storage

  @B2C2-3665
  Scenario: User goes to the account information page where they can enter their details
    When I open the page "/login"
    Then I expect that element "Login.emailAddress" is displayed
    And I set "checkoutdeliveryaddress@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "Account Information" to be displayed

  @B2C2-3665
  Scenario: User goes to the account information page where they can enter their details
    When I open the page "/my-account/delivery-addresses/account-shipto/"
    When I click on the element "DeliveryInformation.country"
    And I click on the element "AccountInformation.unitedStates"
    And I click on the element "MyAccount.stateSelect"
    Then I expect that the values returned by "MyAccount.states" to be in the following order
      """
      Select
      Alabama
      Alaska
      Arizona
      Arkansas
      California
      Colorado
      Connecticut
      Delaware
      District Of Columbia
      Florida
      Georgia
      Hawaii
      Idaho
      Illinois
      Indiana
      Iowa
      Kansas
      Kentucky
      Louisiana
      Maine
      Maryland
      Massachusetts
      Michigan
      Minnesota
      Mississippi
      Missouri
      Montana
      Nebraska
      Nevada
      New Hampshire
      New Jersey
      New Mexico
      New York
      North Carolina
      North Dakota
      Ohio
      Oklahoma
      Oregon
      Pennsylvania
      Rhode Island
      South Carolina
      South Dakota
      Tennessee
      Texas
      Utah
      Vermont
      Virginia
      Washington
      West Virginia
      Wisconsin
      Wyoming
      """
    And I click on the element "California"

  @B2C2-3665
  Scenario: Should validate that when USA is NOT the selected country, then the STATE/PROVINCE/REGION field is a non mandatory textbox and US state selection should not be preserved
    When I click on the element "DeliveryInformation.country"
    And I click on the element "United Kingdom"
    Then I expect that element "MyAccount.stateTextbox" is displayed
    And I expect that element "MyAccount.stateTextbox" not contains any text
    And I set "36characterTestInputIsTooLongXXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "STATE/ PROVINCE/ REGION"
    And I expect that element "MyAccount.inputTooLongMessage" is displayed
    And I set "36characterTestInputIsTooLongXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "STATE/ PROVINCE/ REGION"
    And I expect that element "MyAccount.inputTooLongMessage" is not displayed



