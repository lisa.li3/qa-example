@FREDHOPPER
@SEARCH
@REGRESSION

Feature: Search
  B2C2-1862 Exposed search on desktop
  B2C2-523 Basic search results page
  B2C2-1863 Search results page anchoring
  B2C2-759 Fredhopper Search Redirects
#  759 currently has no tests as determined no value in doing this in an un-integrated env. May need to be covered in integrated environment tests.

  @B2C2-523
  Scenario: Goes to the homepage and seeds in preparation for the tests
    Given I am on the page "/"
    And I seed using the file "search"

  @B2C2-1862 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Search bar is present on desktop view
    Given I am on the page "/"
    Then the element "Navigation.searchBar" is displayed

  @B2C2-1862 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Closing the search clears the value in the search box on desktop
    Given I am on the page "/"
    When I set "Characters" to the inputfield "Navigation.searchBar"
    And I click on the element "Navigation.searchBarClose"
    Then I expect that element "Navigation.searchBar" not contains any text

  @B2C2-1862 @SKIP_DESKTOP
  Scenario: Closing the search clears the value in the search box on mobile and tablet
    Given I am on the page "/"
    When I click on the element "Navigation.burgerButton"
    And I set "Characters" to the inputfield "NavigationDrawer.searchField"
    And I click on the element "Navigation.searchBarClose"
    Then I expect that element "NavigationDrawer.searchField" not contains any text

  @B2C2-523 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Completing a search directs the user to a search results page
    Given I am on the page "/"
    When I set "search0001" to the inputfield "Navigation.searchBar"
    And I press "Enter"
    Then I expect the url to contain "/search/search0001"
    And I expect that element "SearchListPage.SearchHeading" is displayed
    And I expect that the title is "project - search0001"
    And I expect that element "ProductListPage.oneItem" is displayed

  @B2C2-523 @SKIP_DESKTOP
  Scenario: Completing a search directs the user to a search results page
    Given I am on the page "/"
    And I refresh the page
    When I click on the element "Navigation.burgerButton"
    And I set "search0001" to the inputfield "NavigationDrawer.searchField"
    And I press "Enter"
    Then I expect the url to contain "/search/search0001"
    And I expect that the title is "project - search0001"
    And I expect that element "SearchListPage.SearchHeading" is displayed
    And I expect that element "ProductListPage.oneItem" is displayed

  @B2C2-523 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Completing a search with no results directs the user to a search results page stating no result found
    Given I am on the page "/"
    When I set "noResult" to the inputfield "Navigation.searchBar"
    And I press "Enter"
    Then I expect the url to contain "/search/noResult"
    And I expect that element "SearchListPage.SearchHeading" is displayed
    And I expect that the title is "project - noResult"
    And I expect that element "ProductListPage.zeroItems" is displayed
    And I expect that element "SearchListPage.noResultText" is displayed

  @B2C2-523 @SKIP_DESKTOP
  Scenario: Completing a search with no results directs the user to a search results page stating no result found
    Given I am on the page "/"
    And I refresh the page
    When I click on the element "Navigation.burgerButton"
    And I set "noResult" to the inputfield "NavigationDrawer.searchField"
    And I press "Enter"
    Then I expect the url to contain "/search/noResult"
    And I expect that element "SearchListPage.SearchHeading" is displayed
    And I expect that the title is "project - noResult"
    And I expect that element "ProductListPage.zeroItems" is displayed
    And I expect that element "SearchListPage.noResultText" is displayed

  @B2C2-1863
  Scenario: Search results page anchors the user and returns them to the expected product when pressing 'back'
    Given I am on the page "/search/search0001,search0002,search0003,search0004,search0005,search0006,search0007,search0008,search0009,search0010,search0011,search0012,search0013,search0014,search0015"
    When I scroll to element "FifteenthSearchProduct"
    And I click on the element "FifteenthSearchProduct"
    And I go back to the previous page
    Then I expect that element "FifteenthSearchProduct" is within the viewport

  @B2C2-523
  Scenario: Search results page has no 'load more' button when 48 or less results
    Given I am on the page "/search/search0001,search0002,search0003,search0004,search0005,search0006,search0007,search0008,search0009,search0010,search0011,search0012,search0013,search0014,search0015,search0016,search0017,search0018,search0019,search0020,search0021,search0022,search0023,search0024,search0025,search0026,search0027,search0028,search0029,search0030,search0031,search0032,search0033,search0034,search0035,search0036,search0037,search0038,search0039,search0040,search0041,search0042,search0043,search0044,search0045,search0046,search0047,search0048"
    Then the element "ProductListPage.loadMore" is not displayed

  @B2C2-523
  Scenario: Search results page has 'load more' button when over 48 results
    Given I am on the page "/search/search0001,search0002,search0003,search0004,search0005,search0006,search0007,search0008,search0009,search0010,search0011,search0012,search0013,search0014,search0015,search0016,search0017,search0018,search0019,search0020,search0021,search0022,search0023,search0024,search0025,search0026,search0027,search0028,search0029,search0030,search0031,search0032,search0033,search0034,search0035,search0036,search0037,search0038,search0039,search0040,search0041,search0042,search0043,search0044,search0045,search0046,search0047,search0048,search0049"
    Then the element "ProductListPage.loadMore" is displayed

  @B2C2-523
  Scenario: If I click on 'Load More', More Results Displayed
    Given I am on the page "/search/search0001,search0002,search0003,search0004,search0005,search0006,search0007,search0008,search0009,search0010,search0011,search0012,search0013,search0014,search0015,search0016,search0017,search0018,search0019,search0020,search0021,search0022,search0023,search0024,search0025,search0026,search0027,search0028,search0029,search0030,search0031,search0032,search0033,search0034,search0035,search0036,search0037,search0038,search0039,search0040,search0041,search0042,search0043,search0044,search0045,search0046,search0047,search0048,search0049"
    When I scroll to element "ProductListPage.loadMore"
    And I click on the element "ProductListPage.loadMore"
    Then I expect that element "ProductListPage.loadMore" is not displayed

  @B2C2-3494 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Goes to the homepage on desktop and checks that there is a 100 character limit on search input field
	Given I am on the page "/"
    When I set "This_sentence_is_over_one_hundred_characters_long_in_order_to_test_that_the_search_navigation_input_field_is_limited_to_one_hundred_characters_only" to the inputfield "Navigation.searchBar"
    Then I expect the value length of element "Navigation.searchBar" to equal 100

  @B2C2-3494 @SKIP_DESKTOP
  Scenario: Goes to the homepage on mobile, opens the burger menu and checks that there is a 100 character limit on search input field
    Given I am on the page "/"
    When I click on the element "Navigation.burgerButton"
    When I set "This_sentence_is_over_one_hundred_characters_long_in_order_to_test_that_the_search_navigation_input_field_is_limited_to_one_hundred_characters_only" to the inputfield "NavigationDrawer.searchField"
    Then I expect the value length of element "NavigationDrawer.searchField" to equal 100

  @B2C2-524 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Set up for next scenario
    Given I am on the page "/"

  @B2C2-524 @SKIP_TABLET @SKIP_MOBILE
  Scenario Outline: On entering <SearchString> in the search box search suggestions section <ExpectedState>
    When I set "<SearchString>" to the inputfield "Navigation.searchBar"
    Then I expect that element "Mens Bomber Jacket" is <ExpectedState>
    And I expect that element "Mens Coat" is <ExpectedState>
    And I expect that element "Mens Coats" is <ExpectedState>
    And I expect that element "Mens Parka" is <ExpectedState>
    And I expect that element "Mens Shorts" is <ExpectedState>
    And I expect the count of elements in "Navigation.searchResultItems" to equal <Total>
    Examples:
      | SearchString | ExpectedState | Total |
      | J            | not displayed | 0     |
      | zzz          | not displayed | 0     |
      | JE           | displayed     | 5     |

  @B2C2-524 @SKIP_TABLET @SKIP_MOBILE
  Scenario: Search box is cleared after customer closes search bar suggestions
    Given I click on the element "Navigation.searchBarClose"
    Then I expect that element "Navigation.searchBar" not contains any text

  @B2C2-524 @SKIP_DESKTOP
  Scenario: Set up for next scenario
    Given I am on the page "/"
    When I click on the element "Navigation.burgerButton"

  @B2C2-524 @SKIP_DESKTOP
  Scenario Outline: On entering <SearchString> in the search box search suggestions section <ExpectedState>
    When I set "<SearchString>" to the inputfield "NavigationDrawer.searchField"
    Then I expect that element "Mens Bomber Jacket" is <ExpectedState>
    And I expect that element "Mens Coat" is <ExpectedState>
    And I expect that element "Mens Coats" is <ExpectedState>
    And I expect that element "Mens Parka" is <ExpectedState>
    And I expect that element "Mens Shorts" is <ExpectedState>
    And I expect the count of elements in "Navigation.searchResultItems" to equal <Total>
    Examples:
      | SearchString | ExpectedState | Total |
      | J            | not displayed | 0     |
      | zzz          | not displayed | 0     |
      | JE           | displayed     | 5     |

  @B2C2-524 @SKIP_DESKTOP
  Scenario: Search box is cleared after customer closes search bar suggestions
    Given I click on the element "Navigation.searchBarClose"
    Then I expect that element "NavigationDrawer.searchField" not contains any text