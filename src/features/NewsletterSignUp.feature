@NEWSLETTER
@REGRESSION

Feature: Newsletter Sign Up
  @B2C2-755
  Scenario: Open the homepage ready for next scenario
    Given I am on the page "/"

  @B2C2-755
  Scenario Outline: Receives the expected error message when signing up with the email address "<emailFieldInput>" via the newsletter banner
    When I set "<emailFieldInput>" to the inputfield "NewsletterSignupBanner.emailField"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "<resultMessage>" is displayed

    @HAPPY
    Examples:
      | emailFieldInput   | resultMessage                              |
      | plainaddress      | NewsletterSignupBanner.invalidEmailMessage |

    Examples:
      | emailFieldInput                | resultMessage                              |
      | #@%^%#$@#$@#.com               | NewsletterSignupBanner.invalidEmailMessage |
      | @example.com                   | NewsletterSignupBanner.invalidEmailMessage |
      | Joe Smith <email@example.com>  | NewsletterSignupBanner.invalidEmailMessage |
      | email.example.com              | NewsletterSignupBanner.invalidEmailMessage |
      | email@example@example.com      | NewsletterSignupBanner.invalidEmailMessage |
      | .email@example.com             | NewsletterSignupBanner.invalidEmailMessage |
      | email.@example.com             | NewsletterSignupBanner.invalidEmailMessage |
      | email..email@example.com       | NewsletterSignupBanner.invalidEmailMessage |
      | email@example.com (Joe Smith)  | NewsletterSignupBanner.invalidEmailMessage |
      | email@111.222.333.44444        | NewsletterSignupBanner.invalidEmailMessage |
      | email@example..com             | NewsletterSignupBanner.invalidEmailMessage |
      | Abc..123@example.com           | NewsletterSignupBanner.invalidEmailMessage |

  #
  # Created https://supergroupbt.atlassian.net/browse/B2C2-3718 for defect
  #

  @B2C2-755 @DEFECT
  Scenario Outline: Receives the expected success message when signing up with the email address "<emailFieldInput>" via the newsletter banner
    Given I am on the page "/"
    And I click on the element "NewsletterSignupBanner.emailField"
    And I scroll to element "NewsletterSignup.subscribe"
    And I set "<emailFieldInput>" to the inputfield "NewsletterSignupBanner.emailField"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "<resultMessage>" is displayed
    And I click on the element "NewsletterSignup.thankYouDialogClose"

    @HAPPY
    Examples:
      | emailFieldInput   | resultMessage                              |
      | email@example.com | NewsletterSignup.thankYou      |

    Examples:
      | emailFieldInput                | resultMessage                              |
      | firstname.lastname@example.com | NewsletterSignup.thankYou      |
      | email@subdomain.example.com    | NewsletterSignup.thankYou      |
      | firstname+lastname@example.com | NewsletterSignup.thankYou      |
      | 1234567890@example.com         | NewsletterSignup.thankYou      |
      | email@example-one.com          | NewsletterSignup.thankYou      |
      | email@example.museum           | NewsletterSignup.thankYou      |
      | firstname-lastname@example.com | NewsletterSignup.thankYou      |
      | email@example.com              | NewsletterSignup.thankYou      |

  @B2C2-755
  Scenario Outline: Newsletter signup banner is not displayed on "<url>"
    Given I am on the page "<url>"
    When I wait on element "<expectedText>" to be displayed
    Then I expect that element "NewsletterSignupBanner.emailField" is not displayed
    @HAPPY
    Examples:
      | url       | expectedText                |
      | /signup   | Stay Connected              |
      | /register | Register                    |

  #
  # Created https://supergroupbt.atlassian.net/browse/B2C2-3718 for defect
  #

  @B2C2-771 @DEFECT
  Scenario Outline: Receives the expected success message when signing up with the email address "<emailFieldInput>"  via the Newsletter signup page
    Given I am on the page "/signup"
    When I set "<emailFieldInput>" to the inputfield "NewsletterSignup.emailAddressTextbox"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "<resultMessage>" becomes displayed
    And I click on the element "NewsletterSignup.thankYouDialogClose"

    @HAPPY
    Examples:
      | emailFieldInput   | resultMessage                        |
      | email@example.com | NewsletterSignup.thankYou            |

    Examples:
      | emailFieldInput                | resultMessage                        |
      | firstname.lastname@example.com | NewsletterSignup.thankYou            |
      | email@subdomain.example.com    | NewsletterSignup.thankYou            |
      | firstname+lastname@example.com | NewsletterSignup.thankYou            |
      | 1234567890@example.com         | NewsletterSignup.thankYou            |
      | email@example-one.com          | NewsletterSignup.thankYou            |
      | email@example.museum           | NewsletterSignup.thankYou            |
      | firstname-lastname@example.com | NewsletterSignup.thankYou            |
      | email@example.com              | NewsletterSignup.thankYou            |

  @B2C2-771
  Scenario: Open the signup page ready for next scenario
    Given I am on the page "/signup"

  @B2C2-771
  Scenario Outline: Receives the expected error message when signing up with the email address "<emailFieldInput>"  via the Newsletter signup page
    When I set "<emailFieldInput>" to the inputfield "NewsletterSignup.emailAddressTextbox"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "<resultMessage>" becomes displayed

    @HAPPY
    Examples:
      | emailFieldInput   | resultMessage                        |
      | plainaddress      | NewsletterSignup.invalidEmailMessage |

    Examples:
      | emailFieldInput                | resultMessage                        |
      | #@%^%#$@#$@#.com               | NewsletterSignup.invalidEmailMessage |
      | @example.com                   | NewsletterSignup.invalidEmailMessage |
      | Joe Smith <email@example.com>  | NewsletterSignup.invalidEmailMessage |
      | email.example.com              | NewsletterSignup.invalidEmailMessage |
      | email@example@example.com      | NewsletterSignup.invalidEmailMessage |
      | .email@example.com             | NewsletterSignup.invalidEmailMessage |
      | email.@example.com             | NewsletterSignup.invalidEmailMessage |
      | email..email@example.com       | NewsletterSignup.invalidEmailMessage |
      | email@example.com (Joe Smith)  | NewsletterSignup.invalidEmailMessage |
      | email@111.222.333.44444        | NewsletterSignup.invalidEmailMessage |
      | email@example..com             | NewsletterSignup.invalidEmailMessage |
      | Abc..123@example.com           | NewsletterSignup.invalidEmailMessage |

  @B2C2-771 @HAPPY
  Scenario Outline: User is alerted to invalid field entries on the Newsletter signup page
    Given I am on the page "/signup"
    When I set "<value>" to the inputfield "<field>"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "<message>" becomes displayed
    Examples:
      | value                             | field                             | message      |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | NewsletterSignup.firstNameTextbox | Too Long     |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | NewsletterSignup.lastNameTextbox  | Too Long     |
      | 32                                | NewsletterSignup.dateOfBirthDay   | INVALID DATE |
      | 1899                              | NewsletterSignup.dateOfBirthYear  | INVALID DATE |

  #
  # Created https://supergroupbt.atlassian.net/browse/B2C2-3718 for defect
  #

  @B2C2-771 @HAPPY @DEFECT
  Scenario: User is able to proceed when submitting valid values on the Newsletter signup page
    Given I am on the page "/signup"
    When I set "Dave@Smith.com" to the inputfield "NewsletterSignup.emailAddressTextbox"
    And I set "Dave" to the inputfield "NewsletterSignup.firstNameTextbox"
    And I set "Smith" to the inputfield "NewsletterSignup.lastNameTextbox"
    And I set "01" to the inputfield "NewsletterSignup.dateOfBirthDay"
    And I select the option with the text "January" for element "NewsletterSignup.dateOfBirthMonth"
    And I set "1990" to the inputfield "NewsletterSignup.dateOfBirthYear"
    And I select the option with the text "Male" for element "NewsletterSignup.gender"
    And I click on the element "NewsletterSignup.subscribe"
    Then I expect that element "NewsletterSignup.thankYou" becomes displayed
