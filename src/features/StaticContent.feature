@SANA
@CONTENT
@REGRESSION


Feature: Static Content
  Check if static content pages will load as expected

  @B2C2-754 @B2C2-3471 @B2C2-1962
  Scenario: Seed SANA pages
    Given I seed using the file "BaseSanaPages"

  @B2C2-754 @B2C2-3471 @B2C2-1962
  Scenario Outline: Goes to static content page and loads as expected
    Given I open the page <static page>
    When I pause for 10000ms
    Then I expect the url to not contain "/404"
    And I expect that element "StaticContent.sanaDiv" is displayed
    And I expect that element "StaticContent.sanaDiv" contains any text

    Examples:
        | static page                   | 
        | "/footer/sana/klarna"         | 
        | "/footer/sana/invictus-games" | 
        | "/footer/sana/clearpay"       | 
        | "/footer/sana/yext-concepts"  |

    @B2C2-2694
    Scenario: Goes to the sitemap page
      Given I am on the page "/sitemap"
      Then I expect that element "StaticContent.siteMapHeading" is displayed
      And I expect the url to not contain "/404"

      @B2C2-4458
      Scenario: Goes to the seeded static page and expects seeded title and description to be displayed
        Given I am on the page "/footer/framework/seeded-static"
        Then I expect that the title is "Great Marketing Page"
        And the attribute "content" from element "StaticContent.descriptionMeta" is "Seeded description"
