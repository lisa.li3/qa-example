@ACCOUNT
@REGRESSION

Feature: My Account - Order History and Summary

  @B2C2-1206
  Scenario: I login as a user with completed orders
    Given I am on the page "/login"
    And I seed using the file "MyAccountCompletedOrder"
    And I refresh the page
    When I set "orderhistorywithcompleteorders@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"

  @B2C2-1206
  Scenario: Ensure customer is able to go to their order history
    Given I wait on element "MyAccount.orderHistoryButton"
    When I click on the element "MyAccount.orderHistoryButton"
    Then I expect the url to contain "/my-account/order-history"

  @B2C2-1206
  Scenario Outline: Ensure user is presented with correct orders
    Given I am on the page "/my-account/order-history"
    Then I expect that element "<orderNumber>" is displayed
    And I expect that element "<date>" is displayed
    And I expect that element "<price>" is displayed

    Examples:
      | orderNumber     | date               | price   |
      | project_1206_5 | 15 Jan 2021, 00:05 | £100.00 |
      | project_1206_3 | 15 Jan 2021, 00:03 | £100.00 |
      | project_1206_3 | 15 Jan 2021, 00:02 | £105.00 |

  @B2C2-1206
  Scenario: Order history row is highlighted when hovered over
    Given I am on the page "/my-account/order-history"
    When I move to "project_1206_5"
    Then I expect that the css attribute "background-color" from element "project_1206_5" is "rgba(0,0,0,0)"

  @B2C2-1206
    #Todo finish test once B2C2-3588 is fixed
  Scenario: User is presented with order summary with all the correct details

  @B2C2-1206
  Scenario: User is directed to the my account page when clicking the back button from order history
    Given I am on the page "/my-account/order-history"
    When I click on the element "OrderHistory.backButton"
    Then I expect the url to contain "/my-account"
    And I click on the element "MyAccount.logOutButton"

  @B2C2-1206
        #Todo finish test once B2C2-3588 is fixed
  Scenario: User is directed to the my order history page when clicking the back button from order summary

  @B2C2-1206
  Scenario: User is presented with message in their order history when they have no previous orders
    Given I am on the page "/login"
    And I seed using the file "MyAccount"
    And I refresh the page
    When I set "myaccount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "MyAccount.orderHistoryButton"
    When I click on the element "MyAccount.orderHistoryButton"
    Then I expect that element "No orders to display" is displayed



