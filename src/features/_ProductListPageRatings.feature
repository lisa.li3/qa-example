@PLP
@PLP_RATINGS
@REGRESSION

Feature: Product List Page (PLP) - Ratings
  As a user of the Product List Page, I can see and order ratings

  # ---
  #
  # Sort By Rating - standard PLP
  # https://supergroupbt.atlassian.net/browse/B2C2-298
  # https://supergroupbt.atlassian.net/browse/B2C2-2026 - includes the Display of average ratings story
  #
  # ---

  Rule: Items with ratings can be sorted on the PLP by the highest rating (includes promotional products)

  @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Seed products with promotions and ratings on the plp
  Given I seed using the file "ProductListPage_PromosWithRatings"

  @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Check default sorting is applied on opening the PLP page
  Given I open the page "/main/framework/promos-with-ratings"
  Then I expect that the values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Check that the desktop sort dropdown has the default set as 'Sort By' and that the dropdown options are correct
  Given I am on the page "/main/framework/promos-with-ratings"
  Then I expect that element "ProductListPage.sortDefaultOption" is selected
  And I expect that the values returned by "ProductListPage.sortDropdownTextOptions" to be in the following order
        """
        MOST RELEVANT
        HIGHEST RATED
        HIGHEST PRICE
        LOWEST PRICE
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Sort by Highest Rating and check sort order is descending with 'zero' rated products positioned at the end
  Given I select the option with the value "rating" for element "ProductListPage.sortDropdown"
  Then I expect that the values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 5 out of 5 stars
        Rating 4 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 2 out of 5 stars
        Rating 1 out of 5 stars
        No Rating
        No Rating
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Revert to default sorting and check sort order (Sort By Relevance)
  Given I select the option with the value "relevance" for element "ProductListPage.sortDropdown"
  Then I expect that the values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """

  @SKIP_DESKTOP @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Sort by Highest Rating and check sort order is descending with 'zero' rated products positioned at the end
  Given I click on the element "ProductListPage.sortMobile"
  When I click on the element "ProductListPage.mobileSortHighestRated"
  Then I expect that the values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 5 out of 5 stars
        Rating 4 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 2 out of 5 stars
        Rating 1 out of 5 stars
        No Rating
        No Rating
        """

  @SKIP_DESKTOP @B2C2-2971 @B2C2-298 @B2C2-2026
  Scenario: Revert to default sorting and check sort order (Sort By Relevance)
  Given I click on the element "ProductListPage.sortMobile"
  When I click on the element "ProductListPage.mobileSortMostRelevant"
  Then I expect that the values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """

  # ---
  #
  # Sort By Rating - Promotion PLPs
  # https://supergroupbt.atlassian.net/browse/B2C2-2971
  #
  # ---

  Rule: Items with ratings can be sorted on a Promotion PLP by the highest rating

  @B2C2-2971
  Scenario: Seed products with promotions and ratings on the plp
  Given I seed config using the file "PromotionPLP"
  And I open the siteId "promotion-plp1"
  And I seed using the file "PromotionPLP_ProductsWithRatings"
  And I open the page "/main/framework/promotion-plp1"
  And I click on the element "Product 1 - 0 stars"
  And I click on the element "B2C2-2971 PROMO"
  And I wait on element "Product 1 - 0 stars"


  @B2C2-2971
  Scenario: Check default sorting is applied on opening the Promotion PLP page
  Then I expect that the first 8 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """
  And I expect that the last 2 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        No Rating
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971
  Scenario: Check that the desktop sort dropdown has the default set as 'Sort By' and that the dropdown options are correct
  Then I expect that element "ProductListPage.sortDefaultOption" is selected
  And I expect that the values returned by "ProductListPage.sortDropdownTextOptions" to be in the following order
        """
        MOST RELEVANT
        HIGHEST RATED
        HIGHEST PRICE
        LOWEST PRICE
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971
  Scenario: Sort by Highest Rating and check sort order is descending with 'zero' rated products positioned at the end
  Given I select the option with the value "rating" for element "ProductListPage.sortDropdown"
  Then I expect that the first 8 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 5 out of 5 stars
        Rating 4 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 2 out of 5 stars
        Rating 1 out of 5 stars
        Rating 0.5 out of 5 stars
        No Rating
        """
  And I expect that the last 2 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        No Rating
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971
  Scenario: Revert to default sorting and check sort order (Sort By Relevance)
  Given I select the option with the value "relevance" for element "ProductListPage.sortDropdown"
  Then I expect that the first 8 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """

  @SKIP_TABLET @SKIP_MOBILE @B2C2-2971
  Scenario: Click the load more button and check that the last item (default sorting) has a rating of 0.5 stars
  Given I click on the element "ProductListPage.loadMore"
  Then I expect that the last 1 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 0.5 out of 5 stars
        """

  @SKIP_DESKTOP @B2C2-2971
  Scenario: Sort by Highest Rating and check sort order is descending with 'zero' rated products positioned at the end
  Given I click on the element "ProductListPage.sortMobile"
  When I click on the element "ProductListPage.mobileSortHighestRated"
  Then I expect that the first 8 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 5 out of 5 stars
        Rating 4 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 2 out of 5 stars
        Rating 1 out of 5 stars
        Rating 0.5 out of 5 stars
        No Rating
        """
  And I expect that the last 2 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        No Rating
        """
  @SKIP_DESKTOP @B2C2-2971
  Scenario: Revert to default sorting and check sort order (Sort By Relevance)
  Given I click on the element "ProductListPage.sortMobile"
  When I click on the element "ProductListPage.mobileSortMostRelevant"
  Then I expect that the first 8 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        Rating 1 out of 5 stars
        Rating 2 out of 5 stars
        Rating 3.67 out of 5 stars
        Rating 2 out of 5 stars
        Rating 4 out of 5 stars
        Rating 5 out of 5 stars
        No Rating
        """
  And I expect that the last 2 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        No Rating
        No Rating
        """

  @SKIP_DESKTOP @B2C2-2971
  Scenario: Click the load more button and check that the last item (default sorting) has a rating of 0.5 stars
  Given I click on the element "ProductListPage.loadMore"
  Then I expect that the last 1 values returned by "ProductListPage.productsRatingsLabelText" to be in the following order
        """
        Rating 0.5 out of 5 stars
        """
