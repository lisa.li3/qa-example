@PLP
@PLP_CATEGORY
@REGRESSION

Feature: Product List Page (PLP) - Category page and Description
  As a user of the Product List Page, I can view and sort by price

    #
    # Category Page and Description
    # https://supergroupbt.atlassian.net/browse/B2C2-304
    #

  Rule: Check a category page for the title, description text and any sub category links within

    @B2C2-304
    Scenario: Seed a category PLP page with description
      Given I seed using the file "ProductListPage_CategoryNameAndDescription"

    @B2C2-304
    Scenario: It should show the category title and description and sub category link element
      When I open the page "/main/framework/category_name_and_description"
      Then I wait on element "ProductListPage.categoryHeading" to be displayed
      And the element "ProductListPage.categoryHeading" contains the text "CATEGORY HEADING"
      And there is an element "This is the category description" on the page
      And there is an element "sub category" on the page
      And the css attribute "font-weight" from element "ProductListPage.subCategoryLink" is "700"
      And there is an element "This is a continuation of the text in the category description" on the page

    @B2C2-304 @SKIP_DESKTOP @SKIP_TABLET
    Scenario: On mobile, clicking the 'show more' button expands the description text
      Given I click on the element "ProductListPage.mobileShowMoreLessButton"
      Then I expect that element "Expanded text here" is displayed
      And I expect that element "Expanded text here" is within the viewport
      And I expect that the attribute "class" from element "ProductListPage.descriptionSectionDiv" is not "collapsed"

    @B2C2-304 @SKIP_DESKTOP @SKIP_TABLET
    Scenario: On mobile, clicking the 'show less' button collapses the description text
      Given I click on the element "ProductListPage.mobileShowMoreLessButton"
      Then I expect that the attribute "class" from element "ProductListPage.descriptionSectionDiv" is "collapsed"

    @B2C2-304
    Scenario: Clicking on a link in the description takes the user to the relevant sub category page
      Given I click on the element "ProductListPage.subCategoryLink"
      Then I expect the url to contain "/main/framework/category_name_and_description/sub-category"
      And the element "ProductListPage.categoryHeading" contains the text "SUB CATEGORY HEADING"

    #
    # Breadcrumbs
    # https://supergroupbt.atlassian.net/browse/B2C2-437
    #

  Rule: Check the PLP breadcrumbs

    @B2C2-437 @B2C2-3635
    Scenario: Seed a category PLP page with description
      Given I seed using the file "ProductListPage_Breadcrumbs"

    @B2C2-437 @B2C2-3635 @SKIP_TABLET @SKIP_MOBILE
    Scenario: It should open a PLP category page and check that the breadcrumbs are displayed
      When I open the page "/main/framework/breadcrumbs_category/breadcrumbs_sub_category"
      Then the element "ProductListPage.categoryHeading" is displayed
      And I expect that element "ProductListPage.breadcrumbList" is displayed
      And I expect that the values returned by "ProductListPage.breadcrumbListItems" to be in the following order
        """
        HOME
        MAIN
        FRAMEWORK
        BREADCRUMBS CATEGORY
        BREADCRUMBS SUB CATEGORY
        """
      And I expect that the values returned by "ProductListPage.breadcrumbSeparators" to be in the following order
        """
        /
        /
        /
        /
        Separator not displayed
        """

    @B2C2-3635
    Scenario: Category page with no products should display '0 items' text
      When I open the page "/main/framework/breadcrumbs_category/breadcrumbs_sub_category"
      Then I expect that element "ProductListPage.zeroItems" is displayed

    @B2C2-437 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Clicking a link in the breadcrumbs should take the user to the appropriate page
      When I click on the element "BREADCRUMBS CATEGORY"
      Then I expect the url to contain "/main/framework/breadcrumbs_category"
      And the element "ProductListPage.categoryHeading" contains the text "BREADCRUMBS CATEGORY HEADING"
