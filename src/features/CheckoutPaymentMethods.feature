@CHECKOUT
@PAYMENT
@REGRESSION

Feature: Checkout Payment Method

  #Skipping below scenario due to an issue where PayPal Express button takes over 60 seconds to load in Saucelabs. We currently don't have a solution
  @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: Setup environment with CheckoutPaymentMethodsPayPalExpressOn
    Given I seed config using the file "CheckoutPaymentMethodsPayPalExpressOn"
    When I open the siteId "paypalexpresson"
    And I seed using the file "CheckoutPaymentMethodsPayPal" and add the items into my "basket" local storage

  #Skipping below scenario due to an issue where PayPal Express button takes over 60 seconds to load in Saucelabs. We currently don't have a solution
  @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: Load /checkout/login page and assert PayPal express payment button is displayed
    When I open the page "/checkout/login"
    And I wait on element "CheckoutPayment.payPalExpressButton" to be displayed
    Then I expect that element "CheckoutPayment.payPalExpressButton" is displayed

  #Skipping below scenario due to an issue where PayPal Express button takes over 60 seconds to load in Saucelabs. We currently don't have a solution
  @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: Setup environment with CheckoutPaymentMethodsPayPalExpressOn
    Given I seed config using the file "CheckoutPaymentMethodsPayPalExpressOff"
    When I open the siteId "paypalexpressoff"
    And I seed using the file "CheckoutPaymentMethodsPayPal" and add the items into my "basket" local storage

  #Skipping below scenarios due to an issue where PayPal Express button takes over 60 seconds to load in Saucelabs. We currently don't have a solution
  @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: Load /checkout/login page and assert PayPal express payment button is not displayed
    When I open the page "/checkout/login"
    And I wait on element "Continue as guest" to be displayed
    Then I expect that element "CheckoutPayment.payPalExpressButton" is not displayed

@B2C2-4441 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Payment Successfully Goes Through With Customer Credit and Adyen <cardType>
    Given I open the page "/"
    And I spawn the site "customerCreditAdyen" from the base config "adyen-on"
    And I seed using the file "CheckoutCustomerCreditWithAdyen" and add the items into my "basket" local storage
    When I open the page "/checkout/login"
    And I set "customercreditAdyenPass@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£12.99"
    And I wait on element "CheckoutPayment.adyenDropIn" to be displayed
    And I expect that element "CheckoutPayment.payPalBuyNowButton" is displayed
    And I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4111 1111 4555 1142"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "<cardType>" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    And I click on the button "CheckoutPayment.buyNowButton"
    Then I wait on element "Confirmation.orderConfirmationText" to be displayed
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is not displayed
    And I click on the button "MyAccount.logOutButton"

      Examples:
      | cardType    |
      | no3DS       |
#      | 3DS1        | B2C2-5155 raised to fix it

  @B2C2-4441 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Payment Does Not Go Through With Customer Credit and Adyen <cardType> Error
    Given I open the page "/"
    And I spawn the site "customerCreditAdyen" from the base config "adyen-on"
    And I seed using the file "CheckoutCustomerCreditWithAdyen" and add the items into my "basket" local storage
    When I open the page "/checkout/login"
    And I set "customercreditAdyenError@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£12.99"
    And I wait on element "CheckoutPayment.adyenDropIn" to be displayed
    And I expect that element "CheckoutPayment.payPalBuyNowButton" is displayed
    And I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4111 1111 4555 1142"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "<cardType>" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    And I click on the button "CheckoutPayment.buyNowButton"
    Then I wait on element "CheckoutPayment.errorHasOccurred" to be displayed
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed
    And I click on the button "MyAccount.logOutButton"

      Examples:
      | cardType    |
      | no3DS       |
#      | 3DS1        | commented out as the error modal for 3DS1 is being de-scoped for HK release in order to get this ticket in. Defect>>  https://supergroupbt.atlassian.net/browse/B2C2-4723


  @B2C2-4441
  Scenario: Payment when Customer Credit Equals Order Total
      Given I open the page "/"
      And I spawn the site "customerCreditEquals" from the base config "default"
      And I seed using the file "MyAccountCustomerCreditEqualsTotal" and add the items into my "basket" local storage
      When I open the page "/checkout/login"
      And I set "customercredit@project.com" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I scroll to element "Checkout/OrderSummary.heading"
      And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£2,000.00"
      And I expect that element "Checkout/OrderSummary.total" contains the text "£0.00"
      And I wait on element "CheckoutPayment.adyenDropIn" to not be displayed
      And I expect that element "CheckoutPayment.payPalButton" is not displayed
      And I click on the button "CheckoutPayment.buyNowButton"
      Then I wait on element "Confirmation.orderConfirmationText" to be displayed
      And I open the page "/my-account"
      And I expect that element "CustomerCredits.hasAccountCredit" is not displayed
      And I click on the button "MyAccount.logOutButton"

  @B2C2-4441
  Scenario: Payment when Customer Credit Greater Than Order Total
      Given I open the page "/"
      And I spawn the site "customerCreditGT" from the base config "default"
      And I seed using the file "MyAccountCustomerCreditGreaterThanTotal" and add the items into my "basket" local storage
      When I open the page "/checkout/login"
      And I set "customercredit@project.com" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I scroll to element "Checkout/OrderSummary.heading"
      And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£1,000.00"
      And I expect that element "Checkout/OrderSummary.total" contains the text "£0.00"
      And I wait on element "CheckoutPayment.adyenDropIn" to not be displayed
      And I expect that element "CheckoutPayment.payPalButton" is not displayed
      And I click on the button "CheckoutPayment.buyNowButton"
      Then I wait on element "Confirmation.orderConfirmationText" to be displayed
      And I open the page "/my-account"
      And I expect that element "CustomerCredits.hasAccountCredit" is displayed
      And I click on the button "MyAccount.logOutButton"


@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
Scenario: Guest Checkout - Can See Adyen Payment Options
  Given I open the page "/"
  And I seed config using the file "CheckoutPaymentMethodsAdyenMock"
  And I open the siteId "testadyen"
  And I seed using the file "CheckoutAdyen" and add the items into my "basket" local storage
  When I open the page "/checkout"
  Then I wait on element "CheckoutPayment.adyenDropIn" to be displayed

@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
Scenario: Guest Checkout - Customer Details Setup
  When I set "Authorised" to the inputfield "CheckoutAddress.firstNameTextbox"
  And I set "Test" to the inputfield "CheckoutAddress.lastNameTextbox"
  And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
  And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.emailAddressTextbox"
  And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
  And I set "address1" to the inputfield "CheckoutAddress.address1Textbox"
  And I set "city" to the inputfield "CheckoutAddress.cityTextbox"
  And I set "GL51 9NH" to the inputfield "CheckoutAddress.postcodeTextbox"

@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
Scenario: Guest Checkout - Can Complete Payment With Adyen <cardType> 
  Given I seed using the file "CheckoutAdyen" and add the items into my "basket" local storage
  And I open the page "/checkout"
  When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
  And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "4111 1111 4555 1142"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
  And I pause for 2000ms
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "0330"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "737"
  And I switch back to the parent frame
  And I set "<cardType>" to the inputfield "CheckoutPayment.cardPaymentNameInput"
  And I click on the button "CheckoutPayment.buyNowButton"
  Then the element "Confirmation.orderConfirmationText" is displayed
  And the element "Confirmation.orderReferenceText" is displayed
  And the element "Confirmation.orderNumber" is displayed

  Examples:
    | cardType    | 
    | no3DS       | 
#    | 3DS1        | B2C2-5155 raised to fix it

@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
Scenario: Logged In Checkout - Setup
  Given I open the page "/my-account"
  And I set "checkoutadyen@project.local" to the inputfield "Login.emailAddress"
  And I set "its-a-secret" to the inputfield "Login.password"
  And I click on the element "Login.signInButton"

@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
Scenario: Logged In Checkout - Can Complete Payment With Adyen <cardType> 
  Given I seed using the file "CheckoutAdyen" and add the items into my "basket" local storage
  And I open the page "/checkout"
  When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
  And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "4111 1111 4555 1142"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
  And I pause for 2000ms
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "0330"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "737"
  And I switch back to the parent frame
  And I set "<cardType>" to the inputfield "CheckoutPayment.cardPaymentNameInput"
  And I click on the button "CheckoutPayment.buyNowButton"
  Then the element "Confirmation.orderConfirmationText" is displayed
  And the element "Confirmation.orderReferenceText" is displayed
  And the element "Confirmation.orderNumber" is displayed

  Examples:
    | cardType    | 
    | no3DS       | 
#    | 3DS1        | B2C2-5155 raised to fix it

@B2C2-3911 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Log out
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"

@B2C2-3365 @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: If "OR" is Not Displayed, PayPal Buy Now Button is Not Displayed 
    Given I am on the page "/"
    And I seed using the file "CheckoutPaymentMethodsPayPalBuyNow" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I scroll to element "CheckoutPayment.buyNowButton"
    Then I wait on element "CheckoutPayment.or" to not be displayed
    And I expect that element "CheckoutPayment.payPalButton" is not displayed

@B2C2-3365 @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: If "OR" Is Displayed, PayPal Buy Now Button is Displayed
    Given I am on the page "/"
    And I seed using the file "CheckoutPaymentMethodsPayPalBuyNow" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I set "firstName" to the inputfield "CheckoutAddress.firstNameTextbox"
    And I set "lastName" to the inputfield "CheckoutAddress.lastNameTextbox"
    And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
    And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.emailAddressTextbox"
    And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
    And I set "address1" to the inputfield "CheckoutAddress.address1Textbox"
    And I set "city" to the inputfield "CheckoutAddress.cityTextbox"
    And I set "GL51 9NH" to the inputfield "CheckoutAddress.postcodeTextbox"
    And I scroll to element "CheckoutPayment.buyNowButton"
    Then I wait on element "CheckoutPayment.or" to be displayed
    And I expect that element "CheckoutPayment.payPalBuyNowButton" is displayed

  @B2C2-4453
  Scenario: Tidy up - Switch back to uk site for next set of tests
    And I open the siteId "com"
    Then I expect the url to contain "com."

  @B2C2-4453
  Scenario: Data setup for B2C2-4453
    Given I am on the page "/"
    And I seed using the file "CheckoutProductBuyNow" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I set "Authorised" to the inputfield "CheckoutAddress.firstNameTextbox"
    And I set "Test" to the inputfield "CheckoutAddress.lastNameTextbox"
    And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
    And I set "checkoutproductbuynow@project.local" to the inputfield "CheckoutAddress.emailAddressTextbox"
    And I set "checkoutproductbuynow@project.local" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"

  @B2C2-4453
  Scenario: Buy now button button quits loading state when payment details have not been complete
    Given I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    When I click on the button "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutPayment.buyNowButton" is enabled

  @B2C2-4453
  Scenario: Error is displayed for card payment missing values
    Then I expect that element "CheckoutPayment.cardPaymentNumberError" is displayed
    Then I expect that element "CheckoutPayment.cardPaymentDateFrameError" is displayed
    Then I expect that element "CheckoutPayment.cardPaymentCvvFrameError" is displayed
    Then I expect that element "CheckoutPayment.cardPaymentNameInputError" is displayed

  @B2C2-4315
  Scenario: Tidy up - Switch back to uk site for next set of tests
    And I open the siteId "com"
    Then I expect the url to contain "com."

  @B2C2-4315 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Customer Details Setup for B2C2-4315
    Given I open the page "/"
    And I seed using the file "CheckoutProductAydenCvcError" and add the items into my "basket" local storage
    And I open the page "/checkout"
    When I set "Authorised" to the inputfield "CheckoutAddress.firstNameTextbox"
    And I set "Test" to the inputfield "CheckoutAddress.lastNameTextbox"
    And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
    And I set "checkoutadyenproductcvcError@project.local" to the inputfield "CheckoutAddress.emailAddressTextbox"
    And I set "checkoutadyenproductcvcError@project.local" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
    And I set "address1" to the inputfield "CheckoutAddress.address1Textbox"
    And I set "city" to the inputfield "CheckoutAddress.cityTextbox"
    And I set "GL51 9NH" to the inputfield "CheckoutAddress.postcodeTextbox"

  @B2C2-4315 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Payment details setup for B2C2-4315
    Given I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4444 3333 2222 1111"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0331"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "555"
    And I switch back to the parent frame
    And I set "TEST" to the inputfield "CheckoutPayment.cardPaymentNameInput"

  @B2C2-4315 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Customer is displayed error when incorrect cvc has been entered
    Given I wait on element "CheckoutPayment.or" to be displayed
    When I click on the button "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutPayment.orderProcessError" is displayed

  @B2C2-4315 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Customer is able to click and edit ayden components after being presented with an error
    Given I click on the element "CheckoutPayment.orderProcessErrorOkButton"
    And I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "Backspace"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "Backspace"
    And I press "Backspace"
    And I press "Backspace"
    And I press "737"
    And I switch back to the parent frame
    And I set "S" to the inputfield "CheckoutPayment.cardPaymentNameInput"

  @B2C2-4315 @SKIP_MOBILE @SKIP_TABLET
  Scenario: Customer is presented with confirmation page after updating payment details
    Given I click on the button "CheckoutPayment.buyNowButton"
    Then the element "Confirmation.orderConfirmationText" is displayed
    And the element "Confirmation.orderReferenceText" is displayed
    And the element "Confirmation.orderNumber" is displayed

  # The below giftcard failures are mocked postAuth fails
  # Defects added due to bug B2C2-4646 causing failures with customer credit scenarios

  @B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Rollback: Customer Credit + Giftcard 
    Given I open the page "/"
    And I spawn the site "rbackCreditGiftcard" from the base config "gift-cards"
    And I seed using the file "CheckoutCustomerCreditGiftCards" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "customercredit-giftcard@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8388800000000001000" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£12.99"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£10.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£0.00"
    When I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 5000ms
    Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
    And I expect that element "Checkout/OrderSummary.total" contains the text "£10.00"
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed

  @B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Rollback: Customer Credit + 2 Giftcards 
    Given I open the page "/"
    And I spawn the site "rbackCreditGiftcard" from the base config "gift-cards"
    And I seed using the file "CheckoutCustomerCreditGiftCards" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "customercredit-giftcard@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8388800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8378800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8378" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£12.99"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£10.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£0.00"
    When I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 5000ms
    Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
    And I expect that element "Checkout/OrderSummary.total" contains the text "£10.00"
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed

@B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Tidy up - Switch back to uk site for next set of tests
    And I open the siteId "com"
    Then I expect the url to contain "com."

@B2C2-4628 @ROLLBACK
Scenario: Rollback: Giftcard + Adyen
  Given I open the page "/"
  And I seed using the file "CheckoutGiftcards" and add the items into my "basket" local storage
  And I open the page "/checkout/login"
  And I set "giftcards@project.com" to the inputfield "Login.emailAddress"
  And I set "its-a-secret" to the inputfield "Login.password"
  And I click on the element "Login.signInButton"
  And I scroll to element "Checkout/RedeemAGiftCard.accordion"
  And I pause for 2000ms
  And I click on the element "Checkout/RedeemAGiftCard.accordion"
  And I set "8388800000000001000" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
  And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
  And I click on the element "Checkout/RedeemAGiftCard.redeem"
  And I scroll to element "Checkout/OrderSummary.heading"
  And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
  And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£10.00"
  And I expect that element "Checkout/OrderSummary.total" contains the text "£12.99"
  And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "4111 1111 4555 1142"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
  And I pause for 2000ms
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "0330"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "737"
  And I switch back to the parent frame
  And I set "no3DS" to the inputfield "CheckoutPayment.cardPaymentNameInput"
  When I click on the button "CheckoutPayment.buyNowButton"
  And I pause for 5000ms
  Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
  And I click on the button "CheckoutOrderSummary.modalOk"
  And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
  And I expect that element "Checkout/OrderSummary.total" contains the text "£22.99"

  
@B2C2-4628 @ROLLBACK
Scenario: Rollback: 2 Giftcards + Adyen
  Given I open the page "/"
  And I seed using the file "CheckoutGiftcards" and add the items into my "basket" local storage
  And I open the page "/checkout/login"
  And I scroll to element "Checkout/RedeemAGiftCard.accordion"
  And I pause for 2000ms
  And I click on the element "Checkout/RedeemAGiftCard.accordion"
  And I set "8388800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
  And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
  And I click on the element "Checkout/RedeemAGiftCard.redeem"
  And I pause for 2000ms
  And I click on the element "Checkout/RedeemAGiftCard.accordion"
  And I set "8378800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
  And I set "8378" to the inputfield "Checkout/RedeemAGiftCard.pin"
  And I click on the element "Checkout/RedeemAGiftCard.redeem"
  And I pause for 2000ms
  And I scroll to element "Checkout/OrderSummary.heading"
  And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
  And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£10.00"
  And I expect that element "Checkout/OrderSummary.total" contains the text "£12.99"
  And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "4111 1111 4555 1142"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
  And I pause for 2000ms
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "0330"
  And I switch back to the parent frame
  And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
  And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
  And I press "737"
  And I switch back to the parent frame
  And I set "no3DS" to the inputfield "CheckoutPayment.cardPaymentNameInput"
  When I click on the button "CheckoutPayment.buyNowButton"
  And I pause for 5000ms
  Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
  And I click on the button "CheckoutOrderSummary.modalOk"
  And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
  And I expect that element "Checkout/OrderSummary.total" contains the text "£22.99"

  @B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Rollback: Customer Credit + Giftcard + Adyen
    Given I open the page "/"
    And I spawn the site "rbackCreditGiftcard" from the base config "gift-cards"
    And I seed using the file "CheckoutCustomerCreditGiftCards" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "customercredit-giftcard@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8388800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£12.99"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£5.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£5.00"
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4111 1111 4555 1142"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "no3DS" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    When I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 5000ms
    Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
    And I expect that element "Checkout/OrderSummary.total" contains the text "£10.00"
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed

  @B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Rollback: Customer Credit + Giftcard + Adyen
    Given I open the page "/"
    And I spawn the site "rbackCreditGiftcard" from the base config "gift-cards"
    And I seed using the file "CheckoutCustomerCreditGiftCards" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "customercredit-giftcard@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8388800000000000500" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£12.99"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£5.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£5.00"
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4111 1111 4555 1142"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "no3DS" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    When I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 5000ms
    Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
    And I expect that element "Checkout/OrderSummary.total" contains the text "£10.00"
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed

  @B2C2-4628 @ROLLBACK @DEFECT
  Scenario: Rollback: Customer Credit + 2 Giftcards + Adyen
    Given I open the page "/"
    And I spawn the site "rbackCreditGiftcard" from the base config "gift-cards"
    And I seed using the file "CheckoutCustomerCreditGiftCards" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    And I set "customercredit-giftcard@project.com" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I scroll to element "Checkout/RedeemAGiftCard.accordion"  
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8388800000000000250" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8388" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "8378800000000000250" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8378" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    And I scroll to element "Checkout/OrderSummary.heading"
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£22.99"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£12.99"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£5.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£5.00"
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4111 1111 4555 1142"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I pause for 2000ms
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "no3DS" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    When I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 5000ms
    Then I wait on element "CheckoutPayment.unableToApplyGiftcards" to be displayed
    And I click on the button "CheckoutOrderSummary.modalOk"
    And I expect that element "CheckoutOrderSummary.giftCard" is not displayed
    And I expect that element "Checkout/OrderSummary.total" contains the text "£10.00"
    And I open the page "/my-account"
    And I expect that element "CustomerCredits.hasAccountCredit" is displayed

