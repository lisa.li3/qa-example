@ACCOUNT
@REGRESSION

Feature: My Account
  B2C2-505 - User logging in and out of their account
  B2C2-2822 - Logging off .COM site via the LOG OUT button on /my-account page, it does not direct you to Homepage. (AC11)
  B2C2-176 - Request for a forgotten password
  B2C2-2772 - project helpline button on mobile my account pages

  @B2C2-505 @B2C2-2822 @B2C2-2772
  Scenario: Seed for my account section
    Given I seed using the file "MyAccount"
    When I open the page "/login"
    And I wait on element "Login.emailAddress" to be displayed

  @B2C2-505
  Scenario Outline: When <email> and <password> are used to login, the user can see <displayedText>
    Given I wait on element "Login.emailAddress"
    When I set "<email>" to the inputfield "Login.emailAddress"
    And I set "<password>" to the inputfield "Login.password"
    When I click on the element "Login.signInButton"
    Then I expect that element "<displayedText>" is displayed

    Examples:
      | email                    | password     | displayedText                   |
      | testabcgmail.com         | its-a-secret | Login.invalidEmailWarning       |
      | testabc@gmail.com        | its-a-secret | Login.loginNotRecognisedWarning |
      | myaccount@project.local | 1234$abc     | Login.loginNotRecognisedWarning |
      |                          | its-a-secret | Login.valueRequiredWarning      |
      | myaccount@project.local |              | Login.valueRequiredWarning      |

  @B2C2-505 @B2C2-2822
  Scenario: When a valid email and password are used to login, the user is logged in successfully
    When I set "myaccount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    When I click on the element "Login.signInButton"
    Then I expect that element "Bob Dylan" is displayed
    And I click on the element "MyAccount.logOutButton"
    And I expect that the path is "/"

  @B2C2-505
  Scenario: User is logged out when their token expires
    Given I seed config using the file "MyAccount" and I switch to the new site
    And I seed using the file "MyAccountTokenExpiry"
    When I open the page "/login"
    And I set "tokenExpiry@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I pause for 7000ms
    And I refresh the page
    Then I expect that element "Bob Dylan" is not displayed
    And I open the domain "com"

  @B2C2-176
  Scenario: User is is presented with forgot password page
    Given I am on the page "/login"
    Given I click on the element "Login.forgottenPassword"
    Then I expect that element "ForgotPassword.submitButton" is displayed
    And I expect that element "ForgotPassword.emailAddressTextbox" is displayed
    And I expect that element "ForgotPassword.registerButton" is displayed

  @B2C2-176
  Scenario: User is alerted when entering invalid email address to reset password
    Given I set "emailexample.com" to the inputfield "ForgotPassword.emailAddressTextbox"
    When I click on the element "ForgotPassword.submitButton"
    Then I expect that element "ForgotPassword.invalidEmailMessage" is displayed

  @B2C2-176
  Scenario: User is presented with the registration page after clicking the register for account button
    Given I click on the element "ForgotPassword.registerButton"
    Then I expect the url to contain "/register"

  @B2C2-2772 @SKIP_DESKTOP @SKIP_TABLET
  Scenario: Ensure when logged in as user, helpline is displayed on account page
    And I login with "myaccount@project.local" and "its-a-secret"
    And I set the local storage key "wishlist" with the content "wishlist2772-ONESIZE"
    Then I expect that element "MyAccount.projectHelpline" is displayed

  @B2C2-2772 @SKIP_DESKTOP @SKIP_TABLET
  Scenario Outline: Ensure when logged in as user, helpline is displayed on the account information page
    Given I click on the element "MyAccount.<pages>"
    Then I expect that element "MyAccount.projectHelpline" is displayed
    And I am on the page "/my-account"

    Examples:
      | pages                     |
      | accountInformationButton  |
      | deliveryInformationButton |
      | orderHistoryButton        |
      | wishlistButton            |

  @B2C2-2772 @SKIP_DESKTOP @SKIP_TABLET
  Scenario: Login
    Given I seed using the file "myaccount"
    When I open the page "/login"
    And I set "myaccount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
    Then I expect that element "MyAccount.projectHeplineButton" is displayed

  @B2C2-2772 @SKIP_DESKTOP @SKIP_TABLET
  Scenario Outline: When user goes to the <page> using a mobile device, the project helpline is displayed
    Given I click on the element "MyAccount.<page>"
    And I scroll to element "MyAccount.projectHepline"
    And I expect that the attribute "href" from element "MyAccount.projectHeplineButton" is "tel:+44 (0)333 3212222"
    And I am on the page "/my-account"
    Examples:
      | page                      |
      | accountInformationButton  |
      | deliveryInformationButton |
      | orderHistoryButton        |
      | wishlistButton            |
#    |forgottenPassword|