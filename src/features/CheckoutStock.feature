@CHECKOUT
@CHECKOUTSTOCK
@REGRESSION

Feature: Checkout stock

  @B2C2-122
  Scenario: Customer is shown out of stock modal for single product
    Given I am on the page "/"
    And I seed using the file "CheckoutSingleInStockProduct" and add the items into my "basket" local storage
    And I seed using the file "CheckoutSingleOutOfStockProduct"
    When I open the page "/checkout"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProduct"
    And I expect that element "CheckoutOrderSummary.oneRemoved" is displayed

  @B2C2-122
  Scenario: Customer is redirected to the shopping bag page with single out of stock product removed from basket
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    Then I expect the url to contain "/shopping-bag"
    And I expect that element "OutOfStockProduct" is not displayed
    And I expect that element "No items presently in your bag" is displayed

  @B2C2-122
  Scenario: Customer is shown single product that is in stock
    Given I am on the page "/"
    And I seed using the file "CheckoutSingleProduct" and add the items into my "basket" local storage
    When I open the page "/checkout"
    Then I expect that element "SingleStockProduct" is displayed

  @B2C2-122
  Scenario: Customer is notified of out of stock products mixed in with products that are out of stock
    Given I am on the page "/"
    And I seed using the file "CheckoutMultipleInStockProducts" and add the items into my "basket" local storage
    And I seed using the file "CheckoutOutInStockAndOutOfStockProducts"
    When I open the page "/checkout"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductOne"
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductTwo"
    And I expect that element "CheckoutOrderSummary.oneRemoved" is displayed
    And I expect that element "CheckoutOrderSummary.threeRemoved" is displayed

  @B2C2-122
  Scenario: Out of stock products are removed from shopping basket and in stock products are displayed
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    Then I expect that element "SingleStockProductOne" is displayed
    And I expect that element "SingleStockProductTwo" is displayed
    And I expect that element "SingleStockProductThree" is displayed
    And I expect that element "OutOfStockProductOne" is not displayed
    And I expect that element "OutOfStockProductTwo" is not displayed

  @B2C2-122
  Scenario: Customer is notified of two out of stock products
    Given I am on the page "/"
    And I seed using the file "CheckoutTwoInStockProducts" and add the items into my "basket" local storage
    And I seed using the file "CheckoutTwoOutOfStockProducts"
    When I open the page "/checkout"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductOne"
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductTwo"
    And I expect that element "CheckoutOrderSummary.oneRemoved" is displayed
    And I expect that element "CheckoutOrderSummary.threeRemoved" is displayed

  @B2C2-122
  Scenario: Two out of stock products are removed from shopping basket
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    And I expect that element "OutOfStockProductOne" is not displayed
    And I expect that element "OutOfStockProductOne" is not displayed
    And I expect that element "No items presently in your bag" is displayed

  @B2C2-170
  Scenario: Customer is presented with out of stock modal following stock check
    Given I open the page "/"
    And I seed using the file "CheckoutSingleInStockProduct" and add the items into my "basket" local storage
    And I open the page "/checkout"
    And I seed using the file "CheckoutSingleOutOfStockProduct"
    And I scroll to element "CheckoutPayment.buyNowButton"
    And I wait on element "CheckoutPayment.buyNowButton"
    When I click on the element "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProduct"
    And I expect that element "CheckoutOrderSummary.oneRemoved" is displayed

  @B2C2-170
  Scenario: Customer is redirected to the shopping bag page after accepting stock check
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    Then I expect the url to contain "/shopping-bag"
    And I expect that element "OutOfStockProduct" is not displayed
    And I expect that element "No items presently in your bag" is displayed

  @B2C2-170
  Scenario: Logged in customer stock check
    Given I open the page "/"
    And I seed using the file "CheckoutSixInStockProduct" and add the items into my "basket" local storage
    And I open the page "/checkout/login"
    When I set "checkoutoutsixofstockproducts@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "CheckoutDelivery.homeDeliveryRadio"
    And I seed using the file "CheckoutOneInStockProduct"
    And I scroll to element "CheckoutPayment.buyNowButton"
    And I wait on element "CheckoutPayment.buyNowButton"
    When I click on the element "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "ProductMulipleQuantity"
    And I expect that element "CheckoutOrderSummary.fiveRemoved" is displayed

  @B2C2-170
  Scenario: Customer is directed to the checkout page with updated stock level
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    And I expect that element "CheckoutOrderSummary.popUpOk" becomes not displayed
    And I expect that element "CheckoutOrderSummary.quantity" matches the text "1"
    And I expect that element "ProductMulipleQuantity" is displayed

  @B2C2-170
  Scenario: Customer is notified of out of stock products mixed in with products that are out of stock after clicking buy now
    Given I open the page "/"
    And I seed using the file "CheckoutMultipleInStockProducts" and add the items into my "basket" local storage
    When I open the page "/checkout"
    And I seed using the file "CheckoutOutInStockAndOutOfStockProducts"
    And I scroll to element "CheckoutPayment.buyNowButton"
    And I wait on element "CheckoutPayment.buyNowButton"
    When I click on the element "CheckoutPayment.buyNowButton"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductOne"
    And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "OutOfStockProductTwo"
    And I expect that element "CheckoutOrderSummary.oneRemoved" is displayed
    And I expect that element "CheckoutOrderSummary.threeRemoved" is displayed

  @B2C2-170
  Scenario: Customer is directed to the checkout page with updated stock level after removing out of stock products
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    Then I expect that element "SingleStockProductOne" is displayed
    And I expect that element "SingleStockProductTwo" is displayed
    And I expect that element "SingleStockProductThree" is displayed
    And I expect that element "OutOfStockProductOne" is not displayed
    And I expect that element "OutOfStockProductTwo" is not displayed

  @B2C2-170
  Scenario: Customer is presented with out of stock modal for product with two different sizes
    Given I open the page "/"
    And I seed using the file "CheckoutSingleInStockWithDifferentSizesProduct" and add the items into my "basket" local storage
    When I open the page "/checkout"
    And I seed using the file "CheckoutSingleOutOfStockWithDifferentSizesProduct"
    And I scroll to element "CheckoutPayment.buyNowButton"
    And I wait on element "CheckoutPayment.buyNowButton"
    When I click on the element "CheckoutPayment.buyNowButton"
    And I wait on element "CheckoutOrderSummary.outOfStockModal"
    Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
    And I expect that element "CheckoutOrderSummary.outOfStockModal" contains the text "ProductWithTwoSizes"
    And I expect the count of elements in "CheckoutOrderSummary.twoRemoved" to equal 2
    And I expect that element "CheckoutOrderSummary.sizeEleven" is displayed
    And I expect that element "CheckoutOrderSummary.sizeTwelve" is displayed

  @B2C2-170
  Scenario: Customer is redirected to the shopping bag page with the two products of different sizes removed
    Given I click on the element "CheckoutOrderSummary.popUpOk"
    Then I expect the url to contain "/shopping-bag"
    And I expect that element "ProductWithTwoSizes" is not displayed
    And I expect that element "No items presently in your bag" is displayed

