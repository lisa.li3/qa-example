@PDP
@PDP_STAFF
@REGRESSION
#  Only fails when run via the pipeline

Feature: Product Description Page (PDP) - Staff Discount

  #
  # Display of Staff Discount on PDP
  # https://supergroupbt.atlassian.net/browse/B2C2-537
  #

  Rule: I WANT to be shown the staff discounted prices on the PDP

    @B2C2-537
    Scenario: Seed products and a staff discount user on a different site
      Given I seed config using the file "ProductDescriptionPage_StaffDiscountWithProducts"
      And I open the siteId "staffdiscount-pdp"
      And I seed using the file "ProductDescriptionPage_StaffDiscountUserAndProductsWithRecommendations"
      And  I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 1 - Staff discount set to true
      Given I click on the element "Product 1 - Staff discount set to true"

    @B2C2-537
    Scenario: For a 'GUEST' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 1 - Staff discount set to true' is correct
      Then I expect that element "ProductDescription.wasPrice" contains the text "222.00"
      Then I expect that element "ProductDescription.youSave" is not displayed

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 2 - Markdown with Staff discount set to true
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed
      Given I click on the element "Product 2 - Markdown with Staff discount set to true"

    @B2C2-537
    Scenario Outline: For a 'GUEST' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 2 - Markdown with Staff discount set to true' is correct
      Then I expect that element "<element>" <condition> the text "<value>"
      Then I expect that element "ProductDescription.youSave" is displayed

      Examples:
        | element                         | condition     | value   |
        | ProductDescription.wasPrice     | contains      | 100.00  |
        | ProductDescription.nowPrice     | contains      | 25.00   |
        | ProductDescription.youSavePrice | contains      | 75.00   |

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 3 - Does not qualify
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 3 - Does not qualify" becomes displayed
      Given I click on the element "Product 3 - Does not qualify"

    @B2C2-537
    Scenario: For a 'GUEST' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 3 - Does not qualify' is correct
      Then I expect that element "ProductDescription.wasPrice" contains the text "12.00"

    @B2C2-537
    Scenario: Log in with a 'STAFF' user and navigate to a 'CATEGORY PLP'
      Given I open the page "/login"
      And I set "pdpuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithDiscount"
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 1 - Staff discount set to true
      Given I click on the element "Product 1 - Staff discount set to true"

    @B2C2-537
    Scenario Outline: For a logged in 'STAFF' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 1 - Staff discount set to true' is correct
      Then I expect that element "<element>" <condition> the text "<value>"
      Then I expect that element "ProductDescription.youSave" is displayed

      Examples:
        | element                         | condition     | value   |
        | ProductDescription.wasPrice     | contains      | 222     |
        | ProductDescription.nowPrice     | contains      | 88.80   |
        | ProductDescription.youSavePrice | contains      | 133.20  |

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 2 - Markdown with Staff discount set to true
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed
      Given I click on the element "Product 2 - Markdown with Staff discount set to true"

    @B2C2-537
    Scenario Outline: For a logged in 'STAFF' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 2 - Markdown with Staff discount set to true
      Then I expect that element "<element>" <condition> the text "<value>"
      Then I expect that element "ProductDescription.youSave" is displayed

      Examples:
        | element                         | condition     | value   |
        | ProductDescription.wasPrice     | contains      | 100.00  |
        | ProductDescription.nowPrice     | contains      | 40.00   |
        | ProductDescription.youSavePrice | contains      | 60.00   |

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 3 - Does not qualify
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed
      Given I click on the element "Product 3 - Does not qualify"

    @B2C2-537
    Scenario: For a logged in 'STAFF' user on the 'PDP' page, check that the 'WAS/NOW' pricing for the 'Product 3 - Does not qualify'
      Then I expect that element "ProductDescription.wasPrice" contains the text "12.00"
      Then I expect that element "ProductDescription.youSave" is not displayed

    @B2C2-537
    Scenario: Log out 'STAFF' user, and log in with a 'NON STAFF' user and navigate to a 'CATEGORY PLP'
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"
      When I open the page "/login"
      And I set "pdpuser.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "NonStaffUser WithoutStaffDiscount"
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      And I wait on element "Product 1 - Staff discount set to true"

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 1 - Staff discount set to true
      Given I click on the element "Product 1 - Staff discount set to true"

    @B2C2-537
    Scenario: For a logged in 'NON STAFF', check that the 'WAS/NOW' pricing for the 'Product 1 - Staff discount set to true' is correct
      Then I expect that element "ProductDescription.wasPrice" contains the text "222.00"
      Then I expect that element "ProductDescription.youSave" is not displayed

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 2 - Markdown with Staff discount set to true
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed
      Given I click on the element "Product 2 - Markdown with Staff discount set to true"

    @B2C2-537
    Scenario Outline: For a logged in 'NON STAFF', check that the 'WAS/NOW' pricing for the 'Product 2 - Markdown with Staff discount set to true' is correct
      Then I expect that element "<element>" <condition> the text "<value>"
      Then I expect that element "ProductDescription.youSave" is displayed

      Examples:
        | element                         | condition     | value   |
        | ProductDescription.wasPrice     | contains      | 100.00  |
        | ProductDescription.nowPrice     | contains      | 25.00   |
        | ProductDescription.youSavePrice | contains      | 75.00   |

    @B2C2-537
    Scenario: setup for B2C2-537 - Product 3 - Does not qualify
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      Then I expect that element "Product 3 - Does not qualify" becomes displayed
      Given I click on the element "Product 3 - Does not qualify"

    @B2C2-537
    Scenario: For a logged in 'NON STAFF', check that the 'WAS/NOW' pricing for the 'Product 3 - Does not qualify' is correct
      Then I expect that element "ProductDescription.wasPrice" contains the text "12.00"

    @B2C2-537
    Scenario: Log out out previous user and go to a the 'PDP' page for a product with 'Wear it With (WWW)' and 'You Might Also like (YMAL)' products
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      And I click on the element "Product 4 - WIW and YMAL"

    @B2C2-537
    Scenario Outline: For a 'GUEST' user on a 'PDP' page, The 'Wear it With' and 'You Might Also Like' product details for '<productName>' should <condition> contain <details>
      Then I expect the element in "ProductDescription.wwwProductList" containing text "<productName>" to <condition> contain text "<details>"
      Then I expect the element in "ProductDescription.ymalProductList" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details  |
        | Product 1 - Staff discount set to true               | also      | 222.00   |
        | Product 1 - Staff discount set to true               | not       | 88.80    |
        | Product 1 - Staff discount set to true               | not       | 133.20   |
        | Product 2 - Markdown with Staff discount set to true | also      | 100.00   |
        | Product 2 - Markdown with Staff discount set to true | also      | 25.00    |
        | Product 2 - Markdown with Staff discount set to true | also      | 75.00    |
        | Product 2 - Markdown with Staff discount set to true | not       | 50.00    |
        | Product 3 - Does not qualify                         | also      | 12.00    |
        | Product 3 - Does not qualify                         | not       | 4.81     |
        | Product 3 - Does not qualify                         | not       | 7.19     |

    @B2C2-537
    Scenario: For a 'GUEST' user on a 'PDP' page, The 'Wear it With' and 'You Might Also Like' product should show you save for discounted products
      Then the element "ProductDescription.wwwYouSaveOnProductOne" is not displayed
      Then the element "ProductDescription.ymalYouSaveOnProductOne" is not displayed
      Then the element "ProductDescription.wwwYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.ymalYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.wwwYouSaveOnProductThree" is not displayed
      Then the element "ProductDescription.ymalYouSaveOnProductThree" is not displayed

    @B2C2-537
    Scenario: Log in with a 'STAFF' user and go to a the 'PDP' page for a product with 'Wear it With (WWW)' and 'You Might Also like (YMAL)' products
      Given I open the page "/login"
      And I set "pdpuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithDiscount"
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      And I click on the element "Product 4 - WIW and YMAL"

    @B2C2-537
    Scenario Outline: For a 'STAFF' user on a PDP page, The 'Wear it With' and 'You Might Also Like' product details for '<productName>' should <condition> contain <details>
      Then I expect the element in "ProductDescription.wwwProductList" containing text "<productName>" to <condition> contain text "<details>"
      Then I expect the element in "ProductDescription.ymalProductList" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details   |
        | Product 1 - Staff discount set to true               | also      | 222.00    |
        | Product 1 - Staff discount set to true               | also      | 88.80     |
        | Product 1 - Staff discount set to true               | also      | 133.20    |
        | Product 2 - Markdown with Staff discount set to true | also      | 100.00    |
        | Product 2 - Markdown with Staff discount set to true | not       | 25.00     |
        | Product 2 - Markdown with Staff discount set to true | also      | 40.00     |
        | Product 2 - Markdown with Staff discount set to true | also      | 60.00     |
        | Product 3 - Does not qualify                         | also      | 12.00     |
        | Product 3 - Does not qualify                         | not       | 4.81      |
        | Product 3 - Does not qualify                         | not       | 7.19      |

    @B2C2-537
    Scenario: For a 'STAFF' user on a PDP page, The 'Wear it With' and 'You Might Also Like' product should show you save for discounted products
      Then the element "ProductDescription.wwwYouSaveOnProductOne" is displayed
      Then the element "ProductDescription.ymalYouSaveOnProductOne" is displayed
      Then the element "ProductDescription.wwwYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.ymalYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.wwwYouSaveOnProductThree" is not displayed
      Then the element "ProductDescription.ymalYouSaveOnProductThree" is not displayed

    @B2C2-537
    Scenario: Log out 'STAFF' user, and log in with a 'NON STAFF' user go to a the 'PDP' page for a product with 'Wear it With (WWW)' and 'You Might Also like (YMAL)' products
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"
      When I open the page "/login"
      And I set "pdpuser.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "NonStaffUser WithoutStaffDiscount"
      And I open the page "/main/framework/b2c2_537_pdp_staffdiscount"
      And I click on the element "Product 4 - WIW and YMAL"

    @B2C2-537
    Scenario Outline: For a 'NON STAFF' user on a 'PDP' page, The 'Wear it With' and 'You Might Also Like' product details for '<productName>' should <condition> contain <details>
      Then I expect the element in "ProductDescription.wwwProductList" containing text "<productName>" to <condition> contain text "<details>"
      Then I expect the element in "ProductDescription.ymalProductList" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details  |
        | Product 1 - Staff discount set to true               | also      | 222.00   |
        | Product 1 - Staff discount set to true               | not       | 88.80    |
        | Product 1 - Staff discount set to true               | not       | 133.20   |
        | Product 2 - Markdown with Staff discount set to true | also      | 100.00   |
        | Product 2 - Markdown with Staff discount set to true | also      | 25.00    |
        | Product 2 - Markdown with Staff discount set to true | not       | 50.00    |
        | Product 2 - Markdown with Staff discount set to true | also      | 75.00    |
        | Product 3 - Does not qualify                         | also      | 12.00    |
        | Product 3 - Does not qualify                         | not       | 4.81     |
        | Product 3 - Does not qualify                         | not       | 7.19     |

    @B2C2-537
    Scenario: For a 'NON STAFF' user on a 'PDP' page, The 'Wear it With' and 'You Might Also Like' product should show you save for discounted products
      Then the element "ProductDescription.wwwYouSaveOnProductOne" is not displayed
      Then the element "ProductDescription.ymalYouSaveOnProductOne" is not displayed
      Then the element "ProductDescription.wwwYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.ymalYouSaveOnProductTwo" is displayed
      Then the element "ProductDescription.wwwYouSaveOnProductThree" is not displayed
      Then the element "ProductDescription.ymalYouSaveOnProductThree" is not displayed

    @B2C2-537
    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

