@BASKET_PROMOS
@REGRESSION

Feature: Basket Level Promotions

  @B2C2-3043
    Scenario: Seeding the environment
      Given I seed config using the file "BasketLevelPromos"
      When I open the siteId "b2c23043"
      And I seed using the file "BasketLevelPromos"

  @B2C2-3043
    Scenario: Add 3 same products to the basket to trigger a Spend £X, Save £Y promotion to a basket containing an X for Y promo
      When I am on the page "/products?sku=osfp7tespa-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed

  @B2C2-3043
    Scenario: Applying a voucher to trigger a Spend £X, Save £Y promotion to a basket containing an X for Y promo
      Given I open the page "/checkout"
      And I wait on element "CheckoutDelivery.homeDeliveryRadio"
      When I scroll to element "Checkout/PromoCode.accordion"
      And I click on the element "Checkout/PromoCode.accordion"
      And I set "30Off100Spend" to the inputfield "Checkout/PromoCode.promoCode"
      And I click on the element "Checkout/PromoCode.apply"
      And I pause for 2000ms
      Then I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £179.97
        Shipping fee
        £0.00
        3 for 2
        -£59.99
        Promotion Code
        -£30.00
        Total
        £89.98
        """

  @B2C2-3043
    Scenario: Applying a voucher to trigger a Spend £X, Save Y% promotion to a basket containing an X for Y promo
      When I scroll to element "Checkout/PromoCode.accordion"
      And I click on the element "Checkout/PromoCode.accordion"
      And I set "20PercentOff100Spend" to the inputfield "Checkout/PromoCode.promoCode"
      And I click on the element "Checkout/PromoCode.apply"
      And I pause for 2000ms
      Then I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £179.97
        Shipping fee
        £0.00
        3 for 2
        -£59.99
        Promotion Code
        -£24.00
        Total
        £95.98
        """

  @B2C2-3043
    Scenario: Clear the environment to trigger a promotion to a basket containing an X for £Y promo
      Given I open the page "/"
      And I delete the local storage
      And I refresh the page

  @B2C2-3043
    Scenario: Add 3 same products to the basket to trigger a Spend £X, Save £Y promotion to a basket containing an X for £Y promo
      When I am on the page "/products?sku=hnf4pjy3ds-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
  
  @B2C2-3043
    Scenario: Applying a voucher to trigger a Spend £X, Save £Y promotion to a basket containing an X for £Y promo
      Given I open the page "/checkout"
      And I wait on element "CheckoutDelivery.homeDeliveryRadio"
      When I scroll to element "Checkout/PromoCode.accordion"
      And I click on the element "Checkout/PromoCode.accordion"
      And I set "30Off100Spend" to the inputfield "Checkout/PromoCode.promoCode"
      And I click on the element "Checkout/PromoCode.apply"
      And I pause for 2000ms
      Then I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £240.00
        Shipping fee
        £0.00
        3 for £120
        -£120.00
        Promotion Code
        -£30.00
        Total
        £90.00
        """

  @B2C2-3043
    Scenario: Applying a voucher to trigger a Spend £X, Save Y% promotion to a basket containing an X for £Y promo
      When I scroll to element "Checkout/PromoCode.accordion"
      And I click on the element "Checkout/PromoCode.accordion"
      And I set "30PercentOff100Spend" to the inputfield "Checkout/PromoCode.promoCode"
      And I click on the element "Checkout/PromoCode.apply"
      And I pause for 2000ms
      Then I expect that the values returned by "CheckoutOrderSummary.informationInOrderSummary" to be in the following order
        """
        Order Summary
        Subtotal
        £240.00
        Shipping fee
        £0.00
        3 for £120
        -£120.00
        Promotion Code
        -£36.00
        Total
        £84.00
        """
