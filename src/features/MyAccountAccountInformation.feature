@ACCOUNT
@ACCOUNT_INFORMATION
@REGRESSION

Feature: My Account - Account Information
  B2C2-1639 - Account Information Page

  @B2C2-1639 @B2C2-4754
  Scenario: Seed for my account section
    #    Given I seed using the file "MyAccount"
    When I open the page "/login"
    Then I expect that element "Login.emailAddress" is displayed

  @B2C2-2608
  Scenario: Should have the dedicated website helpline displayed
    Given I am on the page "/login"
    Then I wait on element "Login.helplineNumber" to be displayed

  @B2C2-1639 @B2C2-4754
  Scenario: User goes to the account information page where they can enter their details
    When I set "myaccount@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I click on the element "MyAccount.accountInformationButton"
    Then I expect that element "AccountInformation.emailAddressTextbox" is displayed

  @B2C2-1639
  Scenario Outline: Ensure whilst on the Account Information page the following fields are displayed - <expectedValue>
    Then I expect that element "AccountInformation.<expectedValue>" is displayed

    Examples:
      | expectedValue              |
      | emailAddressTextbox        |
      | passwordTextbox            |
      | confirmPasswordTextbox     |
      | phoneTextbox               |
      | mobilPhoneTextbox          |
      | gender                     |
      | dateOfBirthDay             |
      | dateOfBirthMonth           |
      | dateOfBirthYear            |
      | firstNameTextbox           |
      | lastNameTextbox            |
      | address1Textbox            |
      | address2Textbox            |
      | stateProvinceRegionTextbox |
      | zipPostalCode              |
      | country                    |
      | subscribeCheckbox          |

  @B2C2-1639
  Scenario Outline: When entering <value> into the email textbox I am displayed with a message for invalid email address
    Given I set "<value>" to the inputfield "AccountInformation.emailAddressTextbox"
    When I click on the element "AccountInformation.passwordTextbox"
    Then I expect that element "AccountInformation.invalidEmailMessage" is displayed

    Examples:
      | value        |
      | abc          |
      | abc@abc      |
      | abc.com      |
      | .abc@abc.com |
      | abc.@abc.com |

  @B2C2-1639
  Scenario Outline: User is displayed with password rules - <values>
    Then I expect that element "<values>" is displayed

    Examples:
      | values                                  |
      | A password must contain the following   |
      | Minimum of 8 characters                 |
      | One or more uppercase letter            |
      | One or more lowercase letter            |
      | One or more number or special character |

  @B2C2-1639
  Scenario Outline: Ensure when user enters <values> into the password textbox, user is displayed password error <textDisplayed>
    Given I set "<values>" to the inputfield "AccountInformation.passwordTextbox"
    When I click on the element "AccountInformation.emailAddressTextbox"
    Then I expect that element "AccountInformation.passwordErrors" matches the text "<textDisplayed>"

    Examples:
      | values    | textDisplayed                           |
      | abcdefg   | MINIMUM OF 8 CHARACTERS                 |
      | ABCDEFGH1 | ONE OR MORE LOWERCASE LETTER            |
      | Abcdefgh  | ONE OR MORE NUMBER OR SPECIAL CHARACTER |

  @B2C2-1639
  Scenario: User is not shown password validation error
    Given I set "1Th15IsaVal1dPassw*rd" to the inputfield "AccountInformation.passwordTextbox"
    When I click on the element "MyAccount.emailAddressTextbox"
    Then I expect that element "AccountInformation.passwordErrors" is not displayed

  @B2C2-1639
  Scenario: User sets password that does not match with confirm password
    Given I am on the page "/my-account"
    And I click on the element "MyAccount.accountInformationButton"
    And I set "ThisIsNotTheSame" to the inputfield "AccountInformation.confirmPasswordTextbox"
    And I set "1Th15IsaVal1dPassw*rd" to the inputfield "AccountInformation.passwordTextbox"
    When I click on the element "AccountInformation.saveButton"
    Then I expect that element "AccountInformation.confirmPasswordWarning" is displayed

  @B2C2-1639
  Scenario Outline: Ensures correct validation when entering <value> into <field>
    Given I set "<value>" to the inputfield "AccountInformation.<field>"
    When I click on the element "AccountInformation.emailAddressTextbox"
    Then I expect that element "AccountInformation.phoneNumberLongWarning" is displayed
    And I clear the inputfield "AccountInformation.<field>"

    Examples:
      | value                                                            | field             |
      | 64-1111111111111111111111111111111111111111111111111111111111111 | phoneTextbox      |
      | 64-1111111111111111111111111111111111111111111111111111111111111 | mobilPhoneTextbox |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                                | firstNameTextbox  |
      | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa                                | lastNameTextbox   |

  @B2C2-1639
  Scenario Outline: Ensure when entering <value> into the <field>
    Given I set "<value>" to the inputfield "AccountInformation.<field>"
    Then I expect that element "AccountInformation.phoneNumberLongWarning" is not displayed
    And I clear the inputfield "AccountInformation.<field>"

    Examples:
      | value                          | field             |
      | 0[222]+123-32123               | phoneTextbox      |
      | 0[222]+123-32123               | mobilPhoneTextbox |
      | abc123!£&aaaaaaaaaaaaaaaaaaaaa | firstNameTextbox  |
      | abc123!£&aaaaaaaaaaaaaaaaaaaaa | lastNameTextbox   |

  @B2C2-1639
  Scenario Outline: User is alerted when entering values <field>
    And I set "<value>" to the inputfield "AccountInformation.<field>"
    When I click on the element "AccountInformation.saveButton"
    Then I expect that element "AccountInformation.phoneNumberValueTypeWarning" is displayed
    And I clear the inputfield "AccountInformation.<field>"

    Examples:
      | value | field             |
      | a     | phoneTextbox      |
      | a     | mobilPhoneTextbox |

  @B2C2-1639
  Scenario Outline: User is not alerted when entering values of the correct type - <field>
    Given I set "<value>" to the inputfield "AccountInformation.<field>"
    Then I expect that element "AccountInformation.phoneNumberValueTypeWarning" is not displayed
    And I clear the inputfield "AccountInformation.<field>"

    Examples:
      | value            | field             |
      | 0[222]+123-32123 | phoneTextbox      |
      | 0[222]+123-32123 | mobilPhoneTextbox |

  @B2C2-1639
  Scenario Outline: User is able to select from gender options - <gender>
    Given I select the option with the text "<gender>" for element "AccountInformation.gender"

    Examples:
      | gender            |
      | Male              |
      | Female            |
      | Prefer not to say |

  @B2C2-1639 @B2C2-4754
  Scenario: User is presented with option to subscribe to electronic marketing
    When I refresh the page
    Then I expect that the attribute "name" from element "AccountInformation.subscriptionApproval" is "subscriptionApproval"
    And I expect that the attribute "type" from element "AccountInformation.subscriptionApproval" is "checkbox"
    And I expect that element "AccountInformation.subscriptionApprovalLabel" is displayed

  @B2C2-1639 @B2C2-4754
  Scenario: Electronic marketing preference reflects user's selection
    When I click on the element "AccountInformation.subscribeCheckbox"
    Then I expect that checkbox "AccountInformation.subscriptionApproval" is checked
    And I click on the element "AccountInformation.saveButton"
    And I click on the element "MyAccount.accountInformationButton"
    And I expect that element "AccountInformation.emailAddressTextbox" is displayed
    And I expect that checkbox "AccountInformation.subscriptionApproval" is checked
    And I click on the element "AccountInformation.subscribeCheckbox"
    And I expect that checkbox "AccountInformation.subscriptionApproval" is not checked
    And I click on the element "AccountInformation.saveButton"
    And I click on the element "MyAccount.accountInformationButton"
    And I expect that element "AccountInformation.emailAddressTextbox" is displayed
    And I expect that checkbox "AccountInformation.subscriptionApproval" is not checked

  @B2C2-1639
  Scenario: User is not shown newsletter sign up on account information page
    Then I expect that element "NewsletterSignupBanner.complementary" is not displayed

  @B2C2-1639
  Scenario: User is able to reset date of birth fields
    And I clear the inputfield "AccountInformation.dateOfBirthDay"
    And I select the option with the text "Month" for element "AccountInformation.dateOfBirthMonth"
    And I clear the inputfield "AccountInformation.dateOfBirthYear"
    When I click on the element "AccountInformation.saveButton"
    And I click on the element "MyAccount.accountInformationButton"
    Then I expect that element "AccountInformation.dateOfBirthDay" not contains any text
    And I expect that element "Month" is displayed
    And I expect that element "AccountInformation.dateOfBirthYear" not contains any text
    And I am on the page "/my-account"
    And I click on the element "MyAccount.logOutButton"

  @B2C2-3665
  Scenario: Setup environment with CheckoutDeliveryAddress
    Given I seed config using the file "CheckoutDeliveryAddress"
    When I open the siteId "deliveryaddress"
    And I seed using the file "CheckoutDeliveryAddress" and add the items into my "basket" local storage

  @B2C2-3665
  Scenario: User goes to the account information page where they can enter their details
    When I open the page "/login"
    Then I expect that element "Login.emailAddress" is displayed
    And I set "checkoutdeliveryaddress@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I click on the element "MyAccount.accountInformationButton"
    And I expect that element "AccountInformation.emailAddressTextbox" is displayed

  @B2C2-3665
  Scenario: Should validate that when USA is the selected country, then the STATE/PROVINCE/REGION field is a single-select drop-down that lists all USA states in alphabetical order
    When I click on the element "AccountInformation.country"
    And I click on the element "AccountInformation.unitedStates"
    And I click on the element "MyAccount.stateSelect"
    Then I expect that the values returned by "MyAccount.states" to be in the following order
      """
      Select
      Alabama
      Alaska
      Arizona
      Arkansas
      California
      Colorado
      Connecticut
      Delaware
      District Of Columbia
      Florida
      Georgia
      Hawaii
      Idaho
      Illinois
      Indiana
      Iowa
      Kansas
      Kentucky
      Louisiana
      Maine
      Maryland
      Massachusetts
      Michigan
      Minnesota
      Mississippi
      Missouri
      Montana
      Nebraska
      Nevada
      New Hampshire
      New Jersey
      New Mexico
      New York
      North Carolina
      North Dakota
      Ohio
      Oklahoma
      Oregon
      Pennsylvania
      Rhode Island
      South Carolina
      South Dakota
      Tennessee
      Texas
      Utah
      Vermont
      Virginia
      Washington
      West Virginia
      Wisconsin
      Wyoming
      """
    And I click on the element "California"

  @B2C2-3665
  Scenario: Should validate that when USA is NOT the selected country, then the STATE/PROVINCE/REGION field is a non mandatory textbox and US state selection should not be preserved
    When I click on the element "AccountInformation.country"
    And I click on the element "United Kingdom"
    Then I expect that element "MyAccount.stateTextbox" is displayed
    And I expect that element "MyAccount.stateTextbox" not contains any text
    And I set "36characterTestInputIsTooLongXXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "STATE/ PROVINCE/ REGION"
    And I expect that element "MyAccount.inputTooLongMessage" is displayed
    And I set "36characterTestInputIsTooLongXXXXXX" to the inputfield "MyAccount.stateTextbox"
    And I click on the element "STATE/ PROVINCE/ REGION"
    And I expect that element "MyAccount.inputTooLongMessage" is not displayed

  @B2C2-4748
  Scenario: Seed using MyAccount
    Given I am on the page "/"
    And I delete the local storage
    And I refresh the page
    And I seed using the file "MyAccount"
    And I login with "myaccount@project.local" and "its-a-secret"

  @B2C2-4748
  Scenario: D.O.B is retained on account information after save
    Given I am on the page "/my-account/billing-information"
    And I set "5" to the inputfield "AccountInformation.dateOfBirthDay"
    And I set "February" to the inputfield "AccountInformation.dateOfBirthMonth"
    And I set "1905" to the inputfield "AccountInformation.dateOfBirthYear"
    When I click on the element "AccountInformation.saveButton"
    And I wait on element "MyAccount.accountInformationButton" to be displayed
    And I click on the element "MyAccount.accountInformationButton"
    And I refresh the page
    Then I expect that element "AccountInformation.dateOfBirthDay" contains the text "5"
    And I expect that element "AccountInformation.dateOfBirthMonth" contains the text "2"
    And I expect that element "AccountInformation.dateOfBirthYear" contains the text "1905"