@STORE_LOCATOR
@REGRESSION

Feature: Store Locator
  Scenario: Placeholder
    Given I am on the page "/"

#    This test is failing - needs looking into
  @B2C2-1611 @SKIP_MOBILE @SKIP_TABLET @SKIP_DESKTOP
  Scenario: Ensures store locator is visible in the footer and redirects to correct url for desktop and tablet
    Given I am on the page "/"
    And I wait on element "Footer.storeLocator" for 5000ms to be displayed
    And I scroll to element "Footer.storeLocator"
	When I click on the element "Footer.storeLocator"
    Then I expect the url to contain "/stores"

  @B2C2-1611 @SKIP_TABLET @SKIP_DESKTOP @SKIP_MOBILE
  Scenario: Ensures store locator is visible in the footer and redirects to correct url for mobile
    Given I am on the page "/"
    And I wait on element "Footer.footerInformation" for 5000ms to be displayed
    And I scroll to element "Footer.footerInformation"
    When I click on the element "Footer.footerInformation"
    And I scroll to element "Footer.storeLocator"
    And I click on the element "Footer.storeLocator"
    Then I expect the url to contain "/stores"
