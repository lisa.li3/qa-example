@SMOKE
@REGRESSION

Feature: Smoke Test - Guest User Single Item
  As a guest user
  I want to be able to navigate the site and complete a purchase for a single item

  Scenario: Seeding the environment
    Given I am on the page "/"
    And I seed using the file "SMOKE"
    And I refresh the page

  @SKIP_MOBILE @SKIP_TABLET
  Scenario: Navigating the Menu on desktop
    And I click on the button "Header.SmokeMenu"
    And I click on the element "Header.SmokeGuest"

  @SKIP_DESKTOP
  Scenario: Navigating the Menu on tablet and mobile
    When I click on the element "Navigation.burgerButton"
    And I click on the button "Header.SmokeMenu"
    And I click on the element "Header.SmokeGuest"

  Scenario: Navigating the PLP
    When I scroll to element "guestCheck0utname10001seed"
    And I click on the element "guestCheck0utname10001seed"

  Scenario: Add product to bag from PDP
    When I select the option with the text "guestSize10" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

  Scenario: Navigate mini-bag to access checkout gateway
    When I click on the element "Header.basketButton"
    And I click on the button "PopinBasket.checkoutButton"

  Scenario: Use the checkout gateway to proceed as guest
    When I wait on element "PopinBasket.miniBagDrawer" to not be displayed
    And I click on the button "CheckoutLogin.continueAsGuest"

  Scenario: Filling in guest delivery details
    When I set "firstName" to the inputfield "CheckoutAddress.firstNameTextbox"
    And I set "lastName" to the inputfield "CheckoutAddress.lastNameTextbox"
    And I set "123123123" to the inputfield "CheckoutAddress.phoneTextbox"
    And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.emailAddressTextbox"
    And I set "projecttesting+123@gmail.com" to the inputfield "CheckoutAddress.confirmEmailAddressTextbox"
    And I set "address1" to the inputfield "CheckoutAddress.address1Textbox"
    And I set "city" to the inputfield "CheckoutAddress.cityTextbox"
    And I set "GL51 9NH" to the inputfield "CheckoutAddress.postcodeTextbox"

  Scenario: Filling in card payment details
    When I wait on element "CheckoutPayment.cardPaymentNumberFrame" to be displayed
    And I pause for 10000ms
    And I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "4444 3333 2222 1111"
    And I switch back to the parent frame
    And I pause for 2000ms
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "0330"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I press "737"
    And I switch back to the parent frame
    And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"

  Scenario: Placing an order
    When I click on the button "CheckoutPayment.buyNowButton"

  Scenario: Confirmation Screen verification
    When the element "Confirmation.orderConfirmationText" is displayed
    Then the element "Confirmation.orderReferenceText" is displayed
    And the element "Confirmation.orderNumber" is displayed

