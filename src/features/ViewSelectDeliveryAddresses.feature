@VIEW_DELIVERY_ADDRESSES
@SELECT_DELIVERY_ADDRESSES
@REGRESSION

Feature: View/Select Delivery Addresses

  @B2C2-194
  Scenario: Setup environment with ViewSelectDeliveryAddresses
    Given I seed config using the file "ViewSelectDeliveryAddresses"
    When I open the siteId "b2c2194"
      And I seed using the file "ViewSelectDeliveryAddresses" and add the items into my "basket" local storage
      And I refresh the page
      And I login with "viewselectdeliveryaddresses@project.local" and "its-a-secret"

  @B2C2-194
  Scenario: Should check that the delivery addresses are presented in alphabetical order
    Given I am on the page "/my-account/delivery-addresses"
    Then I wait on element "ViewSelectDeliveryAddresses.defaultAddressLink" to be displayed
      And I expect that the values returned by "ViewSelectDeliveryAddresses.addresses" to be in the following order
        """
        A HOME3
        B HOME2
        """

  @B2C2-194
  Scenario: Should check that the correct data is displayed
    Given I am on the page "/checkout"
    When I wait on element "ViewSelectDeliveryAddresses.homeDeliveryRadio"
    Then I expect that element "Foo1 Bar1" is displayed
      And I expect that element "Flat 1, Bar Road 1, Foobar, London, W11 1BA, United Kingdom" is displayed
      And I expect that element "B Home2" is displayed
      And I expect that element "Foo2 Bar2, Flat 2, Bar Road 2, Foobar, London, W11 1BB, United Kingdom" is displayed
      And I expect that element "A Home3" is displayed
      And I expect that element "Foo3 Bar3, Flat 3, Bar Road 3, Foobar, London, W11 1BC, United Kingdom" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.defaultAddress" has the class "selected"
    
  @B2C2-194
  Scenario: Should select a premium delivery option
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryOptionsHeading"
    Then I click on the button "Standard delivery"

  @B2C2-194
  Scenario: Should click on the button to add a new delivery address
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryAddressHeading"
      And I click on the button "ViewSelectDeliveryAddresses.addAddressButton"

  @B2C2-194
  Scenario: Should fill out all the fields and add a new delivery address
    When I wait on element "ViewSelectDeliveryAddresses.addressLine1" to be displayed
      And I set "Home added" to the inputfield "ViewSelectDeliveryAddresses.addressTitle"
      And I set "Name" to the inputfield "ViewSelectDeliveryAddresses.firstName"
      And I set "Lastname" to the inputfield "ViewSelectDeliveryAddresses.lastName"
      And I set "address1" to the inputfield "ViewSelectDeliveryAddresses.addressLine1"
      And I set "address2" to the inputfield "ViewSelectDeliveryAddresses.addressLine2"
      And I set "city" to the inputfield "ViewSelectDeliveryAddresses.city"
      And I set "county" to the inputfield "ViewSelectDeliveryAddresses.county"
      And I set "GL51 9NH" to the inputfield "ViewSelectDeliveryAddresses.postcode"
      And I scroll to element "ViewSelectDeliveryAddresses.postcode"
      And I click on the button "ViewSelectDeliveryAddresses.countryDropdown"
      And I wait on element "ViewSelectDeliveryAddresses.countryUK" to be displayed
      And I click on the button "ViewSelectDeliveryAddresses.countryUK"
      And I set "010101010101" to the inputfield "ViewSelectDeliveryAddresses.phone"
      And I set "020202020202" to the inputfield "ViewSelectDeliveryAddresses.mobilePhone"
      And I click on the button "ViewSelectDeliveryAddresses.saveButton"
    Then I wait on element "ViewSelectDeliveryAddresses.checkoutHeading" to be displayed

  @B2C2-194
  Scenario: Should assert newly added delivery address is selected
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryAddressHeading"
    Then I expect that element "Home added" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.newAddress" has the class "selected"

  @B2C2-194
  Scenario: Should click a different address option and assert that it's selected
    When I click on the button "ViewSelectDeliveryAddresses.firstAddress"
    Then I expect that element "ViewSelectDeliveryAddresses.firstAddress" has the class "selected"

  @B2C2-194
  Scenario: Should check that the previously selected delivery option is preserved
    When I scroll to element "ViewSelectDeliveryAddresses.deliveryOptionsHeading"
    Then I expect that the attribute "aria-pressed" from element "ViewSelectDeliveryAddresses.standardDeliveryButton" is "true"

  @B2C2-194
  Scenario: Should check that viewing default delivery address displays correct information
    Given I am on the page "/my-account/delivery-addresses"
      And I wait on element "ViewSelectDeliveryAddresses.defaultAddressLink" to be displayed
    When I click on the button "ViewSelectDeliveryAddresses.defaultAddressButton"
      And I wait on element "ViewSelectDeliveryAddresses.emailAddressField" to be displayed
    Then I expect that element "ViewSelectDeliveryAddresses.password" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.confirmedPassword" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.phoneField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.mobilePhoneField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.gender" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.firstNameField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.lastNameField" is displayed

  @B2C2-194
  Scenario: Should check that viewing non-default delivery address displays correct fields
    Given I am on the page "/my-account/delivery-addresses"
      And I wait on element "A HOME3" to be displayed
    When I click on the element "A HOME3"
      And I wait on element "ViewSelectDeliveryAddresses.addressTitleField" to be displayed
    Then I expect that element "ViewSelectDeliveryAddresses.firstNameField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.lastNameField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addressLine1Field" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.addressLine2Field" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.cityField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.countyField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.postcodeField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.countryField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.phoneField" is displayed
      And I expect that element "ViewSelectDeliveryAddresses.mobilePhoneField" is displayed
