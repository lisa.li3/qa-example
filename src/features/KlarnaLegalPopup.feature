@KLARNA
@KLARNA_POPUP
@REGRESSION_FLAKY

Feature: Klarna Legal Popup

  @B2C2-1877
  Scenario: Setup environment with KlarnaLegalPopup
    Given I seed config using the file "KlarnaLegalPopup"
    When I open the siteId "b2c21877-1"
      And I seed using the file "KlarnaLegalPopup" and add the items into my "basket" local storage

  @B2C2-1877
  Scenario: Should check header/footer selectors are correct
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "Navigation.projectLogo" is displayed
      And I expect that element "Footer.main" is displayed
  
  @B2C2-1877
  Scenario: Should display legal text link
    When I scroll to element "CheckoutOrderSummary.paymentDetails"
      And I click on the button "KlarnaLegalPopup.klarnaPaymentMethod"
    Then I expect that element "KlarnaLegalPopup.paymentTermsText" is displayed
  
  @B2C2-1877
  Scenario: The linked terms should open in popup with no header and footer
    When I click on the element "KlarnaLegalPopup.paymentTermsLink"
      And I focus the last opened tab
      And I wait on element "KlarnaLegalPopup.sana"
    Then I expect the url "main/framework/test-sana-content-page" is opened in a new tab
      And I expect that element "Navigation.projectLogo" is not displayed
      And I expect that element "Footer.main" is not displayed
      And I focus the last opened tab

  @B2C2-1877
  Scenario: Setup environment with KlarnaLegalPopup2
    Given I seed config using the file "KlarnaLegalPopup2"
    When I open the siteId "b2c21877-2"
      And I seed using the file "KlarnaLegalPopup2" and add the items into my "basket" local storage

  @B2C2-1877
  Scenario: Should check header/footer selectors are correct
    Given I open the page "/checkout"
    When I wait on element "CheckoutDelivery.homeDeliveryRadio"
    Then I expect that element "Navigation.projectLogo" is displayed
      And I expect that element "Footer.main" is displayed

  @B2C2-1877 @REGRESSION_FLAKY @SKIP_DESKTOP @SKIP_MOBILE @SKIP_TABLET
  Scenario: Should not display legal text link
    When I scroll to element "CheckoutOrderSummary.paymentDetails"
      And I click on the button "KlarnaLegalPopup.klarnaPaymentMethod"
    Then I expect that element "KlarnaLegalPopup.paymentTermsText" is not displayed
