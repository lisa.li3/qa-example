@PLP
@PLP_QATB
@REGRESSION
@SKIP_HEADLESS

Feature: Product List Page (PLP) - Quick Add to Bag (QATB)
  As a user of the Product List Page, I can use the QATB functionality

  # ---
  #
  # Quick Add To Bag (QATB)
  # https://supergroupbt.atlassian.net/browse/B2C2-271
  #
  # ---

  Rule: Check Quick Add to Bag functionality on the PLP page

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Seed products on the Product List Page for the Quick Add to Bag functionality
      Given I seed using the file "ProductListPage_QATB"

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Check that the QATB button is not displayed on the PLP when no products are hovered over
      When I open the page "/main/framework/qatb_products"
      Then I expect that element "ProductListPage.quickAddToBagButton" is not displayed
      Then I expect that element "ProductListPage.quickAddToBagSizeDropdown" is not displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Check that only the QATB Button is displayed when an image is hovered over for a single size product
      When I wait on element "Product - 1SIZE" to be displayed
      When I move to "Product - 1SIZE"
      And I pause for 2000ms
      Then I expect that element "ProductListPage.quickAddToBagButton" is displayed
      And I expect that element "ProductListPage.quickAddToBagSizeDropdown" is not displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Ensures once a size is selected, the size is retained if the user navigates away from a product (as long as the user remains on the page)
      Given I move to "Product - 3 size options in stock"
      When I click on the element "ProductListPage.quickAddToBagSizeDropdown"
      And I select the 1st option for element "ProductListPage.quickAddToBagSizeDropdownLabels"
      And I move to "Product - 1SIZE"
      And I move to "Product - 3 size options in stock"
      Then I expect that element "ProductListPage.quickAddToBagSizeDropdown" matches the text "SMALL"

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Ensure that for a product with out of stock items, that they are disabled in the size dropdown for that product
      Given I move to "Product - 1 size option in stock"
      When I expect that element "ProductListPage.quickAddToBagSizeDropdown" is not displayed
      Then I expect that element "ProductListPage.quickAddToBagButton" is displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Ensure that on selecting a size in the size dropdown, it is displayed in the closed dropdown
      Given I move to "Product - 2 size option in stock"
      When I click on the element "ProductListPage.quickAddToBagSizeDropdown"
      And I select the 1st option for element "ProductListPage.quickAddToBagSizeDropdownLabels"
      Then I expect that element "ProductListPage.quickAddToBagSizeDropdown" matches the text "8"

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Check that when clicking 'Add to Bag' for a 1Size product that it is added to the mini-basket
      Given I move to "Product - 1SIZE"
      And I click on the element "ProductListPage.quickAddToBagButton"
      # Removed as loading checks skip minibag loading https://supergroupbt.atlassian.net/browse/B2C2-4515
      #Then I expect that element "PopinBasket.miniBagDrawer" is displayed
      Then I am on the page "/"
      And I click on the element "Navigation.basketButton"
      And I expect that element "PopinBasket.oneSizeProductNameInBasket" is displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Remove item from basket - cleanup
      When I click on the element "PopinBasket.removeButtonSingleItemInBasket"
      And I click on the element "PopinBasket.closeButton"
      Then I wait on element "PopinBasket.miniBagDrawer" to not be displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: Check that when selecting a size and clicking 'Add to Bag' for a multi size product that it is added to the mini-basket
      Given I am on the page "/main/framework/qatb_products"
      When I move to "Product - 3 size options in stock"
      And I click on the element "ProductListPage.quickAddToBagSizeDropdown"
      And I select the 1st option for element "ProductListPage.quickAddToBagSizeDropdownLabels"
      And I click on the element "ProductListPage.quickAddToBagButton"
      And I wait on element "PopinBasket.miniBagDrawer" to not be displayed
      And I am on the page "/"
      And I click on the element "Navigation.basketButton"
      Then I expect that element "PopinBasket.smallSizeInBasket" is displayed

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: QATB - Cleanup basket
      Given I delete the local storage
      And I refresh the page

    @B2C2-271 @SKIP_TABLET @SKIP_MOBILE
    Scenario: For a product which has more sizes than the maximum value set in the site config (UK Com site is set as maxOptionsQATB: 10) there should be no overlay displayed on the product
      Given I am on the page "/main/framework/qatb_products"
      And I move to "Product - 11 size options"
      Then I expect that element "ProductListPage.activeProductCardOverlay" is not displayed
