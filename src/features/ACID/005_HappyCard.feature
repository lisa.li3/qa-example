@ACID
@ACID-005
@ACID-CARD

Feature: 005 - Guest user - Single item - Card
  As a guest user
  I want to be able to purchase a single item
  I want to use my credit card to make payment

  Scenario: Seeding the environment
    Given I open the page "/"
    And I spawn the site "acid5" from the base config "default"
    And I seed "products.basic"

  Scenario: Check product details on PDP are correct
    Given I open the product detail page for "products.basic.first"
    Then I expect that element "PDP/ProductMain.style" matches the text "PRODUCTS.BASIC"
    And I expect that element "PDP/ProductMain.name" matches the text "Blue Hoodie"
    And I expect that element "PDP/ProductMain.standardPrice" matches the text "£54.99"

  Scenario: Add product to bag from PDP
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

  Scenario: Check the seeding as applied correctly
    Given I open the page "/checkout"
    When I scroll to element "Checkout/OrderSummary.heading"
    Then I expect that element "Checkout/OrderSummary.first-delivery" is displayed
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£54.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£54.99"

  Scenario: Add delivery information
    When I scroll to element "Checkout/DeliveryAddress.heading"
    And I set "definedCustomer.FirstName" to the inputfield "Checkout/DeliveryAddress.firstName"
    And I set "definedCustomer.LastName" to the inputfield "Checkout/DeliveryAddress.lastName"
    And I set "definedCustomer.Phone" to the inputfield "Checkout/DeliveryAddress.phone"
    And I set "definedCustomer.Email" to the inputfield "Checkout/DeliveryAddress.email"
    And I set "definedCustomer.Email" to the inputfield "Checkout/DeliveryAddress.confirmEmail"
    And I wait on element "Checkout/DeliveryAddress.address1" to be displayed
    And I set "definedCustomer.Address1" to the inputfield "Checkout/DeliveryAddress.address1"
    And I set "definedCustomer.Address2" to the inputfield "Checkout/DeliveryAddress.address2"
    And I set "definedCustomer.City" to the inputfield "Checkout/DeliveryAddress.city"
    And I set "definedCustomer.County" to the inputfield "Checkout/DeliveryAddress.state"
    And I set "definedCustomer.Postcode" to the inputfield "Checkout/DeliveryAddress.zip"

  Scenario: Make payment
    When I complete a CREDIT_CARD payment
    And I wait on element "CheckoutConfirmation/Main.heading" to be displayed

  Scenario: Order confirmation
    Then I expect the url to contain "/checkout/confirmation/"
    And I expect that element "CheckoutConfirmation/Main.orderReference" is not empty
    And I expect that element "CheckoutConfirmation/Main.confirmationEmail" matches the text "definedCustomer.Email"
    And I expect that element "CheckoutConfirmation/OrderSummary.subTotal" contains the text "£54.99"
    And I expect that element "CheckoutConfirmation/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.total" contains the text "£54.99"

  Scenario: FE02
    When I download the FE02 file from S3
    Then I expect the FE02 path "//oms:Order" has attribute "CustomerEMailID" with value "definedCustomer.Email"

    # Basket Deductions
    And I expect the FE02 path "//oms:Order/oms:Extn" has attribute "StaffOrder" with value "N"

    # Product
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "GiftFlag" with value "N"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "OrderedQty" with value "1"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemShortDesc" with value "Blue Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemID" with value "acid5-005HappyCard-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "LineID" with value "acid5-005HappyCard-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "ListPrice" with value "54.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "UnitPrice" with value "54.99"

    # Delivery Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Billing Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Payment Method
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "PaymentType" with value "CREDIT_CARD"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "CreditCardNo" with value "************1111"
    # NOTE: Should CreditCardExpiryDate be populated?
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "ChargeType" with value "AUTHORIZATION"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "RequestAmount" with value "54.99"










