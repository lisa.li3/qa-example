@ACID
@ACID-001
@ACID-PAYPAL
@ACID-PROMO

#  NOTES:
#   - Extra steps added to allow test to run until B2C2-4384 is fixed

Feature: 001 - Guest User - Multi-line Multi-Quality - Product-Level Promotion - PayPal
  As a guest user
  I want to be able to purchase several items which have promotions
  I want to purchase the item using PayPal

  Scenario: Seeding the environment
    Given I am on the page "/"
    And I spawn the site "acid1" from the base config "default"
    And I seed "products.basic"
    And I seed "products.promo"
    And I seed "users.paypal"
    And I refresh the page

  Scenario Outline: Add product to bag from PDP (<count> of 3)
    Given I open the product detail page for "products.promo.first"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

    Examples:
      | count |
      | 1     |
      | 2     |
      | 3     |

  Scenario Outline: Add second product to bag from PDP (<count> of 2)
    Given I open the product detail page for "products.basic.first"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

    Examples:
      | count |
      | 1     |
      | 2     |

  Scenario: Use the paypal express to proceed as guest
    Given I open the page "/checkout/login"
    When I pause for 20000ms
    And I wait on element "CheckoutPayment.payPalExpressButton" to be displayed

  Scenario: Make payment
    When I complete a PAYPAL_EXPRESS payment
    # TODO: Once B2C2-4384 is fixed the line below should be removed
    And I click on the button "CheckoutPayment.buyNowButton"
    And I pause for 10000ms
    And I wait on element "CheckoutConfirmation/Main.heading" to be displayed

  Scenario: Order confirmation
    Then I expect the url to contain "/checkout/confirmation/"
    And I expect that element "CheckoutConfirmation/Main.orderReference" is not empty
    And I expect that element "CheckoutConfirmation/Main.confirmationEmail" matches the text "rob.wi_1344499904_per@supergroup.co.uk"

  Scenario: Order summary
    When I scroll to element "CheckoutConfirmation/OrderSummary.heading"
    And I expect that element "CheckoutConfirmation/OrderSummary.subTotal" contains the text "£244.95"
    And I expect that element "CheckoutConfirmation/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.first-deduction" contains the text "3 for 2"
    And I expect that element "Checkout/OrderSummary.first-deduction" contains the text "-£44.99"
    And I expect that element "CheckoutConfirmation/OrderSummary.total" contains the text "£199.96"

  Scenario: FE02
    When I download the FE02 file from S3
    Then I expect the FE02 path "//oms:Order" has attribute "CustomerEMailID" with value "rob.wi_1344499904_per@supergroup.co.uk"

    # Basket Deductions
    And I expect the FE02 path "//oms:Order/oms:Extn" has attribute "StaffOrder" with value "N"

    # Product 1
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "OrderedQty" with value "3"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "GiftFlag" with value "N"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemShortDesc" with value "Red Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemID" with value "acid1-001PayPal-promo-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "LineID" with value "acid1-001PayPal-promo-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "ListPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "UnitPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeCategory" with value "PROMOTION"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeName" with value "XForYPromotion"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "Reference" with value "XForYPromotion"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargePerUnit" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]/oms:Extn" has attribute "Reference2" with value "3 for 2"

    # Product 2
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "OrderedQty" with value "2"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "GiftFlag" with value "N"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Item" has attribute "ItemShortDesc" with value "Blue Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Item" has attribute "ItemID" with value "acid1-001PayPal-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Extn" has attribute "LineID" with value "acid1-001PayPal-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:LinePriceInfo" has attribute "ListPrice" with value "54.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:LinePriceInfo" has attribute "UnitPrice" with value "54.99"

    # Delivery Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "FirstName" with value "Conor"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "LastName" with value "O'Potatoe"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "DayPhone" with value "01234 123 123"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "MobilePhone" with value "01234 123 123"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "EMailID" with value "rob.wi_1344499904_per@supergroup.co.uk"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine1" with value "1 Main Terrace"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "City" with value "Wolverhampton"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "ZipCode" with value "W12 4LQ"

    # Billing Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "FirstName" with value "Conor"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "LastName" with value "O'Potatoe"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "DayPhone" with value "01234 123 123"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "MobilePhone" with value "01234 123 123"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "EMailID" with value "rob.wi_1344499904_per@supergroup.co.uk"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine1" with value "1 Main Terrace"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "City" with value "Wolverhampton"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "ZipCode" with value "W12 4LQ"

    # Payment Method
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "PaymentType" with value "PAYPAL"
    # TODO: CreditCardExpiryDate?
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "ChargeType" with value "CHARGE"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "RequestAmount" with value "199.96"



