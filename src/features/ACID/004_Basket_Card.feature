@ACID
@ACID-004
@ACID-PROMO
@ACID-VOUCHER

#  NOTES:
#   - Extra steps added to allow test to run until B2C2-4390 is fixed
#   - If you try to apply a discount requiring a greater spend no message is displayed - This is not a defect/bug
#   - more than 100% discounts can be applied
#   - Delivery information cannot be before promos otherwise promo input isn't found for an unknown reason

Feature: 004 - Guest user - Single item - Basket-Level Promotion - Card
  As a guest user
  I want to be able to purchase a single item
  I want to use a basket level promotion (Failing)
  I want to use a basket level promotion (Passing)
  I want to use my credit card to make payment

  Scenario: Seeding the environment
    Given I open the page "/"
    And I spawn the site "acid4" from the base config "default"
    And I seed "products.promo"

  Scenario: Add product to bag from PDP
    Given I open the product detail page for "products.promo.third"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

  Scenario: Check the seeding as applied correctly
    Given I open the page "/checkout"
    When I scroll to element "Checkout/OrderSummary.heading"
    Then I expect that element "Checkout/OrderSummary.first-delivery" is displayed
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£44.99"

  Scenario: Setup for scenario outline
    When I scroll to element "Checkout/PromoCode.accordion"
    And I click on the element "Checkout/PromoCode.accordion"
    Then I expect that element "Checkout/PromoCode.notice" is not displayed

  Scenario Outline: Check invalid promotion voucher: <promotionCode>
    When I set "<promotionCode>" to the inputfield "Checkout/PromoCode.promoCode"
    And I click on the element "Checkout/PromoCode.apply"
    # TODO: Once B2C2-4390 is fixed the line below should be removed
    And I click on the element "Checkout/PromoCode.accordion"
    And I pause for 1000ms
    Then I expect that element "Checkout/PromoCode.notice" is displayed
    And I expect that element "Checkout/PromoCode.notice" contains the text "<message>"

    Examples:
      | promotionCode               | message                                          |
      | NOT_A_VOUCHER               | Promotion code not found. Please try again.      |
#      | 30Off100Spend               | Promotion code not found. Please try again.      |
      | 10Off30Spend_expired       | Promotion code expired or has already been used. |
      | 10Off30Spend_SingleUseUsed | Promotion code expired or has already been used. |

  Scenario: Check no prices changes were applied
    And I expect that element "Checkout/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£44.99"

  Scenario Outline: Apply valid promotion voucher: <promotionCode>
    When I set "<promotionCode>" to the inputfield "Checkout/PromoCode.promoCode"
    And I click on the element "Checkout/PromoCode.apply"
    And I pause for 2000ms
    # TODO: Once B2C2-4390 is fixed the line below should be removed
    And I click on the element "Checkout/PromoCode.accordion"
    Then I expect that element "Checkout/PromoCode.notice" is not displayed
    And I expect that element "Checkout/OrderSummary.promotionCode" contains the text "-£<discount>"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£<total>"
    And I click on the element "Checkout/PromoCode.accordion"

    Examples:
      | promotionCode | discount | total |
      | 10Off30Spend  | 10       | 34.99 |
      | 20Off30Spend  | 20       | 24.99 |

  Scenario: Check the promotion was applied correctly
    When I scroll to element "Checkout/OrderSummary.heading"
    Then I expect that element "Checkout/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.promotionCode" contains the text "-£20"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£24.99"

  Scenario: Add delivery information
    When I scroll to element "Checkout/DeliveryAddress.heading"
    And I set "definedCustomer.FirstName" to the inputfield "Checkout/DeliveryAddress.firstName"
    And I set "definedCustomer.LastName" to the inputfield "Checkout/DeliveryAddress.lastName"
    And I set "definedCustomer.Phone" to the inputfield "Checkout/DeliveryAddress.phone"
    And I set "definedCustomer.Email" to the inputfield "Checkout/DeliveryAddress.email"
    And I set "definedCustomer.Email" to the inputfield "Checkout/DeliveryAddress.confirmEmail"
    And I wait on element "Checkout/DeliveryAddress.address1" to be displayed
    And I set "definedCustomer.Address1" to the inputfield "Checkout/DeliveryAddress.address1"
    And I set "definedCustomer.Address2" to the inputfield "Checkout/DeliveryAddress.address2"
    And I set "definedCustomer.City" to the inputfield "Checkout/DeliveryAddress.city"
    And I set "definedCustomer.County" to the inputfield "Checkout/DeliveryAddress.state"
    And I set "definedCustomer.Postcode" to the inputfield "Checkout/DeliveryAddress.zip"

  Scenario: Make payment
    When I complete a CREDIT_CARD payment
    And I wait on element "CheckoutConfirmation/Main.heading" to be displayed

  Scenario: Order confirmation
    Then I expect the url to contain "/checkout/confirmation/"
    And I expect that element "CheckoutConfirmation/Main.orderReference" is not empty
    And I expect that element "CheckoutConfirmation/Main.confirmationEmail" matches the text "definedCustomer.Email"
    And I expect that element "CheckoutConfirmation/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "CheckoutConfirmation/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.promotionCode" contains the text "-£20"
    And I expect that element "CheckoutConfirmation/OrderSummary.total" contains the text "£24.99"

  Scenario: FE02
    When I download the FE02 file from S3
    Then I expect the FE02 path "//oms:Order" has attribute "CustomerEMailID" with value "definedCustomer.Email"

    # Basket Deductions
    And I expect the FE02 path "//oms:Order/oms:Extn" has attribute "StaffOrder" with value "N"

    # Product
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "GiftFlag" with value "N"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "OrderedQty" with value "1"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemShortDesc" with value "Grey Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemID" with value "acid4-004BasketCard-promo-002-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "LineID" with value "acid4-004BasketCard-promo-002-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "ListPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "UnitPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeCategory" with value "VOUCHER"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeName" with value "AmountOffPromotion"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargePerUnit" with value "20.00"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "Reference" with value "20Off30Spend"

    # Delivery Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Billing Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Payment Method
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "PaymentType" with value "CREDIT_CARD"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "CreditCardNo" with value "************1111"
    # NOTE: Should CreditCardExpiryDate be populated?
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "ChargeType" with value "AUTHORIZATION"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "RequestAmount" with value "24.99"







