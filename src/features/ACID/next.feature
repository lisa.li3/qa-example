#@ACID
#@ACID-005
#@ACID-HUGE
#@ACID-GUEST
#@ACID-CARD

#  Ideas for new ACID tests:
#   - Huge amount of products
#   - Failing PayPal
#   - Purchase using only customer credit

Feature: 005 - Guest user - Multi-line Multi-Quality at a large scale - Card
  As a guest user
  I want to be able to purchase a very large mount of items
  I want to use my credit card to make payment

#  Scenario: Seeding the environment
#    Given I open the page "/"
#    And I spawn the site "acid5" from the base config "default"
#    And I seed "products.basic"
#
#  Scenario Outline: Add loads of products to the basket (<index> or 49)
#    Given I open the product detail page for "products.basic.<index>"
#    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
#    And I click on the button "ProductDescription.addToBagButton"
#
#    Examples:
#      | index |
#      | 0     |
#      | 1     |
#      | 2     |
#      | 3     |
#      | 4     |
#      | 5     |
#      | 6     |
#      | 7     |
#      | 8     |
#      | 9     |
#      | 10    |
#      | 11    |
#      | 12    |
#      | 13    |
#      | 14    |
#      | 15    |
#      | 16    |
#      | 17    |
#      | 18    |
#      | 19    |
#      | 20    |
#      | 21    |
#      | 22    |
#      | 23    |
#      | 24    |
#      | 25    |
#      | 26    |
#      | 27    |
#      | 28    |
#      | 29    |
#      | 30    |
#      | 31    |
#      | 32    |
#      | 33    |
#      | 34    |
#      | 35    |
#      | 36    |
#      | 37    |
#      | 38    |
#      | 39    |
#      | 40    |
#      | 41    |
#      | 42    |
#      | 43    |
#      | 44    |
#      | 45    |
#      | 46    |
#      | 47    |
#      | 48    |
#      | 49    |