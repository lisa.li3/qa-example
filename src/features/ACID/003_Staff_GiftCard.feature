@ACID
@ACID-003
@ACID-PROMO
@ACID-STAFF-DISCOUNT
@ACID-GIFT-CARD

#  NOTE: Due to bug B2C2-4433 this test cannot be run to completion. Details may need tweaking and more defects may be found!

Feature: 003 - Logged in user - Single item - Markdown - Staff Discount - Gift Card
  As a logged in user
  I want to be able to purchase a single item which has a markdown promotion
  I want my staff discount to be applied (removing the markdown)
  I want to purchase the item using only a gift card

  Scenario: Seeding the environment
    Given I open the page "/"
    And I spawn the site "acid3" from the base config "default"
    And I seed "products.promo"
    And I seed "users.staff"

  Scenario: Check product details with markdown are correct
    Given I open the product detail page for "products.promo.forth"
    Then I expect that element "PDP/ProductMain.style" matches the text "PRODUCTS.PROMO"
    And I expect that element "PDP/ProductMain.name" matches the text "Red Hoodie"
    And I expect that element "PDP/ProductMain.standardPrice" matches the text "£44.99"
    And I expect that element "PDP/ProductMain.markdownPrice" matches the text "£40.49"
    And I expect that element "PDP/ProductMain.youSavePrice" contains the text "£4.50"

  Scenario: Login with staff discount user
    Given I login with the defined customer

  Scenario: Check product has staff discount instead of markdown
    Given I open the product detail page for "products.promo.forth"
    Then I expect that element "PDP/ProductMain.style" matches the text "PRODUCTS.PROMO"
    And I expect that element "PDP/ProductMain.name" matches the text "Red Hoodie"
    And I expect that element "PDP/ProductMain.standardPrice" matches the text "£44.99"
    And I expect that element "PDP/ProductMain.markdownPrice" matches the text "£22.50"
    And I expect that element "PDP/ProductMain.youSavePrice" contains the text "£22.49"

  Scenario: Add product to basket
    Given I open the product detail page for "products.promo.forth"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

  Scenario: Check the staff discount was applied correctly on checkout
    Given I open the page "/checkout"
    When I scroll to element "Checkout/OrderSummary.heading"
    Then I expect that element "Checkout/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.discount" contains the text "-£22.49"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£22.50"

  Scenario: Setup for scenario outline
    When I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    Then I expect that element "Checkout/RedeemAGiftCard.notice" is not displayed

  Scenario Outline: Check invalid gift card: <giftCard>
    When I set "<giftCard>" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "<pin>" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 1000ms
    Then I expect that element "Checkout/RedeemAGiftCard.notice" is displayed
    And I expect that element "Checkout/RedeemAGiftCard.notice" contains the text "<message>"

    Examples:
      | giftCard            | pin  | message                                                   |
      | NOT_A_GIFT_CARD     | 0000 | The gift card number or PIN is invalid. Please try again. |
      | 1000000000000005000 | 0000 | The gift card number or PIN is invalid. Please try again. |

  Scenario: Check no prices changes were applied
    Then I expect that element "Checkout/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.discount" contains the text "-£22.49"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£22.50"

#    TODO: Need to understand why the gift card section is being removed after applying a second gift card
#  Scenario Outline: Apply valid gift card worth: £<value>
#    And I set "<giftCard>" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
#    And I set "<pin>" to the inputfield "Checkout/RedeemAGiftCard.pin"
#    And I click on the element "Checkout/RedeemAGiftCard.redeem"
#    And I pause for 2000ms
#    Then I expect that element "Checkout/RedeemAGiftCard.notice" is not displayed
#    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£<stackedDiscount>"
#    And I expect that element "Checkout/OrderSummary.total" contains the text "£<total>"
#    And I expect that element "Checkout/PaymentDetails.cardNumber" is displayed
#    And I expect that element "Checkout/RedeemAGiftCard.accordion" is displayed
#    And I click on the element "Checkout/RedeemAGiftCard.accordion"
#
#    Examples:
#      | giftCard            | pin  | value | stackedDiscount | total |
#      | 2333300000000000100 | 3333 | 1.00  | 1.00            | 21.50 |
#      | 2444400000000000500 | 4444 | 5.00  | 6.00            | 16.50 |
#
#  Scenario: Check the same gift card cannot be applied twice
#    And I set "2333300000000000100" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
#    And I set "3333" to the inputfield "Checkout/RedeemAGiftCard.pin"
#    And I click on the element "Checkout/RedeemAGiftCard.redeem"
#    And I pause for 2000ms
#    Then I expect that element "Checkout/RedeemAGiftCard.notice" is displayed
#    And I expect that element "Checkout/RedeemAGiftCard.notice" contains the text "Gift card already applied"
#    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£6.00"
#    And I expect that element "Checkout/OrderSummary.total" contains the text "£16.50"

  Scenario: Apply valid gift card worth £50 covering the remaining amount
    When I set "2555500000000005000" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "5555" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    Then I expect that element "Checkout/RedeemAGiftCard.notice" is not displayed
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£22.50"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£0.00"
    And I expect that element "Checkout/PaymentDetails.accordion" is not displayed
    And I expect that element "Checkout/PaymentDetails.cardNumber" is not displayed

  Scenario: Make purchase
    When I click on the element "Checkout/PaymentDetails.buyNow"
    And I pause for 5000ms
    And I wait on element "Checkout/StaffDiscount.dialog" to be displayed
    Then I expect that element "Checkout/StaffDiscount.dialog" is displayed

  Scenario: Agree to staff discount terms and conditions
    When I click on the element "Checkout/StaffDiscount.checkbox"
    And I click on the element "Checkout/StaffDiscount.proceed"
    And I wait on element "CheckoutConfirmation/Main.heading" to be displayed

  Scenario: Order confirmation
    Then I expect the url to contain "/checkout/confirmation/"
    And I expect that element "CheckoutConfirmation/Main.orderReference" is not empty
    And I expect that element "CheckoutConfirmation/Main.confirmationEmail" matches the text "definedCustomer.Email"
    And I expect that element "CheckoutConfirmation/OrderSummary.subTotal" contains the text "£44.99"
    And I expect that element "CheckoutConfirmation/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.discount" contains the text "-£22.49"
    And I expect that element "CheckoutConfirmation/OrderSummary.total" contains the text "£22.50"

  Scenario: FE02
    When I download the FE02 file from S3
    Then I expect the FE02 path "//oms:Order" has attribute "CustomerEMailID" with value "definedCustomer.Email"

    # Basket Deductions
    And I expect the FE02 path "//oms:Order/oms:Extn" has attribute "StaffOrder" with value "Y"

    # Product
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "GiftFlag" with value "Y"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "OrderedQty" with value "1"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemShortDesc" with value "Red Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemID" with value "acid3-003StaffGiftCard-promo-003-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "LineID" with value "acid3-003StaffGiftCard-promo-003-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "ListPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "UnitPrice" with value "44.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeCategory" with value "STAFF"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargeName" with value "STAFF"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "ChargePerUnit" with value "22.49"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LineCharges/oms:LineCharge[1]" has attribute "Reference" with value "STAFFDISC"

    # Delivery Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Billing Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Payment Method
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "PaymentType" with value "CREDIT_CARD"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "CreditCardNo" with value "************1111"
    # NOTE: Should CreditCardExpiryDate be populated?
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "ChargeType" with value "AUTHORIZATION"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "RequestAmount" with value "22.50"
