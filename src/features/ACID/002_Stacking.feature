@ACID
@ACID-002
@ACID-CUSTOMER-CREDIT
@ACID-GIFT-CARD
@ACID-CARD

#  NOTE: Due to bug B2C2-4433 this test cannot be run to completion. Details may need tweaking and more defects may be found!

Feature: 002 - Logged in user - Multi-line Multi-Quality - Customer Credit - Gift Card
  As a logged in user
  I want to be able to purchase several items
  I want my customer credit to be applied covering some of the cost
  I want to apply a gift card covering some of the cost
  I want to use my credit card to pay the rest

  Scenario: Seeding the environment
    Given I open the page "/"
    And I spawn the site "acid2" from the base config "gift-cards"
    And I seed "products.basic"
    And I seed "users.customerCreditSmall"

  Scenario Outline: Add product to bag from PDP (<count> of 2)
    Given I open the product detail page for "products.basic.first"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    And I click on the button "ProductDescription.addToBagButton"

    Examples:
      | count |
      | 1     |
      | 2     |

  Scenario Outline: Add second product to bag from PDP (<count> of 2)
    Given I open the product detail page for "products.basic.second"
    When I select the option with the text "s" for element "ProductDescription.sizeDropdown"
    Then I click on the button "ProductDescription.addToBagButton"

    Examples:
      | count |
      | 1     |
      | 2     |

  Scenario: Login with staff discount user
    Given I login with the defined customer

  Scenario: Check the customer credit was applied correctly
    Given I open the page "/checkout"
    When I scroll to element "Checkout/OrderSummary.heading"
    Then I expect that element "Checkout/OrderSummary.subTotal" contains the text "£219.96"
    And I expect that element "Checkout/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "Checkout/OrderSummary.accountCredit" contains the text "-£10.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£209.96"

  Scenario: Apply valid gift card worth £20 covering some of the value
    When I scroll to element "Checkout/RedeemAGiftCard.accordion"
    And I click on the element "Checkout/RedeemAGiftCard.accordion"
    And I set "2888800000000002000" to the inputfield "Checkout/RedeemAGiftCard.cardNumber"
    And I set "8888" to the inputfield "Checkout/RedeemAGiftCard.pin"
    And I click on the element "Checkout/RedeemAGiftCard.redeem"
    And I pause for 2000ms
    Then I expect that element "Checkout/RedeemAGiftCard.notice" is not displayed
    And I expect that element "CheckoutConfirmation/OrderSummary.accountCredit" contains the text "-£10.00"
    And I expect that element "Checkout/OrderSummary.giftCard" contains the text "-£20.00"
    And I expect that element "Checkout/OrderSummary.total" contains the text "£189.96"

  Scenario: Make purchase
    When I complete a CREDIT_CARD payment
    And I wait on element "CheckoutConfirmation/Main.heading" to be displayed

  Scenario: Order confirmation
    Then I expect the url to contain "/checkout/confirmation/"
    And I expect that element "CheckoutConfirmation/Main.orderReference" is not empty
    And I expect that element "CheckoutConfirmation/Main.confirmationEmail" matches the text "definedCustomer.Email"
    And I expect that element "CheckoutConfirmation/OrderSummary.subTotal" contains the text "£219.96"
    And I expect that element "CheckoutConfirmation/OrderSummary.shippingFee" contains the text "£0.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.accountCredit" contains the text "-£10.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.giftCard" contains the text "-£20.00"
    And I expect that element "CheckoutConfirmation/OrderSummary.total" contains the text "£189.96"

  Scenario: FE02
    When I download the FE02 file from S3
    Then I expect the FE02 path "//oms:Order" has attribute "CustomerEMailID" with value "definedCustomer.Email"

    # Basket Deductions
    And I expect the FE02 path "//oms:Order/oms:Extn" has attribute "StaffOrder" with value "N"
    And I expect the FE02 path "//oms:Order//oms:OrderLine" has attribute "GiftFlag" with value "Y"

    # Product 1
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "GiftFlag" with value "Y"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "OrderedQty" with value "2"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemShortDesc" with value "Blue Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Item" has attribute "ItemID" with value "acid2-002Stacking-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "LineID" with value "acid2-002Stacking-basic-000-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "ListPrice" with value "54.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[1]/oms:LinePriceInfo" has attribute "UnitPrice" with value "54.99"

    # Product 2
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "GiftFlag" with value "Y"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "OrderedQty" with value "2"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]" has attribute "DeliveryMethod" with value "SHP"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Item" has attribute "ItemShortDesc" with value "White Hoodie"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Item" has attribute "ItemID" with value "acid2-002Stacking-basic-001-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Extn" has attribute "LineID" with value "acid2-002Stacking-basic-001-s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:Extn" has attribute "Size" with value "s"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:LinePriceInfo" has attribute "ListPrice" with value "54.99"
    And I expect the FE02 path "//oms:Order//oms:OrderLines/oms:OrderLine[2]/oms:LinePriceInfo" has attribute "UnitPrice" with value "54.99"

    # Delivery Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoShipTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Billing Information
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "FirstName" with value "definedCustomer.FirstName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "LastName" with value "definedCustomer.LastName"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "DayPhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "MobilePhone" with value "definedCustomer.Phone"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "EMailID" with value "definedCustomer.Email"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine1" with value "definedCustomer.Address1"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "AddressLine2" with value "definedCustomer.Address2"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "City" with value "definedCustomer.City"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "State" with value "definedCustomer.County"
    And I expect the FE02 path "//oms:Order/oms:PersonInfoBillTo" has attribute "ZipCode" with value "definedCustomer.Postcode"

    # Payment Method
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "PaymentType" with value "CREDIT_CARD"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod" has attribute "CreditCardNo" with value "************1111"
    # NOTE: Should CreditCardExpiryDate be populated?
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "ChargeType" with value "AUTHORIZATION"
    And I expect the FE02 path "//oms:Order//oms:PaymentMethod//oms:PaymentDetails" has attribute "RequestAmount" with value "189.96"
