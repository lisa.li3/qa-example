@WISH_LIST
@WISH_LIST_GUEST
@REGRESSION

Feature: Wish List (Guest)

    @B2C2-1366 @SKIP_MOBILE @SKIP_TABLET
    Scenario: Guest Wishlist Empty on First Visit
      Given I open the page "/my-account/wish-list"
      And I delete the local storage
      And I seed using the file "CustomerWishList"
      When I refresh the page
      Then I expect that element "Wishlist.WishlistEmpty" is displayed

    @B2C2-1359 @SKIP_MOBILE @SKIP_TABLET
    Scenario: Wishlist Icon Opens Wishlist (Guest)
      Given I am on the page "/"
      When I click on the element "Navigation.wishlistButton"
      Then I expect the url to contain "/my-account/wish-list"

    @B2C2-1359 @SKIP_DESKTOP @SKIP_TABLET
    Scenario: Wishlist Navigation Opens Wishlist (Guest)
      Given I am on the page "/"
      When I click on the element "Navigation.burgerButton"
      And I click on the element "NavigationDrawer.wishList"
      Then I expect the url to contain "/my-account/wish-list"
      And I expect that element "Wishlist.Header" becomes displayed

    @B2C2-1359 @SKIP_MOBILE @SKIP_TABLET
    Scenario: Wishlist Icon in Minibag Opens Wishlist  (Guest)
      Given I am on the page "/"
      And I click on the element "Navigation.basketButton"
      And I wait on element "PopinBasket.wishlistButton" to be displayed
      When I click on the element "PopinBasket.wishlistButton"
      Then I expect the url to contain "/my-account/wish-list"
      And I expect that element "Wishlist.Header" becomes displayed

    @B2C2-1361
    Scenario: Out of Stock items (Guest)
      Given I am on the page "/my-account/wish-list"
      And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
      When I refresh the page
      Then I expect the element in "Wishlist.WishlistRows" containing text "Hoodie Four - OutOfStock" to also contain text "Sorry, this item is currently out of stock."
      When I delete the local storage


    ## Product Detail Page
    @B2C2-1361
    Scenario: Add one Item to wishlist (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.addToWishlistButton"
      And I click on the element "ProductDescription.addToWishlistButton"
      When I wait on element "ProductDescription.addToWishlistButton" to not be displayed
      And I open the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed

    @B2C2-1361
    Scenario: Items added to wishlist show remove button (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      Then I expect that element "ProductDescription.removeFromWishlistButton" is displayed

    @B2C2-1361
    Scenario: Clicking remove button removes the item from the wishlist (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.removeFromWishlistButton"
      When I click on the element "ProductDescription.removeFromWishlistButton"
      And I wait on element "ProductDescription.removeFromWishlistButton" to not be displayed
      And I open the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is not displayed

    @B2C2-1361
    Scenario: Adding an item shows the popup (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.addToWishlistButton"
      When I click on the element "ProductDescription.addToWishlistButton"
      Then I wait on element "Wishlist.AddConfirmed" to be displayed
      And I expect that element "Wishlist.AddConfirmed" is displayed
      And I move to "ProductDescription.removeFromWishlistButton" with an offset of 600,600
      And I wait on element "Wishlist.AddConfirmed" to not be displayed
      And I expect that element "Wishlist.AddConfirmed" is not displayed

    @B2C2-1361
    Scenario: Removing an item shows the popup (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
      And I scroll to element "ProductDescription.removeFromWishlistButton"
      When I click on the element "ProductDescription.removeFromWishlistButton"
      Then I wait on element "Wishlist.RemoveConfirmed" to be displayed
      And I expect that element "Wishlist.RemoveConfirmed" is displayed
      And I move to "ProductDescription.addToWishlistButton" with an offset of 600,600
      And I wait on element "Wishlist.RemoveConfirmed" to not be displayed
      And I expect that element "Wishlist.RemoveConfirmed" is not displayed

    @B2C2-1359
    Scenario: Clicking Wishlist Item Added Popup Opens Wishlist (Guest)
       Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL002/hoodie-two-one-size-maroon"
       And I scroll to element "ProductDescription.addToWishlistButton"
       And I click on the element "ProductDescription.addToWishlistButton"
       And I wait on element "Wishlist.AddConfirmed" to be displayed
       When I click on the element "Wishlist.AddConfirmed"
       Then I expect the url to contain "/my-account/wish-list"
       And I expect that element "Wishlist.Header" becomes displayed

    @B2C2-1361
    Scenario: Adding an item with multiple sizes shows modal (Guest)
      Given I am on the page "/main/framework/customerWishlist/WishListSampleData1/details/CWL001/wishlist-hoodie-one-multiple-sizes-white"
      And I scroll to element "ProductDescription.addToWishlistButton"
      When I click on the element "ProductDescription.addToWishlistButton"
      Then I expect that element "ProductDescription.sizeModal" is displayed

  ## Wish List Page
    @B2C2-1361
    Scenario: Adding an sized item shows correct size in wish list (Guest)
      Given I wait on element "ProductDescription.sizeModal" to be displayed
      And I wait on element "ProductDescription.modalSizeDropdown" to be displayed
      When I select the option with the text "m" for element "ProductDescription.modalSizeDropdown"
      And I click on the element "ProductDescription.modalWishListButton"
      And I open the page "/my-account/wish-list"
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed
      And I expect the element in "Wishlist.WishlistRows" containing text "Hoodie One - Multiple Sizes" to also contain text "Size: M"

    @B2C2-1361
    Scenario: Removing items from the wishlist removes the correct items - prep (Guest)
      Given I am on the page "/my-account/wish-list"
      And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
      When I refresh the page
      Then I expect that element "Wishlist.SecondSeededProduct" is displayed
      And I expect the count of elements in "Wishlist.WishlistRows" to equal 6

            #    @B2C2-1361
#    Scenario: Wishlist Items link to Product Page (Guest)
#      Given I am on the page "/my-account/wish-list"
#      When I click on the element "Wishlist.SecondSeededProduct"
#      And I pause for 5000ms
#      Then I expect the url to contain "CWL002"

    @B2C2-1361
    Scenario: Removing items from the wishlist removes the correct items (Guest)
      Given I am on the page "/my-account/wish-list"
      When I scroll to element "Wishlist.SecondSeededProductRemoveButton"
      And I click on the element "Wishlist.SecondSeededProductRemoveButton"
      And I wait on element "Wishlist.SecondSeededProductRemoveButton" to not be displayed
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 5
      And I expect that element "Wishlist.SecondSeededProduct" is not displayed


     @B2C2-1361
     Scenario: Signin/Register Prompt (Guest)
       Given I am on the page "/my-account/wish-list"
       Then I expect that element "Wishlist.SignInOrRegister" is displayed

     @B2C2-1361
     Scenario: Remove Item from Wishlist displays the Popup (Guest)
       Given I am on the page "/my-account/wish-list"
       When I click on the element "Wishlist.ThirdSeededProductRemoveButton"
       Then I wait on element "Wishlist.RemoveConfirmed" to be displayed
       And I expect that element "Wishlist.RemoveConfirmed" is displayed

     # ReSeeding Data
     @B2C2-1361
     Scenario: Shows Move to Shopping Basket Button (Guest)
       Given I am on the page "/my-account/wish-list"
       And I seed using the file "CustomerWishList" and add the items into my "wishlist" local storage
       When I refresh the page
       Then I expect the count of elements in "Wishlist.WishlistRows" to equal 6
       And I expect that element "Wishlist.SecondSeededProductMoveToShoppingButton" is displayed

     @B2C2-1361
     Scenario: Move Wishlist Item to Shopping Bag (Guest)
      Given I am on the page "/my-account/wish-list"
      When I click on the element "Wishlist.SecondSeededProductMoveToShoppingButton"
      And I wait on element "WishList.SecondSeededProduct" to not be displayed
      Then I expect the count of elements in "Wishlist.WishlistRows" to equal 5
      And I expect that element "Navigation.basketButtonIconText" contains the text "1"

     @B2C2-1361
     Scenario: Move All Wishlist Items to Shopping Bag (Guest)
       Given I am on the page "/my-account/wish-list"
       When I scroll to element "Wishlist.MoveAllToShoppingBag"
       And I click on the element "Wishlist.MoveAllToShoppingBag"
       And I wait on element "Wishlist.MoveAllToShoppingBag" to not be displayed
       Then I expect the count of elements in "Wishlist.WishlistRows" to equal 1
       And I wait for the API call to return
       And I expect that element "Navigation.basketButtonIconText" contains the text "5"

     @B2C2-1361
     Scenario: Move All Wishlist Items to Shopping Bag doesn't move out of stock items (Guest)
      Given I am on the page "/my-account/wish-list"
      Then I expect the element in "Wishlist.WishlistRows" containing text "Hoodie Four - OutOfStock" to also contain text "Sorry, this item is currently out of stock."