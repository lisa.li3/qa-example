@GTM
@REGRESSION

Feature: GTM
  Check the triggering of the data layer push events
  and the UI for any associated changes due to tag configuration

  @B2C2-1637 @B2C2-1189
  Scenario: Setup environment with GTM enabled and seed data for testing
    Given I am on the page "/"
    And I seed config using the file "GTM"
    And I open the siteId "gtm-on"
    And I seed using the file "GTM"

  #
  # Associated GTM Tags :
  #   "UA - Event - Form Submit"
  #

  @B2C2-1189
  Scenario: "Form Submit" -  Register User - Check the data layer push
    Given I am on the page "/register"
    And I set "definedCustomer.Email" to the inputfield "Registration.emailAddressTextbox"
    And I set "definedCustomer.Password" to the inputfield "Registration.passwordTextbox"
    And I set "definedCustomer.Password" to the inputfield "Registration.confirmPasswordTextbox"
    And I set "definedCustomer.Phone" to the inputfield "Registration.phoneTextbox"
    And I set "definedCustomer.Mobile" to the inputfield "Registration.mobilPhoneTextbox"
    And I select the option with the text "Male" for element "Registration.gender"
    And I set "definedCustomer.FirstName" to the inputfield "Registration.firstNameTextbox"
    And I set "definedCustomer.LastName" to the inputfield "Registration.lastNameTextbox"
    And I set "definedCustomer.Address1" to the inputfield "Registration.address1Textbox"
    And I set "definedCustomer.Address2" to the inputfield "Registration.address2Textbox"
    And I set "definedCustomer.City" to the inputfield "Registration.cityTextbox"
    And I set "definedCustomer.Postcode" to the inputfield "Registration.postcodeTextbox"
    And I select the option with the text "United Kingdom" for element "Registration.countryComboBox"
    And I click on the element "Registration.termsAndConditionsParent"
    And I click on the element "Registration.registerButton"
    And I wait on element "MyAccount.logOutButton"
    Then the latest "formSubmit" GTM event should contain property "formTracking" with an object that contains property "name" with the value "Register"
    And I click on the button "MyAccount.logOutButton"

  @B2C2-1189
  Scenario: "Form Submit" - Forgotton Password - Check the data layer push
    Given I open the page "/login"
    And I click on the element "Login.forgottenPassword"
    And I set "gtmtestuser@project.local" to the inputfield "ForgotPassword.emailAddressTextbox"
    And I click on the element "ForgotPassword.submitButton"
    And I wait on element "Resend Email"
    Then the latest "formSubmit" GTM event should contain property "formTracking" with an object that contains property "name" with the value "Forgotten Password"

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Searches"
  #

  @B2C2-1637 @B2C2-1189
  Scenario Outline: "User Searches" - Checks the data layer push
    Given I am on the page "/search/<productId>"
    And I wait on element "<productName>"
    Then the latest "userSearches" event in the GTM data layer data should contain the property "usersSearchTerm" with the value "<productId>"

    Examples:
      | productId                        | productName |
      | gtm00999a1                       | Product1    |
      | gtm00999a2                       | Product2    |
      | gtm00999a1,gtm00999a2            | Product1    |
      | gtm00999a1,gtm00999a2,gtm00999a3 | Product3    |

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Clicks on PLP Product"
  #   "Fredhopper - Users Views Product on PDP"
  #   "UA - Event - Product Click"
  #

  @B2C2-1637 @B2C2-1189
  Scenario Outline: "User Clicks on PLP Product" and "Users Views Product on PDP" - Checks the data layer push
    Given I click on the element "<productName>"
    Then the element "ProductDescription.addToBagButton" is displayed
    And the latest "userClicksOnPLPProduct" event in the GTM data layer data should contain the property "plpProductIdClicked" with the value "<productId>"
    And the latest "userViewsProductOnPDP" event in the GTM data layer data should contain the property "pdpProductIdViewed" with the value "<productId>"
    And I go back to the previous page

    Examples:
      | productId  | productName |
      | gtm00999a1 | Product1    |
      | gtm00999a2 | Product2    |
      | gtm00999a3 | Product3    |
 #
 # Associated GTM Tags :
 #   "Fredhopper - User Clicks on PLP Filter"
 #   "UA - Event - Products Colour Filter Click"
 #   "UA - Event - Products Type Filter Click"
 #

  @B2C2-1637 @B2C2-1189 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: "User Clicks on PLP Filter" - Checks the data layer push
    Given I click on the element "ProductListPage.<filterType>"
    And I select the <optionNumber> option for element "ProductListPage.facetOptions"
    And I pause for 500ms
    Then the latest "userClicksOnPLPFilter" event in the GTM data layer data should contain the property "<filterNameKey>" with the value "<filterNameValue>"
    And the latest "userClicksOnPLPFilter" event in the GTM data layer data should contain the property "<filterValueKey>" with the value "<filterValueValue>"
    And I click on the element "ProductListPage.<filterType>"

    Examples:
      | optionNumber | filterType        | filterNameKey        | filterNameValue | filterValueKey        | filterValueValue |
      | 1st          | productTypeFilter | plpProductFilterName | types           | plpProductFilterValue | CASUAL TOP       |
      | 2nd          | productTypeFilter | plpProductFilterName | types           | plpProductFilterValue | DENIM TOP        |
      | 1st          | genderFilter      | plpProductFilterName | genders         | plpProductFilterValue | WOMENS           |
      | 2nd          | genderFilter      | plpProductFilterName | genders         | plpProductFilterValue | MENS             |
      | 1st          | colourFilter      | plpProductFilterName | colours         | plpProductFilterValue | DARK GREEN       |
      | 2nd          | colourFilter      | plpProductFilterName | colours         | plpProductFilterValue | DARK BLUE        |
      | 3rd          | colourFilter      | plpProductFilterName | colours         | plpProductFilterValue | DARK RED         |

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Clicks on PLP Filter"
  #   "UA - Event - Products Colour Filter Click"
  #   "UA - Event - Products Type Filter Click"

  @B2C2-1637 @B2C2-1189 @SKIP_DESKTOP
  Scenario: On mobile, open the 'Refine By' menu ready for checking filtering
    Given I click on the element "ProductListPage.refineBy"
    Then the element "ProductListPage.productTypeFilterMobile" is displayed

  @B2C2-1637 @B2C2-1189 @SKIP_DESKTOP
  Scenario Outline: "User Clicks on PLP Filter" - Checks the data layer push
    Given I click on the element "ProductListPage.<filterType>"
    And I click on the element "<facetName>"
    And I pause for 500ms
    Then the latest "userClicksOnPLPFilter" event in the GTM data layer data should contain the property "<filterNameKey>" with the value "<filterNameValue>"
    And the latest "userClicksOnPLPFilter" event in the GTM data layer data should contain the property "<filterValueKey>" with the value "<filterValueValue>"
    And I click on the element "ProductListPage.<filterType>"

    Examples:
      | facetName  | filterType              | filterNameKey        | filterNameValue | filterValueKey        | filterValueValue |
      | CASUAL TOP | productTypeFilterMobile | plpProductFilterName | types           | plpProductFilterValue | CASUAL TOP       |
      | DENIM TOP  | productTypeFilterMobile | plpProductFilterName | types           | plpProductFilterValue | DENIM TOP        |
      | WOMENS     | genderFilterMobile      | plpProductFilterName | genders         | plpProductFilterValue | WOMENS           |
      | DARK GREEN | colourFilterMobile      | plpProductFilterName | colours         | plpProductFilterValue | DARK GREEN       |


  @B2C2-1637 @B2C2-1189 @SKIP_MOBILE @SKIP_TABLET
  Scenario Outline: Ensure that deselecting the filters does not trigger a new push to the data layer
    Given I click on the element "ProductListPage.<filterType>"
    And I select the <option1> option for element "ProductListPage.facetOptions"
    And I select the <option2> option for element "ProductListPage.facetOptions"
    Then the latest "userClicksOnPLPFilter" event in the GTM data layer data should contain the property "plpProductFilterName" with the value "colours"

    Examples:
      | filterType        | option1 | option2 |
      | productTypeFilter | 1st     | 2nd     |
      | genderFilter      | 1st     | 2nd     |

  #
  # Associated GTM Tags :
  #   "UA - Event - Sort Filter on PLP"
  #

  @B2C2-1189 @SKIP_TABLET @SKIP_MOBILE
  Scenario Outline: Sort Filter on PLP - Checks the data layer push
    Given I select the option with the value "<sortOption>" for element "ProductListPage.sortDropdown"
    Then the latest "sortClick" event in the GTM data layer data should contain the property "productsSortedBy" with the value "<sortOption>"

    Examples:
      | sortOption |
      | rating     |
      | price~desc |
      | price~asc  |
      | relevance  |

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Clicks on PDP Product Recommendation"
  #   "UA- Event - Listing Interaction"
  #

  @B2C2-1637 @B2C2-1189
  Scenario: Go to a PDF page for a product with has WWW and YAML recommendations
    Given I open the page "/category/details/gtm00999a3/testproduct"
    Then I expect that element "Product3" becomes displayed
#    And I refresh the page

  @B2C2-1637 @B2C2-1189
  Scenario Outline: "User Clicks on PDP Product Recommendation" - Checks the data layer push
    Given I scroll to element "ProductDescription.<region>"
    When I click on the element "<productName>" within "ProductDescription.<region>"
    Then I expect the url to contain "<productId>"
    And the latest "userClicksOnPDPProductRecommendation" event in the GTM data layer data should contain the property "pdpRecommendedProductIdClicked" with the value "<productId>"
    And the latest "userClicksOnPDPProductRecommendation" GTM event should contain property "listingAction" with an object that contains property "productId" with the value "<productId>"
    And the latest "userClicksOnPDPProductRecommendation" GTM event should contain property "listingAction" with an object that contains property "type" with the value "<type>"
    And the latest "userViewsProductOnPDP" event in the GTM data layer data should contain the property "pdpProductIdViewed" with the value "<productId>"
    And I go back to the previous page
    And I pause for 1000ms

    Examples:
      | productId  | region     | productName | type                  |
      | gtm00999a2 | ymalRegion | product2    | recommendations_click |
      | gtm00999a1 | wwwRegion  | product1    | wiw_click             |

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Adds Product to Basket"
  #   "Fredhopper - User Removed Product from Basket"
  #   "UA - Event - Add to Basket"
  #   "UA - Event - Remove Basket"

  @B2C2-1637 @B2C2-1189 @SKIP_MOBILE @SKIP_TABLET
  Scenario: "User Adds Product to Basket" - Checks the data layer push
    Given I am on the page "/category/details/gtm00999a1/testproduct"
    When I click on the element "ProductDescription.addToBagButton"
    Then the latest "userAddsProductToBasket" event in the GTM data layer data should contain the property "productIdAddedToBasket" with the value "gtm00999a1-8"
    Then I wait on element "PopinBasket.removeButtonSingleItemInBasket" to not be displayed

  @B2C2-1637 @B2C2-1189 @SKIP_MOBILE @SKIP_TABLET
  Scenario: "User Removed Product from Basket" - Checks the data layer push
    Given I click on the element "Navigation.basketButton"
    And I click on the element "PopinBasket.removeButtonSingleItemInBasket"
    And I pause for 500ms
    Then I expect that element "PopinBasket.miniBagDrawer" contains the text "empty"
    And the latest "userRemovesProductFromBasket" event in the GTM data layer data should contain the property "productIdRemovedFromBasket" with the value "gtm00999a1-8"

  @B2C2-1637 @B2C2-1189 @SKIP_DESKTOP
  Scenario Outline: User Adds Product to Basket and User Removed Product from Basket - Checks the data layer push
    Given I open the page "/category/details/<productId>/testproduct"
    When I click on the element "ProductDescription.addToBagButton"
    Then the latest "userAddsProductToBasket" event in the GTM data layer data should contain the property "productIdAddedToBasket" with the value "<productId>-8"
    And I refresh the page
    And I click on the element "Navigation.basketButton"
    And I click on the element "PopinBasket.removeButtonSingleItemInBasket"
    And I pause for 500ms
    Then the latest "userRemovesProductFromBasket" event in the GTM data layer data should contain the property "productIdRemovedFromBasket" with the value "<productId>-8"

    Examples:
      | productId  |
      | gtm00999a1 |
      | gtm00999a2 |

  @B2C2-1637 @B2C2-1189
  Scenario: Add product to basket ready for checkout
    Given I open the page "/category/details/gtm00999a1/testproduct"
    And I click on the element "ProductDescription.addToBagButton"

  @B2C2-1637 @B2C2-1189
  Scenario: Add second product to basket ready for checkout
    Given I open the page "/category/details/gtm00999a2/testproduct"
    And I click on the element "ProductDescription.addToBagButton"

  #
  # Associated GTM Tags :
  #   "UA - Event - Form Submit"
  #

  @B2C2-1637 @B2C2-1189
  Scenario: Log a user in - "Form Submit" - Checks the data layer push
    Given I open the page "/login"
    And I set "gtmtestuser@project.local" to the inputfield "Login.emailAddress"
    And I set "its-a-secret" to the inputfield "Login.password"
    And I click on the element "Login.signInButton"
    And I wait on element "GTM USER"
    Then the latest "formSubmit" GTM event should contain property "formTracking" with an object that contains property "name" with the value "Sign In"

  #
  # Associated GTM Tags :
  #   "UA - Event - Is User Logged-In On Checkout"
  #

  @B2C2-1637 @B2C2-1189
  Scenario: Logged in "User Lands on checkout" - Checks the data layer push
    Given I open the page "/checkout"
    Then the latest "userLandedOnCheckout" GTM event with property "ecommerce" should have a child property "checkout" which contains an object with property "isLoggedIn" and value "registered"

  @B2C2-1637
  Scenario: Complete card payment and click BUY NOW
    Given I scroll to element "CheckoutPayment.cardPaymentNumberFrame"
    When I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentNumberFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I set "4444 3333 2222 1111" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
    And I switch back to the parent frame
    And I pause for 2000ms
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentDateFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I set "0330" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
    And I switch back to the parent frame
    And I switch to the child iFrame with a selector "CheckoutPayment.cardPaymentCvvFrame"
    And I click on the iframe button "CheckoutPayment.cardPaymentFrameInput"
    And I set "737" to the inputfield "CheckoutPayment.cardPaymentFrameInput"
    And I switch back to the parent frame
    And I set "Test User" to the inputfield "CheckoutPayment.cardPaymentNameInput"
    And I click on the button "CheckoutPayment.buyNowButton"
    Then the element "Confirmation.orderConfirmationText" is displayed
    And the element "Confirmation.orderReferenceText" is displayed
    And the element "Confirmation.orderNumber" is displayed

  #
  # Associated GTM Tags :
  #   "Fredhopper - User Receives Order Confirmation"
  #

  @B2C2-1637
  Scenario Outline: "User Receives Order Confirmation" - Checks the data layer push
    Given the element "Confirmation.confirmationOrderSummary" is displayed
    Then the latest "userReceivesOrderConfirmation" GTM event should contain property "productsSuccessfullyOrdered" with an array where the <index> element contains property "<elementProperty>" with the value "<elementValue>"

    Examples:
      | index | elementProperty | elementValue |
      | 1st   | currency        | GBP          |
      | 1st   | listPrice       | 10           |
      | 1st   | productId       | gtm00999a1-8 |
      | 1st   | quantity        | 1            |
      | 2nd   | currency        | GBP          |
      | 2nd   | listPrice       | 15           |
      | 2nd   | productId       | gtm00999a2-8 |
      | 2nd   | quantity        | 1            |

  #
  # Associated GTM Tags :
  #   "UA - Event - Is User Logged-In On Checkout"
  #

  @B2C2-1189
  Scenario: Guest "User Lands on checkout" - Checks the data layer push
    Given I open the page "/my-account"
    And I click on the button "MyAccount.logOutButton"
    And I open the page "/category/details/gtm00999a3/testproduct"
    And I click on the element "ProductDescription.addToBagButton"
    And I open the page "/checkout"
    Then the latest "userLandedOnCheckout" GTM event with property "ecommerce" should have a child property "checkout" which contains an object with property "isLoggedIn" and value "guest"


