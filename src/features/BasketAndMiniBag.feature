@BASKET
@MINI_BAG
@REGRESSION

Feature: Basket and Mini Bag

  @B2C2-898
  Scenario: Minibag should open when the User clicks the Mini Bag icon in the site header
    Given I am on the page "/"
    And the element "Navigation.basketButton" is displayed
    When I click on the element "Navigation.basketButton"
    Then I wait on element "PopinBasket.miniBagDrawer" to be displayed

  @B2C2-989
  Scenario: Should verify that the initial guest basket is empty
    Given I am on the page "/"
    And I refresh the page
    When I click on the element "Navigation.basketButton"
    And I wait on element "PopinBasket.miniBagDrawer"
    Then I expect that element "PopinBasket.miniBagDrawer" contains the text "empty"

  @B2C2-919 @B2C2-898 @B2C2-989
  Scenario: Seeding for 919, 898, 989
    Given I am on the page "/"
    And I seed using the file "BasketAndMiniBag" and add one of each item into my "basket" local storage

  @B2C2-919 @B2C2-898 @B2C2-989
  Scenario: Should verify that when a guest adds products to their basket, and then leaves and revisits the site, the basket is maintained
    When I open the page "/shopping-bag"
    Then I expect that element "bambg Hoodie" is displayed

  @B2C2-898
  Scenario: Minibag should not open if on "/checkout/login"
    Given I am on the page "/checkout/login"
    Then the element "Navigation.basketButton" is not enabled

  @B2C2-919 @B2C2-898 @B2C2-989
  Scenario: The user should be logged in for the next set of scenarios
    Given I login with "basketandminibag@project.mini" and "its-a-secret"

  @B2C2-898
  Scenario Outline: Minibag should not open if on <page>
    Given I am on the page <page>
    Then the element "Navigation.basketButton" is not enabled

    Examples:
      | page                        |
      | "/checkout"                 |
      | "/checkout/account-shipto"  |
      | "/checkout/account-billing" |

  @B2C2-989
  Scenario: Should check that items in the guest basket are transferred to the logged in users basket
    And I am on the page "/"
    When I click on the element "Navigation.basketButton"
    Then I expect that element "bambg Hoodie" is displayed

  @B2C2-919
  Scenario: Should check that the initial basket state contains the basket items previously added for a registered User
    Given I refresh the page
    When I click on the element "Navigation.basketButton"
    Then I expect that element "bambg Hoodie" is displayed

  @B2C2-919
  Scenario: Should check that on clearing the cache and reloading, the basket is empty for a guest user
    Given I am on the page "/"
    And I delete the local storage
    And I refresh the page
    When I click on the element "Navigation.basketButton"
    And I wait on element "PopinBasket.miniBagDrawer" to be displayed
    Then I expect that element "PopinBasket.miniBagDrawer" contains the text "empty"

  @B2C2-1166
  Scenario: Seeding for 1166
    Given I am on the page "/"
    Given I seed using the file "BasketAndMiniBagShoppingBag" and add one of each item into my "basket" local storage

  @B2C2-1166
  Scenario: Product should be displayed with correct information in the shopping bag
    Given I open the page "/shopping-bag"
    Then I expect that element "ShoppingBag.name" is displayed
    And I expect that the attribute "src" from element "ShoppingBag.getImage" is "https://image1.project.com/static/images/optimised/upload9223368955665543436.jpg"
    And I expect that element "ShoppingBag.colour" is displayed
    And I expect that element "ShoppingBag.size" is displayed
    And I expect that element "ShoppingBag.shoppingBagGrid" contains the text "£29.99"
    And I expect that element "ShoppingBag.subtotalContainer" is displayed
    And I expect that element "ShoppingBag.totalContainer" is displayed
    And I expect that element "ShoppingBag.subtotal" is displayed
    And I expect that element "ShoppingBag.subtotalContainer" contains the text "£29.99"
    And I expect that element "ShoppingBag.totalContainer" is displayed
    And I expect that element "ShoppingBag.totalContainer" contains the text "£29.99"

  @B2C2-1166
  Scenario: Checks customer can increase quantity
    Given I am on the page "/shopping-bag"
    When I click on the element "ShoppingBag.incrementButton"
    And I click on the element "ShoppingBag.incrementButton"
    # TODO: SAM!!!
    And I wait on element "ShoppingBag.incrementButton" to not be enabled
    And I pause for 5000ms
    Then I expect that element "ShoppingBag.incrementButton" is not enabled
    And I expect that element "ShoppingBag.decrementButton" is enabled
    And I expect that element "ShoppingBag.quantityText" matches the text "3"

  @B2C2-1166
  Scenario: Checks customer can decrease quantity
    Given I am on the page "/shopping-bag"
    When I click on the element "ShoppingBag.decrementButton"
    And I click on the element "ShoppingBag.decrementButton"
    # TODO: SAM!!!
    And I wait on element "ShoppingBag.incrementButton" to be enabled
    And I pause for 5000ms
    Then I expect that element "ShoppingBag.decrementButton" is not enabled
    And I expect that element "ShoppingBag.incrementButton" is enabled
    And I expect that element "ShoppingBag.quantityText" matches the text "1"

  @B2C2-1166
  Scenario: Checks user is directed to the Checkout Gateway page
    Given I am on the page "/shopping-bag"
    When I click on the element "ShoppingBag.checkoutButton"
    Then I expect that the path is "/checkout/login"

  @B2C2-1166
  Scenario: Checks customer can remove a product from their shopping bag using the X option
    Given I am on the page "/shopping-bag"
    When I click on the element "ShoppingBag.removeButton"
    Then I expect that element "ShoppingBag.name" does not exist

  @B2C2-1166
  Scenario: Checks message is displayed when basket is empty
    Given I am on the page "/shopping-bag"
    Then I expect that element "ShoppingBag.noItemsPresentText" is displayed

  #
  # Promotions "Promo Slug" Display
  # https://supergroupbt.atlassian.net/browse/B2C2-540
  # https://supergroupbt.atlassian.net/browse/B2C2-2501
  #
  # AC2b
  # Given a customer does not have a Staff Discount linked to their account
  # when the customer adds a product to their basket that is included in an active promotion
  # and the customer opens their mini-bag
  # then a promo slug shall be displayed that shows the product is included within the promotion
  @B2C2-540 @B2C2-2501 @PENDING
  Scenario: Pending
    Given I pause for 500ms

  #
  # Promotions "Promo Slug" Display
  # https://supergroupbt.atlassian.net/browse/B2C2-540
  # https://supergroupbt.atlassian.net/browse/B2C2-2501
  #
  # AC3
  # Given a customer is browsing the project site
  # when the customer selects a Promo Slug for a promotion either from the PLP, PDP or mini-bag
  # then the customer is directed to the merchandised PLP for that promotion.
  @B2C2-540 @B2C2-2501 @PENDING
  Scenario: Pending
    Given I pause for 500ms


#
# Display of Staff Discount on Mini-Bag
# https://supergroupbt.atlassian.net/browse/B2C2-537
#

  Rule: I WANT to be shown the staff discounted prices in the mini-bag

    @B2C2-537
    Scenario: Seed products and a staff discount user on a different site and add to the basket
      Given I seed config using the file "BasketAndMiniBag_StaffDiscountWithProducts"
      And I open the siteId "staffdiscount-basket"
      And I seed using the file "BasketAndMiniBag_StaffDiscountUserAndProducts" and add one of each item into my "basket" local storage
      And  I open the page "/main/framework/b2c2_537_basket_staffdiscount"
      Then I expect that element "Product 1 - Staff discount set to true" becomes displayed

    @B2C2-537
    Scenario: Open the basket
      When I click on the element "Navigation.basketButton"
      And I wait on element "PopinBasket.miniBagDrawer"
      And I wait on element "PopinBasket.checkoutButton"

    @B2C2-537
    Scenario Outline: For a 'Guest' user within the basket, The product details for <productName> should <condition> contain <details>
      Then I expect the element in "PopinBasket.miniBagProductsInfoGridItems" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details |
        | Product 1 - Staff discount set to true               | also      | £222.00 |
        | Product 2 - Markdown with Staff discount set to true | also      | £25.00  |
        | Product 3 - Does not qualify                         | also      | £12.00  |

    @B2C2-537
    Scenario: For a 'Guest' user within the basket, The basket footer section should have the correct totals information including any discount (shown left to right)
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £334.00
        Discount
        -£75.00
        Total
        £259.00
        Free UK standard delivery
        """

    @B2C2-537
    Scenario: For a 'Guest' user within the 'Shopping Bag', The order summary section should have the correct totals information including any discount (shown left to right)
      Given I open the page "/shopping-bag"
      And I wait on element "ShoppingBag.subtotalContainer"
      And I expect that the values returned by "ShoppingBag.itemsInOrderSummary" to be in the following order
        """
        Subtotal
        £334.00
        Discount
        -£75.00
        Total
        £259.00
        """

    @B2C2-537
    Scenario: Log in with 'STAFF' user and open the basket (basket should be promoted from previous test)
      Given I open the page "/login"
      And I set "basketuser.displaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "StaffUser WithDiscount"

    @B2C2-537
    Scenario: Open the basket
      When I click on the element "Navigation.basketButton"
      And I wait on element "PopinBasket.miniBagDrawer"
      And I wait on element "PopinBasket.checkoutButton"

    @B2C2-537
    Scenario Outline: For a 'STAFF' user within the basket, The product details for <productName> should <condition> contain <details>
      Then I expect the element in "PopinBasket.miniBagProductsInfoGridItems" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details |
        | Product 1 - Staff discount set to true               | also      | £111.00 |
        | Product 2 - Markdown with Staff discount set to true | also      | £50.00  |
        | Product 3 - Does not qualify                         | also      | £12.00  |

    @B2C2-537
    Scenario: For a 'STAFF' user within the basket, The basket footer section should have the correct totals information including any discount (shown left to right)
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £334.00
        Staff Discount
        -£161.00
        Total
        £173.00
        Free UK standard delivery
        """

    @B2C2-537
    Scenario: For a 'STAFF' user within the 'Shopping Bag', The order summary section should have the correct totals information including any discount (shown left to right)
      Given I open the page "/shopping-bag"
      And I wait on element "ShoppingBag.shoppingBagGrid.subtotalContainer" to be displayed
      And I expect that the values returned by "ShoppingBag.itemsInOrderSummary" to be in the following order
        """
        Subtotal
        £334.00
        Staff Discount
        -£161.00
        Total
        £173.00
        """

    @B2C2-537
    Scenario: Log out the 'STAFF' user
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"

    @B2C2-537
    Scenario: Reseed the data and add items to the basket (the basket items will be promoted when the 'NON STAFF' user logs in)
      Given I seed using the file "BasketAndMiniBag_StaffDiscountUserAndProducts" and add one of each item into my "basket" local storage

    @B2C2-537
    Scenario: Log in with a 'NON STAFF' user
      When I open the page "/login"
      And I set "basketuser.donotdisplaystaffdiscount@project.local" to the inputfield "Login.emailAddress"
      And I set "its-a-secret" to the inputfield "Login.password"
      And I click on the element "Login.signInButton"
      And I wait on element "NonStaffUser WithoutStaffDiscount"

    @B2C2-537
    Scenario: Open the basket
      When I click on the element "Navigation.basketButton"
      And I wait on element "PopinBasket.miniBagDrawer"
      And I wait on element "PopinBasket.checkoutButton"

    @B2C2-537
    Scenario Outline: For a 'NON STAFF' user within the basket, The product details for <productName> should <condition> contain <details>
      Then I expect the element in "PopinBasket.miniBagProductsInfoGridItems" containing text "<productName>" to <condition> contain text "<details>"

      Examples:
        | productName                                          | condition | details |
        | Product 1 - Staff discount set to true               | also      | £222.00 |
        | Product 2 - Markdown with Staff discount set to true | also      | £25.00  |
        | Product 3 - Does not qualify                         | also      | £12.00  |

    @B2C2-537
    Scenario: For a 'NON STAFF' user within the basket, The basket footer section should have the correct totals information including any discount (shown left to right)
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £334.00
        Discount
        -£75.00
        Total
        £259.00
        Free UK standard delivery
        """

    @B2C2-537
    Scenario: For a 'NON STAFF' user within the 'Shopping Bag', The order summary section should have the correct totals information including any discount (shown left to right)
      Given I open the page "/shopping-bag"
      And I wait on element "ShoppingBag.shoppingBagGrid.subtotalContainer" to be displayed
      And I expect that the values returned by "ShoppingBag.itemsInOrderSummary" to be in the following order
        """
        Subtotal
        £334.00
        Discount
        -£75.00
        Total
        £259.00
        """


    Scenario: Tidy up - Switch back to uk site for next set of tests
      And I open the siteId "com"
      Then I expect the url to contain "com."

    @B2C2-997
    Scenario: Setup environment for items added to basket
      Given I seed config using the file "BasketAndMiniBagAbandonedProduct"
      And I open the siteId "b2c2997"
      And I seed using the file "BasketAndMiniBagAbandonedProduct"
      And I login with the defined customer
      And I expect that element "MyAccount.accountInformationButton" is displayed
      And I open the page "/main/framework/997_abandoned_products"
      And I move to "abandonedBasketProduct"
      And I click on the element "ProductListPage.quickAddToBagButton"
      And I pause for 3000ms
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"

    @B2C2-997
    Scenario: Guest customer is presented with empty shopping bag
      When I am on the page "/shopping-bag"
      Then I expect that element "No items presently in your bag" is displayed

    @B2C2-997
    Scenario: Guest customer is presented with abandoned products after visiting url to retrieve abandoned basket
      When I am on the page "/shopping-bag?abandonedBasket={{userSeed.attributes.id}}_{{Math.random()}}"
      Then I wait on element "abandonedBasketProduct" to be displayed

    @B2C2-997
    Scenario: Setup environment for out of stock abandoned products
      Given I seed config using the file "BasketAndMiniBagAbandonedProduct"
      And I open the siteId "b2c2997"
      And I seed using the file "BasketAndMiniBagAbandonedInStockProducts"
      And I login with the defined customer
      And I expect that element "MyAccount.accountInformationButton" is displayed
      And I open the page "/main/framework/abandoned_in_stock_products"
      And I move to "abandonedProdOne"
      And I click on the element "ProductListPage.quickAddToBagButton"
      And I open the page "/main/framework/abandoned_in_stock_products"
      And I move to "abandonedProdTwo"
      And I click on the element "ProductListPage.quickAddToBagButton"
      Given I open the page "/my-account"
      And I click on the button "MyAccount.logOutButton"

    @B2C2-997
    Scenario: Guest customer is redirected to shopping bag with in stock and out of stock products
      Given I seed using the file "BasketAndMiniBagAbandonedInStockAndOutOfStockProducts"
      When I am on the page "/shopping-bag?abandonedBasket={{userSeed.attributes.id}}_{{Math.random()}}"
      Then I expect the url to contain "/shopping-bag"
      And I expect that element "Shopping Bag" is displayed
      And I expect that element "abandonedProdOne" is displayed
      And I expect that element "abandonedProdTwo" is displayed

    @B2C2-997
    Scenario: Guest customer is presented with out of stock modal when visiting the checkout page
      When I open the page "/checkout"
      Then I expect that element "CheckoutOrderSummary.outOfStockModal" is displayed
      And I expect that element "CheckoutOrderSummary.outOfStockModalContent" contains the text "abandonedProdTwo"

    @B2C2-997
    Scenario: Out of stock product is removed from checkout after accepting stock check
      Given I click on the element "CheckoutOrderSummary.popUpOk"
      And the element "abandonedProdOne" is displayed
      And the element "abandonedProdTwo" is not displayed

    @B2C2-997
    Scenario: Reset back to COM for future scenarios
      Given I open the siteId "com"

    @B2C2-3203 @B2C2-3216
    Scenario: 3 for 2 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £59.97
        3 for 2
        -£19.99
        Total
        £39.98
        Free UK standard delivery
        """

     @B2C2-3203 @B2C2-3216 @DEFECT
     # Test above passes, (3 of the same product) but it fails when selecting different skus
     # https://supergroupbt.atlassian.net/jira/software/c/projects/B2C2/boards/177?modal=detail&selectedIssue=B2C2-3216
     Scenario: 3 for 2 shows correct calculated prices in basket with same product but different sizes
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "l" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £59.97
        3 for 2
        -£19.99
        Total
        £39.98
        Free UK standard delivery
        """

    @B2C2-3231
    Scenario: 3 for 2 with 6 items shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag"
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I pause for 1000ms
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £119.94
        3 for 2
        -£39.98
        Total
        £79.96
        Free UK standard delivery
        """

    @B2C2-3231
    Scenario: 3 for 2 with 6 items of two different sizes shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £119.94
        3 for 2
        -£39.98
        Total
        £79.96
        Free UK standard delivery
        """

    @B2C2-3231
    Scenario: 3 for 2 with 6 items of three different sizes shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "l" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £119.94
        3 for 2
        -£39.98
        Total
        £79.96
        Free UK standard delivery
        """
    
    @B2C2-3231 @DEFECT
     # Test above passes, (3 of the same product) but it fails when selecting different skus
     # https://supergroupbt.atlassian.net/browse/B2C2-3231
     Scenario: 3 for 2 with 6 items shows correct calculated prices in basket with same product but different sizes (4xS, 1xM, 1xL)
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "l" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £119.94
        3 for 2
        -£39.98
        Total
        £79.96
        Free UK standard delivery
        """

     @B2C2-3231 @DEFECT
     # Test above passes, (3 of the same product) but it fails when selecting different skus
     # https://supergroupbt.atlassian.net/jira/software/c/projects/B2C2/boards/177?modal=detail&selectedIssue=B2C2-3231
     Scenario: 3 for 2 with 6 items shows correct calculated prices in basket with same product but different sizes (2xS, 2xM, 2xL)
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag"
      When I am on the page "/products?sku=mhuq4h1vf2-s"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "l" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "ProductDescription.addToBagButton"
       And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBag3for2" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £119.94
        3 for 2
        -£39.98
        Total
        £79.96
        Free UK standard delivery
        """

    @B2C2-3203 @B2C2-3216
    Scenario: 3 for £19.99 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag"
      When I am on the page "/products?sku=mhuq4h1vf3-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I wait on element "PopinBasket.miniBag3for1999" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £26.97
        3 for £19.99
        -£6.98
        Total
        £19.99
        Free UK standard delivery
        """

    @B2C2-3231
    Scenario: 3 for £19.99 with 6 items shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag"
      When I am on the page "/products?sku=mhuq4h1vf3-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I click on the button "PopinBasket.incrementButton"
      And I wait on element "PopinBasket.miniBag3for1999" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £53.94
        3 for £19.99
        -£13.96
        Total
        £39.98
        Free UK standard delivery
        """

    @B2C2-3217
    Scenario: 2 for £14.99 shows correct calculated prices in basket
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf4-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I click on the button "PopinBasket.incrementButton"
      And I wait on element "PopinBasket.miniBag2for1499" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £15.98
        2 for £14.99
        -£0.99
        Total
        £14.99
        Free UK standard delivery
        """

    @B2C2-3217 @DEFECT
     # Test above passes, (2 of the same product) but it fails when selecting different skus
     # https://supergroupbt.atlassian.net/browse/B2C2-3217
     Scenario: 2 for £14.99 shows correct calculated prices in basket with same product but different sizes
      Given I am on the page "/"
      And I delete the local storage
      And I seed using the file "BasketAndMiniBag" 
      When I am on the page "/products?sku=mhuq4h1vf4-s"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I select the option with the text "m" for element "ProductDescription.sizeDropdown"
      And I click on the button "ProductDescription.addToBagButton"
      And I wait on element "PopinBasket.miniBagFooter" to not be displayed
      And I click on the button "PopinBasket.miniBasketButton"
      And I wait on element "PopinBasket.miniBagFooter" to be displayed
      And I wait on element "PopinBasket.miniBag2for1499" to be displayed
      Then I expect that the values returned by "PopinBasket.itemsInFooterSection" to be in the following order
        """
        Subtotal
        £15.98
        2 for £14.99
        -£0.99
        Total
        £14.99
        Free UK standard delivery
        """