/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-191-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-191', {
      isActive: true,
      buffer: 0,
    }),

    GenerateConfig.site('site-uk', 'site-191', 'b2c2191', {
      shopperGroupId: 'site-191-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-191',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'INTND',
          fulfilmentCentres: ['fc-191'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 0,
                  transitTime: 0,
                },
              },
              GBR: {
                default: {
                  price: 0,
                  transitTime: 0,
                },
              },
            },
          },
        },
      ],
    })
  );
};
