/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (domainPrefix: string, oldSiteId?: string): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'sizeguide-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.site(oldSiteId ? oldSiteId : 'site-uk', 'sizeguide', 'sizeguide', {
      shopperGroupId: 'sizeguide-shoppergroup',
      authoritativeCountries: ['GBR'],
    }),
    GenerateConfig.sizeGuide('site-uk', 'sizeguide', {
      'size-guides': {
        desktop:
          'https://cdn.project.com/size_guides/en-GB/desktop/Desktop-Mens-Jeans-Trousers-Shorts-Swimshorts-Joggers-LoungeBottoms.jpg',
        mobile:
          'https://cdn.project.com/size_guides/en-GB/mobile/Mobile-Mens-Jeans-Trousers-Shorts-Swimshorts-Joggers-LoungeBottoms.jpg',
      },
    })
  );
};
