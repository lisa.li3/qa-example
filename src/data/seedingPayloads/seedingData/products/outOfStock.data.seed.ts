import generateSeededProducts from '../../utils/generateSeededProducts';
import { IProductSeeder } from '../../../../support/types/IProduct';
import { IInventorySeeder } from '../../../../support/types/IInventory';
import { IProduct } from '../../products/BaseProduct';
import { Tees } from '../../products';
import path from 'path';

const STYLE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const products: Array<IProduct> = [
  {
    ...Tees.Male.GreenTee,
    sizes: [
      { size: 's', stock: 0 },
      { size: 'm', stock: 0 },
      { size: 'l', stock: 0 },
    ],
  },
  {
    ...Tees.Male.TealTee,
    sizes: [
      { size: 's', stock: 100 },
      { size: 'm', stock: 0 },
      { size: 'l', stock: 0 },
    ],
  },
];

export default (): { product: Array<IProductSeeder>; inventory: Array<IInventorySeeder> } => {
  return generateSeededProducts({ products, file: STYLE });
};
