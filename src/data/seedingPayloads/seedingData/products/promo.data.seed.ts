import generateSeededProducts from '../../utils/generateSeededProducts';
import { IProductSeeder } from '../../../../support/types/IProduct';
import { IInventorySeeder } from '../../../../support/types/IInventory';
import { Hoodies } from '../../products';
import { IProduct } from '../../products/BaseProduct';
import {
  XforYAmountPromotion,
  XforYPromotion,
  AmountOffPromotion,
  PercentOffSpendAmountPromotion,
  Markdown,
} from '../../promotions';
import path from 'path';

const STYLE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export default (): { product: Array<IProductSeeder>; inventory: Array<IInventorySeeder> } => {
  const products: Array<IProduct> = [
    {
      ...Hoodies.Female.RedHoodie,
      style: STYLE,
      promotions: XforYPromotion.ThreeForTwo,
    },
    { ...Hoodies.Male.WhiteHoodie, style: STYLE, promotions: XforYAmountPromotion.ThreeFor1999 },
    { ...Hoodies.Male.GreyHoodie, style: STYLE, promotions: AmountOffPromotion },
    { ...Hoodies.Male.RedHoodie, style: STYLE, promotions: Markdown.TenPercentOffProduct },
    { ...Hoodies.Male.GreyHoodie, style: STYLE, promotions: AmountOffPromotion },
  ];
  return generateSeededProducts({ products, file: STYLE });
};
