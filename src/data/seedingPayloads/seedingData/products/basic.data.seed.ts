import generateSeededProducts from '../../utils/generateSeededProducts';
import { IProductSeeder } from '../../../../support/types/IProduct';
import { IInventorySeeder } from '../../../../support/types/IInventory';
import { Hoodies } from '../../products';
import { IProduct } from '../../products/BaseProduct';
import { times, random } from 'lodash';
import path from 'path';

const countOfProducts = 50;
const STYLE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const products: Array<IProduct> = [
  { ...Hoodies.Male.BlueHoodie, style: STYLE },
  { ...Hoodies.Male.WhiteHoodie, style: STYLE },
  { ...Hoodies.Male.GreyHoodie, style: STYLE },
  { ...Hoodies.Male.RedHoodie, style: STYLE },
  { ...Hoodies.Female.RedHoodie, style: STYLE },
];

if (products.length < countOfProducts) {
  const baseProducts = products;
  times(countOfProducts - products.length, (index) => {
    products.push({ ...baseProducts[random(0, products.length - 1)], style: STYLE });
  });
}

export default (): { product: Array<IProductSeeder>; inventory: Array<IInventorySeeder> } => {
  return generateSeededProducts({ products, file: STYLE });
};
