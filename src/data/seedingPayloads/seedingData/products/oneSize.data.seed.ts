import generateSeededProducts from '../../utils/generateSeededProducts';
import { IProductSeeder } from '../../../../support/types/IProduct';
import { IInventorySeeder } from '../../../../support/types/IInventory';
import { IProduct } from '../../products/BaseProduct';
import { Swimwear } from '../../products';
import path from 'path';

const STYLE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const products: Array<IProduct> = [
  { ...Swimwear.Male.BlackShorts, style: STYLE, sizes: [{ size: 'm' }] },
  { ...Swimwear.Female.Stripy, style: STYLE, sizes: [{ size: 'm' }] },
];

export default (): { product: Array<IProductSeeder>; inventory: Array<IInventorySeeder> } => {
  return generateSeededProducts({ products, file: STYLE });
};
