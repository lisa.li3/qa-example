import generateSeededUsers from '../../utils/generateSeededUsers';
import { Staff } from '../../users';
import { IUser } from '../../users/BaseUser';
import path from 'path';

const FILE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const users: Array<IUser> = [{ ...Staff.Staff }];

export default (): { user } => {
  return generateSeededUsers({ users, file: FILE });
};
