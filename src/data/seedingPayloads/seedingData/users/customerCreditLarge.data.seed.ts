import generateSeededUsers from '../../utils/generateSeededUsers';
import { CustomerCredit } from '../../users';
import { IUser } from '../../users/BaseUser';
import path from 'path';
import { map } from 'lodash';
import moment from 'moment';

const FILE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const users: Array<IUser> = [{ ...CustomerCredit.LargeCustomerCredit }];

export default (): { user; payment } => {
  const response = generateSeededUsers({ users, file: FILE });
  return {
    user: response.user,
    payment: map(response.user, (u) => {
      console.log('u', u);
      return {
        attributes: {
          emailAddress: u.attributes.emailAddress,
          credits: [
            {
              name: 'Active Credit',
              id: 'activeCredit',
              disabled: false,
              startDate: moment().subtract(1, 'day').toISOString(),
              endDate: moment().add(1, 'year').toISOString(),
              initialValue: 2000,
              remainingValue: 2000,
              transactions: [],
            },
          ],
        },
      };
    }),
  };
};
