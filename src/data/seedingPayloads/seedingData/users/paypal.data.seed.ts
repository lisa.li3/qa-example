import generateSeededUsers from '../../utils/generateSeededUsers';
import { Paypal } from '../../users';
import { IUser } from '../../users/BaseUser';
import path from 'path';

const FILE = `${path.basename(__dirname)}.${path.basename(__filename).split('.data.seed.ts')[0]}`;

export const users: Array<IUser> = [{ ...Paypal.Paypal }];

export default (): { user } => {
  return generateSeededUsers({ users, file: FILE });
};
