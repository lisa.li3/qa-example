/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { getAsSeedUser } from '../../support/functions/definedCustomer';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let content: IContentSeeder[] | undefined;

  const categoryId = 'WishListSampleData1';

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [await getAsSeedUser()];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: `/main/framework/customerWishlist/${categoryId}`,
        head: {
          title: 'Wish List Sample Data Title',
        },
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: 'CWL001',
      sizes: ['s', 'm', 'l'],
      attributes: {
        style: 'WishList',
        name: 'Hoodie One - Multiple Sizes',
        defaultCategoryId: categoryId,
      },
    },
    {
      id: 'CWL002',
      sizes: ['s'],
      attributes: {
        style: 'WishList',
        name: 'Hoodie Two - One Size',
        defaultCategoryId: categoryId,
      },
    },
    {
      id: 'CWL003',
      sizes: ['s'],
      attributes: {
        style: 'WishList',
        name: 'Hoodie Three - Fuchsia',
        defaultCategoryId: categoryId,
        colour: {
          id: 'utbv84blug',
          name: 'Fuchsia',
          description: 'Refined fuchsia',
        },
      },
    },
    {
      id: 'CWL004',
      sizes: ['s'],
      attributes: {
        style: 'WishList',
        name: 'Hoodie Four - OutOfStock',
        defaultCategoryId: categoryId,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['CWL001'], // 'Hoodie One - Multiple Sizes'
      sizes: ['s', 'm', 'l'],
      stock: 10,
    },
    {
      skus: ['CWL002'], // 'Hoodie Two - One Size',
      sizes: ['s'],
      stock: 10,
    },
    {
      skus: ['CWL003'], // 'Hoodie Three - Fuchsia',
      sizes: ['s'],
      stock: 10,
    },
    {
      skus: ['CWL004'], // 'Hoodie Four - OutOfStock',
      sizes: ['s'],
      stock: 0,
    },
  ];

  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  content ? (seedEvents.content = content) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  content ? (seedEvents.content = content) : null;

  return seedEvents;
};
