/**
 Perform Data Seeding for the given test case
 */

import { GenerateConfig } from '@project/site-config-transformer/dist';
export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-b2c23605-shoppergroup', {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'AUS', name: 'Australia' },
      ],
    }),

    GenerateConfig.site('site-uk', 'b2c23605def', 'b2c23605def', {
      shopperGroupId: 'site-b2c23605-shoppergroup',
      defaultCountryCode: 'GBR',
      paymentPlanProviders: [
        {
          id: 'klarna',
          name: 'Klarna',
          logoImageUrl: '/images/klarna-small.png',
          installments: 1,
          url: '/klarna',
          supportedCountries: ['GBR'],
        },
        {
          id: 'clearpay',
          name: 'Clearpay',
          logoImageUrl: '/images/clearpay-small.png',
          installments: 1,
          url: '/clearpay',
          supportedCountries: ['GBR'],
        },
      ],
    })
  );
};
