/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-ViewSelectDeliveryAddresses',
        emailAddress: 'viewselectdeliveryaddresses@project.local',
        password: 'its-a-secret',
        firstName: 'Foo',
        lastName: 'Bar',
        gender: 'male',
        dateOfBirth: '2000-01-01',
        addresses: [
          {
            id: '2d2e91',
            firstName: 'Foo1',
            lastName: 'Bar1',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road 1',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BA',
            isDefault: true,
            nickname: 'C Home1',
          },
          {
            id: '2d2e92',
            firstName: 'Foo2',
            lastName: 'Bar2',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 2',
            addressLine2: 'Bar Road 2',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BB',
            isDefault: false,
            nickname: 'B Home2',
          },
          {
            id: '2d2e94',
            firstName: 'Foo3',
            lastName: 'Bar3',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 3',
            addressLine2: 'Bar Road 3',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BC',
            isDefault: false,
            nickname: 'A Home3',
          },
        ],
        tokensValidFromDate: '2020-01-01T00:00:00Z',
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: [11],
      attributes: {
        defaultCategoryId: `category-1940`,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      productQuantity: 1,
      stock: 1,
      fulfilmentCentres: ['fc-194'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
