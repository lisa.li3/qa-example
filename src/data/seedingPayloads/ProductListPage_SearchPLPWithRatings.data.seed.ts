/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IReviewSeeder } from '../../support/types/IReview';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `searchplp_with_ratings_${timestamp}`;
  const path = `/main/framework/searchplp-with-ratings`;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `rating00p1`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 1 - 0 stars',
      },
    },
    {
      id: `rating00p2`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 2 - 1 star',
      },
    },
    {
      id: `rating00p3`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 3 - 2 stars',
      },
    },
    {
      id: `rating00p4`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 4 - 3.5 stars',
      },
    },
    {
      id: `rating00p5`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 5 - 0.5 stars',
      },
    },
    {
      id: `rating00p6`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 6 - 4 stars',
      },
    },
    {
      id: `rating00p7`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 7 - 5 stars',
      },
    },
    {
      id: `rating00p8`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 8 - 0 stars',
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [
        `rating00p1`,
        `rating00p2`,
        `rating00p3`,
        `rating00p4`,
        `rating00p5`,
        `rating00p6`,
        `rating00p7`,
        `rating00p8`,
      ],
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
  ];

  /**
   *  Ratings
   */
  // eslint-disable-next-line prefer-const
  review = [
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p1',
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p2',
        averageRating: {
          averageRating: 1,
          totalRatings: 32,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p3',
        averageRating: {
          averageRating: 2,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p4',
        averageRating: {
          averageRating: 3.67,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p5',
        averageRating: {
          averageRating: 0.5,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p6',
        averageRating: {
          averageRating: 4,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p7',
        averageRating: {
          averageRating: 5,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: 'rating00p8',
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;

  return seedEvents;
};
