/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const FC_SOURCING_LOGIC_1 = 'fc-sourcing-logic-1';
  const FC_SOURCING_LOGIC_2 = 'fc-sourcing-logic-2';
  const FC_SOURCING_LOGIC_3 = 'fc-sourcing-logic-3';
  const FC_SOURCING_LOGIC_4 = 'fc-sourcing-logic-4';
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `sourcing-logic-shoppergroup`, {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_SOURCING_LOGIC_1, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_SOURCING_LOGIC_2, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_SOURCING_LOGIC_3, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_SOURCING_LOGIC_4, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'site-sourcing-logic', 'b2c2125', {
      shopperGroupId: 'sourcing-logic-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: FC_SOURCING_LOGIC_1,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: FC_SOURCING_LOGIC_2,
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: FC_SOURCING_LOGIC_3,
          priority: 3,
          isDistributionCentre: false,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: FC_SOURCING_LOGIC_4,
          priority: 4,
          isDistributionCentre: false,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'UK4',
          fulfilmentCentres: [FC_SOURCING_LOGIC_4],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK3',
          fulfilmentCentres: [FC_SOURCING_LOGIC_3],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK2',
          fulfilmentCentres: [FC_SOURCING_LOGIC_2],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK1',
          fulfilmentCentres: [FC_SOURCING_LOGIC_1],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK1', {
      name: 'UK delivery1',
      message: 'Delivery in 2 days',
      description: 'UK delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK2', {
      name: 'UK delivery2',
      message: 'Delivery in 5 days',
      description: 'UK delivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK3', {
      name: 'UK delivery3',
      message: 'Delivery in 3 days',
      description: 'UK delivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK4', {
      name: 'UK delivery4',
      message: 'Delivery in 3 days',
      description: 'UK delivery3 description',
      type: 'ToAddress',
      displayOrder: 4,
    })
  );
};
