/**
 * Perform Config Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (domainPrefix: string, oldSiteId?: string) => {
  return await browser.configureEnv(
    GenerateConfig.site(oldSiteId ? oldSiteId : 'site-uk', 'b2c2997', 'b2c2997', {
      abandonedBasket: {
        enabledAbandonedBasketEmail: true,
        abandonedBasketProgramId: 118978,
        abandonedBasketDelayMinutes: 25,
        abandonedBasketAccountId: 'DM-4169606694w-01',
      },
    })
  );
};
