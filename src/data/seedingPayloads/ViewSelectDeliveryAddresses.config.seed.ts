/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-194-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-194', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FAST194', {
      name: 'Very fast delivery',
      message: 'Delivery in 2 days',
      description: 'Very fast delivery description',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'SLOW194', {
      name: 'Very slow delivery',
      message: 'Delivery in 5 days',
      description: 'Very slow delivery description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'STD194', {
      name: 'Standard delivery',
      message: 'Delivery in 3 days',
      description: 'Standard delivery description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.site('site-uk', 'site-194', 'b2c2194', {
      shopperGroupId: 'site-194-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-194',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'STD194',
          fulfilmentCentres: ['fc-194'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'SLOW194',
          fulfilmentCentres: ['fc-194'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FAST194',
          fulfilmentCentres: ['fc-194'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
