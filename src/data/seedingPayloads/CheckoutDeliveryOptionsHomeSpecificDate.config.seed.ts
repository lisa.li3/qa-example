/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';
import { DeliveryType } from '@project/site-config-transformer/dist/types/configs';

export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-132-1', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('DOMSTD', 'STD132', {
      name: 'Standard delivery',
      message: 'Delivery on %s',
      description: 'Standard delivery',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('DOMSPEC', 'SPEC132', {
      name: 'Specific day delivery',
      message: 'Delivery on %s',
      description: 'Specific day delivery',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('DOMEVE', 'EVE132', {
      name: 'Evening delivery',
      message: 'Delivery on %s',
      description: 'Evening delivery',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.site('site-uk', 'site-132', 'b2c2132', {
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-132-1',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'STD132',
          fulfilmentCentres: ['fc-132-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'SPEC132',
          fulfilmentCentres: ['fc-132-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 7.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'EVE132',
          fulfilmentCentres: ['fc-132-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 10.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
