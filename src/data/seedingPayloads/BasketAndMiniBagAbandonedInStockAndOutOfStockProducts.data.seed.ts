/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { getAsSeedUser } from '../../support/functions/definedCustomer';

export default async () => {
  const categoryId = 'test-basketandminibagMini';
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  // user = [await getAsSeedUser()];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: ['2size'],
      attributes: {
        name: 'abandonedProdOne',
        id: 'abandonedProdOne',
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      sizes: ['3size'],
      id: 'abandonedProdTwo',
      attributes: {
        name: 'abandonedProdTwo',
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
  ];
  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['abandonedProdOne'],
      sizes: ['2size'],
      stock: 1,
    },
    {
      skus: ['abandonedProdTwo'],
      sizes: ['3size'],
      stock: 0,
    },
  ];

  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
