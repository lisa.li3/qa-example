/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (domainPrefix: string, oldSiteId?: string): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'promotion-plp1-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.site(oldSiteId ? oldSiteId : 'site-uk', 'promotion-plp1', 'promotion-plp1', {
      shopperGroupId: 'promotion-plp1-shoppergroup',
      authoritativeCountries: ['GBR'],
    })
  );
};
