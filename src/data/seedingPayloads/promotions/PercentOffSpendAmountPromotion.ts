const PercentOffSpendAmountPromotion = {
  vouchers: [
    {
      code: '20PercentOff200Spend',
      type: 'PercentOffSpendAmountPromotion',
      status: 'active',
      parameters: {
        valueX: 200,
        valueY: 20,
      },
    },
    {
      code: '20PercentOff30Spend',
      type: 'PercentOffSpendAmountPromotion',
      status: 'active',
      parameters: {
        valueX: 30,
        valueY: 20,
      },
    },
    {
      code: '10PercentOff30Spend',
      type: 'PercentOffSpendAmountPromotion',
      status: 'active',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10PercentOff30Spend_expired',
      type: 'PercentOffSpendAmountPromotion',
      status: 'expired',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10PercentOff30Spend_SingleUse',
      type: 'PercentOffSpendAmountPromotion',
      status: 'singleUse',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10PercentOff30Spend_SingleUseUsed',
      type: 'PercentOffSpendAmountPromotion',
      status: 'singleUseUsed',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
  ],
};

export default PercentOffSpendAmountPromotion;
