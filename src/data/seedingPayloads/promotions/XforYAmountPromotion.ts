const ThreeFor1999 = {
  product: {
    type: 'XForYAmountPromotion',
    status: 'active',
    id: 'XForYPromotion',
    slug: '3 for £19.99',
    parameters: {
      valueX: 3,
      valueY: 19.99,
    },
  },
};

const TwoFor1499 = {
  product: {
    type: 'XForYAmountPromotion',
    status: 'active',
    id: 'XForYPromotion',
    slug: '2 for £14.99',
    parameters: {
      valueX: 2,
      valueY: 14.99,
    },
  },
};

const XforYPromotion = {
  ThreeFor1999,
  TwoFor1499,
};

export default XforYPromotion;
