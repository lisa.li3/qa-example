const ThreeForTwo = {
  product: {
    type: 'XForYPromotion',
    status: 'active',
    id: 'XForYPromotion',
    slug: '3 for 2',
    parameters: {
      valueX: 3,
      valueY: 2,
    },
  },
};

const XforYPromotion = {
  ThreeForTwo,
};

export default XforYPromotion;
