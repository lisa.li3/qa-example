const AmountOffPromotion = {
  vouchers: [
    {
      code: '30Off100Spend',
      type: 'AmountOffPromotion',
      status: 'active',
      parameters: {
        valueX: 100,
        valueY: 30,
      },
    },
    {
      code: '20Off30Spend',
      type: 'AmountOffPromotion',
      status: 'active',
      parameters: {
        valueX: 30,
        valueY: 20,
      },
    },
    {
      code: '10Off30Spend',
      type: 'AmountOffPromotion',
      status: 'active',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10Off30Spend_expired',
      type: 'AmountOffPromotion',
      status: 'expired',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10Off30Spend_SingleUse',
      type: 'AmountOffPromotion',
      status: 'singleUse',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
    {
      code: '10Off30Spend_SingleUseUsed',
      type: 'AmountOffPromotion',
      status: 'singleUseUsed',
      parameters: {
        valueX: 30,
        valueY: 10,
      },
    },
  ],
};

export default AmountOffPromotion;
