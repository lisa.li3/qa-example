const TenPercentOffProduct = {
  markdown: {
    type: 'XPercentOffProductPromotion',
    status: 'active',
    id: 'TenPercentOffProduct',
    slug: '10% off',
    parameters: {
      valueX: 10,
    },
  },
};

const Markdown = {
  TenPercentOffProduct,
};

export default Markdown;
