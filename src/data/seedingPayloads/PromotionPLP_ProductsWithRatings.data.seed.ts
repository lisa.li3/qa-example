/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IReviewSeeder } from '../../support/types/IReview';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `b2c2_2971_promo_plp_${timestamp}`;
  const path = `/main/framework/promotion-plp1`;
  // const XForYPromoId = `promotionplp-${getSalt() + 3}`;
  const XForYPromoId = `promotionplp-${timestamp}`;
  const shopperGroupId = 'promotion-plp1-shoppergroup';
  const SLUG = 'B2C2-2971 PROMO';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [];
  product.push(
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 1 - 0 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 10.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 2 - 1 star',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 30,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 3 - 2 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 15,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 4 - 3.5 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 9.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 5 - 2 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 19.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 6 - 4 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 29.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 7 - 5 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 5.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 8 - 0 stars',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 1.59,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    }
  );

  for (let count = 0; count < 42; count++) {
    // @ts-ignore
    product.push({
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: `Product ${9 + count}`,
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: SLUG,
          parameters: {
            valueX: 6,
            valueY: 4,
          },
        },
      },
    });
  }

  // @ts-ignore

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    {
      stock: 2,
      sizes: ['8'],
    },
    { productQuantity: 41, sizes: ['8'] },
  ];

  /**
   *  Ratings
   */
  // eslint-disable-next-line prefer-const
  review = [
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 1,
          totalRatings: 32,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 2,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 3.67,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 2,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 4,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 5,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 41,
      attributes: {
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 0.5,
          totalRatings: 5,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;

  return seedEvents;
};
