import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const DELIVERY_IN_2_DAYS = 'Delivery in 2 days';
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `site-uk-shoppergroup`, {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-split-1`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-2', `fc-split-2`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-split-1`, {
      name: 'Delivery FC1UK',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC1UK description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-split-2`, {
      name: 'Delivery FC2UKDE',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC2UKDE description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.site('site-uk', `split`, `b2c23861`, {
      shopperGroupId: `site-uk-shoppergroup`,
      fulfilmentCentres: [
        {
          fulfilmentCentreId: `fc-split-1`,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-split-2`,
          priority: 4,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: `Delivery-split-1`,
          fulfilmentCentres: [`fc-split-1`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: `Delivery-split-2`,
          fulfilmentCentres: [`fc-split-2`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              DEU: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
      ],
    })
  );
};
