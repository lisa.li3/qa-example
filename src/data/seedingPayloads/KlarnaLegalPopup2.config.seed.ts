/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.site('site-uk', 'b2c21877-2', 'b2c21877-2', {
      helpline: '** TEST 2 WORKING **',
      paymentTermsLink: [],
      shopperGroupId: 'b2c21877-2-shoppergroup',
    }),
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'b2c21877-2-shoppergroup', {})
  );
};
