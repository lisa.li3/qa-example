import BaseProduct, { IProduct } from '../../BaseProduct';

const TealTee: IProduct = {
  ...BaseProduct,
  name: 'Teal T-Shirt',
  pricing: {
    price: 8.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540742.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540746.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540748.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540750.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540752.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540754.jpg',
  ],
};

const GreenTee: IProduct = {
  ...BaseProduct,
  name: 'Green T-Shirt',
  pricing: {
    price: 8.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540874.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540878.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540880.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540882.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540884.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540886.jpg',
  ],
};

export default {
  TealTee,
  GreenTee,
};
