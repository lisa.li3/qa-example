import BaseProduct, { IProduct } from '../../BaseProduct';

const PinkTee: IProduct = {
  ...BaseProduct,
  name: 'Pink T-Shirt',
  pricing: {
    price: 8.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540558.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540562.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540564.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540566.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540568.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540572.jpg',
  ],
};

const PurpleTee: IProduct = {
  ...BaseProduct,
  name: 'Purple T-Shirt',
  pricing: {
    price: 8.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540582.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540586.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540588.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540590.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540592.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540596.jpg',
  ],
};

export default {
  PinkTee,
  PurpleTee,
};
