import BaseProduct, { IProduct } from '../../BaseProduct';

const Stripy: IProduct = {
  ...BaseProduct,
  name: 'Stripy Set',
  pricing: {
    price: 11.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540256.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540262.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540280.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540248.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540252.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540284.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540276.jpg',
  ],
};

export default {
  Stripy,
};
