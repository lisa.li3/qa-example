import BaseProduct, { IProduct } from '../../BaseProduct';

const BlackShorts: IProduct = {
  ...BaseProduct,
  name: 'Black Shorts',
  pricing: {
    price: 11.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540038.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540044.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540046.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540048.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540050.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540052.jpg',
  ],
};

export default {
  BlackShorts,
};
