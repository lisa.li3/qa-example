import BaseProduct, { IProduct } from '../../BaseProduct';

const TealTartanShirt: IProduct = {
  ...BaseProduct,
  name: 'Teal Tartan Shirt',
  pricing: {
    price: 12.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540770.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540772.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540774.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540776.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540778.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540788.jpg',
  ],
};

const NavyBlueShirt: IProduct = {
  ...BaseProduct,
  name: 'Navy Blue Shirt',
  pricing: {
    price: 16.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665541046.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665541026.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665541030.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665541032.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665541034.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665541038.jpg',
  ],
};

export default {
  TealTartanShirt,
  NavyBlueShirt,
};
