export interface ISize {
  size: string;
  stock?: number;
}

export interface IColour {
  name: string;
  description?: string;
}

export interface IBaseProduct {
  style: string;
  pricing: { price: number; currency: string; shopperGroupId: string };
  sizes: Array<ISize>;
}

export interface IProduct extends IBaseProduct {
  name: string;
  mainImage: string;
  media: string[];
  promotions?: any;
  colour?: IColour;
}

export interface IProductType {
  gender: Array<IProduct>;
}

// export interface IProductTypeColour {
//   colour: string;
// }

const BaseProduct: IBaseProduct = {
  style: 'Base Product',
  pricing: {
    price: 54.99,
  },
  sizes: [
    {
      size: 's',
      stock: 100,
    },
    {
      size: 'm',
      stock: 100,
    },
    {
      size: 'l',
      stock: 100,
    },
  ],
};

export default BaseProduct;
