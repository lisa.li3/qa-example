import BaseProduct, { IProduct } from '../../BaseProduct';

const BlueHoodie: IProduct = {
  ...BaseProduct,
  name: 'Blue Hoodie',
  pricing: {
    price: 54.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665543436.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665543436.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665543432.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665543428.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665543426.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665543416.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665543414.mp4',
  ],
  colour: {
    name: 'Blue',
  },
};

const WhiteHoodie: IProduct = {
  ...BaseProduct,
  name: 'White Hoodie',
  pricing: {
    price: 54.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665741982.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665741986.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665741982.jpg',
  ],
  colour: {
    name: 'White',
  },
};

const GreyHoodie: IProduct = {
  ...BaseProduct,
  name: 'Grey Hoodie',
  pricing: {
    price: 44.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540066.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540070.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540072.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540074.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540076.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540082.jpg',
  ],
  colour: {
    name: 'Grey',
    description: 'Something between white and black',
  },
};

const RedHoodie: IProduct = {
  ...BaseProduct,
  name: 'Red Hoodie',
  pricing: {
    price: 44.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540119.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540122.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540124.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540126.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540128.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540134.jpg',
  ],
  colour: {
    name: 'Red',
  },
};

export default {
  BlueHoodie,
  RedHoodie,
  GreyHoodie,
  WhiteHoodie,
};
