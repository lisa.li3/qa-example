import BaseProduct, { IProduct } from '../../BaseProduct';

const RedHoodie: IProduct = {
  ...BaseProduct,
  name: 'Red Hoodie',
  pricing: {
    price: 44.99,
  },
  mainImage: 'https://image1.project.com/static/images/optimised/upload9223368955665540144.jpg',
  media: [
    'https://image1.project.com/static/images/optimised/upload9223368955665540148.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540150.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540152.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540154.jpg',
    'https://image1.project.com/static/images/optimised/upload9223368955665540156.jpg',
  ],
  colour: {
    name: 'Red',
  },
};

export default {
  RedHoodie,
};
