/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;

  const categoryId = `breadcrumbs_category`;
  const subCategoryId = 'breadcrumbs-sub-category';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        body: `
                <h1>Breadcrumbs Category Heading</h1>
                
                The following is a link to the sub category : 
                  <a href="/main/framework/breadcrumbs_category/breadcrumbs_sub_category">sub category</a> 
               `,
        path: '/main/framework/breadcrumbs_category',
      },
    },
    {
      type: 'category',
      attributes: {
        categoryId: subCategoryId,
        body: `
                <h1>Breadcrumbs Sub Category Heading</h1>
              `,
        path: '/main/framework/breadcrumbs_category/breadcrumbs_sub_category',
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      attributes: { defaultCategoryId: categoryId }, // optional, but required if you wish to navigate to the product via a category
    },
    {
      attributes: { defaultCategoryId: subCategoryId, name: 'sub-category' }, // optional, but required if you wish to navigate to the product via a category
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;

  return seedEvents;
};
