/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CheckoutOrderSummary',
        emailAddress: 'checkoutordersummary@project.local',
        password: 'its-a-secret',
        firstName: 'Chuck',
        lastName: 'Norris',
        gender: 'male',
        dateOfBirth: '1950-01-01',
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['10'],
      attributes: {
        name: 'Brown Jeans',
        pricing: [
          {
            shopperGroupId: 'order-summary-shoppergroup',
            price: 29.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: ['10'],
      attributes: {
        name: 'Blue Jeans',
        pricing: [
          {
            shopperGroupId: 'order-summary-shoppergroup',
            price: 39.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: ['10'],
      stock: 1,
      fulfilmentCentres: ['fc-order-summary'],
    },
    {
      sizes: ['10'],
      stock: 1,
      fulfilmentCentres: ['fc-order-summary'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
