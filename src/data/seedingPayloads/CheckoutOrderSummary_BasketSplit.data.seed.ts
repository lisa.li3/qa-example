/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IShipping } from '../../support/types/IShipping';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let shipping: IShipping[] | undefined;

  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  (product = [
    {
      quantity: 1,
      sizes: [10],
      attributes: {
        name: 'Product 1',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: [10],
      attributes: {
        name: 'Product 2',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: [10],
      attributes: {
        name: 'Product 3',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: [10],
      attributes: {
        name: 'Product 4',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ]),
    /**
     * Inventory
     */
    // eslint-disable-next-line prefer-const
    (inventory = [
      {
        sizes: [10],
        stock: 1,
        fulfilmentCentres: ['fc-2087-1'],
      },
      {
        sizes: [10],
        stock: 1,
        fulfilmentCentres: ['fc-2087-2'],
      },
      {
        sizes: [10],
        stock: 1,
        fulfilmentCentres: ['fc-2087-3'],
      },
      {
        sizes: [10],
        stock: 1,
        fulfilmentCentres: ['fc-2087-4'],
      },
    ]);
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  shipping = [
    {
      storeIds: [],
      destinationCountries: ['GB'],
      fulfilmentCentres: ['fc-2087-3'],
      methodIds: ['FC3-2'],
      attributes: {
        ServiceGroup: {
          promises: [
            {
              day: 'Monday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Tuesday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Wednesday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Thursday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Friday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Saturday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Sunday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
          ],
        },
      },
    },
    {
      storeIds: [],
      destinationCountries: ['GB'],
      fulfilmentCentres: ['fc-2087-4'],
      methodIds: ['FC4-1'],
      attributes: {
        ServiceGroup: {
          promises: [
            {
              day: 'Monday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Tuesday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Wednesday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Thursday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Friday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Saturday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Sunday',
              cutoff: '23:59:59',
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
          ],
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;
  shipping ? (seedEvents.shipping = shipping) : null;

  return seedEvents;
};
