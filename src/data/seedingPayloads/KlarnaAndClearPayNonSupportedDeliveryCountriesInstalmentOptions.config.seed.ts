/**
 Perform Data Seeding for the given test case
 */

import { GenerateConfig } from '@project/site-config-transformer/dist';
export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-b2c23605-shoppergroup', {
      deliveryCountries: [
        { code: 'AUS', name: 'Australia' },
        { code: 'USA', name: 'United States' },
      ],
    }),

    GenerateConfig.site('site-uk', 'b2c23605non', 'b2c23605non', {
      shopperGroupId: 'site-b2c23605-shoppergroup',
      defaultCountryCode: 'AUS',
      paymentPlanProviders: [
        {
          id: 'klarna',
          name: 'Klarna',
          logoImageUrl: '/images/klarna-small.png',
          installments: 3,
          url: '/klarna',
          supportedCountries: ['GBR', 'USA'],
        },
        {
          id: 'clearpay',
          name: 'Clearpay',
          logoImageUrl: '/images/clearpay-small.png',
          installments: 4,
          url: '/clearpay',
          supportedCountries: ['GBR', 'USA'],
        },
      ],
    })
  );
};
