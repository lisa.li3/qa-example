/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';
import { IReviewSeeder } from '../../support/types/IReview';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-2044', path: '/main/framework/B2C2-2044' },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `delivery44`,
      sizes: ['select size', 'size_modal_1', 'size_modal_2'],
      attributes: {
        defaultCategoryId: 'B2C2-2044',
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`delivery44`],
      sizes: ['select size', 'size_modal_1', 'size_modal_2'],
      stock: 10,
    },
  ];

  /**
   * Reviews
   */
  // eslint-disable-next-line prefer-const

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-PDP-DeliveryPanel',
        emailAddress: 'PDP@project.local',
        password: 'its-a-secret',
        firstName: 'PDP',
        lastName: 'test',
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
