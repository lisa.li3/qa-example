/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (domainPrefix: string, oldSiteId?: string): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'staffdiscount-checkout-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),

    GenerateConfig.site(
      oldSiteId ? oldSiteId : 'site-uk',
      'staffdiscount-checkout',
      'staffdiscount-checkout',
      {
        shopperGroupId: 'staffdiscount-checkout-shoppergroup',
        authoritativeCountries: ['GBR'],
      }
    )
  );
};
