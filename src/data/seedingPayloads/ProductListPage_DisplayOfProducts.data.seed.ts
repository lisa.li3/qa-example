/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import { IReviewSeeder } from '../../support/types/IReview';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { getSalt } from '../../support/wdioHelpers/seedValueSalt';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `b2c2_308_${timestamp}`;
  const path = `/main/framework/display_of_products`;
  const markdownAmountOffPromoId = `promo-x-off-b2c2-308-${getSalt() + 2}`;
  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  const family = [
    {
      id: 'prodv0col1',
      name: 'red thing',
      description: 'red thing desc',
      image: 'https://via.placeholder.com/150/FF0000',
    },
    {
      id: 'prodv0col2',
      name: 'blue thing',
      description: 'blue thing desc',
      image: 'https://via.placeholder.com/150/0000FF',
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: 'prodv0col1',
      sizes: ['1size'],
      attributes: {
        name: 'Product 1 - Colour Variations',
        defaultCategoryId: categoryId,
        family,
        style: 'PRODUCTCOLLECTION',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 1000,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: 'prodv0col2',
      sizes: ['1size'],
      attributes: {
        name: 'Product 2 - Colour Variations',
        defaultCategoryId: categoryId,
        family,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 33,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `allin308n1`,
      attributes: {
        name: 'Product 3',
        defaultCategoryId: categoryId,
        style: 'PRODUCTCONCEPT',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 999,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        markdown: {
          type: 'XAmountOffProductPromotion',
          status: 'active',
          id: markdownAmountOffPromoId,
          slug: '£99 off',
          parameters: {
            valueX: 99,
          },
        },
      },
    },
    {
      id: `extra308no`,
      attributes: {
        name: 'Product 4',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 100,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `stock308no`,
      attributes: {
        name: 'Product - Out of stock',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 100,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['prodv0col1', 'prodv0col2'],
      sizes: ['1size'],
      stock: 10,
    },
    { skus: ['allin308n1', 'extra308no'], stock: 10 },
    { skus: ['stock308no'], stock: 0 },
  ];

  /**
   * Reviews
   */
  // eslint-disable-next-line prefer-const
  review = [
    {
      productQuantity: 1,
      reviewQuantity: 5,
      attributes: {
        productId: `allin308n1`,
        averageRating: {
          averageRating: 3,
          totalRatings: 5,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;

  return seedEvents;
};
