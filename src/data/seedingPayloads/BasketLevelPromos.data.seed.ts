/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { getSalt } from '../../support/wdioHelpers/seedValueSalt';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  const categoryId = 'test-basketlevelpromos';

  const XForYPromoId = `promo-pdp-3-for-2-${getSalt() + 1}`;
  const XForYAmountPromoId = `promo-pdp-3-for-amount-${getSalt() + 2}`;

  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const SHOPPER_GROUP_ID = 'site-3043-shoppergroup';
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-BasketLevelPromos',
        emailAddress: 'basketlevelpromos@project.local',
        password: 'its-a-secret',
        firstName: 'Basket',
        lastName: 'Promo',
        gender: 'male',
        dateOfBirth: '1900-01-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId,
        path: `/main/category-mens-uk/category-mens-hoodies-uk/${categoryId}`,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: 'osfp7tespa',
      sizes: ['s', 'm', 'l'],
      attributes: {
        name: '3for2 Hoodie',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 59.99,
            currency: 'GBP',
          },
        ],
      },

      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
        vouchers: [
          {
            code: '30Off100Spend',
            type: 'AmountOffPromotion',
            status: 'active',
            parameters: {
              valueX: 100,
              valueY: 30,
            },
          },
          {
            code: '20PercentOff100Spend',
            type: 'PercentOffSpendAmountPromotion',
            status: 'active',
            parameters: {
              valueX: 100,
              valueY: 20,
            },
          },
        ],
      },
    },
    {
      id: 'hnf4pjy3ds',
      sizes: ['s', 'm', 'l'],
      attributes: {
        name: '3for120 Hoodie',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 80.0,
            currency: 'GBP',
          },
        ],
      },

      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £120',
          parameters: {
            valueX: 3,
            valueY: 120,
          },
        },
        vouchers: [
          {
            code: '30Off100Spend',
            type: 'AmountOffPromotion',
            status: 'active',
            parameters: {
              valueX: 100,
              valueY: 30,
            },
          },
          {
            code: '30PercentOff100Spend',
            type: 'PercentOffSpendAmountPromotion',
            status: 'active',
            parameters: {
              valueX: 100,
              valueY: 30,
            },
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['osfp7tespa'],
      sizes: ['s', 'm', 'l'],
      stock: 100,
      fulfilmentCentres: ['fc-3043'],
    },
    {
      skus: ['hnf4pjy3ds'],
      sizes: ['s', 'm', 'l'],
      stock: 100,
      fulfilmentCentres: ['fc-3043'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
