/**
 * Perform Data Seeding for the given test case
 */
import IPaymentSeeder from '../../support/types/IPayment';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  let payment: IPaymentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let user: IUser[] | undefined;

  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CustomerCredit',
        emailAddress: 'customercredit@project.com',
        password: 'its-a-secret',
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11],
      attributes: {
        name: 'CheckoutProduct1',
        pricing: [
          {
            shopperGroupId: 'customercreditequals-shoppergroup',
            price: 2000.0,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const startDate = '2019-08-01T18:53:13.674Z';
  const EndDate = '2050-06-02T05:06:46.326Z';

  // eslint-disable-next-line prefer-const
  payment = [
    {
      attributes: {
        emailAddress: 'customercredit@project.com',
        credits: [
          {
            disabled: false,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 2000,
            name: 'Active',
            remainingValue: 2000,
            startDate: startDate,
            transactions: [],
          },
          {
            disabled: false,
            endDate: '2020-08-01T18:53:13.674Z',
            id: 'fzpx04az58',
            initialValue: 100,
            name: 'Expired',
            remainingValue: 100,
            startDate: startDate,
            transactions: [],
          },
          {
            disabled: true,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 200,
            name: 'Disabled',
            remainingValue: 200,
            startDate: startDate,
            transactions: [],
          },
          {
            id: 'pending-credit',
            disabled: false,
            endDate: '2050-06-02T05:06:46.326Z',
            initialValue: 100,
            name: 'pending-credit',
            remainingValue: 100,
            startDate: '2049-08-01T18:53:13.674Z',
            transactions: [],
          },
        ],
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  payment ? (seedEvents.payment = payment) : null;

  return seedEvents;
};
