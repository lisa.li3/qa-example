/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-unableToDeliver3',
        emailAddress: 'unableToDeliver3@deliveryOptions.com',
        password: 'its-a-secret',
        firstName: 'Unable',
        lastName: 'ToDeliver',
        gender: 'male',
        dateOfBirth: '1990-01-01',
        addresses: [
          {
            id: '2d2126',
            firstName: 'UK',
            lastName: 'home',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road 1',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'GL51 9NH',
            isDefault: true,
            nickname: 'UK home',
          },
          {
            id: '2d2127',
            firstName: 'Foo1',
            lastName: 'Bar1',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flotowstr. 78',
            addressLine2: 'Test',
            city: 'Sangerhausen',
            county: 'Sachsen-Anhalt',
            country: 'DEU',
            postcode: '06513',
            isDefault: false,
            nickname: 'DE home',
          },
        ],
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  (product = [
    {
      sizes: [10, 11],
      quantity: 2,
      attributes: {
        pricing: [
          {
            shopperGroupId: `shoppergroup-UnableToDeliver`,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ]),
    /**
     * Inventory
     */
    // eslint-disable-next-line prefer-const
    (inventory = [
      {
        sizes: [10],
        stock: 1,
        fulfilmentCentres: ['fc-unableToDeliver-1'],
      },
      {
        sizes: [11],
        stock: 1,
        fulfilmentCentres: ['fc-unableToDeliver-4'],
      },
    ]);
  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
