/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'gtm-on-shoppergroup', {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),

    GenerateConfig.site('site-uk', 'gtm-on', 'gtm-on', {
      gtmContainerShared: {
        gtmId: 'GTM-M84TN9J',
        auth: '5cbd_A_Vw3-Zdr_7EDZ5kA',
        preview: 'env-3',
      },
      gtmContainerAnalytics: {
        gtmId: 'GTM-NXSGB3T',
        auth: 'zI1uDnm3ZdBYsyrNz-VzBQ',
        preview: 'env-3',
      },
      shopperGroupId: 'gtm-on-shoppergroup',
      authoritativeCountries: ['GBR'],
    })
  );
};
