/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-ViewSelectBillingAddress',
        emailAddress: 'viewselectbillingaddress@project.local',
        password: 'its-a-secret',
        firstName: 'Foo',
        lastName: 'Bar',
        gender: 'male',
        dateOfBirth: '2000-01-01',
        addresses: [
          {
            id: '2d2e6',
            firstName: 'Foo',
            lastName: 'Bar',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BB',
            isDefault: true,
            nickname: 'Home',
          },
        ],
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: [11],
      attributes: {
        defaultCategoryId: `category-1940`,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      productQuantity: 1,
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
