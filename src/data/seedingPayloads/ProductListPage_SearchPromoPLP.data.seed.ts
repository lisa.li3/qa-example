/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `b2c2_2259_searchpromo_plp_${timestamp}`;
  const path = `/main/framework/b2c2_2259/searchpromo_plp`;
  const XForYPromoId = `xfory-${timestamp}`;
  const XForYAmountPromoId = `xforyamount-${timestamp}`;
  const shopperGroupId = 'searchplp-promo-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `promo666p1`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 1',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 12.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '4 for 2',
          parameters: {
            valueX: 4,
            valueY: 2,
          },
        },
      },
    },
    {
      id: `promo666p2`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 2',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 6.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '4 for £20',
          parameters: {
            valueX: 4,
            valueY: 20,
          },
        },
      },
    },
    {
      id: `promo666p3`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 3',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 10.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `promo666p4`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 4',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 6.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '4 for £20',
          parameters: {
            valueX: 4,
            valueY: 20,
          },
        },
      },
    },
    {
      id: `promo666p5`,
      sizes: ['8', '10', '12'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 5',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 6.99,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId + 1,
          slug: '5 for 3',
          parameters: {
            valueX: 5,
            valueY: 3,
          },
        },
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`promo666p1`, `promo666p2`, `promo666p3`, `promo666p4`],
      sizes: ['8'],
      stock: 10,
    },
    {
      skus: [`promo666p5`],
      sizes: ['8', '10', '12'],
      stock: 10,
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
