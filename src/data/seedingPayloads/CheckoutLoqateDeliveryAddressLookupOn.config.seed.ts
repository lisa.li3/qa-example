/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';
import { getState } from '../../support/functions/state';

export const getSiteConfig = async (domainPrefix: string, oldSiteId?: string) => {
  const { constants } = getState();
  const mockAddressServiceUrl = `https://${constants.STAGE}.eu-west-1.prometheus.sd.co.uk/test-mock/address-lookup-service`;

  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'deliveryloqate-on-shoppergroup', {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'FRA', name: 'France' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-delivery-options', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.serviceSet('eu-west-1-dynamic', 'override-service-set', {
      services: {
        'address-lookup': mockAddressServiceUrl,
      },
    }),
    GenerateConfig.site(
      oldSiteId ? oldSiteId : 'site-uk',
      'deliveryloqate-on',
      'deliveryloqate-on',
      {
        shopperGroupId: 'deliveryloqate-on-shoppergroup',
        authoritativeCountries: ['GBR', 'FRA'],
        addressLookup: true,
        serviceSet: 'override-service-set',
        fulfilmentCentres: [
          {
            fulfilmentCentreId: 'fc-delivery-options',
            priority: 1,
            isDistributionCentre: true,
            // @ts-ignore
            buffer: 0,
          },
        ],
        deliveryMethods: [
          {
            id: 'UK300',
            fulfilmentCentres: ['fc-delivery-options'],
            extensions: {
              enabled: true,
              destinationConfig: {
                GBR: {
                  default: {
                    price: 2.5,
                    transitTime: 10,
                  },
                },
                FRA: {
                  default: {
                    price: 6.95,
                    transitTime: 10,
                  },
                },
              },
            },
          },
          {
            id: 'UK200',
            fulfilmentCentres: ['fc-delivery-options'],
            extensions: {
              enabled: true,
              destinationConfig: {
                GBR: {
                  default: {
                    price: 0,
                    transitTime: 10,
                  },
                },
                FRA: {
                  default: {
                    price: 6.95,
                    transitTime: 10,
                  },
                },
              },
            },
          },
          {
            id: 'UK100',
            fulfilmentCentres: ['fc-delivery-options'],
            extensions: {
              enabled: true,
              destinationConfig: {
                GBR: {
                  default: {
                    price: 3.95,
                    transitTime: 1,
                  },
                },
                FRA: {
                  default: {
                    price: 12.95,
                    transitTime: 1,
                  },
                },
              },
            },
          },
        ],
      }
    ),
    GenerateConfig.deliveryMethod('INTND', 'UK100', {
      name: 'UK delivery1',
      message: 'Delivery in 2 days',
      description: 'UK delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK200', {
      name: 'UK delivery2',
      message: 'Delivery in 5 days',
      description: 'UK delivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK300', {
      name: 'UK delivery3',
      message: 'Delivery in 3 days',
      description: 'UK delivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    })
  );
};
