/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IReviewSeeder } from '../../support/types/IReview';
import { getSalt } from '../../support/wdioHelpers/seedValueSalt';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `promos_with_ratings_${timestamp}`;
  const path = `/main/framework/promos-with-ratings`;
  const XForYAmountPromoId = `promo-3-for-amount-${getSalt() + 1}`;
  const XForYPromoId = `promo-3-for-2-${getSalt() + 2}`;

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      staffDiscountRate: 50,
      attributes: {
        id: 'AT-PLP-Ratings',
        emailAddress: 'plp.staffdiscount@project.local',
        password: 'its-a-secret',
        firstName: 'User',
        lastName: 'WithDiscount',
        gender: 'male',
        dateOfBirth: '1900-01-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 1 - 0 stars',
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'expired',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 2 - 1 star',
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 3 - 2 stars',
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 4 - 3.5 stars',
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 5 - 2 stars',
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 6 - 4 stars',
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 7 - 5 stars',
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 8 - 0 stars',
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
  ];

  /**
   *  Ratings
   */
  // eslint-disable-next-line prefer-const
  review = [
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 1,
          totalRatings: 32,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 2,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 3.67,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 2,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 4,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 5,
          totalRatings: 12,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
