/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';
import { DeliveryType } from '@project/site-config-transformer/dist/types/configs';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-pdp-shoppergroup', {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'FRA', name: 'France' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-2044', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FAST2044', {
      name: 'Very fast delivery',
      message: 'Delivery in 2 days',
      description: 'Very fast delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'SLOW2044', {
      name: 'Very slow delivery',
      message: 'Delivery in 5 days',
      description: 'Very slow delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'STD2044', {
      name: 'Standard delivery',
      message: 'Delivery in 3 days',
      description: 'Standard delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRAFAST2044', {
      name: 'FRAVery fast delivery',
      message: 'FRADelivery in 2 days',
      description: 'FRAVery fast delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRASLOW2044', {
      name: 'FRAVery slow delivery',
      message: 'FRADelivery in 5 days',
      description: 'FRAVery slow delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRASTD2044', {
      name: 'FRAStandard delivery',
      message: 'FRADelivery in 3 days',
      description: 'FRAStandard delivery description',
      type: 'ToAddress' as DeliveryType,
      displayOrder: 2,
    }),
    GenerateConfig.site('site-uk', 'site-pdp', 'site-pdp', {
      shopperGroupId: 'site-pdp-shoppergroup',
      defaultLocale: 'en-GB',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-2044',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'STD2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'SLOW2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FAST2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FRASLOW2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRASTD2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 29,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRAFAST2044',
          fulfilmentCentres: ['fc-2044'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 399,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
