/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';

export default async () => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CheckoutOutSixOfStockProducts',
        emailAddress: 'checkoutoutsixofstockproducts@project.local',
        password: 'its-a-secret',
        firstName: 'Bob',
        lastName: 'Dylan',
        gender: 'male',
        dateOfBirth: '1900-01-01',
        addresses: [
          {
            id: '11111111',
            firstName: 'Name',
            lastName: 'First',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 4, Copper court',
            addressLine2: '23 Kings Road',
            city: 'London',
            county: 'London',
            country: 'GBR',
            postcode: 'W14 9YA',
            isDefault: true,
            nickname: 'Home',
          },
          {
            id: '22222222',
            firstName: 'Name',
            lastName: 'Second',
            contactNumber: {
              phone: '1342342342234',
              mobile: '723124123123',
            },
            addressLine1: 'Flat 3, Copper court',
            addressLine2: '22 Kings Road',
            city: 'London',
            county: 'London',
            country: 'GBR',
            postcode: 'W14 9YA',
            isDefault: false,
            nickname: '2nd Address',
          },
        ],
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11],
      quantity: 1,
      attributes: {
        name: 'PaymentMethodProduct',
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11],
      quantity: 1,
      attributes: {
        name: 'ProductMulipleQuantity',
        defaultCategoryId: `B2C2-122`,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      stock: 6,
      productQuantity: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
