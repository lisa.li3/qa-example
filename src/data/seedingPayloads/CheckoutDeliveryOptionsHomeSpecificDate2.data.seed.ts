/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IShipping } from '../../support/types/IShipping';
import moment from 'moment';

export default async () => {
  const cutoff = moment()
    .subtract(moment().isDST() ? 61 : 1, 'minutes')
    .format('HH:mm:ss');

  /**
   * Seed each part of the desired objects
   */
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let shipping: IShipping[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  shipping = [
    {
      storeIds: [],
      destinationCountries: ['GB'],
      fulfilmentCentres: ['fc-132-1'],
      methodIds: ['STD132'],
      attributes: {
        ServiceGroup: {
          promises: [
            {
              day: 'Monday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Tuesday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Wednesday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Thursday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Friday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Saturday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
            {
              day: 'Sunday',
              cutoff,
              transit_time: '1',
              specific_days: {
                day: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
              },
            },
          ],
        },
      },
    },
  ];

  const seedEvents: any = {};
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  shipping ? (seedEvents.shipping = shipping) : null;

  return seedEvents;
};
