/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: `test-898-${timestamp}-48`,
        path: `/main/framework/b2c2-275/48-products`,
      },
    },
    {
      type: 'category',
      attributes: {
        categoryId: `test-898-${timestamp}-100`,
        body: `
                <h1>Stylish Products</h1>
                <p>This is the description</p>
              `,
        path: `/main/framework/b2c2-275/100-products`,
      },
    },
    {
      type: 'category',
      attributes: {
        categoryId: `test-898-${timestamp}-3`,
        path: `/main/framework/b2c2-300/3-products`,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 48,
      sizes: [11, 16],
      attributes: { defaultCategoryId: `test-898-${timestamp}-48` },
    },
    {
      quantity: 100,
      sizes: [11, 16],
      attributes: { defaultCategoryId: `test-898-${timestamp}-100` },
    },
    {
      quantity: 1,
      sizes: [14],
      attributes: {
        name: 'Blue Hoodie',
        type: 'Bomber Jacket',
        gender: 'mens',
        colour: {
          id: 'shrv67dwc4',
          name: 'Dark Red',
          description: 'Deep red inspired by lapis lazuli',
        },
        defaultCategoryId: `test-898-${timestamp}-3`,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 25.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: [11, 12],
      attributes: {
        name: 'Green Hoodie',
        type: 'Casual Jacket',
        gender: 'womens',
        colour: {
          id: 'shrv5fwdc4',
          name: 'Dark Green',
          description: 'Deep green inspired by lapis lazuli',
        },
        // family: [
        //   {
        //     name: 'jacket',
        //     id: `test-898-${timestamp}-cat1`,
        //   },
        // ],
        defaultCategoryId: `test-898-${timestamp}-3`,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 10.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: [11, 12, 13],
      attributes: {
        name: 'Black Hoodie',
        type: 'Denim Jacket',
        gender: 'womens',
        colour: {
          id: 'shrv5fwwc4',
          name: 'Dark Blue',
          description: 'Deep blue inspired by lapis lazuli',
        },
        // family: [
        //   {
        //     name: 'hoodie',
        //     id: `test-898-${timestamp}-cat2`,
        //   },
        // ],
        defaultCategoryId: `test-898-${timestamp}-3`,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 19.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    { sizes: [11, 16], productQuantity: 48, stock: 1 },
    { sizes: [11, 16], productQuantity: 100, stock: 1 },
    { sizes: [14], productQuantity: 1, stock: 5 },
    { sizes: [11], productQuantity: 30, stock: 2 },
    { sizes: [11, 12, 13], productQuantity: 12, stock: 6 },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
