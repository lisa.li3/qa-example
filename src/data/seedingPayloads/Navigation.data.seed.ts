/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-57', path: `/main/menuTest/B2C2-57` },
    },
    {
      type: 'category',
      attributes: {
        categoryId: 'testCategory2026-2',
        path: '/main/framework/test-multiple',
        body: 'example',
      },
    },
    {
      type: 'static',
      attributes: { path: `/footer/framework/footerscroll` },
    },
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-2903-t', path: `/main/sale/limited/tshirts` },
    },
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-2903-s', path: `/main/sale/limited/shorts` },
    },
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-2903-j', path: `/main/sale/limited/jackets` },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      attributes: {
        id: `vyf81i5abc`,
        sku: `vyf81i5abc-8`,
        plu: `vyf81i5abc-plu`,
        name: 'test-product-name',
        defaultCategoryId: `testCategory2026-2`,
        colour: {
          id: `vyf81i5abc`,
          name: 'sea-blue',
          description: 'A special Blue',
        },
        family: [
          {
            name: 'sea-blue',
            image:
              'https://image1.project.com/static/images/optimised/upload9223368955665870143.jpg ',
            description: 'My lovely blue product',
            id: `vyf81i5abc`,
          },
        ],
        availableSizes: [
          {
            name: 'size8Label',
            id: `vyf81i5abc-8`,
            ean: `vyf81i5abc-8`,
            sku: `vyf81i5abc-8`,
          },
          {
            name: '10',
            id: `vyf81i5abc-10`,
            ean: `vyf81i5abc-10`,
            sku: `vyf81i5abc-10`,
          },
          {
            name: '12',
            id: `vyf81i5abc-12`,
            ean: `vyf81i5abc-12`,
            sku: `vyf81i5abc-12`,
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`vyf81i5abc`],
    },
  ];
  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
