/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `site-191-shoppergroup`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'USA', name: 'United States' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-191', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'site-delivery-address', 'deliveryaddress', {
      shopperGroupId: 'site-191-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-191',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'UKUS3',
          fulfilmentCentres: ['fc-191'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UKUS2',
          fulfilmentCentres: ['fc-191'],
          extensions: {
            enabled: true,
            destinationConfig: {
              USA: {
                default: {
                  price: 7,
                  transitTime: 10,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UKUS2', {
      name: 'UKUS delivery2',
      message: 'Delivery in 5 days',
      description: 'UKUSdelivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UKUS3', {
      name: 'UKUSdelivery3',
      message: 'Delivery in 3 days',
      description: 'UKUSdelivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    })
  );
};
