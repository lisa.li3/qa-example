/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `site-3043-shoppergroup`, {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-3043', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'site-3043', 'b2c23043', {
      shopperGroupId: 'site-3043-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-3043',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'INTND',
          fulfilmentCentres: ['fc-3043'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 0,
                  transitTime: 0,
                },
              },
              GBR: {
                default: {
                  price: 0,
                  transitTime: 0,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UKUS2', {
      name: 'UKUS delivery2',
      message: 'Delivery in 5 days',
      description: 'UKUSdelivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UKUS3', {
      name: 'UKUSdelivery3',
      message: 'Delivery in 3 days',
      description: 'UKUSdelivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    })
  );
};
