import { IInventorySeeder } from '../../support/types/IInventory';
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import IUser from '../../support/types/IUser';
import { getAsSeedUser } from '../../support/functions/definedCustomer';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  const giftCardCategoryId = '9999999';
  const colours = [
    {
      id: 'black',
      image: 'https://image1.project.com/static/images/optimised/upload9223368955665663356.jpg',
    },
    {
      id: 'orange',
      image: 'https://image1.project.com/static/images/optimised/upload9223368955665666139.jpg',
    },
    {
      id: 'pink',
      image: 'https://image1.project.com/static/images/optimised/upload2313425547902654024.jpg',
    },
  ];
  /**
   * Seed each part of the desired objects
   */
  let content: IContentSeeder[] | undefined;
  let user: IUser[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const categoryId = `9999999`;
  const path = `/main/framework/${categoryId}`;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-2724', path: `/main/menuTest/B2C2-2724` },
    },
    {
      type: 'category',
      attributes: {
        categoryId: 'testCategory2026-2',
        path: '/main/framework/test-multiple',
        body: 'example',
      },
    },
    {
      type: 'static',
      attributes: { path: path },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = colours.map((colour) => ({
    quantity: 1,
    sizes: [],
    attributes: {
      name: `${colour.id} giftcard`,
      id: `giftcard-${colour.id}`,
      ean: `giftcard-orange-ean`,
      plu: 'giftcard',
      type: 'giftcard',
      style: 'project',
      media: [colour.image],
      mainImage: colour.image,
      defaultCategoryId: giftCardCategoryId,
      pricing: [
        {
          shopperGroupId: 'site-uk-shoppergroup',
          price: 5,
          currency: 'GBP',
        },
      ],
      availableSizes: [
        {
          name: 'ONESIZE',
          id: `giftcard-${colour.id}-ONESIZE`,
          sku: `giftcard-${colour.id}-ONESIZE`,
          ean: `giftcard-${colour.id}-ONESIZE`,
        },
      ],
      colour: {
        id: `giftcard-${colour.id}`,
        name: `${colour.id.toUpperCase()}`,
        description: `${colour.id}`,
      },
      family: colours.map((familyColour) => ({
        name: familyColour.id.toUpperCase(),
        image: familyColour.image,
        description: familyColour.id,
        id: `giftcard-${familyColour.id}`,
      })),
    },
  }));
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [await getAsSeedUser()];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [{ stock: 0 }];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
