/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: ['uniqueSize1'],
      attributes: {
        name: 'Product 1 name',
        pricing: [
          {
            shopperGroupId: 'sourcing-logic-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
        colour: {
          id: 'a11122',
          name: 'Product 1 colour',
          description: 'Product 1 colour',
        },
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: ['uniqueSize1'],
      attributes: {
        location: {
          'fc-sourcing-logic-2': 1,
          'fc-sourcing-logic-3': 1,
          'fc-sourcing-logic-4': 1,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
