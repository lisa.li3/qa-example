/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { IInventorySeeder } from '../../support/types/IInventory';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['s', 'm', 'l'],
      attributes: {
        pricing: [
          {
            shopperGroupId: 'PayPalExpress-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      sizes: ['m'],
      attributes: {
        pricing: [
          {
            shopperGroupId: 'PayPalExpress-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: ['s', 'm', 'l'],
      productQuantity: 1,
      fulfilmentCentres: [`fc-uk-1`],
      stock: 12,
    },
    {
      sizes: ['m'],
      stock: 0,
    },
  ];

  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
