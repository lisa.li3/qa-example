import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const DELIVERY_IN_2_DAYS = 'Delivery in 2 days';
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `shoppergroup-UnableToDeliver`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'DEU', name: 'Germany' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-unableToDeliver-1`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-unableToDeliver-2`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-unableToDeliver-3`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-unableToDeliver-4`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-unableToDeliver-1`, {
      name: 'Delivery FC1UK',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC1UK description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-unableToDeliver-2`, {
      name: 'Delivery FC2DE',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC2DE description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-unableToDeliver-3`, {
      name: 'Delivery FC3FFS',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC3FFS description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-unableToDeliver-4`, {
      name: 'Delivery FC4UKDE',
      message: DELIVERY_IN_2_DAYS,
      description: 'Delivery FC4UKDE description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.site('site-uk', `site-unableToDeliver`, `b2c2186`, {
      shopperGroupId: `shoppergroup-UnableToDeliver`,
      fulfilmentCentres: [
        {
          fulfilmentCentreId: `fc-unableToDeliver-1`,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-unableToDeliver-2`,
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-unableToDeliver-3`,
          priority: 3,
          isDistributionCentre: false,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-unableToDeliver-4`,
          priority: 4,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: `Delivery-unableToDeliver-1`,
          fulfilmentCentres: [`fc-unableToDeliver-1`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: `Delivery-unableToDeliver-2`,
          fulfilmentCentres: [`fc-unableToDeliver-2`],
          extensions: {
            enabled: true,
            destinationConfig: {
              DEU: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: `Delivery-unableToDeliver-3`,
          fulfilmentCentres: [`fc-unableToDeliver-3`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: `Delivery-unableToDeliver-4`,
          fulfilmentCentres: [`fc-unableToDeliver-4`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              DEU: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
      ],
    })
  );
};
