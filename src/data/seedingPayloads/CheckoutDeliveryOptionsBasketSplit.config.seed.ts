import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const DELIVERY_ON = 'Delivery on %s';
  return await browser.configureEnv(
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-1', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-2', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-3', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-4', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-5', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-128-6', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC1-1', {
      name: 'FC1 delivery 1',
      message: DELIVERY_ON,
      description: 'Fast delivery',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC1-2', {
      name: 'FC1 delivery 2',
      message: DELIVERY_ON,
      description: 'Fast delivery',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC2-1', {
      name: 'FC2 delivery 1',
      message: DELIVERY_ON,
      description: 'Slow delivery',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC2-2', {
      name: 'FC2 delivery 2',
      message: DELIVERY_ON,
      description: 'Slow delivery',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC3-1', {
      name: 'FC3 delivery 1',
      message: DELIVERY_ON,
      description: 'Standard delivery',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC3-2', {
      name: 'FC3 delivery 2',
      message: DELIVERY_ON,
      description: 'Standard delivery',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC4-1', {
      name: 'FC4 delivery 1',
      message: DELIVERY_ON,
      description: 'Free delivery',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.site('site-uk', 'site-128', 'b2c2128', {
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-128-1',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: 'fc-128-2',
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: 'fc-128-3',
          priority: 3,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: 'fc-128-4',
          priority: 4,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'FC1-1',
          fulfilmentCentres: ['fc-128-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 10.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC1-2',
          fulfilmentCentres: ['fc-128-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC2-1',
          fulfilmentCentres: ['fc-128-2'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 10.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC2-2',
          fulfilmentCentres: ['fc-128-2'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 1.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC3-1',
          fulfilmentCentres: ['fc-128-3'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 10.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC3-2',
          fulfilmentCentres: ['fc-128-3'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 5.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC4-1',
          fulfilmentCentres: ['fc-128-4'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 10.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC4-2',
          fulfilmentCentres: ['fc-128-4'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 9.99,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
