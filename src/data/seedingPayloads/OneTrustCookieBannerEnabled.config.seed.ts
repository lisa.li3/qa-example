/**
 Perform Data Seeding for the given test case
 */

import { GenerateConfig } from '@project/site-config-transformer/dist';
export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.site('site-uk', 'b2c23603', 'b2c23603', {
      cookieConsentEnabled: true,
      cookieConsentRef: '0a9903b5-aa4e-4e39-81d9-cae63b52b537-test',
    })
  );
};
