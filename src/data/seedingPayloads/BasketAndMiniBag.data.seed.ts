/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { getSalt } from '../../support/wdioHelpers/seedValueSalt';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  const categoryId = 'test-basketandminibagMini';

  const XForYPromoId = `promo-pdp-3-for-2-${getSalt() + 1}`;
  const XForYAmountPromoId = `promo-pdp-3-for-amount-${getSalt() + 2}`;

  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-BasketAndMiniBagMini',
        emailAddress: 'basketandminibag@project.mini',
        password: 'its-a-secret',
        firstName: 'Bob',
        lastName: 'Dylan',
        gender: 'male',
        dateOfBirth: '1900-01-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId,
        path: `/main/category-mens-uk/category-mens-hoodies-uk/${categoryId}`,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: 'mhuq4h1vf1',
      attributes: {
        name: 'bambg Hoodie',
        defaultCategoryId: categoryId,
      },
    },
    {
      id: 'mhuq4h1vf2',
      sizes: ['s', 'm', 'l'],
      attributes: {
        name: '3for2 Hoodie',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 19.99,
            currency: 'GBP',
          },
        ],
      },

      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
      },
    },
    {
      id: 'mhuq4h1vf3',
      sizes: ['s', 'm', 'l'],
      attributes: {
        name: '3for£19.99 Hoodie',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 8.99,
            currency: 'GBP',
          },
        ],
      },

      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £19.99',
          parameters: {
            valueX: 3,
            valueY: 19.99,
          },
        },
      },
    },
    {
      id: 'mhuq4h1vf4',
      sizes: ['s', 'm', 'l'],
      attributes: {
        name: '2for£14.99 Hoodie',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 7.99,
            currency: 'GBP',
          },
        ],
      },

      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '2 for £14.99',
          parameters: {
            valueX: 2,
            valueY: 14.99,
          },
        },
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['mhuq4h1vf1'],
      stock: 3,
    },
    {
      skus: ['mhuq4h1vf2'],
      sizes: ['s', 'm', 'l'],
      stock: 100,
    },
    {
      skus: ['mhuq4h1vf3'],
      sizes: ['s', 'm', 'l'],
      stock: 100,
    },
    {
      skus: ['mhuq4h1vf4'],
      sizes: ['s', 'm', 'l'],
      stock: 100,
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
