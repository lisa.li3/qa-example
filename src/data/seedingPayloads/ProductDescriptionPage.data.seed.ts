/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';
import { IReviewSeeder } from '../../support/types/IReview';
import { getSalt } from '../../support/wdioHelpers/seedValueSalt';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let review: IReviewSeeder[] | undefined;

  const XForYAmountPromoId = `promo-pdp-3-for-amount-${getSalt() + 1}`;
  const XForYPromoId = `promo-pdp-3-for-2-${getSalt() + 2}`;

  const family = [
    {
      id: 'hover0col1',
      name: 'red thing',
      description: 'red thing desc',
      image: 'https://via.placeholder.com/150/FF0000',
    },
    {
      id: 'hover0col2',
      name: 'blue thing',
      description: 'blue thing desc',
      image: 'https://via.placeholder.com/150/0000FF',
    },
  ];

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      staffDiscountRate: 50,
      attributes: {
        id: 'AT-PDP',
        emailAddress: 'pdp.staffdiscount@project.local',
        password: 'its-a-secret',
        firstName: 'User',
        lastName: 'WithDiscount',
        gender: 'male',
        dateOfBirth: '1900-01-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: { categoryId: 'B2C2-441', path: '/main/framework/B2C2-441' },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `modal444n5`,
      sizes: ['select size', 'size_modal_1', 'size_modal_2'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `vyf81i5s8e`,
      attributes: {
        name: 'ProductName001',
        style: 'BrandName',
        defaultCategoryId: 'B2C2-441',
        description:
          '<p>This is my description text and it needs to be very long so that the text in the paragraph gets truncated at the point at which it has gone beyond the required length to be truncated so I have typed a very long string here</p>>',
      },
    },
    {
      id: `oos168oos1o`,
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: 'hover0col1',
      sizes: ['1size'],
      attributes: {
        name: 'Poolside Pique Polo Shirt',
        gender: 'Mens',
        defaultCategoryId: 'B2C2-441',
        family,
        model: {
          wears: 'XXXL',
          height: "17'10",
        },
      },
    },
    {
      id: 'hover0col2',
      sizes: ['1size'],
      attributes: {
        name: 'Second product name',
        gender: 'Womens',
        defaultCategoryId: 'B2C2-441',
        family,
      },
    },
    {
      id: `onesize443`,
      sizes: ['1size'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `longsiz443`,
      sizes: ['WAIST:32 (81CM) LENGTH:32 (81CM)', 'WAIST:32 (81CM) LENGTH:34 (86CM)'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `shortsz443`,
      sizes: ['S', 'M'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },

    {
      id: `intsize443`,
      sizes: ['6', '7'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `splitsz443`,
      sizes: ['S/M', 'M/L'],
      attributes: {
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `trust643no`,
      sizes: ['1size'],
      attributes: {
        name: 'trust643no',
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `trust643r0`,
      sizes: ['1size'],
      attributes: {
        name: 'trust643r0',
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `trust643l6`,
      sizes: ['1size'],
      attributes: {
        name: 'trust643l6',
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `trust643o6`,
      sizes: ['1size'],
      attributes: {
        name: 'trust643o6',
        defaultCategoryId: 'B2C2-441',
      },
    },
    {
      id: `promo540o7`,
      sizes: ['1size'],
      attributes: {
        name: 'promo-xforyamount-discount',
        defaultCategoryId: 'B2C2-441',
        staffDiscountAvailable: true,
      },
      promotions: {
        product: {
          type: 'XForYAmountPromotion',
          status: 'active',
          id: XForYAmountPromoId,
          slug: '3 for £20',
          parameters: {
            valueX: 3,
            valueY: 20,
          },
        },
      },
    },
    {
      id: `promo540o8`,
      sizes: ['1size'],
      attributes: {
        name: 'promo-xfory-discount',
        defaultCategoryId: 'B2C2-441',
        staffDiscountAvailable: true,
      },
      promotions: {
        product: {
          type: 'XForYPromotion',
          status: 'active',
          id: XForYPromoId,
          slug: '3 for 2',
          parameters: {
            valueX: 3,
            valueY: 2,
          },
        },
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`modal444n5`],
      sizes: ['select size', 'size_modal_1'],
      stock: 10,
    },
    {
      skus: ['vyf81i5s8e', 'hover0col1', 'hover0col2'],
      sizes: ['1size'],
      stock: 30,
    },
    {
      skus: ['onesize443'],
      sizes: ['1size'],
      stock: 1,
    },
    {
      skus: ['longsiz443'],
      sizes: ['WAIST:32 (81CM) LENGTH:32 (81CM)'],
      stock: 1,
    },
    {
      skus: ['shortsz443'],
      sizes: ['S'],
      stock: 1,
    },
    {
      skus: ['intsize443'],
      sizes: ['6'],
      stock: 1,
    },
    {
      skus: ['splitsz443'],
      sizes: ['S/M'],
      stock: 1,
    },
    {
      skus: ['trust643no', `trust643r0`, `trust643l6`, `trust643o6`, 'promo540o7', `promo540o8`],
      sizes: ['1size'],
      stock: 1,
    },
  ];

  /**
   * Reviews
   */
  // eslint-disable-next-line prefer-const
  review = [
    {
      productQuantity: 1,
      reviewQuantity: 5,
      attributes: {
        productId: `trust643l6`,
        averageRating: {
          averageRating: 4.67,
          totalRatings: 5,
        },
        reviews: [
          {
            name: 'Test name',
            content: 'This is a review',
          },
        ],
      },
    },
    {
      productQuantity: 1,
      reviewQuantity: 32,
      attributes: {
        productId: `trust643o6`,
        averageRating: {
          averageRating: 4.67,
          totalRatings: 32,
        },
      },
    },
    {
      productQuantity: 1,
      attributes: {
        productId: `trust643r0`,
        averageRating: {
          averageRating: 0,
          totalRatings: 0,
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  review ? (seedEvents.review = review) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
