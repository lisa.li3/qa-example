/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import moment from 'moment';
import { getAsSeedUser } from '../../support/functions/definedCustomer';

export default async () => {
  const categoryId = 'test-abandoned-basket-products';
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  const timestamp = moment().unix();
  const path = `/main/framework/997_abandoned_products`;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [await getAsSeedUser()];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['1size'],
      attributes: {
        name: 'abandonedBasketProduct',
        defaultCategoryId: categoryId,
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [{ sizes: ['1size'], productQuantity: 1, stock: 1 }];

  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;
  return seedEvents;
};
