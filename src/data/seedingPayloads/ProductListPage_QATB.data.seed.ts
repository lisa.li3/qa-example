/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `b2c2_271_${timestamp}`;
  const path = `/main/framework/qatb_products`;
  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,

      attributes: {
        name: 'Product - 1 size option in stock',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      quantity: 1,

      attributes: {
        name: 'Product - 2 size option in stock',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      quantity: 1,
      sizes: ['SMALL', 'MEDIUM', 'LARGE'],
      attributes: {
        name: 'Product - 3 size options in stock',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      quantity: 1,
      sizes: ['8', '10', '12', '14', '16', '18', '20', '22', '24', '26', '28'],
      attributes: {
        name: 'Product - 11 size options',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      quantity: 1,
      sizes: ['1size'],
      attributes: {
        name: 'Product - 1SIZE',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    { sizes: ['8'], productQuantity: 1, stock: 1 },
    { sizes: ['8', '10', '12'], productQuantity: 1, stock: 1 },
    { sizes: ['SMALL', 'MEDIUM', 'LARGE'], productQuantity: 1, stock: 1 },
    {
      sizes: ['8', '10', '12', '14', '16', '18', '20', '22', '24', '26', '28'],
      productQuantity: 1,
      stock: 1,
    },
    { sizes: ['1size'], productQuantity: 1, stock: 1 },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
