/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11],
      quantity: 1,
      attributes: {
        name: 'SingleStockProductOne',
        defaultCategoryId: 'B2C2-122-1',
      },
    },
    {
      sizes: [12],
      quantity: 1,
      attributes: {
        name: 'SingleStockProductTwo',
        defaultCategoryId: 'B2C2-122-2',
      },
    },
    {
      sizes: [13],
      quantity: 1,
      attributes: {
        name: 'SingleStockProductThree',
        defaultCategoryId: 'B2C2-122-3',
      },
    },
    {
      sizes: [14],
      quantity: 0,
      attributes: {
        name: 'OutOfStockProductOne',
        defaultCategoryId: 'B2C2-122-4',
      },
    },
    {
      sizes: [15],
      quantity: 0,
      attributes: {
        name: 'OutOfStockProductTwo',
        defaultCategoryId: 'B2C2-122-5',
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
    {
      sizes: [12],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
    {
      sizes: [13],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
    {
      sizes: [14],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
    {
      sizes: [15],
      stock: 3,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];
  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
