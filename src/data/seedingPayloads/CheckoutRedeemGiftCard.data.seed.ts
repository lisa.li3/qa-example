/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import moment from 'moment';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: `redeemGiftCard${timestamp}Guest`,
        path: `/main/framework/redeemGiftCard`,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['guestSize8', 'guestSize10', 'guestSize12'],
      attributes: {
        style: 'redeemGiftCard',
        defaultCategoryId: `redeemGiftCard${timestamp}Guest`,
        name: 'redeemGiftCard',
        pricing: [
          {
            shopperGroupId: `site-uk-shoppergroup`,
            price: 19.96,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: ['guestSize8', 'guestSize10', 'guestSize12'],
      productQuantity: 1,
      fulfilmentCentres: [`fc-uk-1`],
      stock: 1,
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
