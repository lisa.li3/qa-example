/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `sort_by_price_${timestamp}`;
  const path = `/main/framework/sort-by-price`;
  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Medium Priced Product - 1',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 49.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Highest Priced Product',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 200,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Lowest Priced Product',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 5.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Medium Priced Product - 2',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 49.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      quantity: 1,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Medium Priced Product - 3',
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 39.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
    {
      stock: 2,
      sizes: ['8'],
      fulfilmentCentres: [`fc-uk-1`],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
