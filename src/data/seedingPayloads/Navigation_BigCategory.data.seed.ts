import { IInventorySeeder } from '../../support/types/IInventory';
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { times, padStart } from 'lodash';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let content: IContentSeeder[] | undefined;
  let user: IUser[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = times(50, (index) => {
    const numberInList = `${index + 1}`;
    return {
      type: 'category',
      attributes: {
        categoryId: `big-category-item-${padStart(numberInList, 3, '0')}`,
        path: `/main/big-category/big-category-item-${padStart(numberInList, 3, '0')}`,
      },
    };
  });

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
