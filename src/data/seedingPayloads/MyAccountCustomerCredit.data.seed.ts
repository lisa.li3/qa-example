/**
 * Perform Data Seeding for the given test case
 */
import IPaymentSeeder from '../../support/types/IPayment';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  let payment: IPaymentSeeder[] | undefined;
  let user: IUser[] | undefined;

  const EndDate = '2050-06-02T05:06:46.326Z';

  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CustomerCredit',
        emailAddress: 'customercredit@project.com',
        password: 'its-a-secret',
      },
    },
    {
      attributes: {
        id: 'AT-CustomerCreditNoCredit',
        emailAddress: 'customercreditNoCredit@project.com',
        password: 'its-a-secret',
      },
    },
  ];

  const startDate = '2019-08-01T18:53:13.674Z';
  // eslint-disable-next-line prefer-const
  payment = [
    {
      attributes: {
        emailAddress: 'customercredit@project.com',
        credits: [
          {
            disabled: false,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 2000,
            name: 'Active',
            remainingValue: 2000,
            startDate: startDate,
            transactions: [],
          },
          {
            disabled: false,
            endDate: '2020-08-01T18:53:13.674Z',
            id: 'fzpx04az58',
            initialValue: 100,
            name: 'Expired',
            remainingValue: 100,
            startDate: startDate,
            transactions: [],
          },
          {
            disabled: true,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 200,
            name: 'Disabled',
            remainingValue: 200,
            startDate: startDate,
            transactions: [],
          },
          {
            id: 'pending-credit',
            disabled: false,
            endDate: '2050-06-02T05:06:46.326Z',
            initialValue: 100,
            name: 'pending-credit',
            remainingValue: 100,
            startDate: '2049-08-01T18:53:13.674Z',
            transactions: [],
          },
        ],
      },
    },
  ];
  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  payment ? (seedEvents.payment = payment) : null;

  return seedEvents;
};
