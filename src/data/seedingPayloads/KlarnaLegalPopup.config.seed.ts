/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.site('site-uk', 'b2c21877-1', 'b2c21877-1', {
      helpline: '** TEST 1 WORKING **',
      paymentTermsLink: [
        {
          id: 'klarna',
          name: 'KlarnaTestName',
          url: 'main/framework/test-sana-content-page',
        },
      ],
      shopperGroupId: 'b2c21877-1-shoppergroup',
    }),
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'b2c21877-1-shoppergroup', {})
  );
};
