/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `site-814fr-shoppergroup`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'FRA', name: 'France' },
      ],
    }),
    GenerateConfig.site('site-uk', 'georedirect', 'site-814fr', {
      shopperGroupId: 'site-814fr-shoppergroup',
      defaultCountryCode: 'FRA',
      defaultLocale: 'fr-FR',
      pimLocale: 'fr_FR',
      authoritativeCountries: ['FRA', 'GBR'],
    })
  );
};
