/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';
import moment from 'moment';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `staffdiscountproducts_${timestamp}`;
  const path = `/main/framework/b2c2_537_basket_staffdiscount`;
  const XPercentOffProductPromotion = `xpercentoff-${timestamp}`;
  const shopperGroupId = 'staffdiscount-basket-shoppergroup';

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      staffDiscountRate: 50,
      attributes: {
        id: 'at-basket-displaystaffdiscount',
        emailAddress: 'basketuser.displaystaffdiscount@project.local',
        password: 'its-a-secret',
        firstName: 'StaffUser',
        lastName: 'WithDiscount',
        gender: 'female',
        dateOfBirth: '1999-01-01',
      },
    },
    {
      attributes: {
        id: 'at-basket-notstaffnodiscount',
        emailAddress: 'basketuser.donotdisplaystaffdiscount@project.local',
        password: 'its-a-secret',
        firstName: 'NonStaffUser',
        lastName: 'WithoutStaffDiscount',
        gender: 'male',
        dateOfBirth: '2000-02-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `bsk00666p1`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 1 - Staff discount set to true',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 222,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `bsk00666p2`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: true,
        name: 'Product 2 - Markdown with Staff discount set to true',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 100,
            currency: 'GBP',
          },
        ],
      },
      promotions: {
        markdown: {
          type: 'XPercentOffProductPromotion',
          status: 'active',
          id: XPercentOffProductPromotion,
          slug: '75% off',
          parameters: {
            valueX: 75,
          },
        },
      },
    },
    {
      id: `bsk00666p3`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        staffDiscountAvailable: false,
        name: 'Product 3 - Does not qualify',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 12,
            currency: 'GBP',
          },
        ],
      },
    },
  ];
  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`bsk00666p1`, `bsk00666p2`, `bsk00666p3`],
      sizes: ['8'],
      stock: 10,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
