/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `search0001`,
      sizes: ['1size'],
    },
    {
      id: `search0002`,
      sizes: ['1size'],
    },
    {
      id: `search0003`,
      sizes: ['1size'],
    },
    {
      id: `search0004`,
      sizes: ['1size'],
    },
    {
      id: `search0005`,
      sizes: ['1size'],
    },
    {
      id: `search0006`,
      sizes: ['1size'],
    },
    {
      id: `search0007`,
      sizes: ['1size'],
    },
    {
      id: `search0008`,
      sizes: ['1size'],
    },
    {
      id: `search0009`,
      sizes: ['1size'],
    },
    {
      id: `search0010`,
      sizes: ['1size'],
    },
    {
      id: `search0011`,
      sizes: ['1size'],
    },
    {
      id: `search0012`,
      sizes: ['1size'],
    },
    {
      id: `search0013`,
      sizes: ['1size'],
    },
    {
      id: `search0014`,
      sizes: ['1size'],
    },
    {
      id: `search0015`,
      sizes: ['1size'],
      attributes: {
        name: 'FifteenthSearchProduct',
      },
    },
    {
      id: `search0016`,
      sizes: ['1size'],
    },
    {
      id: `search0017`,
      sizes: ['1size'],
    },
    {
      id: `search0018`,
      sizes: ['1size'],
    },
    {
      id: `search0019`,
      sizes: ['1size'],
    },
    {
      id: `search0020`,
      sizes: ['1size'],
    },
    {
      id: `search0021`,
      sizes: ['1size'],
    },
    {
      id: `search0022`,
      sizes: ['1size'],
    },
    {
      id: `search0023`,
      sizes: ['1size'],
    },
    {
      id: `search0024`,
      sizes: ['1size'],
    },
    {
      id: `search0025`,
      sizes: ['1size'],
    },
    {
      id: `search0026`,
      sizes: ['1size'],
    },
    {
      id: `search0027`,
      sizes: ['1size'],
    },
    {
      id: `search0028`,
      sizes: ['1size'],
    },
    {
      id: `search0029`,
      sizes: ['1size'],
    },
    {
      id: `search0030`,
      sizes: ['1size'],
    },
    {
      id: `search0031`,
      sizes: ['1size'],
    },
    {
      id: `search0032`,
      sizes: ['1size'],
    },
    {
      id: `search0033`,
      sizes: ['1size'],
    },
    {
      id: `search0034`,
      sizes: ['1size'],
    },
    {
      id: `search0035`,
      sizes: ['1size'],
    },
    {
      id: `search0036`,
      sizes: ['1size'],
    },
    {
      id: `search0037`,
      sizes: ['1size'],
    },
    {
      id: `search0038`,
      sizes: ['1size'],
    },
    {
      id: `search0039`,
      sizes: ['1size'],
    },
    {
      id: `search0040`,
      sizes: ['1size'],
    },
    {
      id: `search0041`,
      sizes: ['1size'],
    },
    {
      id: `search0042`,
      sizes: ['1size'],
    },
    {
      id: `search0043`,
      sizes: ['1size'],
    },
    {
      id: `search0044`,
      sizes: ['1size'],
    },
    {
      id: `search0045`,
      sizes: ['1size'],
    },
    {
      id: `search0046`,
      sizes: ['1size'],
    },
    {
      id: `search0047`,
      sizes: ['1size'],
    },
    {
      id: `search0048`,
      sizes: ['1size'],
    },
    {
      id: `search0049`,
      sizes: ['1size'],
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [
        `search0001`,
        `search0002`,
        `search0003`,
        `search0004`,
        `search0005`,
        `search0006`,
        `search0007`,
        `search0008`,
        `search0009`,
        `search0010`,
        `search0011`,
        `search0012`,
        `search0013`,
        `search0014`,
        `search0015`,
        `search0016`,
        `search0017`,
        `search0018`,
        `search0019`,
        `search0020`,
        `search0021`,
        `search0022`,
        `search0023`,
        `search0024`,
        `search0025`,
        `search0026`,
        `search0027`,
        `search0028`,
        `search0029`,
        `search0030`,
        `search0031`,
        `search0032`,
        `search0033`,
        `search0034`,
        `search0035`,
        `search0036`,
        `search0037`,
        `search0038`,
        `search0039`,
        `search0040`,
        `search0041`,
        `search0042`,
        `search0043`,
        `search0044`,
        `search0045`,
        `search0046`,
        `search0047`,
        `search0048`,
        `search0049`,
      ],
      sizes: ['1size'],
      stock: 1,
    },
  ];

  const seedEvents: ISeedEvent = {};
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
