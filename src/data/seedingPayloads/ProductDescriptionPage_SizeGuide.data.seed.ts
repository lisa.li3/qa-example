/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const pathwithsizeguides = `/main/framework/size-guides`;
  const pathwithnosizeguides = `/main/framework/no-size-guides`;
  const shopperGroupId = 'sizeguide-shoppergroup';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: 'size-guides',
        path: pathwithsizeguides,
      },
    },
    {
      type: 'category',
      attributes: {
        categoryId: 'no-size-guides',
        path: pathwithnosizeguides,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `szguide0p1`,
      sizes: [
        'Waist: 28" (71.12cm)  Leg: 30" (76.2cm)',
        'Waist: 30" (76.2cm)  Leg: 30" (76.2cm)',
        'Waist: 32" (81.28cm)  Leg: 30" (76.2cm)',
        'Waist: 34" (86.36cm)  Leg: 30" (76.2cm)',
        'Waist: 36" (91.44cm)  Leg: 30" (76.2cm)',
        'Waist: 38" (96.52cm)  Leg: 30" (76.2cm)',
        'Waist: 28" (71.12cm)  Leg: 32" (81.28cm)',
        'Waist: 30" (76.2cm)  Leg: 32" (81.28cm)',
      ],
      attributes: {
        defaultCategoryId: 'size-guides',
        name: 'Product with Size Guide',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 29.99,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `noguide0p2`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: 'no-size-guides',
        name: 'Product with no Size Guide',
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 12.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];
  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['szguide0p1'],
      sizes: [
        'Waist: 28" (71.12cm)  Leg: 30" (76.2cm)',
        'Waist: 30" (76.2cm)  Leg: 30" (76.2cm)',
        'Waist: 32" (81.28cm)  Leg: 30" (76.2cm)',
        'Waist: 34" (86.36cm)  Leg: 30" (76.2cm)',
        'Waist: 36" (91.44cm)  Leg: 30" (76.2cm)',
        'Waist: 38" (96.52cm)  Leg: 30" (76.2cm)',
        'Waist: 28" (71.12cm)  Leg: 32" (81.28cm)',
        'Waist: 30" (76.2cm)  Leg: 32" (81.28cm)',
      ],
      stock: 10,
    },
    {
      skus: ['noguide0p2'],
      sizes: ['8'],
      stock: 10,
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
