/**
 * Perform Data Seeding for the given test case
 */
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let content: IContentSeeder[] | undefined;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'sana',
      attributes: {
        path: '/footer/sana/klarna',
        sanaId: '48',
        head: {
          title: 'Klarna AT Page',
          meta: [
            {
              content: 'content',
              name: 'name',
              property: 'property',
            },
          ],
        },
      },
    },
    {
      type: 'sana',
      attributes: {
        path: '/footer/sana/invictus-games',
        sanaId: '41',
        head: {
          title: 'Invictus Games AT Page',
          meta: [
            {
              content: 'content',
              name: 'name',
              property: 'property',
            },
          ],
        },
      },
    },
    {
      type: 'sana',
      attributes: {
        path: '/footer/sana/clearpay',
        sanaId: '86',
        head: {
          title: 'Clearpay AT Page',
          meta: [
            {
              content: 'content',
              name: 'name',
              property: 'property',
            },
          ],
        },
      },
    },
    {
      type: 'sana',
      attributes: {
        path: '/footer/sana/yext-concepts',
        sanaId: '83',
        head: {
          title: 'Yext Concepts AT Page',
          meta: [
            {
              content: 'content',
              name: 'name',
              property: 'property',
            },
          ],
        },
      },
    },
    {
      type: 'static',
      attributes: {
        path: '/footer/framework/seeded-static',
        body: 'Seeded test body',
        head: {
          title: 'Great Marketing Page',
          meta: [
            {
              content: 'content',
              name: 'name',
              property: 'property',
            },
            {
              name: 'description',
              content: 'Seeded description',
            },
          ],
        },
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;

  return seedEvents;
};
