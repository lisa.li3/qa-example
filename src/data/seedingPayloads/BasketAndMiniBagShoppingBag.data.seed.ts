/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  const categoryId = 'test-basketandminibagShop';
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const JPG_URL =
    'https://image1.project.com/static/images/optimised/upload9223368955665543436.jpg';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId,
        path: `/main/category-mens-uk/category-mens-hoodies-uk/${categoryId}`,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: 'PID123SHOP',
      sizes: ['1size'],
      attributes: {
        name: 'Awesome Blue Hoodie',
        media: [JPG_URL],
        mainImage: JPG_URL,
        defaultCategoryId: categoryId,
        colour: {
          id: 'Blue123',
          name: 'Persian Blue',
          description: 'Deep blue inspired by lapis lazuli',
        },
        family: [
          {
            id: 'Blue123',
            name: 'Persian Blue',
            image: JPG_URL,
            description: 'Deep blue inspired by lapis lazuli',
          },
        ],
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 29.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['PID123SHOP'],
      sizes: ['1size'],
      stock: 3,
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
