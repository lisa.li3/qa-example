/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  (product = [
    {
      sizes: [10, 11],
      quantity: 2,
      attributes: {
        name: 'split delivery product',
        pricing: [
          {
            shopperGroupId: `site-uk-shoppergroup`,
            price: 99.99,
            currency: 'GBP',
          },
        ],
        colour: {
          id: 'a11122',
          name: 'Product 1 colour',
          description: 'Product 1 colour',
        },
      },
    },
  ]),
    /**
     * Inventory
     */
    // eslint-disable-next-line prefer-const
    (inventory = [
      {
        sizes: [11],
        stock: 1,
        fulfilmentCentres: ['fc-split-1'],
      },
      {
        sizes: [11],
        stock: 1,
        fulfilmentCentres: ['fc-split-2'],
      },
    ]);
  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
