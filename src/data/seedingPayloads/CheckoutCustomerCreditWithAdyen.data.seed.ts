/**
 * Perform Data Seeding for the given test case
 */
import IPaymentSeeder from '../../support/types/IPayment';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import { IProductSeeder } from '../../support/types/IProduct';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  let payment: IPaymentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  let user: IUser[] | undefined;

  const EndDate = '2050-06-02T05:06:46.326Z';

  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CustomerCreditAdyenPass',
        firstName: 'Authorised',
        emailAddress: 'customercreditAdyenPass@project.com',
        password: 'its-a-secret',
        addresses: [
          {
            id: '2d2e6',
            firstName: 'Authorised',
            lastName: 'Tester',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BB',
            isDefault: true,
            nickname: 'Home',
          },
        ],
      },
    },
    {
      attributes: {
        id: 'AT-CustomerCreditAdyenError',
        firstName: 'Error',
        emailAddress: 'customercreditAdyenError@project.com',
        password: 'its-a-secret',
        addresses: [
          {
            id: '2d2e6',
            firstName: 'Error',
            lastName: 'Tester',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'London',
            country: 'GBR',
            postcode: 'W11 1BB',
            isDefault: true,
            nickname: 'Home',
          },
        ],
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11],
      attributes: {
        name: 'CheckoutProduct',
        defaultCategoryId: `B2C2-122`,
        pricing: [
          {
            shopperGroupId: 'customercreditadyen-shoppergroup',
            price: 22.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11],
      stock: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const startDate = '2019-08-01T18:53:13.674Z';
  // eslint-disable-next-line prefer-const
  payment = [
    {
      attributes: {
        emailAddress: 'customercreditadyenpass@project.com',
        credits: [
          {
            disabled: false,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 10,
            name: 'Active',
            remainingValue: 10,
            startDate: startDate,
            transactions: [],
          },
        ],
      },
    },
    {
      attributes: {
        emailAddress: 'customercreditadyenerror@project.com',
        credits: [
          {
            disabled: false,
            endDate: EndDate,
            id: 'fzpx04az58',
            initialValue: 10,
            name: 'Active',
            remainingValue: 10,
            startDate: startDate,
            transactions: [],
          },
        ],
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  payment ? (seedEvents.payment = payment) : null;

  return seedEvents;
};
