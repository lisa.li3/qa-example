/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import moment from 'moment';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `gtm_${timestamp}`;
  const path = `/main/framework/data_for_gtm`;
  const shopperGroupId = 'gtm-on-shoppergroup';

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-GTMUSER',
        emailAddress: 'gtmtestuser@project.local',
        password: 'its-a-secret',
        firstName: 'GTM',
        lastName: 'USER',
        gender: 'male',
        dateOfBirth: '1950-01-01',
      },
    },
  ];

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `gtm00999a1`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        name: 'Product1',
        type: 'CASUAL TOP',
        gender: 'womens',
        colour: {
          id: 'gtm00999a1',
          name: 'Dark Green',
          description: 'Deep green inspired by lapis lazuli',
        },
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 10,
            currency: 'GBP',
          },
        ],
      },
    },
    {
      id: `gtm00999a2`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        name: 'Product2',
        type: 'CASUAL TOP',
        gender: 'womens',
        colour: {
          id: 'gtm00999a2',
          name: 'Dark Blue',
          description: 'Deep blue inspired by lapis lazuli',
        },
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 15,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  const productwithRecommendations = [
    ...product,
    {
      id: `gtm00999a3`,
      sizes: ['8'],
      attributes: {
        defaultCategoryId: categoryId,
        name: 'Product3',
        type: 'DENIM TOP',
        gender: 'mens',
        colour: {
          id: 'gtm00999a3',
          name: 'Dark Red',
          description: 'Deep red inspired by lapis lazuli',
        },
        pricing: [
          {
            shopperGroupId: shopperGroupId,
            price: 20,
            currency: 'GBP',
          },
        ],
      },
      mockMetadata: {
        recommendations: [
          {
            type: 'WIW',
            fallback: false,
            productIds: [`gtm00999a1`],
          },
          {
            type: 'YMAL',
            fallback: false,
            productIds: [`gtm00999a2`],
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: [`gtm00999a1`, `gtm00999a2`, 'gtm00999a3'],
      sizes: ['8'],
      stock: 10,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];
  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = productwithRecommendations) : null;
  inventory ? (seedEvents.inventory = inventory) : null;

  return seedEvents;
};
