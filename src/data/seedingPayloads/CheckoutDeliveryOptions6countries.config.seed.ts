/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const FC_DELIVERY_OPTIONS = 'fc-delivery-options';
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `delivery-options-shoppergroup`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'DEU', name: 'Germany' },
        { code: 'FRA', name: 'France' },
        { code: 'ITA', name: 'Italy' },
        { code: 'POL', name: 'Poland' },
        { code: 'AUT', name: 'Austria' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_DELIVERY_OPTIONS, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'site-delivery-options', '6countries', {
      shopperGroupId: 'delivery-options-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: FC_DELIVERY_OPTIONS,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'UK3',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK2',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              DEU: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              FRA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              ITA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              POL: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              AUT: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              BGR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              HRV: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              CYP: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              CZE: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              EST: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              DNK: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              FIN: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              GRC: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              HUN: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              LVA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              LTU: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              LUX: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              MLT: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              NLD: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
              ROU: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK1',
          fulfilmentCentres: ['fc-delivery-options'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK1', {
      name: 'UK delivery1',
      message: 'Delivery in 2 days',
      description: 'UK delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK2', {
      name: 'UK delivery2',
      message: 'Delivery in 5 days',
      description: 'UK delivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK3', {
      name: 'UK delivery3',
      message: 'Delivery in 3 days',
      description: 'UK delivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    })
  );
};
