/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-CheckoutDeliveryAddress-UnsupportedAll',
        emailAddress: 'checkoutdeliveryaddress@unsupported.address',
        password: 'its-a-secret',
        firstName: 'Test',
        lastName: 'delivery',
        gender: 'male',
        dateOfBirth: '1950-01-01',
        addresses: [
          {
            id: 'ITAaddress',
            firstName: 'Foo',
            lastName: 'Bar',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'county',
            country: 'ITA',
            postcode: '111111',
            isDefault: true,
            nickname: 'Home',
          },
          {
            id: 'FRAaddress',
            firstName: 'Foo',
            lastName: 'Bar',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'county',
            country: 'FRA',
            postcode: '1233321',
            isDefault: true,
            nickname: 'Home',
          },
          {
            id: 'POLaddress',
            firstName: 'Foo',
            lastName: 'Bar',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 1',
            addressLine2: 'Bar Road',
            city: 'Foobar',
            county: 'county',
            country: 'POL',
            postcode: '85535',
            isDefault: true,
            nickname: 'Home',
          },
        ],
      },
    },
  ];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 1,
      attributes: {
        pricing: [
          {
            shopperGroupId: 'site-191-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [10],
      stock: 1,
      fulfilmentCentres: ['fc-191'],
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
