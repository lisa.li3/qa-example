/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import moment from 'moment';
import { getAsSeedUser } from '../../support/functions/definedCustomer';

export default async () => {
  const categoryId = 'test-basketandminibagMiniabandoned';
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;
  const timestamp = moment().unix();
  const path = `/main/framework/abandoned_in_stock_products`;
  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];
  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [await getAsSeedUser()];
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: ['2size'],
      id: 'abandonedProdOne',
      attributes: {
        name: 'abandonedProdOne',
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
    {
      sizes: ['3size'],
      id: 'abandonedProdTwo',
      attributes: {
        name: 'abandonedProdTwo',
        pricing: [
          {
            shopperGroupId: 'site-uk-shoppergroup',
            price: 99.99,
            currency: 'GBP',
          },
        ],
        defaultCategoryId: categoryId,
      },
    },
  ];
  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      skus: ['abandonedProdOne'],
      sizes: ['2size'],
      stock: 1,
    },
    {
      skus: ['abandonedProdTwo'],
      sizes: ['3size'],
      stock: 1,
    },
  ];
  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
