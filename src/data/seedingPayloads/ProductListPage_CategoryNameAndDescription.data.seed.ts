/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */

  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;

  const categoryId = `category_name_and_description`;
  const subCategoryId = 'sub-category';

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        body: `
                <h1>Category Heading</h1>
                <p>
                  <span>This is the category description.</span> 
                  The following is a link : 
                  <a href="/main/framework/category_name_and_description/sub-category">sub category</a> 
                  <span> .This is a continuation of the text in the category description. The description can take multiple lines of text and some text should not appear in the mobile view until the button is pressed. Expanded text here</span></p>
              `,
        path: '/main/framework/category_name_and_description',
      },
    },
    {
      type: 'category',
      attributes: {
        categoryId: subCategoryId,
        body: `
                <h1>Sub Category Heading</h1>
              `,
        path: '/main/framework/category_name_and_description/sub-category',
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      attributes: { defaultCategoryId: categoryId }, // optional, but required if you wish to navigate to the product via a category
    },
    {
      attributes: { defaultCategoryId: subCategoryId, name: 'sub-category' }, // optional, but required if you wish to navigate to the product via a category
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;

  return seedEvents;
};
