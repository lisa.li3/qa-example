/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IUser from '../../support/types/IUser';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const WISHLIST_ID = 'wishlist2772-ONESIZE';

  /**
   * User
   */
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: 'AT-MyAccount',
        emailAddress: 'myaccount@project.local',
        password: 'its-a-secret',
        firstName: 'Bob',
        lastName: 'Dylan',
        gender: 'male',
        dateOfBirth: '1905-01-01',
        addresses: [
          {
            id: '11111111',
            firstName: 'Name',
            lastName: 'First',
            contactNumber: {
              phone: '2342342342234',
              mobile: '123124123123',
            },
            addressLine1: 'Flat 4, Copper court',
            addressLine2: '23 Kings Road',
            city: 'London',
            county: 'London',
            country: 'GBR',
            postcode: 'W14 9YA',
            isDefault: true,
            nickname: 'Home',
          },
          {
            id: '22222222',
            firstName: 'Name',
            lastName: 'Second',
            contactNumber: {
              phone: '1342342342234',
              mobile: '723124123123',
            },
            addressLine1: 'Flat 3, Copper court',
            addressLine2: '22 Kings Road',
            city: 'London',
            county: 'London',
            country: 'GBR',
            postcode: 'W14 9YA',
            isDefault: false,
            nickname: '2nd Address',
          },
        ],
      },
    },
  ];

  // eslint-disable-next-line prefer-const
  product = [
    {
      id: `wishlist2772`,
      sizes: ['ONESIZE'],
      attributes: {
        availableSizes: [
          {
            name: 'ONESIZE',
            id: WISHLIST_ID,
            ean: WISHLIST_ID,
            sku: WISHLIST_ID,
          },
        ],
        defaultCategoryId: 'B2C2-2772',
      },
    },
  ];

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
