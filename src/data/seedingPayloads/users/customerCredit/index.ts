import BaseUser, { IUser } from '../BaseUser';
import { getAsSeedUser } from '../../../../support/functions/definedCustomer';
import moment from 'moment';

const LargeCustomerCredit: IUser = {
  ...BaseUser,
  ...getAsSeedUser(),
};

const SmallCustomerCredit: IUser = {
  ...BaseUser,
  ...getAsSeedUser(),
};

export default {
  LargeCustomerCredit,
  SmallCustomerCredit,
};
