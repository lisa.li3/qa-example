export type UserGender = 'male' | 'female' | 'prefer_not_to_say';

export interface IUser {
  subscriptionApproval?: boolean;
  credit?: number;
  staffDiscountRate?: number;
  attributes?: {
    id: string;
    emailAddress: string;
    password: string;
    firstName?: string;
    lastName?: string;
    gender?: UserGender;
    dateOfBirth?: string;
    addresses?: {
      id?: string;
      firstName?: string;
      lastName?: string;
      contactNumber?: {
        phone?: string;
        mobile?: string;
      };
      addressLine1?: string;
      addressLine2?: string;
      city?: string;
      county?: string;
      country?: string;
      postcode?: string;
      isDefault?: boolean;
      nickname?: string;
    }[];
    credits?: {
      status: string;
      disabled: boolean;
      startDate: string;
      endDate: string;
      id: string;
      name: string;
      initialValue: number;
      remainingValue: number;
      transactions?: [];
    }[];
  };
}

const BaseUser: IUser = {};

export default BaseUser;
