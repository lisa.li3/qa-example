import BaseUser, { IUser } from '../BaseUser';
import { getAsSeedUser } from '../../../../support/functions/definedCustomer';

const User: IUser = {
  ...BaseUser,
  ...getAsSeedUser(),
};

const PayPal: IUser = {
  ...User,
  attributes: {
    ...User.attributes,
    id: 'paypal-user',
    emailAddress: 'rob.wi_1344499904_per@supergroup.co.uk',
    password: '344499834',
    firstName: 'Conor',
    lastName: "O'Potatoe",
    addresses: [
      {
        contactNumber: {
          phone: '01234 123 123',
          mobile: '01234 123 123',
        },
        addressLine1: '1 Main Terrace',
        city: 'Wolverhampton',
        postcode: 'W12 4LQ',
      },
    ],
  },
};

export default {
  PayPal,
};
