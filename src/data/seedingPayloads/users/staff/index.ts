import BaseUser, { IUser } from '../BaseUser';
import { getAsSeedUser } from '../../../../support/functions/definedCustomer';

const Staff: IUser = {
  ...BaseUser,
  ...getAsSeedUser(),
  staffDiscountRate: 50,
};

export default {
  Staff,
};
