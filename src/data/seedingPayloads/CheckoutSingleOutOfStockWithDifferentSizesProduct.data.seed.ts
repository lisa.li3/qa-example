/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';

export default async () => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      sizes: [11, 12],
      quantity: 1,
      attributes: {
        name: 'ProductWithTwoSizes',
        defaultCategoryId: `B2C2-170`,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    {
      sizes: [11, 12],
      stock: 0,
      productQuantity: 1,
      fulfilmentCentres: ['fc-uk-1'],
    },
  ];

  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
