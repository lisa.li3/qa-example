/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import moment from 'moment';

export default async () => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const timestamp = moment().unix();
  const categoryId = `b2c2_192_billingloqate_${timestamp}`;
  const path = `/main/framework/billingloqate-checkout`;

  /**
   * Content
   */
  // eslint-disable-next-line prefer-const
  content = [
    {
      type: 'category',
      attributes: {
        categoryId: categoryId,
        path: path,
      },
    },
  ];

  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  product = [
    {
      quantity: 3,
      attributes: {
        defaultCategoryId: categoryId,
      },
    },
  ];

  /**
   * Inventory
   */
  // eslint-disable-next-line prefer-const
  inventory = [
    { productQuantity: 3, sizes: [10], stock: 1, fulfilmentCentres: ['fc-delivery-options'] },
  ];

  const seedEvents: any = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
