/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', 'site-195-shoppergroup', {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'FRA', name: 'France' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-195', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FAST195', {
      name: 'Very fast delivery',
      message: 'Delivery in 2 days',
      description: 'Very fast delivery description',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'SLOW195', {
      name: 'Very slow delivery',
      message: 'Delivery in 5 days',
      description: 'Very slow delivery description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'STD195', {
      name: 'Standard delivery',
      message: 'Delivery in 3 days',
      description: 'Standard delivery description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRAFAST195', {
      name: 'FRAVery fast delivery',
      message: 'FRADelivery in 2 days',
      description: 'FRAVery fast delivery description',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRASLOW195', {
      name: 'FRAVery slow delivery',
      message: 'FRADelivery in 5 days',
      description: 'FRAVery slow delivery description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRASTD195', {
      name: 'FRAStandard delivery',
      message: 'FRADelivery in 3 days',
      description: 'FRAStandard delivery description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.site('site-uk', 'site-195', 'b2c2195', {
      shopperGroupId: 'site-195-shoppergroup',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-195',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'STD195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'SLOW195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FAST195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FRASLOW195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRASTD195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRAFAST195',
          fulfilmentCentres: ['fc-195'],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
