/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.site('site-uk', 'site-1707', 'b2c21707', {
      redeemGiftCardsEnabled: false,
      giftCardBillingCountries: [],
    })
  );
};
