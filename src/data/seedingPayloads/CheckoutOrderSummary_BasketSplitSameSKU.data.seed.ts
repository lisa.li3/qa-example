/**
 * Perform Data Seeding for the given test case
 */
import { IProductSeeder } from '../../support/types/IProduct';
import { IContentSeeder } from '../../support/types/IContent';
import { IInventorySeeder } from '../../support/types/IInventory';
import IUser from '../../support/types/IUser';
import { ISeedEvent } from '../../support/types/ISeedEvent';

export default async (): Promise<ISeedEvent> => {
  /**
   * Seed each part of the desired objects
   */
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;
  let product: IProductSeeder[] | undefined;
  let inventory: IInventorySeeder[] | undefined;

  const SHOPPER_GROUP_ID = 'site-uk-shoppergroup';
  /**
   * Product
   */
  // eslint-disable-next-line prefer-const
  (product = [
    {
      quantity: 4,
      sizes: [10],
      attributes: {
        name: 'Product 1',
        sku: `asdfgfcvhhg`,
        pricing: [
          {
            shopperGroupId: SHOPPER_GROUP_ID,
            price: 99.99,
            currency: 'GBP',
          },
        ],
      },
    },
  ]),
    /**
     * Inventory
     */
    // eslint-disable-next-line prefer-const
    (inventory = [
      {
        sizes: ['10'],
        stock: 0,
        attributes: {
          location: { 'fc-3343-1': 1, 'fc-3343-2': 3 },
        },
      },
    ]);

  const seedEvents: ISeedEvent = {};
  content ? (seedEvents.content = content) : null;
  product ? (seedEvents.product = product) : null;
  inventory ? (seedEvents.inventory = inventory) : null;
  user ? (seedEvents.user = user) : null;

  return seedEvents;
};
