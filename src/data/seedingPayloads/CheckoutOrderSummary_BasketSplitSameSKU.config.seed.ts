import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const DELIVERY_ON = 'Delivery on %s';
  return await browser.configureEnv(
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-3343-1', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-3343-2', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC1-1', {
      name: 'FC1 delivery 1',
      message: DELIVERY_ON,
      description: 'Fast delivery',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FC1-2', {
      name: 'FC1 delivery 2',
      message: DELIVERY_ON,
      description: 'Fast delivery',
      displayOrder: 2,
    }),

    GenerateConfig.site('site-uk', 'site-3343', 'b2c23343', {
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-3343-1',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: 'fc-3343-2',
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'FC1-1',
          fulfilmentCentres: ['fc-3343-1'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'FC1-2',
          fulfilmentCentres: ['fc-3343-2'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    })
  );
};
