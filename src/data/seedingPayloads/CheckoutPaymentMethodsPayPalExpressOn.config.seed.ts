/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `PayPalExpress-shoppergroup`, {}),
    GenerateConfig.site('site-uk', 'site-paypalexpresson', 'paypalexpresson', {
      shopperGroupId: 'PayPalExpress-shoppergroup',
      enablePayPalExpress: true,
    })
  );
};
