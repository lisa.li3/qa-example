/**
 * Perform Data Seeding for the given test case
 */
import { ISeedEvent } from '../../support/types/ISeedEvent';
import IOrderSeeder from '../../support/types/IOrder';
import IUser from '../../support/types/IUser';
import { IContentSeeder } from '../../support/types/IContent';

export default async (): Promise<ISeedEvent> => {
  let order: IOrderSeeder[] | undefined;
  let user: IUser[] | undefined;
  let content: IContentSeeder[] | undefined;

  const ID = 'at-myaccountorderhistorywithcompleteorder';
  // eslint-disable-next-line prefer-const
  user = [
    {
      attributes: {
        id: ID,
        emailAddress: 'orderhistorywithcompleteorders@project.com',
        password: 'its-a-secret',
        firstName: 'Order',
        lastName: 'History',
        gender: 'male',
        dateOfBirth: '1900-01-01',
      },
    },
  ];
  // eslint-disable-next-line prefer-const
  order = [
    {
      products: [{ price: 100 }],
      attributes: {
        id: `project_1206_1`,
        createdAt: '2021-01-15T00:01:00.000Z',
        status: 'Draft',
        userId: ID,
      },
    },
    {
      products: [
        {
          id: '0',
          image: 'https://image1.project.com/static/images/optimised/upload9223368955665763693.jpg',
          name: 'Vegan trainers',
          sizeName: '9',
          price: 25,
          quantity: 2,
          colour: {
            description: 'Refined fuchsia',
            id: 'foo',
            name: 'Refined fuchsia',
          },
        },
        {
          id: '1',
          image: 'https://image1.project.com/static/images/optimised/upload9223368955665422694.jpg',
          name: 'Glow in the dark shorts',
          sizeName: 'S',
          price: 50,
          colour: {
            description: 'Pure white',
            id: 'foo',
            name: 'Pure white',
          },
        },
      ],
      attributes: {
        id: `project_1206_2`,
        createdAt: '2021-01-15T00:02:00.000Z',
        status: 'Payment Pending',
        pricing: { totals: { shipping: 5, total: 105 } },
        userId: ID,
      },
    },
    {
      products: [{ price: 100 }],
      attributes: {
        id: `project_1206_3`,
        createdAt: '2021-01-15T00:03:00.000Z',
        status: 'Payment Authorised',
        userId: ID,
      },
    },
    {
      products: [{ price: 100 }],
      attributes: {
        id: `project_1206_4`,
        createdAt: '2021-01-15T00:04:00.000Z',
        status: 'Payment Error',
        userId: ID,
      },
    },
    {
      products: [{ price: 100 }],
      attributes: {
        id: `project_1206_5`,
        createdAt: '2021-01-15T00:05:00.000Z',
        status: 'Sent to OMS',
        userId: ID,
      },
    },
  ];
  const seedEvents: ISeedEvent = {};
  user ? (seedEvents.user = user) : null;
  content ? (seedEvents.content = content) : null;
  order ? (seedEvents.order = order) : null;

  return seedEvents;
};
