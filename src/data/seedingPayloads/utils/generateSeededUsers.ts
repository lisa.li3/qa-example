import { getState } from '../../../support/functions/state';
import { camelCase, tail, truncate } from 'lodash';
import { IUser } from '../users/BaseUser';

const getFeatureId = () => {
  const { scenario } = getState();
  if (!Object.keys(scenario).length) throw Error('No scenario set in state');
  const uri = scenario.uri.split('/');
  return uri[uri.length - 1].replace('.feature', '');
};

export default ({ users, file }: { users: Array<IUser>; file: string }): { user: Array<IUser> } => {
  const user: IUser[] = [];

  const featureId = getFeatureId();
  const state = getState();

  users.forEach((item, index) => {
    const idParts = [
      camelCase(state.siteConfig.siteId),
      camelCase(featureId),
      camelCase(tail(file.split('.')).join('.')),
      leftPad(index),
    ];
    const id = item?.attributes?.id || idParts.join('-');

    const formattedUser = {
      ...item,
      attributes: {
        ...item.attributes,
        id,
      },
    };

    console.log({ formattedUser });

    user.push(formattedUser);
  });

  return {
    user,
  };
};

const leftPad = (value): string => {
  const zeroes = new Array(3 + 1).join('0');
  return (zeroes + value).slice(-3);
};
