import { IProductSeeder } from '../../../support/types/IProduct';
import { IInventorySeeder } from '../../../support/types/IInventory';
import { getState } from '../../../support/functions/state';
import IPromotion from '../../../support/types/IPromotion';
import IPromotionVoucher from '../../../support/types/IPromotionVoucher';
import { camelCase, tail, truncate } from 'lodash';
import { IProduct } from '../products/BaseProduct';

export interface IProductBuilderSize {
  size: string;
  stock: number;
}

export interface IProductBuilder {
  name: string;
  style: string;
  media: string[];
  mainImage: string;
  sizes: Array<IProductBuilderSize>;
  pricing: { shopperGroupId?: string; price?: number; currency?: string };
  promotions?: { product?: IPromotion; markdown?: IPromotion; vouchers?: IPromotionVoucher[] };
}

const getFeatureId = () => {
  const { scenario } = getState();
  if (!Object.keys(scenario).length) throw Error('No scenario set in state');
  const uri = scenario.uri.split('/');
  return uri[uri.length - 1].replace('.feature', '');
};

export default ({
  products,
  file,
}: {
  products: Array<IProduct>;
  file: string;
}): { product: Array<IProductSeeder>; inventory: Array<IInventorySeeder> } => {
  const product: Array<IProductSeeder> = [];
  const inventory: Array<IInventorySeeder> = [];

  const featureId = getFeatureId();
  const state = getState();

  products.forEach((item, index) => {
    const idParts = [
      camelCase(state.siteConfig.siteId),
      camelCase(featureId),
      camelCase(tail(file.split('.')).join('.')),
      leftPad(index),
    ];
    const id = idParts.join('-');

    const formattedProduct: IProductSeeder = {
      sizes: item.sizes.map((size) => size.size),
      id: id,
      attributes: {
        name: item.name,
        style: item.style,
        media: item.media,
        mainImage: item.mainImage,
        pricing: [
          {
            shopperGroupId:
              item.pricing.shopperGroupId || `${state.siteConfig.siteId}-shoppergroup`,
            price: item.pricing.price || 9.99,
            currency: item.pricing.currency || 'GBP',
          },
        ],
        // colour: item.colour
        //   ? {
        //       id: camelCase(`${featureId}-${item.colour.name.toLowerCase()}`),
        //       name: item.colour?.name,
        //       description: item.colour?.description || item.colour.name,
        //     }
        //   : undefined,
        // family: item.colour
        //   ? [
        //       {
        //         skus: [camelCase(`${featureId}-${item.colour.name.toLowerCase()}`)],
        //         name: item.colour.name,
        //         description: item.colour.description || item.colour.name,
        //         id: camelCase(`${featureId}-${item.colour.name.toLowerCase()}`),
        //         image:
        //           'https://image1.project.com/static/images/optimised/upload9223368955665738482.jpg',
        //       },
        //     ]
        //   : undefined,
      },
      promotions: item.promotions,
    };

    const inventoryItem = {
      skus: [id],
      sizes: [],
      stock: 100,
    };

    item.sizes.forEach((size) => {
      if (!size.stock || size.stock === 100) {
        // using default stock
        inventoryItem.sizes.push(size.size);
      } else {
        inventory.push({
          skus: [id],
          sizes: [size.size],
          stock: size.stock,
        });
      }
    });

    if (inventoryItem.sizes.length) {
      inventory.push(inventoryItem);
    }

    product.push(formattedProduct);
  });

  console.log('product', product);
  console.log('inventory', inventory);

  return {
    product,
    inventory,
  };
};

const leftPad = (value): string => {
  const zeroes = new Array(3 + 1).join('0');
  return (zeroes + value).slice(-3);
};
