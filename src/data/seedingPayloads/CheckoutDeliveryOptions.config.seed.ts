/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async (): Promise<void> => {
  const FC_DELIVERY_OPTIONS = 'fc-delivery-options';
  const FC_DELIVERY_OPTIONS_NOSTORE = 'fc-delivery-options-noStore';
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `delivery-options-shoppergroup`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom', taxExcluded: false, taxRate: 20 },
        { code: 'FRA', name: 'France', taxExcluded: false, taxRate: 20 },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_DELIVERY_OPTIONS, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', FC_DELIVERY_OPTIONS_NOSTORE, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'site-134', 'b2c2134', {
      shopperGroupId: 'delivery-options-shoppergroup',
      helpline: '+44 1892-221343',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: FC_DELIVERY_OPTIONS,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
        {
          fulfilmentCentreId: FC_DELIVERY_OPTIONS_NOSTORE,
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'UK3',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS, FC_DELIVERY_OPTIONS_NOSTORE],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK2',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS, FC_DELIVERY_OPTIONS_NOSTORE],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'UK1',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS, FC_DELIVERY_OPTIONS_NOSTORE],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
        {
          id: 'STORE',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRA2',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 0,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRA3',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
        {
          id: 'FRA1',
          fulfilmentCentres: [FC_DELIVERY_OPTIONS],
          extensions: {
            enabled: true,
            destinationConfig: {
              FRA: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK1', {
      name: 'UK delivery1',
      message: 'Delivery in 2 days',
      description: 'UK delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK2', {
      name: 'UK delivery2',
      message: 'Delivery in 5 days',
      description: 'UK delivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK3', {
      name: 'UK delivery3',
      message: 'Delivery in 3 days',
      description: 'UK delivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    }),
    GenerateConfig.deliveryMethod('STORECOLL', 'STORE', {
      name: 'Store delivery',
      message: 'Delivery on %s',
      description: 'Standard delivery',
      type: 'ToStore',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRA1', {
      name: 'FRA delivery1',
      message: 'FRADelivery in 2 days',
      description: 'FRA delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRA2', {
      name: 'FRA delivery2',
      message: 'FRADelivery in 5 days',
      description: 'FRA delivery2 description',
      type: 'ToAddress',
      displayOrder: 2,
    }),
    GenerateConfig.deliveryMethod('INTND', 'FRA3', {
      name: 'FRA delivery3',
      message: 'FRADelivery in 3 days',
      description: 'FRA delivery3 description',
      type: 'ToAddress',
      displayOrder: 3,
    })
  );
};
