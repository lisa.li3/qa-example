/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async () => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `order-summary-shoppergroup`, {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom', taxExcluded: false, taxRate: 20 }],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', 'fc-order-summary', {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.site('site-uk', 'b2c23609', 'b2c23609', {
      shopperGroupId: 'order-summary-shoppergroup',
      helpline: '+44 1892-221343',
      fulfilmentCentres: [
        {
          fulfilmentCentreId: 'fc-order-summary',
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: 'UK1',
          fulfilmentCentres: ['fc-order-summary'],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 3.95,
                  transitTime: 1,
                },
              },
            },
          },
        },
      ],
    }),
    GenerateConfig.deliveryMethod('INTND', 'UK1', {
      name: 'UK delivery1',
      message: 'Delivery in 2 days',
      description: 'UK delivery1 description',
      type: 'ToAddress',
      displayOrder: 1,
    })
  );
};
