/**
 * Perform Data Seeding for the given test case
 */
import { GenerateConfig } from '@project/site-config-transformer/dist';

export const getSiteConfig = async ({ newSite }: { newSite: string }): Promise<void> => {
  return await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `${newSite}-shoppergroup`, {
      deliveryCountries: [{ code: 'GBR', name: 'United Kingdom' }],
    }),

    GenerateConfig.site('site-uk', newSite, newSite, {
      shopperGroupId: `${newSite}-shoppergroup`,
      authoritativeCountries: ['GBR'],
      redeemGiftCardsEnabled: true,
    })
  );
};
