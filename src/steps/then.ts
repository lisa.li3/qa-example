import { Then } from '@cucumber/cucumber';

import checkClass from '../support/check/checkClass';
import checkContainsAnyText from '../support/check/checkContainsAnyText';
import checkIsEmpty from '../support/check/checkIsEmpty';
import checkContainsText from '../support/check/checkContainsText';
import checkSiblingContainsText from '../support/check/checkSiblingContainsText';
import checkCookieContent from '../support/check/checkCookieContent';
import checkCookieExists from '../support/check/checkCookieExists';
import checkDimension from '../support/check/checkDimension';
import checkEqualsText from '../support/check/checkEqualsText';
import checkFocus from '../support/check/checkFocus';
import checkGTMLatestNamedEvent from '../support/check/checkGTMLatestNamedEvent';
import checkGTMLatestNamedEventWithArray from '../support/check/checkGTMLatestNamedEventWithArray';
import checkGTMLatestNamedEventWithSingleObject from '../support/check/checkGTMLatestNamedEventWithSingleObject';
import checkGTMLatestNamedEventWithNestedObject from '../support/check/checkGTMLatestNamedEventWithNestedObject';
import checkGTMDataLayer from '../support/check/checkGTMDataLayer';
import checkInURLPath from '../support/check/checkInURLPath';
import checkIsOpenedInNewWindow from '../support/check/checkIsOpenedInNewWindow';
import checkModal from '../support/check/checkModal';
import checkModalText from '../support/check/checkModalText';
import checkNewWindow from '../support/check/checkNewWindow';
import checkOffset from '../support/check/checkOffset';
import checkProperty from '../support/check/checkProperty';
import checkFontProperty from '../support/check/checkFontProperty';
import checkSelected from '../support/check/checkSelected';
import checkScriptsOnPage from '../support/check/checkScriptsOnPage';
import getElement from '../support/check/getElement';
import checkTitle from '../support/check/checkTitle';
import checkTitleContains from '../support/check/checkTitleContains';
import checkTitleBeginsWith from '../support/check/checkTitleBeginsWith';
import checkURL from '../support/check/checkURL';
import checkURLPath from '../support/check/checkURLPath';
import checkWithinViewport from '../support/check/checkWithinViewport';
import compareText from '../support/check/compareText';
import isEnabled from '../support/check/isEnabled';
import isExisting from '../support/check/isExisting';
import isVisible from '../support/check/isDisplayed';
import waitOnElement from '../support/action/waitFor';
import waitForVisible from '../support/action/waitForDisplayed';
import checkIfElementExists from '../support/lib/checkIfElementExists';
import checkMatchesOrdering from '../support/check/checkMatchesOrdering';
import checkFE02Attribute from '../support/check/checkFE02Attribute';
import checkElementsCount from '../support/check/checkElementsCount';
import checkElementLength from '../support/check/checkElementLength';
import takeScreenshot from '../support/action/takeScreenshot';
import waitForTextChange from '../support/check/waitForTextChange';

Then(/^I check the selector "([^"]*)?"$/, getElement);

Then(/^I expect that the title is( not)* "([^"]*)?"$/, checkTitle);

Then(/^I expect that the title( not)* contains "([^"]*)?"$/, checkTitleContains);

Then(
  /^I expect that the title (does not)*(?: begin|begins) with "([^"]*)?"$/,
  checkTitleBeginsWith
);

Then(
  /^I expect that element "([^"]*)?" does( not)* appear exactly "([^"]*)?" times$/,
  checkIfElementExists
);

Then(/^I expect that element "([^"]*)?" is( not)* displayed$/, isVisible);

Then(/^I expect that element "([^"]*)?" becomes( not)* displayed$/, waitForVisible);

Then(/^I expect that element "([^"]*)?" is( not)* within the viewport$/, checkWithinViewport);

Then(/^I expect that element "([^"]*)?" does( not)* exist$/, isExisting);

Then(
  /^I expect that element "([^"]*)?"( not)* contains the same text as element "([^"]*)?"$/,
  compareText
);

Then(
  /^I expect that (button|element|the \d+st element of|the \d+nd element of|the \d+rd element of|the \d+th element of) "([^"]*)?"( not)* matches the text "([^"]*)?"$/,
  async (elementType, selector, falseCase, expectedText) => {
    await checkEqualsText({
      elementType: elementType || 'element',
      selector,
      falseCase: falseCase || false,
      expectedText,
    });
  }
);

Then(
  /^I expect that (button|element|container) "([^"]*)?"( not)* contains the text "([^"]*)?"$/,
  checkContainsText
);

Then(/^I expect that (button|element) "([^"]*)?"( not)* contains any text$/, checkContainsAnyText);

Then(/^I expect that (button|element) "([^"]*)?" is( not)* empty$/, checkIsEmpty);

Then(/^I expect that the url is( not)* "([^"]*)?"$/, checkURL);

Then(/^I expect that the path is( not)* "([^"]*)?"$/, checkURLPath);

Then(/^I expect the url to( not)* contain "([^"]*)?"$/, checkInURLPath);

Then(
  /^I expect that the(?: length of the values returned for)?( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"$/,
  checkProperty
);

Then(
  /^I expect that the font( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"$/,
  checkFontProperty
);

Then(/^I expect that checkbox "([^"]*)?" is( not)* checked$/, checkSelected);

Then(/^I expect that element "([^"]*)?" is( not)* selected$/, checkSelected);

Then(
  /^I expect that (?:the )?((\d+)?(st |nd |rd |th )?)?element "([^"]*)?" is( not)* enabled$/,
  isEnabled
);

Then(/^I expect that cookie "([^"]*)?"( not)* contains "([^"]*)?"$/, checkCookieContent);

Then(/^I expect that cookie "([^"]*)?"( not)* exists$/, checkCookieExists);

Then(/^I expect that element "([^"]*)?" is( not)* ([\d]+)px (broad|tall)$/, checkDimension);

Then(
  /^I expect that element "([^"]*)?" is( not)* positioned at ([\d+.?\d*]+)px on the (x|y) axis$/,
  checkOffset
);

Then(/^I expect that element "([^"]*)?" (has|does not have) the class "([^"]*)?"$/, checkClass);

Then(/^I expect a new (window|tab) has( not)* been opened$/, checkNewWindow);

Then(/^I expect the url "([^"]*)?" is opened in a new (tab|window)$/, checkIsOpenedInNewWindow);

Then(/^I expect that element "([^"]*)?" is( not)* focused$/, checkFocus);

Then(
  /^I wait on element "([^"]*)?"(?: for (\d+)ms)*(?: to( not)* (be checked|be enabled|be selected|be displayed|contain a text|contain a value|exist))*$/,
  waitOnElement
);

Then(/^I expect that a (alertbox|confirmbox|prompt) is( not)* opened$/, checkModal);

Then(
  /^I expect that a (alertbox|confirmbox|prompt)( not)* contains the text "([^"]*)?"$/,
  checkModalText
);

Then(
  /^I expect that the (first|last)?( \d+ )?values returned by "([^"]*)?" to be in the following order$/,
  checkMatchesOrdering
);
Then(
  /^I expect the element in "([^"]*)" containing text "([^"]*)" to (also|not) contain text "([^"]*)"( and is displayed| and is not displayed)*$/,
  checkSiblingContainsText
);

Then(
  /^I expect the FE02 path "([^"]*)" has attribute "([^"]*)" with value "([^"]*)"$/,
  checkFE02Attribute
);

Then(
  /^I expect the count of elements in "([^"]*)" to (equal|be greater than|be less than|be greater than or equal to|be less than or equal to) (\d+)$/,
  checkElementsCount
);

Then(
  /^I expect the (text|value) length of element "([^"]*)" to (equal|be greater than|be less than|be greater than or equal to|be less than or equal to) (\d+)$/,
  checkElementLength
);

Then(/^I wait for text to change in "([^"]*)"$/, waitForTextChange);

Then(/^I take a screenshot$/, takeScreenshot);

Then(
  /^the latest "([^"]*)?" event in the GTM data layer data should contain the property "([^"]*)?" with the value "([^"]*)?"$/,
  checkGTMLatestNamedEvent
);

Then(
  /^the latest "([^"]*)?" GTM event should contain property "([^"]*)?" with an array where the ((\d+)?(st |nd |rd |th )?)?element contains property "([^"]*)?" with the value "([^"]*)?"$/,
  checkGTMLatestNamedEventWithArray
);

Then(
  /^the latest "([^"]*)?" GTM event should contain property "([^"]*)?" with an object that contains property "([^"]*)?" with the value "([^"]*)?"$/,
  checkGTMLatestNamedEventWithSingleObject
);

Then(
  /^the latest "([^"]*)?" GTM event with property "([^"]*)?" should have a child property "([^"]*)?" which contains an object with property "([^"]*)?" and value "([^"]*)?"$/,
  checkGTMLatestNamedEventWithNestedObject
);

Then(
  /^the ((\d+)?(st |nd |rd |th )?)?item in the GTM data layer should contain the property "([^"]*)?"( with the value "([^"]*)?")*$/,
  checkGTMDataLayer
);

Then(
  /^the page should( not)* contain a script in the (head|body) section where the source URL contains the text "([^"]*)?"$/,
  checkScriptsOnPage
);
