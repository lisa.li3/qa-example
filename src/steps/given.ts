import { Given } from '@cucumber/cucumber';

import checkContainsAnyText from '../support/check/checkContainsAnyText';
import checkIsEmpty from '../support/check/checkIsEmpty';
import checkContainsText from '../support/check/checkContainsText';
import checkCookieContent from '../support/check/checkCookieContent';
import checkCookieExists from '../support/check/checkCookieExists';
import checkDimension from '../support/check/checkDimension';
import checkElementExists from '../support/check/checkElementExists';
import checkEqualsText from '../support/check/checkEqualsText';
import checkModal from '../support/check/checkModal';
import checkOffset from '../support/check/checkOffset';
import checkProperty from '../support/check/checkProperty';
import checkSelected from '../support/check/checkSelected';
import checkTitle from '../support/check/checkTitle';
import checkUrl from '../support/check/checkURL';
import closeAllButFirstTab from '../support/action/closeAllButFirstTab';
import compareText from '../support/check/compareText';
import isEnabled from '../support/check/isEnabled';
import isDisplayed from '../support/check/isDisplayed';
import openWebsite from '../support/action/openWebsite';
import setWindowSize from '../support/action/setWindowSize';
import seedData from '../support/seeding/seedData';
import seedNewData from '../support/seeding/seedNewData';
import seedConfig from '../support/seeding/seedConfig';
import spawnSite from '../support/seeding/spawnSite';
import { setLocalStorage, deleteLocalStorage } from '../support/action/localStorage';
import addDataToS3 from '../support/seeding/addDataToS3';
import openPDP from '../support/action/openPDP';

Given(
  /^I (open|am on) the (url|site|page|siteId|domain) "([^"]*)?"( without waiting for page load)*$/,
  openWebsite
);

Given(/^I open the product detail page for "([^"]*)?"*$/, openPDP);

Given(/^the element "([^"]*)?" is( not)* displayed$/, isDisplayed);

Given(/^the ((\d+)?(st |nd |rd |th )?)?element "([^"]*)?" is( not)* enabled$/, isEnabled);

Given(/^the element "([^"]*)?" is( not)* selected$/, checkSelected);

Given(/^the checkbox "([^"]*)?" is( not)* checked$/, checkSelected);

Given(/^there is (an|no) element "([^"]*)?" on the page$/, checkElementExists);

Given(/^the title is( not)* "([^"]*)?"$/, checkTitle);

Given(/^the element "([^"]*)?" contains( not)* the same text as element "([^"]*)?"$/, compareText);

Given(/^the (button|element) "([^"]*)?"( not)* matches the text "([^"]*)?"$/, checkEqualsText);

Given(
  /^the (button|element|container) "([^"]*)?"( not)* contains the text "([^"]*)?"$/,
  checkContainsText
);

Given(/^the (button|element) "([^"]*)?"( not)* contains any text$/, checkContainsAnyText);

Given(/^the (button|element) "([^"]*)?" is( not)* empty$/, checkIsEmpty);

Given(/^the page url is( not)* "([^"]*)?"$/, checkUrl);

Given(
  /^the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"$/,
  checkProperty
);

Given(/^the cookie "([^"]*)?" contains( not)* the value "([^"]*)?"$/, checkCookieContent);

Given(/^the cookie "([^"]*)?" does( not)* exist$/, checkCookieExists);

Given(/^the element "([^"]*)?" is( not)* ([\d]+)px (broad|tall)$/, checkDimension);

Given(/^the element "([^"]*)?" is( not)* positioned at ([\d]+)px on the (x|y) axis$/, checkOffset);

Given(/^I have a screen that is ([\d]+) by ([\d]+) pixels$/, setWindowSize);

Given(/^I have closed all but the first (window|tab)$/, closeAllButFirstTab);

Given(/^a (alertbox|confirmbox|prompt) is( not)* opened$/, checkModal);

Given(
  /^I seed using the file "([^"]*)?"(?: and add( the items| one of each item| custom quantity) into my "([^"]*)?" local storage)*(?: with the locale "([^"]*)?")*(?: with the array "([^"]*)?")*$/,
  seedData
);

Given(/^I (seed|define) "([^"]*)?"*$/, seedNewData);

Given(
  /^I seed config using the file "([^"]*)?"(?: from the existing "([^"]*)?" site config)*( and I switch to the new site)*$/,
  seedConfig
);

Given(/^I spawn the site "([^"]*)?" from the base config "([^"]*)?"*$/, spawnSite);

Given(/^I delete the local storage( keys "([^"]*)?")*$/, deleteLocalStorage);

Given(/^I set the local storage key "([^"]*)?" with the content "([^"]*)?"$/, setLocalStorage);

Given(/^I upload the "([^"]*)" file to the S3 bucket "([^"]*)" with path "([^"]*)"$/, addDataToS3);
