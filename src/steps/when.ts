import { When } from '@cucumber/cucumber';

import browserBack from '../support/action/browserBack';
import clearInputField from '../support/action/clearInputField';
import clickElement from '../support/action/clickElement';
import clickAllElements from '../support/action/clickAllElements';
import closeLastOpenedWindow from '../support/action/closeLastOpenedWindow';
import deleteCookies from '../support/action/deleteCookies';
import dragElement from '../support/action/dragElement';
import focusLastOpenedWindow from '../support/action/focusLastOpenedWindow';
import handleModal from '../support/action/handleModal';
import moveTo from '../support/action/moveTo';
import pause from '../support/action/pause';
import pressButton from '../support/action/pressButton';
import scroll from '../support/action/scroll';
import selectOption from '../support/action/selectOption';
import selectOptionByIndex from '../support/action/selectOptionByIndex';
import setCookie from '../support/action/setCookie';
import setInputField from '../support/action/setInputField';
import setPromptText from '../support/action/setPromptText';
import switchToChildFrame from '../support/action/switchToChildFrame';
import switchToParentFrame from '../support/action/switchToParentFrame';
import {
  loginWithUsernameAndPassword,
  loginWithDefinedCustomer,
} from '../support/action/loginWith';
import downloadFE02 from '../support/action/downloadFE02';
import completePayment from '../support/action/completePayment';
import clearWishlist from '../support/action/clearWishlist';
import clearShoppingBasket from '../support/action/clearShoppingBasket';
import waitForApi from '../support/action/waitForApi';
import switchToWindow from '../support/action/switchToWindow';
import setPaypalCookies from '../support/action/setPaypalCookies';

When(
  /^I (click|doubleclick) on (the|the iframe) (link|button|element) "([^"]*)?"( within "([^"]*)?")*$/,
  clickElement
);

When(/^I (click|doubleclick) on all elements in "([^"]*)?"$/, clickAllElements);

When(/^I (add|set) "([^"]*)?" to the inputfield "([^"]*)?"$/, setInputField);

When(/^I clear the inputfield "([^"]*)?"$/, clearInputField);

When(/^I drag element "([^"]*)?" to element "([^"]*)?"$/, dragElement);

When(/^I pause for (\d+)ms$/, pause);

When(/^I set a cookie "([^"]*)?" with the content "([^"]*)?"$/, setCookie);

When(/^I delete the cookie "([^"]*)?"$/, deleteCookies);

When(/^I press "([^"]*)?"$/, pressButton);

When(/^I (accept|dismiss) the (alertbox|confirmbox|prompt)$/, handleModal);

When(/^I enter "([^"]*)?" into the prompt$/, setPromptText);

When(/^I scroll to element "([^"]*)?"$/, scroll);

When(/^I close the last opened (window|tab)$/, closeLastOpenedWindow);

When(/^I focus the last opened (window|tab)$/, focusLastOpenedWindow);

When(/^I select the (\d+)(st|nd|rd|th) option for element "([^"]*)?"$/, selectOptionByIndex);

When(
  /^I select the option with the (name|value|text) "([^"]*)?" for element "([^"]*)?"$/,
  selectOption
);

When(/^I move to "([^"]*)?"(?: with an offset of (\d+),(\d+))*$/, moveTo);

When(/^I refresh the page$/, async () => {
  await browser.refresh();
});
When(/^I switch to the child iFrame(?: with a selector "([^"]*)?")*$/, switchToChildFrame);

When(/^I switch back to the parent frame$/, switchToParentFrame);

When(/^I enter debug mode$/, async () => {
  await browser.debug();
});

When(/^I login with "([^"]*)?" and "([^"]*)?"$/, loginWithUsernameAndPassword);
When(/^I login with the( (\d+)(st|nd|rd|th))* defined customer$/, loginWithDefinedCustomer);
When(/^I download the FE02 file from S3$/, downloadFE02);

When(/^I complete a (BANK_TRANSFER|CREDIT_CARD|PAYPAL_EXPRESS) payment$/, completePayment);

When(/^I clear the wishlist(?: and return to the page "([^"]*)?")*$/, clearWishlist);
When(/^I clear the shopping basket(?: and return to the page "([^"]*)?")*$/, clearShoppingBasket);
When(/^I wait for the API call to return$/, waitForApi);
When(/^I switch to the window called "([^"]*)?"$/, switchToWindow);
When(/^I go back to the previous page$/, browserBack);
When(/^I set the paypal cookies$/, setPaypalCookies);
