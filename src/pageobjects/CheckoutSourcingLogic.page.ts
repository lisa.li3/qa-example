import Page from './page';

const DELIVERY_X_OF_Y = 'Delivery {{x}} of {{y}}';

export class CheckoutSourcingLogic extends Page {
  get productImages(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return await browser.queryAllByRole('img', { name: await browser.getTranslation('Product') });
    })();
  }
  get delivery1ofText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({ text: DELIVERY_X_OF_Y });
      translation = translation.replace('{{x}}', '1');
      translation = translation.replace('{{y}}', '');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get delivery2ofText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({ text: DELIVERY_X_OF_Y });
      translation = translation.replace('{{x}}', '2');
      translation = translation.replace('{{y}}', '');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get delivery3ofText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({ text: DELIVERY_X_OF_Y });
      translation = translation.replace('{{x}}', '3');
      translation = translation.replace('{{y}}', '');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get delivery4ofText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({ text: DELIVERY_X_OF_Y });
      translation = translation.replace('{{x}}', '4');
      translation = translation.replace('{{y}}', '');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get splitDeliveryHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Your order will be a split delivery!'),
      });
    })();
  }
  get splitDeliveryText1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation({
          text: 'Please note that in order for us to get your order to you as quickly as possible, it will be split into',
          startsWith: true,
        })
      );
    })();
  }
  get splitDeliveryText2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation({
          text: 'separate deliveries.',
          endsWith: true,
        })
      );
    })();
  }
}

export default new CheckoutSourcingLogic();
