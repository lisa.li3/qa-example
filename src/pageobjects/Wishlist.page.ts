import Page from './page';
import { within } from '@project/testing-library-webdriverio';
import getData from '../data/seedingPayloads/CustomerWishList.data.seed';
import { IProductSeeder } from '../support/types/IProduct';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

const REMOVE_FROM_WISHLIST = 'Remove from wish list';

// TODO: Should this be moved somewhere more centrally?
const getProductByIndex = async (index: number): Promise<IProductSeeder> => {
  const seedData = await getData();
  return seedData.product[index];
};

export class Wishlist extends Page {
  get Header(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('heading', {
        name: await browser.getTranslation('Wish List'),
      });
    })();
  }

  get Grid(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('grid', {});
    })();
  }

  get GridRowGroup(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const rowGroups = await within(await this.Grid).getAllByRole('rowgroup', {});
      return rowGroups[rowGroups.length - 1]; // Always the last group
    })();
  }

  get WishlistRows(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.GridRowGroup).getAllByRole('row', {});
    })();
  }

  get SecondSeededProduct(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const product: IProductSeeder = await getProductByIndex(1);
      cucumberJson.attach(`Second Seeded Product Name: ${product.attributes?.name}`, 'text/plain');
      return browser.getByRole('row', {
        name: await browser.getTranslation(product.attributes?.name as string),
      });
    })();
  }

  get SecondSeededProductRemoveButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.SecondSeededProduct).getByTitle(
        await browser.getTranslation(REMOVE_FROM_WISHLIST)
      );
    })();
  }

  get SecondSeededProductMoveToShoppingButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.SecondSeededProduct).getByRole('button', {
        name: await browser.getTranslation('MOVE TO SHOPPING BAG'),
      });
    })();
  }

  get ThirdSeededProduct(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const product: IProductSeeder = await getProductByIndex(2);
      cucumberJson.attach(`Third Seeded Product Name: ${product.attributes?.name}`, 'text/plain');
      return browser.getByRole('row', {
        name: await browser.getTranslation(product.attributes?.name as string),
      });
    })();
  }

  get ThirdSeededProductRemoveButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.ThirdSeededProduct).getByTitle(
        await browser.getTranslation(REMOVE_FROM_WISHLIST)
      );
    })();
  }

  get OutOfStock(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const elements = await within(await this.Grid).getAllByText(
        await browser.getTranslation('Sorry, this item is currently out of stock.')
      );
      for (const element of elements) {
        if (await element.isDisplayed()) {
          return element;
        }
      }
      throw Error('Did not find a product out of stock');
    })();
  }

  get WishlistEmpty(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation(
          'You have no items saved. Still on the fence? Add items here just in case.'
        )
      );
    })();
  }

  get SignInOrRegister(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Sign in or register an account'));
    })();
  }

  get RemoveFromWishlist(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const closeButtons = await browser.getAllByTitle(
        await browser.getTranslation(REMOVE_FROM_WISHLIST)
      );
      for (const button of closeButtons) {
        if (await button.isDisplayed()) {
          return button;
        }
      }
      throw Error("Remove button wasn't found");
    })();
  }

  get RemoveFromWishlistButtons(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return browser.getAllByTitle(await browser.getTranslation(REMOVE_FROM_WISHLIST));
    })();
  }

  get AddConfirmed(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation('This item has been added to your wish list.'),
        {}
      );
    })();
  }

  get RemoveConfirmed(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation('This item has been removed from your wish list.'),
        {}
      );
    })();
  }

  get MoveToShoppingBag(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const moveButtons = await browser.getAllByText(
        await browser.getTranslation('MOVE TO SHOPPING BAG')
      );
      for (const button of moveButtons) {
        if (await button.isDisplayed()) {
          return button;
        }
      }
      throw Error("Move to Shopping Bag button wasn't found");
    })();
  }

  get MoveAllToShoppingBag(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('MOVE ALL TO SHOPPING BAG'));
    })();
  }
}

export default new Wishlist();
