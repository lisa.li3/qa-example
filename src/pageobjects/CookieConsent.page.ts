import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class CookieConsent extends Page {
  get cookieConsentBanner(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: await browser.getTranslation('cookieconsent'),
      });
    })();
  }

  get accept(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cookieConsentBanner).getByRole('button', {
        name: await browser.getTranslation('allow cookies'),
      });
    })();
  }
}

export default new CookieConsent();
