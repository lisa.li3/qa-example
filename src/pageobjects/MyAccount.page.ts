import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class MyAccount extends Page {
  get main(): Promise<WebdriverIO.Element> {
    return browser.getByRole('main', {});
  }

  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('heading', {
        level: 1,
        name: await browser.getTranslation('Customer Account'),
      });
    })();
  }

  get loggedInAs(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByText(await browser.getTranslation('Logged in as'));
    })();
  }

  get accountInformationButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Account Information'),
      });
    })();
  }

  get deliveryInformationButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Delivery Information'),
      });
    })();
  }

  get orderHistoryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Order History'),
      });
    })();
  }

  get wishlistButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Wish List'),
      });
    })();
  }

  get registerOrLostButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Register / Lost gift card'),
      });
    })();
  }

  get logOutButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Log out'),
      });
    })();
  }

  get accountCreditAmount(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByTestId('amount', {});
    })();
  }

  get creditHistoryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Credit History'),
      });
    })();
  }

  get projectHepline(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('link', {
        name: await browser.getTranslation('dedicated website helpline'),
      });
    })();
  }
  get stateTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('textbox', {
        name: await browser.getTranslation('STATE/ PROVINCE/ REGION'),
      });
    })();
  }
  get stateSelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('combobox', {
        name: await browser.getTranslation('STATE/ PROVINCE/ REGION*'),
      });
    })();
  }

  get getStates(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.stateSelect).getAllByRole('option', {});
    })();
  }
  get states(): Promise<string> {
    return (async (): Promise<string> => {
      let states = '';
      let stateText;
      for (const state of await this.getStates) {
        stateText = await state.getText();
        states = states.concat((await stateText) + '\n');
      }
      states = states.replace(/\n*$/, ''); // remove the last new line character
      return states;
    })();
  }
  get inputTooLongMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Too long'));
    })();
  }
}

export default new MyAccount();
