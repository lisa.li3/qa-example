import Page from './page';

export class StaticContent extends Page {
  get sanaDiv(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//main[@class='sana']");
    })();
  }
  get siteMapHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //const translatedText = await browser.getTranslation({ text: 'sitemap' });
      return await browser.getByRole('heading', { name: await browser.getTranslation('sitemap') });
    })();
  }
}

export default new StaticContent();
