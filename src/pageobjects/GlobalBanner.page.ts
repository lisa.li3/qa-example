import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class GlobalBanner extends Page {
  get bannerMain(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('global-banner', {});
    })();
  }
  get close(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Close') });
    })();
  }
  get link(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.bannerMain).getByRole('link', {});
    })();
  }
}

export default new GlobalBanner();
