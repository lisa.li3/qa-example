import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { replace, trim } from 'lodash';

export class PDP_ProductMain extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.sticky-box > header');
    })();
  }

  //
  // Elements
  //
  get headings(): Promise<{
    style: WebdriverIO.Element;
    price: WebdriverIO.Element;
    name: WebdriverIO.Element;
  }> {
    return (async (): Promise<{
      style: WebdriverIO.Element;
      price: WebdriverIO.Element;
      name: WebdriverIO.Element;
    }> => {
      // @ts-ignore
      const headings = await within(await this.main).getAllByRole('heading');
      console.log({ headings });
      return {
        style: headings[0],
        name: headings[1],
        price: headings[2],
      };
    })();
  }

  get style(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await this.headings).style;
    })();
  }

  get name(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await this.headings).name;
    })();
  }

  get standardPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await within(await this.main).getByTestId('standard-price');
    })();
  }

  get markdownPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await within(await this.main).getByTestId('markdown-price');
    })();
  }

  get youSavePrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await within(await this.main).getByTestId('yousave-price');
    })();
  }
}
export default new PDP_ProductMain();
