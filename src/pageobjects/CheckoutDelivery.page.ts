import { within } from '@project/testing-library-webdriverio';
import Page from './page';

const DELIVERY_LOCATION = 'Delivery location';

export class CheckoutDelivery extends Page {
  get checkoutHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Checkout'),
      });
    })();
  }
  get callHelplineLink(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.findByText(await browser.getTranslation('dedicated website helpline:'));
    })();
  }
  get countrySelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation(DELIVERY_LOCATION),
      });
    })();
  }

  get deliveryCountries(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.countrySelect()).getByRole('button', {
        name: await browser.getTranslation(DELIVERY_LOCATION),
      });
    })();
  }

  get deliveryChoices(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('option', {});
    })();
  }
  get defaultDeliveryCountry1country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('generic', {
        name: await browser.getTranslation(DELIVERY_LOCATION),
      });
    })();
  }
  get defaultDeliveryCountry(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation(DELIVERY_LOCATION),
      });
    })();
  }
  get countrySearchTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('filterable-input', {});
    })();
  }
  get noResultsMatched(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByText(
        await browser.getTranslation('No results matched')
      );
    })();
  }
  get unitedKingdomButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selector = await within(await this.countrySelect).getAllByRole('button', {
        name: await browser.getTranslation('United Kingdom'),
      });
      return selector[1];
    })();
  }
  get germanyButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation('Germany'),
      });
    })();
  }
  get franceButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation('France'),
      });
    })();
  }
  get italyButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation('Italy'),
      });
    })();
  }
  get polandButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation('Poland'),
      });
    })();
  }
  get austriaButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.countrySelect).getByRole('button', {
        name: await browser.getTranslation('Austria'),
      });
    })();
  }

  get importantNoticeDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: await browser.getTranslation('Important notice'),
      });
    })();
  }

  get importantNoticeHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Important notice'),
      });
    })();
  }
  get deliveryLocation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Delivery Location'));
    })();
  }
  get importantNoticeBody(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          "Because you've changed your delivery country, your currency, delivery date(s) and shipping costs may have changed. Please check your order before proceeding."
        )
      );
    })();
  }

  get sorryDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: await browser.getTranslation(`We're Sorry`),
      });
    })();
  }

  get popUpOk(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.importantNoticeDialog).getByText(
        await browser.getTranslation('Ok')
      );
    })();
  }

  get popUpOkSorry(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sorryDialog).getByText(await browser.getTranslation('Ok'));
    })();
  }
  get popUpYes(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Yes'));
    })();
  }
  get homeDeliveryRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: await browser.getTranslation('Home delivery'),
      });
    })();
  }
  get storeCollectionRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: await browser.getTranslation('Store Collection'),
      });
    })();
  }
  get deliveryMethods(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.deliveryChoices).getAllByRole('radio', {});
    })();
  }
  get cheapestDeliveryOptionButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('delivery2'),
      });
    })();
  }
  get mostExpensiveDeliveryOptionButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('delivery1'),
      });
    })();
  }
  get deliveryOptionsButtonText(): Promise<string> {
    return (async (): Promise<string> => {
      let text = '';
      // @ts-ignore
      const options = await within(await browser.getByTestId('checkout-delivery_1')).getAllByRole(
        'button',
        {
          name: await browser.getTranslation('delivery'),
        }
      );
      for (const option of options) {
        text = text.concat((await option.getText()) + '\n');
      }
      text = text.replace(/\n*$/, ''); // remove the last new line character
      return text;
    })();
  }
  get UK2deliveryOptionButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('UK delivery2'),
      });
    })();
  }
  get removeFirstItemButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const removeButton = await browser.getAllByRole('button', {
        name: await browser.getTranslation('Remove'),
      });
      return removeButton[0];
    })();
  }

  get modalHeader(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await browser.getByRole('dialog')).getByText(
        await browser.getTranslation("We're sorry")
      );
    })();
  }
  get modalLine1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          "We're unable to deliver at least one of your items to your chosen country."
        )
      );
    })();
  }
  get modalLine2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          'Please change your address or remove items to complete your purchase.'
        )
      );
    })();
  }
  get cantDeliverProductFirstMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const text = await browser.getAllByText(
        await browser.getTranslation(
          "Sorry, we can't deliver this item to your location. Please remove it, or change your delivery destination to continue."
        )
      );
      return text[0];
    })();
  }
  get cantDeliverProductSecondMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const text = await browser.getAllByText(
        await browser.getTranslation(
          "Sorry, we can't deliver this item to your location. Please remove it, or change your delivery destination to continue."
        )
      );
      return text[1];
    })();
  }
  get cantDeliverBuyNowMessage1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          "You have items in your basket that aren't available for delivery to your chosen location."
        )
      );
    })();
  }
  get cantDeliverBuyNowMessage2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          'Please remove them from your basket or change your delivery destination to continue.'
        )
      );
    })();
  }
  get firstDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('checkout-delivery_1');
    })();
  }
  get firstDeliveryFC1delivery2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.firstDelivery).getByRole('button', { name: 'FC1 delivery 2' });
    })();
  }
  get firstDeliveryFC1delivery1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.firstDelivery).getByRole('button', { name: 'FC1 delivery 1' });
    })();
  }

  get secondDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('checkout-delivery_2');
    })();
  }
  get secondDeliveryFC2delivery2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.secondDelivery).getByRole('button', { name: 'FC2 delivery 2' });
    })();
  }
  get secondDeliveryFC2delivery1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.secondDelivery).getByRole('button', { name: 'FC2 delivery 1' });
    })();
  }
  get thirdDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('checkout-delivery_3');
    })();
  }
  get thirdDeliveryFC3delivery2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.thirdDelivery).getByRole('button', { name: 'FC3 delivery 2' });
    })();
  }
  get thirdDeliveryFC3delivery1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.thirdDelivery).getByRole('button', { name: 'FC3 delivery 1' });
    })();
  }
  get fourthDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('checkout-delivery_4');
    })();
  }
  get fourthDeliveryFC4delivery2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.fourthDelivery).getByRole('button', { name: 'FC4 delivery 2' });
    })();
  }
  get FC3delivery2TimedDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.thirdDeliveryFC3delivery2).getByTestId('select-input');
    })();
  }
  get FC4delivery1TimedDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.fourthDeliveryFC4delivery1).getByTestId('select-input');
    })();
  }
  get fourthDeliveryFC4delivery1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.fourthDelivery).getByRole('button', { name: 'FC4 delivery 1' });
    })();
  }
  get FC3delivery2TimedDeliveryMonday(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.FC3delivery2TimedDelivery).getByRole('option', {
        name: new RegExp(`^Monday`),
      });
    })();
  }
  get FC4delivery1TimedDeliveryFriday(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.FC4delivery1TimedDelivery).getByRole('option', {
        name: new RegExp(`^Friday`),
      });
    })();
  }
  get deliveryDays(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return await within(
        // @ts-ignore
        await browser.getByRole('button', {
          name: await browser.getTranslation('standard delivery'),
        })
      ).getAllByTestId('option');
    })();
  }
  get specificDayDeliveryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('button', {
        name: await browser.getTranslation('specific day delivery'),
      });
    })();
  }
  get eveningDeliveryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('button', {
        name: await browser.getTranslation('evening delivery'),
      });
    })();
  }
  get orderSummary(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('Order Summary'),
      });
    })();
  }
}
export default new CheckoutDelivery();
