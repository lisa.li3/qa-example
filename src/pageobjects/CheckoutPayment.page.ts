import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class CheckoutPayment extends Page {
  get cardPaymentNumberFrame(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__cardNumber__input > iframe');
    })();
  }
  get cardPaymentDateFrame(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__exp-date__input > iframe');
    })();
  }
  get cardPaymentCvvFrame(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__cvc__input > iframe');
    })();
  }
  get cardPaymentFrameInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.js-iframe-input');
    })();
  }
  get cardPaymentNameInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $("[placeholder='J\\. Smith']");
    })();
  }
  get cardPaymentNumberError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__field--cardNumber.adyen-checkout__field--error > label');
    })();
  }
  get cardPaymentDateFrameError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__field--expiryDate.adyen-checkout__field--error');
    })();
  }
  get cardPaymentCvvFrameError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__field--securityCode.adyen-checkout__field--error');
    })();
  }
  get cardPaymentNameInputError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__holderName.adyen-checkout__field--error');
    })();
  }
  get buyNowButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Buy now'),
      });
    })();
  }
  get payPalExpressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTitle(await browser.getTranslation('PayPal'));
    })();
  }

  get adyenDropIn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId(await browser.getTranslation('adyen-dropin'));
    })();
  }

  get incompleteFields(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return await within(await this.adyenDropIn).getAllByText(
        await browser.getTranslation('Incomplete field')
      );
    })();
  }

  get invalidCardholderName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.adyenDropIn).getByText(
        await browser.getTranslation('Invalid cardholder name')
      );
    })();
  }

  get payPalBuyNowButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return $("//div[@id='PaypalButton']");
    })();
  }

  get buyNowDiv(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//div[@class='buy-now']");
    })();
  }

  get or(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.buyNowDiv).getByText(await browser.getTranslation('or'));
    })();
  }

  get phoneInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return browser.getByLabelText(await browser.getTranslation('Phone'));
    })();
  }

  get errorHasOccurred(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('An error has occurred.'),
      });
    })();
  }

  get unableToApplyGiftcards(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('We are unable to apply your giftcards'),
      });
    })();
  }

  get orderProcessError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('heading', {
        name: await browser.getTranslation('an error has occurred'),
      });
    })();
  }
  get orderProcessErrorOkButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('ok'),
      });
    })();
  }
}
export default new CheckoutPayment();
