import Page from './page';

export class HelpCentre extends Page {
  get iFrame(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('Iframe', {});
    })();
  }
  get Title(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      console.log('In');
      // @ts-ignore
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('WELCOME TO OUR SUPPORT CENTRE'),
      });
    })();
  }
}

export default new HelpCentre();
