import { within } from '@project/testing-library-webdriverio';
import Page from './page';

export class CheckoutStaffDiscountTerms extends Page {
  get termsDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: /staff discount/i,
      });
    })();
  }

  get checkbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.termsDialog).getByRole('checkbox');
    })();
  }

  get proceedButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.termsDialog).getByRole('button', { name: /proceed/i });
    })();
  }
}
export default new CheckoutStaffDiscountTerms();
