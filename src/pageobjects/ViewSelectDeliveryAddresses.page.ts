import Page from './page';

export class ViewSelectDeliveryAddresses extends Page {
  get defaultAddressLink(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Default (same as billing address)'),
      });
    })();
  }
  get addresses(): Promise<string> {
    return (async (): Promise<string> => {
      let text = '';
      const addresses = await browser.getAllByText(new RegExp(/(A Home3)|(B Home2)/i), {});
      for (const address of addresses) {
        text = text.concat((await address.getText()) + '\n');
      }
      text = text.replace(/\n*$/, ''); // remove the last new line character
      return text;
    })();
  }
  get homeDeliveryRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: await browser.getTranslation('Home delivery'),
      });
    })();
  }
  get defaultAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const addresses = await browser.getAllByTestId(new RegExp(/address-label/i), {});
      return addresses[0];
    })();
  }
  get firstAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const addresses = await browser.getAllByTestId(new RegExp(/address-label/i), {});
      return addresses[1];
    })();
  }
  get newAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const addresses = await browser.getAllByTestId(new RegExp(/address-label/i), {});
      return addresses[3];
    })();
  }
  get deliveryOptionsHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Delivery Options'),
      });
    })();
  }
  get deliveryAddressHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Delivery Address'),
      });
    })();
  }
  get checkoutHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', { name: await browser.getTranslation('Checkout') });
    })();
  }
  get addAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Add a new address'),
      });
    })();
  }
  get manuallyEnterYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }
  get saveButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Save'),
      });
    })();
  }
  get addressTitle(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address Title'),
      });
    })();
  }
  get firstName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('First Name'),
      });
    })();
  }
  get lastName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Last Name'),
      });
    })();
  }
  get addressLine1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address 1'),
      });
    })();
  }
  get addressLine2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address 2'),
      });
    })();
  }
  get city(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('City'),
      });
    })();
  }
  get county(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('State/ Province/ Region'),
      });
    })();
  }
  get postcode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Zip/Postal Code'),
      });
    })();
  }
  get phone(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation({
          text: 'Phone',
          startsWith: true,
        }),
      });
    })();
  }
  get mobilePhone(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Mobile Phone'),
      });
    })();
  }
  get countryDropdown(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('combobox', {
        name: await browser.getTranslation('Country'),
      });
    })();
  }
  get countryUK(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('option', {
        name: await browser.getTranslation('United Kingdom'),
      });
    })();
  }
  get countryDE(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('option', {
        name: await browser.getTranslation('Germany'),
      });
    })();
  }
  get standardDeliveryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('standard delivery'),
      });
    })();
  }
  get emailAddressField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Email Address'));
    })();
  }
  get password(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByLabelText(await browser.getTranslation('Password')))[0];
    })();
  }
  get confirmedPassword(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByLabelText(await browser.getTranslation('Password')))[1];
    })();
  }
  get phoneField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByLabelText(await browser.getTranslation('Phone')))[0];
    })();
  }
  get mobilePhoneField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Mobile phone'));
    })();
  }
  get gender(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Gender'));
    })();
  }
  get firstNameField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('First Name'));
    })();
  }
  get lastNameField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Last Name'));
    })();
  }
  get addressTitleField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Address Title'));
    })();
  }
  get addressLine1Field(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Address 1'));
    })();
  }
  get addressLine2Field(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Address 2'));
    })();
  }
  get cityField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('City'));
    })();
  }
  get countyField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('State/ Province/ Region'));
    })();
  }
  get postcodeField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Zip/Postal code'));
    })();
  }
  get countryField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Country'));
    })();
  }
  get defaultAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation('Default (same as billing address)')
      );
    })();
  }
}

export default new ViewSelectDeliveryAddresses();
