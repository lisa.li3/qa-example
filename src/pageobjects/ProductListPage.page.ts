import Page from './page';
import { within } from '@project/testing-library-webdriverio';
import { escapeRegExp } from 'lodash';
import { isString } from 'lodash';

const YOU_SAVE_AMOUNT = 'You save {{amount}}';
const AMOUNT = '{{amount}}';

export class ProductListPage extends Page {
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('main', {});
    })();
  }

  get productListContainer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('list', {});
    })();
  }

  get mens(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const all = await within(await this.main).getAllByText(await browser.getTranslation('Mens'));
      return all[1];
    })();
  }

  get facetOptions(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getAllByTestId(await browser.getTranslation('-Input'));
    })();
  }

  get productList(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return await within(await this.productListContainer).getAllByRole('listitem', {});
    })();
  }

  get productListItem(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const items = await within(await this.productListContainer).getAllByRole('listitem', {});
      return await items[24];
    })();
  }

  get clearAllButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Clear All'),
      });
    })();
  }

  get productsRatingsLabelText(): Promise<string> {
    return (async (): Promise<string> => {
      let ratings = '';
      let ratingLabelText;
      for (const product of await this.productList) {
        // @ts-ignore
        const ratingLabel = await within(await product).queryByLabelText(
          await browser.getTranslation('Rating')
        );
        if (ratingLabel !== null && ratingLabel.isDisplayed()) {
          ratingLabelText = ratingLabel.getAttribute('aria-label');
        } else {
          ratingLabelText = 'No Rating';
        }
        ratings = ratings.concat((await ratingLabelText) + '\n');
      }
      ratings = ratings.replace(/\n*$/, ''); // remove the last new line character
      return ratings;
    })();
  }

  get productsNames(): Promise<string> {
    return (async (): Promise<string> => {
      // @ts-ignore
      const productNames = within(await this.productListContainer).getAllByText(
        await browser.getTranslation('product')
      );
      let productNamesString = '';
      let nameText;
      for (const productName of await productNames) {
        nameText = await (await productName).getText();
        productNamesString = productNamesString.concat((await nameText) + '\n');
      }
      productNamesString = productNamesString.replace(/\n*$/, ''); // remove the last new line character
      return productNamesString;
    })();
  }

  get loadMore(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('LOAD MORE'));
    })();
  }

  get refineBy(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Refine By'),
      });
    })();
  }

  get refineByDrawer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: await browser.getTranslation('Refine By'),
      });
    })();
  }

  get closeButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.refineByDrawer).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }

  get filtersDrawer(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return await browser.getByTestId('FiltersDrawer');
    })();
  }

  get noResults(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(`We couldn't find what you’re looking for. Try again?`)
      );
    })();
  }

  get zeroItems(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('0 ITEMS'));
    })();
  }

  get oneItem(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('1 ITEMS'));
    })();
  }

  get twoItems(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('2 ITEMS'));
    })();
  }

  get threeItems(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('3 ITEMS'));
    })();
  }

  get hundredItems(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('100 ITEMS'));
    })();
  }

  get mensFacetOption(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('Mens');
    })();
  }

  get removeFacet(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('active-filter');
    })();
  }

  get mobileDarkGreen(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('Dark green-input');
    })();
  }

  get mobileDarkRed(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('Dark red-input');
    })();
  }

  get mobileWomens(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('Womens-input');
    })();
  }

  get mobileMens(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('Mens-input');
    })();
  }

  get mobile14(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('14-input');
    })();
  }

  get mobile11(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('11-input');
    })();
  }

  get sortDropdown(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('select-input');
    })();
  }

  get sortDropdownTextOptions(): Promise<string> {
    return (async (): Promise<string> => {
      let optionsText = '';
      // @ts-ignore
      const options = await within(await this.sortDropdown).getAllByTestId('option', {
        exact: false,
      });
      for (const option of options) {
        optionsText = optionsText.concat((await option.getText()) + '\n');
      }
      optionsText = optionsText.replace(/\n*$/, '');
      return optionsText;
    })();
  }

  get sortDefaultOption(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sortDropdown).getByText(
        await browser.getTranslation('Sort by')
      );
    })();
  }

  get loadedMoreItems(): Promise<WebdriverIO.Element | void> {
    return (async (): Promise<WebdriverIO.Element | void> => {
      let translationText = await browser.getTranslation({
        page: 'translation',
        item: 'General.ShowingOf',
      });
      if (isString(translationText)) {
        translationText = translationText.replace('{{x}}', '96');
        translationText = translationText.replace('{{y}}', '100');
        return await browser.getByText(new RegExp(translationText, 'i'));
      }
    })();
  }

  get sortMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Sort by'));
    })();
  }

  get sortDrawerMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('SortDrawer', {});
    })();
  }

  get mobileSortMostRelevant(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sortDrawerMobile).getByText(
        await browser.getTranslation('Most relevant')
      );
    })();
  }

  get mobileSortHighestPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sortDrawerMobile).getByText(
        await browser.getTranslation('Highest Price')
      );
    })();
  }

  get mobileSortLowestPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sortDrawerMobile).getByText(
        await browser.getTranslation('Lowest Price')
      );
    })();
  }

  get mobileSortHighestRated(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sortDrawerMobile).getByText(
        await browser.getTranslation('Highest rated')
      );
    })();
  }

  get activeProductCardOverlay(): Promise<WebdriverIO.Element | void> {
    return (async (): Promise<WebdriverIO.Element | void> => {
      // @ts-ignore
      const productCardOverlays = await within(await this.productListContainer).getAllByTestId(
        'product-card-overlay'
      );
      for (const productOverlay of productCardOverlays) {
        const isDisplayed = await productOverlay.isDisplayed();
        if (isDisplayed) {
          return productOverlay;
        }
      }
    })();
  }

  get quickAddToBagButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.activeProductCardOverlay).getByText(
        await browser.getTranslation('Add To Bag')
      );
    })();
  }

  get quickAddToBagSizeDropdown(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.activeProductCardOverlay).getByTestId('qatb-size-selected');
    })();
  }

  get quickAddToBagSizeDropdownLabels(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      const qatbSizeOptions = within(
        // @ts-ignore
        await this.activeProductCardOverlay
      ).getAllByTestId('quick-add-to-bag-label');
      return await qatbSizeOptions;
    })();
  }

  // This is to check for enabled and disabled states on the elements returned - couldn't use a testing library selector
  get quickAddToBagSizeSelectorInputOptions(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return $$('.quick-add-to-bag-selector__radio');
    })();
  }

  get colourFilter(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//div[contains(text(),'Colour')]");
    })();
  }

  get productTypeFilter(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//div[contains(text(),'Product Type')]");
    })();
  }

  get sizeFilter(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//div[contains(text(),'Size')]");
    })();
  }

  get genderFilter(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $("//div[contains(text(),'Gender')]");
      // Product List Page Product Images
    })();
  }

  get colourFilterMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('COLOUR'));
    })();
  }

  get productTypeFilterMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('PRODUCT TYPE'));
    })();
  }

  get sizeFilterMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('SIZE'));
    })();
  }

  get genderFilterMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('GENDER'));
    })();
  }

  get productImages(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return await within(await this.productListContainer).getAllByRole('img', {
        name: await browser.getTranslation('product'),
      });
    })();
  }

  //
  // B2C2-304 - Selectors for Category Name and Description Tests
  //

  get categoryHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        level: 1,
        name: await browser.getTranslation('category heading'),
      });
    })();
  }

  get subCategoryLink(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('sub category'),
      });
    })();
  }

  get mobileShowMoreLessButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('show'));
    })();
  }

  // ToDo: See if there is a way to improve this - Had to use a horrible WDIO selector to get the div to show whether the category description is collapsed or not on mobile
  get descriptionSectionDiv(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $('#project > div:nth-child(3) > section > div');
    })();
  }

  // PLP Breadcrumbs

  get breadcrumbNavigation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('navigation', {
        name: await browser.getTranslation('breadcrumb'),
      });
    })();
  }

  get breadcrumbList(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.breadcrumbNavigation).getByRole('list');
    })();
  }

  get breadcrumbListItems(): Promise<string> {
    return (async (): Promise<string> => {
      let breadCrumbsList = '';
      const listItems = $$('.breadcrumb__item');
      for (const item of await listItems) {
        breadCrumbsList = breadCrumbsList.concat((await item.getText()) + '\n');
      }
      breadCrumbsList = breadCrumbsList.replace(/\n*$/, ''); // remove the last new line character
      return breadCrumbsList;
    })();
  }

  get breadcrumbSeparators(): Promise<string> {
    return (async (): Promise<string> => {
      return await browser.execute(async () => {
        const elm = document.querySelectorAll('.breadcrumb .breadcrumb__item');
        let items = '';
        for (const element of elm) {
          if (window.getComputedStyle(element, '::after').display !== 'none') {
            items = items.concat(window.getComputedStyle(element, '::after').content + '\n');
          } else {
            items = items.concat('Separator not displayed' + '\n');
          }
        }
        items = items.replace(/\n*$/, ''); // remove the last new line character
        items = items.replace(/['"]+/g, ''); // remove double quotes around each elements computed style text
        return items;
      });
    })();
  }
  get productOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('product-id-plp00666p1', {});
    })();
  }
  get productTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('product-id-plp00666p2', {});
    })();
  }
  get productThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId('product-id-plp00666p3', {});
    })();
  }
  get youSaveOnProductOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: YOU_SAVE_AMOUNT,
      });
      // @ts-ignore
      translationText = translationText.replace(AMOUNT, '');
      return await within(await this.productOne).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }
  get youSaveOnProductTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: YOU_SAVE_AMOUNT,
      });
      // @ts-ignore
      translationText = translationText.replace(AMOUNT, '');
      return await within(await this.productTwo).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }
  get youSaveOnProductThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: YOU_SAVE_AMOUNT,
      });
      // @ts-ignore
      translationText = translationText.replace(AMOUNT, '');
      return await within(await this.productThree).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }
  get firstImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('Product 1 - Staff discount set to true'),
      });
    })();
  }
  get secondImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('Product 2 - Markdown with Staff discount set to true'),
      });
    })();
  }
  get thirdImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('Product 3 - Does not qualify'),
      });
    })();
  }
}

export default new ProductListPage();
