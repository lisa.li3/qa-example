import { isNull } from 'lodash';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { within } from '@project/testing-library-webdriverio';
/**
 * main page object containing all methods, selectors and functionality
 * that is shared across all page objects
 */
export default class Page {
  get firstNameTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('First name'),
      });
    })();
  }
  get manuallyEnterYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }
  get lastNameTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Last name'),
      });
    })();
  }
  get phoneTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation({
          text: 'Phone',
          startsWith: true,
        }),
      });
    })();
  }
  get inputTooLongWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Too long'));
    })();
  }
  get phoneNumberValueTypeWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Must be a phone number'));
    })();
  }
  get emailAddressTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation({
          text: 'Email Address',
          startsWith: true,
        }),
      });
    })();
  }
  get invalidEmailMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Please enter a valid email address.'));
    })();
  }
  get passwordTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const elements = await browser.getAllByLabelText(await browser.getTranslation('password'));
      return elements[0];
    })();
  }
  get confirmPasswordTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const elements = await browser.getAllByLabelText(await browser.getTranslation('password'));
      return elements[1];
    })();
  }
  get confirmPasswordWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Passwords must match'));
    })();
  }
  get confirmEmailAddressTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Confirm email address'),
      });
    })();
  }
  get mobilPhoneTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Mobile phone'));
    })();
  }
  get addressTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByPlaceholderText(
        await browser.getTranslation('Start typing a postcode or an address')
      );
    })();
  }
  get address1Textbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address 1'),
      });
    })();
  }
  get address2Textbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address 2'),
      });
    })();
  }
  get cityTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('city'),
      });
    })();
  }
  get postcodeTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Zip/Postal code'),
      });
    })();
  }

  get countryComboBox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('combobox', {
        name: await browser.getTranslation('country'),
      });
    })();
  }
  get gender(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Gender'));
    })();
  }
  get dateOfBirthDay(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Day of birth'));
    })();
  }
  get dateOfBirthMonth(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Month of birth'));
    })();
  }
  get dateOfBirthYear(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Year of birth'));
    })();
  }
  get findYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('button', {
        name: await browser.getTranslation({ text: 'Find Your Address' }),
      });
    })();
  }
  get pageNotFound(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('OOPS!'));
    })();
  }
  get geoRedirect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(await browser.getTranslation('Shopping from United Kingdom?'))
      )[0];
    })();
  }
  get firstHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByRole('heading', {}))[0];
    })();
  }
  get subscriptionApprovalLabel(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('I subscribe to electronic marketing'));
    })();
  }
  get subscriptionApproval(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('checkbox', {
        name: await browser.getTranslation('I subscribe to electronic marketing'),
      });
    })();
  }
  get stateProvinceRegionTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('State/ Province/ Region'),
      });
    })();
  }
  get saveButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Save'));
    })();
  }
  get firstNameErrors(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-firstName');
    })();
  }
  get lastNameErrors(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-lastName');
    })();
  }
  get address1Error(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-addressLine1');
    })();
  }
  get cityError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-city');
    })();
  }
  get postcodeError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-postcode');
    })();
  }
  get countryError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-country');
    })();
  }
  get phoneError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-contactNumber\\.phone');
    })();
  }
  get mobileError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-contactNumber\\.mobile');
    })();
  }
  //TODO: Replace with testing library selector when ticket B2C2-3465 is done
  get subscribeCheckbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.control__indicator--tickbox');
    })();
  }
  //TODO: Replace with testing library selector when ticket B2C2-3449 is done
  get passwordErrors(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-password');
    })();
  }

  checkForTestId(testId: string): Promise<void> {
    return (async (): Promise<void> => {
      const element = await browser.queryByTestId(testId);
      if (isNull(element)) {
        cucumberJson.attach(`Test id ${testId} still not found.`, 'text/plain');
      } else {
        cucumberJson.attach(
          `Test id ${testId} FOUND! Please update the page object to use this and remove this check!`,
          'text/plain'
        );
      }
    })();
  }
  get helplineNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Dedicated Website Helpline: '));
    })();
  }
  get descriptionMeta(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await $("meta[name='description']");
    })();
  }
}
