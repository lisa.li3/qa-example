import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class NewsletterSignupBanner extends Page {
  get complementary(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('complementary', {});
    })();
  }
  get emailField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.complementary).getByRole('textbox', {});
    })();
  }
}

export default new NewsletterSignupBanner();
