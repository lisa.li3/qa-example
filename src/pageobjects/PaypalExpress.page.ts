import Page from './page';

export class PaypalExpress extends Page {
  get emailAddressInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByPlaceholderText('Email address or mobile number');
    })();
  }

  get emailAddressNextBtn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('Next');
    })();
  }

  get passwordInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByPlaceholderText('Password');
    })();
  }

  get loginBtn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('Log In');
    })();
  }

  get welcomeMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('Hi Conor,');
    })();
  }

  get payNowBtn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('Pay Now');
    })();
  }

  get visaCheckbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('The Bank Card Platinum Rewards');
    })();
  }

  get cookieBtn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText('Accept Cookies');
    })();
  }
}

export default new PaypalExpress();
