import Page from './page';

export class SearchListPage extends Page {
  get SearchHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Search'),
      });
    })();
  }
  get noResultText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByText(
        await browser.getTranslation('We couldn’t find what you’re looking for. Try again?')
      );
    })();
  }
}

export default new SearchListPage();
