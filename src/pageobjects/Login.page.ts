import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class Login extends Page {
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('main', {});
    })();
  }
  get emailAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByPlaceholderText(
        await browser.getTranslation({ text: 'Email Address', startsWith: true })
      );
    })();
  }
  get invalidEmailWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByText(
        await browser.getTranslation('Please enter a valid email address.')
      );
    })();
  }
  get loginNotRecognisedWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByText(
        await browser.getTranslation(
          'Your email address and/or password were not recognised. Please try again.'
        )
      );
    })();
  }

  get valueRequiredWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByText(await browser.getTranslation('Required'));
    })();
  }

  get password(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByLabelText(await browser.getTranslation('Password'));
    })();
  }
  get signInButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Sign in'),
      });
    })();
  }
  get forgottenPassword(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Forgotten your password?'),
      });
    })();
  }
  get register(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Register'),
      });
    })();
  }
  get loginSuccess(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByText(await browser.getTranslation('Logged in'));
    })();
  }
}
export default new Login();
