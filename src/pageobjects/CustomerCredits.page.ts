import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class CustomerCredits extends Page {
  get creditHistory(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.credit-history');
    })();
  }
  get backButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.creditHistory).getByRole('link', {
        name: await browser.getTranslation('back'),
      });
    })();
  }
  get issueDateHeader(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Issue Date'));
    })();
  }

  get creditValueHeader(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('columnheader', {
        name: await browser.getTranslation('Credit Value'),
      });
    })();
  }

  get hasAccountCredit(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Account Credit'));
    })();
  }

  get remainingBalanceHeader(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('columnheader', {
        name: await browser.getTranslation('Remaining Balance'),
      });
    })();
  }

  get endDateHeader(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('columnheader', {
        name: await browser.getTranslation('End Date'),
      });
    })();
  }

  get expiredStatus(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('cell', {
        name: await browser.getTranslation('expired'),
      });
    })();
  }

  get activeStatus(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('cell', {
        name: await browser.getTranslation('active'),
      });
    })();
  }

  get pendingStatus(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('cell', {
        name: await browser.getTranslation('pending'),
      });
    })();
  }

  get disabledStatus(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('cell', {
        name: await browser.getTranslation('disabled'),
      });
    })();
  }
}
export default new CustomerCredits();
