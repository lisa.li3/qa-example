import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class GiftCards extends Page {
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('heading', {
        name: await browser.getTranslation('Gift Card'),
      });
    })();
  }

  get shopNowButtonText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Shop Now'));
    })();
  }

  get viewBalanceText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('View Balance'));
    })();
  }

  get registerYourCardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Register Your Card'));
    })();
  }

  get findNearestStoreText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Find My Nearest Store'));
    })();
  }

  get registerCardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Register Card'));
    })();
  }

  get lostCardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Lost Card'));
    })();
  }

  get registerOrLostCardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Register / Lost Gift Card'));
    })();
  }

  get giftCardBlackColour(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('black giftcard'),
      });
    })();
  }

  get giftCardPinkColour(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('pink giftcard'),
      });
    })();
  }

  get giftCardOrangeColour(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('orange giftcard'),
      });
    })();
  }

  get orangeGiftcardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('orange giftcard'));
    })();
  }

  get blackGiftcardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('black giftcard'));
    })();
  }

  get pinkGiftcardText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('pink giftcard'));
    })();
  }

  get dedicatedHelpline(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('dedicated website helpline:'));
    })();
  }

  get buttonAmountsTen(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation({ text: '£10', endsWith: true }),
      });
    })();
  }

  get buttonAmountsTwenty(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('£20'),
      });
    })();
  }

  get buttonAmountsFifty(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation({ text: '£50', endsWith: true }),
      });
    })();
  }

  get buttonAmountsOneHundred(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('£100'),
      });
    })();
  }
  buttonAmountFiveHundred;
  get buttonAmountsFiveHundred(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('£500'),
      });
    })();
  }

  get manualEntryField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('spinbutton', {});
    })();
  }

  get manualEntryValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const fieldWrapper = (await this.manualEntryField).parentElement();
      // @ts-ignore
      return within(await fieldWrapper).getByRole('alert');
    })();
  }

  get errorMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Gift amount must be 10 or more'));
    })();
  }

  get registeredCardsTable(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('table', {});
    })();
  }

  get cardNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.registeredCardsTable).getByText(
        await browser.getTranslation('Card Number')
      );
    })();
  }

  get balance(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.registeredCardsTable).getByText(
        await browser.getTranslation('balance')
      );
    })();
  }

  get noRegisteredGiftCards(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.registeredCardsTable).getByText(
        await browser.getTranslation('No gift cards registered.')
      );
    })();
  }

  get giftCardInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('gift card number*'),
      });
    })();
  }

  get invalidGiftCardInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation('PLEASE ENTER A VALID GIFT CARD NUMBER')
      );
    })();
  }
}

export default new GiftCards();
