import { within } from '@project/testing-library-webdriverio';
import Page from './page';
import { escapeRegExp } from 'lodash';

export class CheckoutOrderSummary extends Page {
  get remove(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByRole('button', { name: await browser.getTranslation('Remove') })
      )[0];
    })();
  }

  get confirmRemove(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Yes') });
    })();
  }

  get removeModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('Remove from basket'),
      });
    })();
  }

  get outOfStockModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('out of stock'),
      });
    })();
  }

  get anyModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog');
    })();
  }

  get modalOk(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.anyModal).getByRole('button', {
        name: await browser.getTranslation('Ok'),
      });
    })();
  }

  get modalClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.anyModal).getByRole('button', {
        name: await browser.getTranslation('close'),
      });
    })();
  }

  get orderSummary(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('Order Summary'),
      });
    })();
  }

  get expectedDelivery(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.orderSummary).getByText(
        await browser.getTranslation('Expected delivery')
      );
    })();
  }

  get subtotal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.orderSummary).getByText(
        await browser.getTranslation('Subtotal')
      );
    })();
  }

  get shippingFee(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await this.subtotal;
    })();
  }

  get total(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const text = await browser.getTranslation('Total');
      // @ts-ignore
      return (await within(await this.orderSummary).getAllByText(text))[1];
    })();
  }

  get deliveryOptions(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByText(await browser.getTranslation('Delivery Options')))[0];
    })();
  }

  get deliveryAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const deliveryAddressHeading = await browser.getTranslation({
        text: 'Delivery Address',
      });
      return (await browser.getAllByText(new RegExp(`${deliveryAddressHeading}`, 'i'), {}))[0];
    })();
  }

  get billingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const billingAddressHeading = await browser.getTranslation({
        text: 'Billing Address',
      });
      return (await browser.getAllByText(new RegExp(`${billingAddressHeading}`, 'i'), {}))[0];
    })();
  }

  get paymentDetails(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const paymentDetailsHeading = await browser.getTranslation({
        text: 'Payment Details',
      });
      return browser.getByText(new RegExp(`${paymentDetailsHeading}`, 'i'), {});
    })();
  }

  get countrySelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('Delivery Location'),
      });
    })();
  }

  get popUpOk(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.outOfStockModal).getByRole('button', {
        name: await browser.getTranslation('Ok'),
      });
    })();
  }

  get homeDeliveryRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: await browser.getTranslation('Home delivery'),
      });
    })();
  }

  get allOrderSummaryCells(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      const orderSummary = await this.orderSummary;
      return orderSummary.$$('.column');
    })();
  }

  get informationInOrderSummary(): Promise<string> {
    return (async (): Promise<string> => {
      let textItemsToReturn = '';
      // @ts-ignore
      for (const item of await this.allOrderSummaryCells) {
        const itemText = await item.getText();
        let translatedItemText = await browser.getTranslation({ text: await itemText });
        if (typeof translatedItemText === 'boolean') {
          translatedItemText = await itemText;
        }
        textItemsToReturn = textItemsToReturn.concat(translatedItemText + '\n');
      }
      textItemsToReturn = textItemsToReturn.replace(/\n*$/, ''); // remove the last new line character
      return textItemsToReturn;
    })();
  }
  get outOfStockModalContent(): Promise<WebdriverIO.Element> {
    //todo add translation after B2C2-3990
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.modal__body');
    })();
  }
  get quantity(): Promise<WebdriverIO.Element> {
    //todo add translation after B2C2-3990
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('div.delivery-product > div > div.column-quantity');
    })();
  }

  get giftCard() {
    return (async () => {
      return await $('div.summary-row.giftcard');
    })();
  }

  get oneRemoved(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: '{{quantity}} removed from your basket.',
      });
      translationText = translationText.replace('{{quantity}}', '1');
      return await browser.getByText(new RegExp(escapeRegExp(translationText), 'i'), {});
    })();
  }
  get twoRemoved(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: '{{quantity}} removed from your basket.',
      });
      translationText = translationText.replace('{{quantity}}', '2');
      return await browser.getByText(new RegExp(escapeRegExp(translationText), 'i'), {});
    })();
  }
  get threeRemoved(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: '{{quantity}} removed from your basket.',
      });
      translationText = translationText.replace('{{quantity}}', '3');
      return await browser.getByText(new RegExp(escapeRegExp(translationText), 'i'), {});
    })();
  }
  get fiveRemoved(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: '{{quantity}} removed from your basket.',
      });
      translationText = translationText.replace('{{quantity}}', '5');
      return await browser.getByText(new RegExp(escapeRegExp(translationText), 'i'), {});
    })();
  }

  get sizeEleven(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: 'Size: {{size}}',
      });
      translationText = translationText.replace('{{size}}', '11');
      // @ts-ignore
      return within(await this.outOfStockModalContent).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }

  get sizeTwelve(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: 'Size: {{size}}',
      });
      translationText = translationText.replace('{{size}}', '12');
      // @ts-ignore
      return within(await this.outOfStockModalContent).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }

  get quantityPopUpLine(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $('p.text.text--p.text--medium.text--left');
    })();
  }
}

export default new CheckoutOrderSummary();
