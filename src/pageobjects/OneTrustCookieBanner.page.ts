import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class OneTrustCookieBanner extends Page {
  get cookieBanner() {
    return (async () => {
      return await browser.getByRole('alertdialog', {
        name: await browser.getTranslation('cookie banner'),
      });
    })();
  }
}

export default new OneTrustCookieBanner();
