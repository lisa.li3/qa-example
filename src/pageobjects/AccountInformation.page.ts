import Page from './page';

export class AccountInformation extends Page {
  get passwordTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(
        await browser.getTranslation({ text: 'Password', regex: false })
      );
    })();
  }
  get confirmPasswordTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Confirm password'));
    })();
  }
  get confirmPasswordWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Passwords must match'));
    })();
  }
  get phoneNumberLongWarning(): Promise<WebdriverIO.Element> {
    // TODO: this will match any "Too Long" message, this need to be in a within!
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Too long'));
    })();
  }

  get phoneNumberValueTypeWarning(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Must be a phone number'));
    })();
  }

  get mobilPhoneTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Mobile phone'));
    })();
  }
  get stateProvinceRegionTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('State/ Province/ Region'),
      });
    })();
  }
  get zipPostalCode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Zip/Postal Code'),
      });
    })();
  }
  get country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByLabelText(await browser.getTranslation('Country'));
    })();
  }
  get unitedStates(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('option', {
        name: await browser.getTranslation('United States'),
      });
    })();
  }
  get address2Textbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('Address 2'),
      });
    })();
  }
  get subscriptionApprovalLabel(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('I subscribe to electronic marketing'));
    })();
  }
  get subscriptionApproval(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('checkbox', {
        name: await browser.getTranslation('I subscribe to electronic marketing'),
      });
    })();
  }
  //TODO: Replace with testing library selector when ticket B2C2-3465 is done
  get subscribeCheckbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.control__indicator--tickbox');
    })();
  }
  //TODO: Replace with testing library selector when ticket B2C2-3449 is done
  get passwordErrors(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-password');
    })();
  }
}
export default new AccountInformation();
