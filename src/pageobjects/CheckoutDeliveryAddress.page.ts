import { within } from '@project/testing-library-webdriverio';
import Page from './page';
import { isString } from 'lodash';

export class CheckoutDeliveryAddress extends Page {
  get buyNow(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Buy Now') });
    })();
  }
  get orderNotSavedModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog', {
        name: await browser.getTranslation('Order not saved'),
      });
    })();
  }
  get allRequiredAlerts(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return await browser.getAllByText(await browser.getTranslation('Required'));
    })();
  }
  get popUpOk(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation({ text: 'Ok' }));
    })();
  }
  get emailNotMatch(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('alert', {
        name: await browser.getTranslation("The email address you've entered does not match."),
      });
    })();
  }
  get phoneGDPR(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation(
          'We need your phone number in case we need to contact you about your order. We will never use your phone number for any other purpose.'
        )
      );
    })();
  }
  get countrySelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('Delivery Location'),
      });
    })();
  }
  get stateSelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('combobox', {
        name: await browser.getTranslation('county'),
      });
    })();
  }
  get getStates(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.stateSelect).getAllByRole('option', {});
    })();
  }
  get states(): Promise<string> {
    return (async (): Promise<string> => {
      let states = '';
      let stateText;
      for (const state of await this.getStates) {
        stateText = await state.getText();
        states = states.concat((await stateText) + '\n');
      }
      states = states.replace(/\n*$/, ''); // remove the last new line character
      return states;
    })();
  }
  get cantDeliver2Addresses(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({
        text: 'Unfortunately we can no longer ship to {{invalidDeliveryAddressesLength}} of your delivery addresses',
      });
      translation = translation.replace('{{invalidDeliveryAddressesLength}}', '2');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get cantDeliver3Addresses(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translation = await browser.getTranslation({
        text: 'Unfortunately we can no longer ship to {{invalidDeliveryAddressesLength}} of your delivery addresses',
      });
      translation = translation.replace('{{invalidDeliveryAddressesLength}}', '3');
      return await browser.getByText(new RegExp(`^${translation}`, 'i'));
    })();
  }
  get validUKaddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('address-label-UKaddress');
    })();
  }
  get validUKaddressRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: 'Foo Bar Flat 1, Bar Road, Foobar, London, W11 1BB, United Kingdom',
      });
    })();
  }
  get cantDeliverITaddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByTestId(
        'address-label-ITAaddress'
      );
    })();
  }
  get cantDeliverITaddressRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByRole('radio', {
        name: 'Foo Bar Flat 1, Bar Road, Foobar, county, W11 1BB, Italy',
      });
    })();
  }
  get cantDeliverFRAaddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByTestId(
        'address-label-FRAaddress'
      );
    })();
  }
  get cantDeliverFRAaddressRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByRole('radio', {
        name: 'Foo Bar Flat 1, Bar Road, Foobar, county, 1233321, France',
      });
    })();
  }
  get cantDeliverPOLaddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByTestId(
        'address-label-POLaddress'
      );
    })();
  }
  get cantDeliverPOLaddressRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getByRole('radio', {
        name: 'Flat 1, Bar Road, Foobar, county, 85535, Poland',
      });
    })();
  }
  get cantDeliverContentOpen(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('generic', {
        name: await browser.getTranslation('Collapse content open'),
      });
    })();
  }
  get cantDeliverErrorMessage(): Promise<WebdriverIO.Element | void> {
    return (async (): Promise<WebdriverIO.Element | void> => {
      const translation = (
        await browser.getTranslation('Please order from {{siteUrls}} to deliver to this country')
      ).toString();
      const translation1 = translation.substring(1, translation.lastIndexOf('{') - 4);
      const translation2 = translation.substring(
        translation.lastIndexOf('}') + 1,
        translation.length - 2
      );
      // @ts-ignore
      return await within(await this.cantDeliverContentOpen).getAllByText(
        new RegExp(`^${translation1}.*${translation2}$`),
        {}
      );
    })();
  }
}

export default new CheckoutDeliveryAddress();
