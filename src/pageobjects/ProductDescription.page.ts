import Page from './page';
import { within } from '@project/testing-library-webdriverio';
import { escapeRegExp } from 'lodash';

const SIZE_GUIDE = 'Size Guide';
export class ProductDescription extends Page {
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('main', {});
    })();
  }
  get sizeDropdown(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('combobox');
    })();
  }
  get sizeModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('Please select your size'),
      });
    })();
  }
  get sizeModalClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeModal).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }
  get addToBagButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Add to bag'),
      });
    })();
  }

  get addToWishlistButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Add to wish list'),
      });
    })();
  }

  get removeFromWishlistButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Remove from wish list'),
      });
    })();
  }

  get outOfStockMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation('Sorry, this item is currently out of stock.')
      );
    })();
  }
  get homeBreadcrumb(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const navigation = await browser.getByRole('navigation', {
        name: await browser.getTranslation('breadcrumb'),
      });
      // @ts-ignore
      return await within(navigation).getByRole('link', {
        name: await browser.getTranslation('home'),
      });
    })();
  }
  get categoryBreadcrumb(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const navigation = await browser.getByRole('navigation', {
        name: await browser.getTranslation('breadcrumb'),
      });
      // @ts-ignore
      return await within(navigation).getByText(await browser.getTranslation('Category'));
    })();
  }
  get VYF81I5S8EBreadcrumb(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const navigation = await browser.getByRole('navigation', {
        name: await browser.getTranslation('breadcrumb'),
      });
      // @ts-ignore
      return await within(navigation).getByText(await browser.getTranslation('VYF81I5S8E'));
    })();
  }
  get TESTPRODUCTBreadcrumb(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const navigation = await browser.getByRole('navigation', {
        name: await browser.getTranslation('breadcrumb'),
      });
      // @ts-ignore
      return await within(navigation).getByText(await browser.getTranslation('TESTPRODUCT'));
    })();
  }
  get detailsCareButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('details & care'),
      });
    })();
  }
  get detailsCarePanel(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('DetailsAndCareDrawer');
    })();
  }
  get detailsCarePanelClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.detailsCarePanel).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }

  get descriptionHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('description'),
      });
    })();
  }
  get itemCodeHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('item code'),
      });
    })();
  }
  get compositionHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('composition & care'),
      });
    })();
  }
  get colourFamilyRedImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('red thing desc'),
      });
    })();
  }
  get colourFamilyBlueImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('blue thing desc'),
      });
    })();
  }

  get longSizeEnabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(
        await browser.getTranslation('WAIST:32 (81CM) LENGTH:32 (81CM)')
      );
    })();
  }
  get longSizeDisabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(
        await browser.getTranslation('WAIST:32 (81CM) LENGTH:34 (86CM)')
      );
    })();
  }
  get shortSizeEnabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(
        await browser.getTranslation({ text: 'S' })
      );
    })();
  }
  get shortSizeDisabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(
        await browser.getTranslation({ text: 'M' })
      );
    })();
  }
  get integerSizeEnabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(await browser.getTranslation('6'));
    })();
  }
  get integerSizeDisabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(await browser.getTranslation('7'));
    })();
  }
  get splitSizeEnabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(await browser.getTranslation('S/M'));
    })();
  }
  get splitSizeDisabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeDropdown).getByText(await browser.getTranslation('M/L'));
    })();
  }
  get wishListButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return browser.getByRole('button', {
        name: await browser.getTranslation('add to wish list'),
      });
    })();
  }
  get modalSizeDropdown(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeModal).getByTestId('select-input', {});
    })();
  }
  get modalSizeEnabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.modalSizeDropdown).getByText(
        await browser.getTranslation('size_modal_1')
      );
    })();
  }
  get modalSizeDisabled(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.modalSizeDropdown).getByText(
        await browser.getTranslation('size_modal_2')
      );
    })();
  }
  get modalAddToBagButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeModal).getByRole('button', {
        name: await browser.getTranslation('Add to bag'),
      });
    })();
  }
  get modalWishListButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeModal).getByRole('button', {
        name: await browser.getTranslation('add to wish list'),
      });
    })();
  }
  get addToBagToastMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByText(
        await browser.getTranslation('This item has been successfully added to your shopping bag')
      );
    })();
  }
  get wishListToastMessage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByText(
        await browser.getTranslation('This item has been added to your wish list.')
      );
    })();
  }
  get miniBagDrawer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('MiniBagDrawer', {});
    })();
  }
  get sizeModalInBag(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByText(
        await browser.getTranslation('size_modal_1')
      );
    })();
  }
  get closeModal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.sizeModal).getByRole('button', {
        name: await browser.getTranslation('close'),
      });
    })();
  }
  get undefinedAverageRating(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Rating undefined out of 5 stars'),
      });
    })();
  }
  get zeroAverageRating(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Rating 0 out of 5 stars'),
      });
    })();
  }
  get setAverageRating(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Rating 4.67 out of 5 stars'),
      });
    })();
  }
  get reviewsPanelOpenButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('review-and-rating', {});
    })();
  }
  get reviewsPanelDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('ReviewsDrawer', {});
    })();
  }
  get reviewsPanelCloseButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.reviewsPanelDialog).getByRole('button', {
        name: await browser.getTranslation('close'),
      });
    })();
  }
  get reviewsPanelNextButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.reviewsPanelDialog).getByText(
        await browser.getTranslation('next')
      );
    })();
  }
  get deliveryPanelOpenButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Free Delivery & Free Returns'),
      });
    })();
  }
  get deliveryPanelDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('DeliveryAndReturnsDrawer', {});
    })();
  }
  get deliveryPanelCloseButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.deliveryPanelDialog).getByRole('button', {
        name: await browser.getTranslation('close'),
      });
    })();
  }
  get deliveryPanelCountrySelector(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('cc-select', {});
    })();
  }

  //
  // Selectors for Wear it With (www) and You Might Also Like (yaml) sections
  //

  get wwwRegion(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('wear it with'),
      });
    })();
  }

  get ymalRegion(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('region', {
        name: await browser.getTranslation('you might also like'),
      });
    })();
  }

  get wwwProductList(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return await within(await this.wwwRegion).getAllByRole('listitem', {});
    })();
  }

  get ymalProductList(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.ymalRegion).getAllByRole('listitem', {});
    })();
  }

  // Size Guide

  get sizeGuideButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation(SIZE_GUIDE),
      });
    })();
  }

  get sizeGuideDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation(SIZE_GUIDE),
      });
    })();
  }

  get sizeGuideDialogClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.sizeGuideDialog).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }

  get sizeGuideImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('img', {
        name: await browser.getTranslation(SIZE_GUIDE),
      });
    })();
  }
  get youSave(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      let translationText = await browser.getTranslation({
        text: 'You save {{amount}}',
      });
      // @ts-ignore
      translationText = translationText.replace('{{amount}}', '');
      return await within(await this.main).getByText(
        new RegExp(escapeRegExp(translationText), 'i'),
        {}
      );
    })();
  }
  get wwwProductOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwRegion).getByTestId('product-id-pdp00666a1', {});
    })();
  }
  get wwwProductTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwRegion).getByTestId('product-id-pdp00666a2', {});
    })();
  }
  get wwwProductThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwRegion).getByTestId('product-id-pdp00666a3', {});
    })();
  }
  get ymalProductOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalRegion).getByTestId('product-id-pdp00666a1', {});
    })();
  }
  get ymalProductTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalRegion).getByTestId('product-id-pdp00666a2', {});
    })();
  }
  get ymalProductThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalRegion).getByTestId('product-id-pdp00666a3', {});
    })();
  }
  get wwwYouSaveOnProductOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwProductOne).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get wwwYouSaveOnProductTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwProductTwo).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get wwwYouSaveOnProductThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.wwwProductThree).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get ymalYouSaveOnProductOne(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalProductOne).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get ymalYouSaveOnProductTwo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalProductTwo).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get ymalYouSaveOnProductThree(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ymalProductThree).getByText(
        await browser.getTranslation({
          text: 'You save',
          startsWith: true,
        })
      );
    })();
  }
  get wasPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByTestId('standard-price', {});
      {
      }
    })();
  }
  get nowPrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByTestId('markdown-price', {});
      {
      }
    })();
  }
  get youSavePrice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByTestId('yousave-price', {});
      {
      }
    })();
  }
  get payInThreeKlarna(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const transTemplate = await browser.getTranslation({
        page: 'translation',
        item: 'ProductPage.PayInInstallmentsOfX_plural',
      });
      let textToFind = JSON.stringify(transTemplate);
      textToFind = textToFind.replace('<1>{{count}}</1>', '3');
      textToFind = textToFind.replace('<1>{{installmentPrice}}</1>', '£33.33');
      textToFind = textToFind.replace(' <0>{{name}}</0>.', '');
      textToFind = textToFind.replace(/^"|"$/g, ''); // removes boundary quotes;
      return await browser.getByRole('link', { name: `${textToFind} Klarna.`, exact: false });
    })();
  }

  get klarnaDeferredPaymentOptionText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const transTemplate = await browser.getTranslation({
        page: 'translation',
        item: 'ProductPage.PayInInstallmentsOfX',
      });
      let textToFind = JSON.stringify(transTemplate);
      textToFind = textToFind.replace(' <0>{{name}}</0>.', '');
      textToFind = textToFind.replace(/^"|"$/g, ''); // removes boundary quotes;
      return await browser.getByRole('link', { name: `${textToFind} Klarna.`, exact: false });
    })();
  }
  get klarnaLogo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.payInThreeKlarna).getByRole('img', {
        name: await browser.getTranslation('klarna'),
      });
    })();
  }
  get payInFourClearPay(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const transTemplate = await browser.getTranslation({
        page: 'translation',
        item: 'ProductPage.PayInInstallmentsOfX_plural',
      });
      let textToFind = JSON.stringify(transTemplate);
      textToFind = textToFind.replace('<1>{{count}}</1>', '4');
      textToFind = textToFind.replace('<1>{{installmentPrice}}</1>', '£25.00');
      textToFind = textToFind.replace(' <0>{{name}}</0>.', '');
      textToFind = textToFind.replace(/^"|"$/g, ''); // removes boundary quotes;
      return await browser.getByRole('link', { name: `${textToFind} Clearpay.`, exact: false });
    })();
  }
  get clearpayDeferredPaymentOptionText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const transTemplate = await browser.getTranslation({
        page: 'translation',
        item: 'ProductPage.PayInInstallmentsOfX',
      });
      let textToFind = JSON.stringify(transTemplate);
      textToFind = textToFind.replace(' <0>{{name}}</0>.', '');
      textToFind = textToFind.replace(/^"|"$/g, ''); // removes boundary quotes;
      return await browser.getByRole('link', { name: `${textToFind} Clearpay.`, exact: false });
    })();
  }
  get clearpayLogo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.payInFourClearPay).getByRole('img', {
        name: await browser.getTranslation('clearpay'),
      });
    })();
  }
  get descriptionMeta(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await $("meta[name='description']");
    })();
  }
}

export default new ProductDescription();
