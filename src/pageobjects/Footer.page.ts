import Page from './page';
import { within } from '@project/testing-library-webdriverio';
import moment from 'moment';

export class Footer extends Page {
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('contentinfo', {});
    })();
  }

  get customerServicesColumn(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const customerServicesTitle = await within(await this.main).getByText(
        await browser.getTranslation('Customer Services')
      );
      return await customerServicesTitle.parentElement();
    })();
  }
  get customerServicesMobileButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Customer Services'),
      });
    })();
  }
  get customerServicesMobile(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const customerServicesButton = await this.customerServicesMobileButton;
      return await customerServicesButton.parentElement();
    })();
  }

  get footerScroll(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('footerscroll'));
    })();
  }

  get siteSelectorButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //site button at the boottom left
      return browser.getByTestId('countrySelect', {});
    })();
  }

  get sitePanelClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const button = await browser.getAllByRole('button', {
        name: new RegExp('close', 'i'),
      });
      return button[0];
    })();
  }

  get storeLocator(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('stores directory'));
    })();
  }

  get footerInformation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('information'),
      });
    })();
  }
  get copyright() {
    return (async () => {
      return await browser.getByText(
        await browser.getTranslation(`project ${moment().format('YYYY')}`)
      );
    })();
  }
}

export default new Footer();
