import { within } from '@project/testing-library-webdriverio';
import Page from './page';

export class CheckoutRedeemGiftCard extends Page {
  get RedeemGiftCardButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('redeem a gift card'),
      });
    })();
  }

  get EnterCardNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('enter card number'),
      });
    })();
  }

  get EnterPin(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('enter pin'),
      });
    })();
  }

  get NeedHelpButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('need help'),
      });
    })();
  }

  get NeedHelpPopupText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation(
          'your card number is displayed on the back of your gift card. scratch off the grey area with a coin to reveal your pin.'
        )
      );
    })();
  }

  get ModalContent(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const modals = await browser.getAllByRole('dialog', {});
      for (const modal of modals) {
        if (await modal.isDisplayed()) {
          return modal;
        }
      }
      throw Error('No modals displayed');
    })();
  }

  get NeedHelpPopupCloseButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.ModalContent).getByRole('button', {
        name: await browser.getTranslation('close'),
      });
    })();
  }
}

export default new CheckoutRedeemGiftCard();
