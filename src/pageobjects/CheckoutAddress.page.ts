import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class CheckoutAddress extends Page {
  get billingAddressSameTick(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation('My billing address is the same as my delivery address')
      );
    })();
  }
  get delivery_Form(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('heading', {
        name: await browser.getTranslation('Delivery address'),
      });
    })();
  }
  get manuallyEnterYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }
  get addressTitle(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('textbox', {
        name: await browser.getTranslation('address title'),
      });
    })();
  }
  get country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('combobox', {
        name: await browser.getTranslation('country'),
      });
    })();
  }
  get addressTitleError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('span#errors-nickname');
    })();
  }
  get deleteButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('delete'),
      });
    })();
  }
  get manageDeliveryAddressesTitle(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Manage Delivery Addresses'));
    })();
  }

  //
  // Targeted Selectors for the Delivery Address Section - Loqate address finder story - B2C2-192
  //

  get deliveryAddressSection(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $('.delivery-address');
    })();
  }

  get deliveryAddressFinderError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const addressFinderWrapper = (await this.deliveryAddressSection).$('.address-finder-wrapper');
      return addressFinderWrapper.$('.form__msg--error');
    })();
  }

  get deliveryAddressFinder(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const deliverySection = await this.deliveryAddressSection;
      return (await deliverySection).$('#addressSearch');
    })();
  }

  get deliveryManualAddressEntryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.deliveryAddressSection).getByRole('button', {
        name: await browser.getTranslation({ text: 'Manually enter your address' }),
      });
    })();
  }

  get deliveryFindYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.deliveryAddressSection).getByRole('button', {
        name: await browser.getTranslation({ text: 'Find Your Address' }),
      });
    })();
  }

  //
  // Targeted Selectors for Billing Address Section - Loqate address finder story - B2C2-192
  //

  get billingAddressSection(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return $('.billing');
    })();
  }

  get billingAddressFinderError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const addressFinderWrapper = (await this.billingAddressSection).$('.address-finder-wrapper');
      return addressFinderWrapper.$('.form__msg--error');
    })();
  }

  get billingManualAddressEntryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }

  get billingAddressFinderTextInput(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const billingAddressSection = await this.billingAddressSection;
      return billingAddressSection.$('#addressSearch');
    })();
  }

  get billingFindYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('button', {
        name: await browser.getTranslation({ text: 'Find Your Address' }),
      });
    })();
  }

  // Common Selectors for the Address Finder dropdown - Loqate address finder story - B2C2-192

  get addressFinderContainer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const addressFinderContainer = await $('.address-finder-container');
      return await addressFinderContainer;
    })();
  }

  get suggestAddressList(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      await browser.pause(1000);
      // @ts-ignore
      return await within(await this.addressFinderContainer).getByRole('list', {});
    })();
  }

  get suggestedAddressListItems(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.suggestAddressList).getAllByRole('button', {});
    })();
  }

  get suggestedAddressListItemsText(): Promise<string> {
    return (async (): Promise<string> => {
      let addressesText = '';
      // @ts-ignore
      const addressList = await this.suggestedAddressListItems;
      console.log(`Address list items: ${addressList.length}, ${addressList}`);
      for (const address of addressList) {
        addressesText = addressesText.concat((await address.getAttribute('title')) + '\n');
      }
      addressesText = addressesText.replace(/\n*$/, ''); // remove the last new line character
      return addressesText;
    })();
  }
}
export default new CheckoutAddress();
