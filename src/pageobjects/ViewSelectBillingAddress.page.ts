import Page from './page';

export class ViewSelectBillingAddress extends Page {
  get homeDeliveryRadio(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('radio', {
        name: await browser.getTranslation('Home delivery'),
      });
    })();
  }
  get seededNameDeliveryAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //TODO: seeded data!!
      return (await browser.getAllByText(await browser.getTranslation('Foo Bar')))[0];
    })();
  }
  get seededNameBillingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //TODO: seeded data!!
      return (await browser.getAllByText(await browser.getTranslation('Foo Bar')))[1];
    })();
  }
  get manualNameDeliveryAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //TODO: needs within!
      return (await browser.getAllByText(await browser.getTranslation('Lastname')))[0];
    })();
  }
  get manualNameBillingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      //TODO: needs within!
      return (await browser.getAllByText(await browser.getTranslation('Lastname')))[1];
    })();
  }
  get seededAddressDeliveryAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(
          //TODO: seeded data!!
          await browser.getTranslation('Flat 1, Bar Road, Foobar, London, W11 1BB, United Kingdom')
        )
      )[0];
    })();
  }
  get seededAddressBillingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(
          //TODO: seeded data!!
          await browser.getTranslation('Flat 1, Bar Road, Foobar, London, W11 1BB, United Kingdom')
        )
      )[1];
    })();
  }
  get manualAddressDeliveryAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(
          //TODO: seeded data!!
          await browser.getTranslation('address1, address2, city, county, GL51 9NH, United Kingdom')
        )
      )[0];
    })();
  }
  get manualAddressBillingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(
          //TODO: seeded data!!
          await browser.getTranslation('address1, address2, city, county, GL51 9NH, United Kingdom')
        )
      )[1];
    })();
  }
  get billingAddressHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Billing Address'),
      });
    })();
  }
  get editAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Edit Address'),
      });
    })();
  }
  get firstName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('First Name'));
    })();
  }
  get lastName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Last Name'));
    })();
  }
  get addressLine1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Address 1'));
    })();
  }
  get addressLine2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Address 2'));
    })();
  }
  get city(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('City'));
    })();
  }
  get county(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('State/ Province/ Region'));
    })();
  }
  get postcode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Zip/Postal Code'));
    })();
  }
  get country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByLabelText(await browser.getTranslation('Country'));
    })();
  }
  get countryUK(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('option', {
        name: await browser.getTranslation('United Kingdom'),
      });
    })();
  }
  get accountInformationHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Your Account Information'),
      });
    })();
  }
  get manuallyEnterYourAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }
  get saveButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('Save'),
      });
    })();
  }
  get deliveryOptionsHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Delivery Options'),
      });
    })();
  }
  get deliveryAddressHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('Delivery Address'),
      });
    })();
  }
  get standardDeliveryButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('standard delivery'),
      });
    })();
  }
  get secondAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const addresses = await browser.getAllByTestId(new RegExp(/address-label/i), {});
      return addresses[1];
    })();
  }
}
export default new ViewSelectBillingAddress();
