import Page from './page';

export class Registration extends Page {
  get registerButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('button', {
        name: await browser.getTranslation('register'),
      });
    })();
  }
  get duplicateEmailError(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (
        await browser.getAllByText(
          await browser.getTranslation('THIS EMAIL ADDRESS IS ALREADY IN USE. PLEASE TRY ANOTHER')
        )
      )[0];
    })();
  }
  get termsAndConditionsLink(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByText(await browser.getTranslation('Terms and Conditions')))[0];
    })();
  }
  get termsAndConditions(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('terms-checkbox', {});
    })();
  }
  get termsAndConditionsParent(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await (await browser.getByTestId('terms-checkbox', {})).parentElement();
    })();
  }
  get allRequiredAlerts(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      return await browser.getAllByText(await browser.getTranslation('Required'));
    })();
  }
  get firstAddressSuggestion(): Promise<Promise<WebdriverIO.Element> | undefined> {
    return (async (): Promise<Promise<WebdriverIO.Element> | undefined> => {
      const suggestions = $$('.address-finder-container__suggestions > li');
      return suggestions[0];
    })();
  }
}
export default new Registration();
