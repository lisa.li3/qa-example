import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class Navigation extends Page {
  get navigationMain(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('navigation', {
        name: await browser.getTranslation('navigation'),
      });
    })();
  }
  get shortURLsProductHeading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('heading', {
        name: await browser.getTranslation('test-product-name'),
      });
    })();
  }
  get burgerButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('button', {
        name: await browser.getTranslation('Open'),
      });
    })();
  }
  get searchBar(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('textbox', {});
    })();
  }
  get searchResult(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('listbox', {});
    })();
  }

  get searchResultItems(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.searchResult).findAllByRole('option', {});
    })();
  }

  get searchBarBackdrop(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('backdrop', {});
    })();
  }
  get searchBarClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Reset') });
    })();
  }

  get searchIcon(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Search') });
    })();
  }

  get accountButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('link', {
        name: await browser.getTranslation('My Account'),
      });
    })();
  }

  get wishlistButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('link', {
        name: await browser.getTranslation('Wish List'),
      });
    })();
  }

  get basketButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('button', {
        name: await browser.getTranslation('Basket'),
      });
    })();
  }

  get basketButtonIconText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.basketButton).getByTestId('icon-text', {});
    })();
  }

  get projectLogo(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.navigationMain).getByRole('img', {
        name: await browser.getTranslation('project Logo - Back to Home'),
      });
    })();
  }

  get notFoundURL() {
    return (async () => {
      return browser.getByTitle(/Credit History - project/i, {});
    })();
  }
}

export default new Navigation();
