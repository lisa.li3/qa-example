import { Checkout_OrderSummary } from '../Checkout/OrderSummary.page';

export class CheckoutConfirmation_OrderSummary extends Checkout_OrderSummary {
  // Nothing added
}
export default new CheckoutConfirmation_OrderSummary();
