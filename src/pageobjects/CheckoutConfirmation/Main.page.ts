import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { replace, trim } from 'lodash';

export class CheckoutConfirmation_Main extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.checkout-confirmation');
    })();
  }

  //
  // Elements
  //
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('heading', {
        name: await browser.getTranslation('Thank you for your order'),
      });
    })();
  }

  get orderReference(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const translatedText = await browser.getTranslation({
        text: 'Your order reference is',
      });
      // @ts-ignore
      const fullLine = await within(await this.main).getByText(
        new RegExp(`^${translatedText}`, 'i')
      );
      const orderNumber = trim(replace(await fullLine.getText(), translatedText, ''));
      // @ts-ignore
      return await within(fullLine).getByText(orderNumber);
    })();
  }

  get confirmationEmail(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const translatedText = await browser.getTranslation({
        text: 'A confirmation email has been sent to',
      });
      // @ts-ignore
      const fullLine = await within(await this.main).getByText(
        new RegExp(`^${translatedText}`, 'i')
      );
      const email = trim(replace(await fullLine.getText(), translatedText, ''));
      // @ts-ignore
      return await within(fullLine).getByText(email);
    })();
  }
}
export default new CheckoutConfirmation_Main();
