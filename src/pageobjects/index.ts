import Login from './Login.page';
import Header from './Header.page';
import HelpCentre from './HelpCentre.page';
import Footer from './Footer.page';
import GlobalBanner from './GlobalBanner.page';
import MyAccount from './MyAccount.page';
import Navigation from './Navigation.page';
import NavigationDrawer from './NavigationDrawer.page';

export default {
  GlobalBanner,
  Footer,
  Header,
  Login,
  MyAccount,
  Navigation,
  HelpCentre,
  NavigationDrawer,
};
