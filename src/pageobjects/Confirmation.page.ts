import Page from './page';

export class Confirmation extends Page {
  get orderConfirmationText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Thank you for your order'));
    })();
  }
  get orderReferenceText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Your order reference is'));
    })();
  }
  get orderNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('project_'));
    })();
  }

  get confirmationOrderSummary(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const orderDetails = await browser.getTranslation({ text: 'Order Summary' });
      return await browser.findByRole(
        'region',
        { name: new RegExp(orderDetails, 'i') },
        { timeout: browser.config.waitforTimeout_find }
      );
    })();
  }

  get allConfirmationOrderSummaryColumns(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      const orderSummary = await this.confirmationOrderSummary;
      return orderSummary.$$('.column');
    })();
  }

  get informationInConfirmationOrderSummary(): Promise<string> {
    return (async (): Promise<string> => {
      let textItemsToReturn = '';
      // @ts-ignore
      for (const item of await this.allConfirmationOrderSummaryColumns) {
        const itemText = await item.getText();
        let translatedItemText = await browser.getTranslation({ text: await itemText });
        if (typeof translatedItemText === 'boolean') {
          translatedItemText = await itemText;
        }
        textItemsToReturn = textItemsToReturn.concat(translatedItemText + '\n');
      }
      textItemsToReturn = textItemsToReturn.replace(/\n*$/, ''); // remove the last new line character
      return textItemsToReturn;
    })();
  }
}
export default new Confirmation();
