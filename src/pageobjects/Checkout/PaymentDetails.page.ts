import { within } from '@project/testing-library-webdriverio';
import Page from '../page';

export class Checkout_PaymentDetails extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const heading = await browser.getByRole('heading', {
        name: await browser.getTranslation('Payment Details'),
      });
      return await heading.parentElement();
    })();
  }

  //
  // Elements
  //
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('heading', {
        name: await browser.getTranslation('Payment Details'),
      });
    })();
  }

  get buyNow(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Buy now'),
      });
    })();
  }

  //
  // Fields
  //
  get cardNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__cardNumber__input > iframe');
    })();
  }

  get expiryDate(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__exp-date__input > iframe');
    })();
  }
  get cvc(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.adyen-checkout__card__cvc__input > iframe');
    })();
  }
  get name(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $("[placeholder='J\\. Smith']");
    })();
  }

  // Generic input which appears cardNumber, expiryDate, cvc
  get input(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.js-iframe-input');
    })();
  }
}
export default new Checkout_PaymentDetails();
