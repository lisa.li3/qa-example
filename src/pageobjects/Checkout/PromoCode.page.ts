import { within } from '@project/testing-library-webdriverio';
import Page from '../page';

export class Checkout_PromoCode extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const button = await browser.getByRole('button', {
        name: await browser.getTranslation({ text: 'Promo Code' }),
      });
      return await button.parentElement();
    })();
  }

  //
  // Elements
  //
  get accordion(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return this.main;
    })();
  }

  get button(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Promo Code'),
      });
    })();
  }

  get promoCode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Enter promo code', startsWith: true }),
      });
    })();
  }
  get promoCodeValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.promoCode).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get apply(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Apply'),
      });
    })();
  }

  get notice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('alert');
    })();
  }
}
export default new Checkout_PromoCode();
