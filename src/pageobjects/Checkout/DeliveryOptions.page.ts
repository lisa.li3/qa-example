import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { nth } from 'lodash';

export class Checkout_DeliveryOptions extends Page {
  //
  // Main
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('section.delivery-options');
    })();
  }

  //
  // Elements
  //
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('heading', {
        name: await browser.getTranslation('Delivery Options'),
      });
    })();
  }

  get splitDeliveryNotification(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // TODO: Request TEST ID: checkout-deliveryOptions-splitDelivery
      await this.checkForTestId('checkout-deliveryOptions-splitDelivery');
      return await $('.delivery-alert');
    })();
  }

  get confirmRemove(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation({ text: 'Yes' }),
      });
    })();
  }

  //
  // Lists
  //
  delivery__LIST({ index }: { index: number }): Promise<WebdriverIO.Element | undefined> {
    return (async (): Promise<WebdriverIO.Element | undefined> => {
      return nth(await $$('.checkout-delivery'), index);
    })();
  }

  product__LIST({
    index,
    parent,
  }: {
    index: number;
    parent: WebdriverIO.Element;
  }): Promise<WebdriverIO.Element | undefined> {
    return (async (): Promise<WebdriverIO.Element | undefined> => {
      return nth(await parent.$$('.delivery-product'), index);
    })();
  }

  //
  // Deep Elements
  //
  $removeProduct({ parent }: { parent: WebdriverIO.Element }): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await within(parent).getByRole('button', {
        name: await browser.getTranslation('Remove'),
      });
    })();
  }
}
export default new Checkout_DeliveryOptions();
