import { within } from '@project/testing-library-webdriverio';
import Page from '../page';

export class Checkout_RedeemAGiftCard extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const button = await browser.getByRole('button', {
        name: await browser.getTranslation({ text: 'Redeem a Gift Card' }),
      });
      return await button.parentElement();
    })();
  }

  //
  // Elements
  //
  get accordion(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return this.main;
    })();
  }

  get button(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Redeem a Gift Card'),
      });
    })();
  }

  get cardNumber(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Enter card number', startsWith: true }),
      });
    })();
  }
  get cardNumberValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.cardNumber).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get pin(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Enter PIN', startsWith: true }),
      });
    })();
  }
  get pinValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.pin).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get redeem(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation({ text: 'Redeem' }),
      });
    })();
  }

  get needHelp(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Need Help?'),
      });
    })();
  }

  get notice(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('alert');
    })();
  }

  get needHelpDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('#modal-gift-card-help');
    })();
  }

  get needHelpDialogClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.needHelpDialog).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }
}
export default new Checkout_RedeemAGiftCard();
