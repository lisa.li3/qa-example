import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { nth } from 'lodash';

export class Checkout_DeliveryAddress extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('section.delivery-address');
    })();
  }

  //
  // Elements
  //
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('heading', {
        name: await browser.getTranslation('Delivery Address'),
      });
    })();
  }

  get manuallyEnterYourAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }

  //
  // Fields
  //
  get firstName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'First Name', startsWith: true }),
      });
    })();
  }
  get firstNameValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.firstName).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get lastName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Last Name', startsWith: true }),
      });
    })();
  }
  get lastNameValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.lastName).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get phone(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Phone', startsWith: true }),
      });
    })();
  }
  get phoneValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.phone).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get email(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Email Address', startsWith: true }),
      });
    })();
  }
  get emailValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.email).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get confirmEmail(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Confirm Email Address', startsWith: true }),
      });
    })();
  }
  get confirmEmailValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.confirmEmail).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get address1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Address 1', startsWith: true }),
      });
    })();
  }
  get address1Validation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.address1).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get address2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Address 2', startsWith: true }),
      });
    })();
  }
  get address2Validation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.address2).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get city(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'City', startsWith: true }),
      });
    })();
  }
  get cityValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.city).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get state(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'STATE/ PROVINCE/ REGION', startsWith: true }),
      });
    })();
  }
  get stateValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.state).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }

  get zip(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('textbox', {
        name: await browser.getTranslation({ text: 'Zip/Postal Code', startsWith: true }),
      });
    })();
  }
  get zipValidation(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const input = await (await this.zip).parentElement();

      // @ts-ignore
      return await within(input).getByRole('alert');
    })();
  }
}
export default new Checkout_DeliveryAddress();
