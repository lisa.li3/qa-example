import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { nth } from 'lodash';

export class Checkout_BillingAddress extends Page {
  //
  // Main
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // TODO: Request TEST ID: checkout-billingAddress
      await this.checkForTestId('checkout-billingAddress');
      return await $('.billing');
    })();
  }

  //
  // Elements
  //
  get editAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('link', {
        name: await browser.getTranslation('Edit Address'),
      });
    })();
  }

  //
  // Lists
  //
  address__LIST({ index }): Promise<WebdriverIO.Element | undefined> {
    return (async (): Promise<WebdriverIO.Element | undefined> => {
      return nth(await $$('.billing .options-list-option'), index);
    })();
  }
}
export default new Checkout_BillingAddress();
