import { within } from '@project/testing-library-webdriverio';
import Page from '../page';

export class Checkout_StaffDiscount extends Page {
  //
  // Container
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('dialog', {
        name: await browser.getTranslation('Staff discount'),
      });
    })();
  }

  //
  // Elements
  //
  get dialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return this.main;
    })();
  }

  get checkbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('checkbox');
    })();
  }

  get proceed(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.main).getByRole('button', {
        name: await browser.getTranslation('Proceed'),
      });
    })();
  }
}
export default new Checkout_StaffDiscount();
