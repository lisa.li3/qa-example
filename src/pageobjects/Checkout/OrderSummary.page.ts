import { within } from '@project/testing-library-webdriverio';
import Page from '../page';
import { map, nth } from 'lodash';

export class Checkout_OrderSummary extends Page {
  //
  // Main
  //
  get main(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // TODO: Request TEST ID: checkout-orderSummary
      await this.checkForTestId('checkout-orderSummary');
      return await $('.order-summary');
    })();
  }

  //
  // Elements
  //
  get heading(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.main).getByRole('heading', {
        name: await browser.getTranslation('Order Summary'),
      });
    })();
  }

  get subTotal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Subtotal'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get shippingFee(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Shipping Fee'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get discount(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Discount'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get giftCard(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Gift Card'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get promotionCode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Promotion Code'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get accountCredit(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation('Account Credit'),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  get total(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const selectedRow = await within(await this.main).getByRole('row', {
        name: await browser.getTranslation({ text: 'Total', startsWith: true }),
      });
      // @ts-ignore
      return (await within(selectedRow).getAllByRole('cell'))[1];
    })();
  }

  //
  // Lists
  //
  delivery__LIST({ index }: { index: number }): Promise<WebdriverIO.Element | undefined> {
    return (async (): Promise<WebdriverIO.Element | undefined> => {
      // @ts-ignore
      const innerTags = await within(await this.main).getAllByText(
        await browser.getTranslation('Expected delivery:')
      );
      return nth(
        await Promise.all(map(innerTags, async (innerTag) => await innerTag.parentElement())),
        index
      );
    })();
  }

  deduction__LIST({ index }: { index: number }): Promise<WebdriverIO.Element | undefined> {
    return (async (): Promise<WebdriverIO.Element | undefined> => {
      const allDeductions = await $$('.order-summary .summary-row.deduction');
      return nth(allDeductions, index);
    })();
  }
}
export default new Checkout_OrderSummary();
