import Page from './page';

export class OrderHistory extends Page {
  get backButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('back'));
    })();
  }

  get firstOrder(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const firstOrder = await browser.findAllByText(
        /project_[0-9]*/,
        {
          exact: false,
        },
        { timeout: 7000 }
      );
      return firstOrder[0];
    })();
  }

  get orderHistoryOrderSummary(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const orderDetails = await browser.getTranslation({ text: 'Order Summary' });
      return await browser.findByRole(
        'region',
        { name: new RegExp(orderDetails, 'i') },
        { timeout: browser.config.waitforTimeout_find }
      );
    })();
  }

  get orderHistoryOrderSummaryColumns(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      const orderSummary = await this.orderHistoryOrderSummary;
      return orderSummary.$$('.column');
    })();
  }

  get informationInOrderHistoryOrderSummary(): Promise<string> {
    return (async (): Promise<string> => {
      let textItemsToReturn = '';
      // @ts-ignore
      for (const item of await this.orderHistoryOrderSummaryColumns) {
        const itemText = await item.getText();
        let translatedItemText = await browser.getTranslation({ text: await itemText });
        if (typeof translatedItemText === 'boolean') {
          translatedItemText = await itemText;
        }
        textItemsToReturn = textItemsToReturn.concat(translatedItemText + '\n');
      }
      textItemsToReturn = textItemsToReturn.replace(/\n*$/, ''); // remove the last new line character
      return textItemsToReturn;
    })();
  }
}
export default new OrderHistory();
