import Page from './page';

export class Header extends Page {
  get basketButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Basket') });
    })();
  }
  get miniBagIcon(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', { name: await browser.getTranslation('Basket') });
    })();
  }
  get bannerMain(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('global-banner', {});
    })();
  }
  get BurgerMenu(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('Open') });
    })();
  }
  get SmokeMenu(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('SmokeMenu') });
    })();
  }
  get SmokeGuest(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      const selector = await browser.getByText(await browser.getTranslation('Smokeguest'));
      return await selector.parentElement();
    })();
  }
}

export default new Header();
