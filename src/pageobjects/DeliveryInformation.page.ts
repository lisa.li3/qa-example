import Page from './page';

export class DeliveryInformation extends Page {
  get addNewAddressButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('link', { name: await browser.getTranslation('add new address') });
    })();
  }
  get addressTestNickname(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return (await browser.getAllByText(await browser.getTranslation('2nd Address')))[0];
    })();
  }
  get country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByRole('combobox', { name: await browser.getTranslation('Country') });
    })();
  }
}
export default new DeliveryInformation();
