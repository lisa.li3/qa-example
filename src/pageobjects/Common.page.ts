import Page from './page';

export class Common extends Page {
  // Blank Class to allow tests to access common Page objects without first extending
}

export default new Common();
