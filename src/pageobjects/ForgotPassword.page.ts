import Page from './page';

export class ForgotPassword extends Page {
  get submitButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('submit'));
    })();
  }

  get registerButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('register for an account'), {});
    })();
  }
}
export default new ForgotPassword();
