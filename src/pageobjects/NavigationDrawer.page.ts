import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class NavigationDrawer extends Page {
  get drawer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('NavigationDrawer', {});
    })();
  }
  get searchField(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.drawer).getByRole('textbox', {});
    })();
  }
  get bigCategory(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.drawer).getByRole('button', {
        name: new RegExp('Big Category', 'i'),
      });
    })();
  }
  get sale(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.drawer).getByRole('button', {
        name: await browser.getTranslation('Sale'),
      });
    })();
  }
  get saleLimited(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.drawer).getByRole('button', {
        name: await browser.getTranslation('Limited'),
      });
    })();
  }

  get backDepth2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const backButtons = await within(await this.drawer).getAllByLabelText(
        await browser.getTranslation('Back')
      );
      for (const button of backButtons) {
        const isDisplayed = await button.isDisplayed();
        if (isDisplayed) {
          return button;
        }
      }
      throw new Error('No Back button to return');
    })();
  }

  get closeDepth2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const closeButtons = await within(await this.drawer).getAllByLabelText(
        await browser.getTranslation('Close')
      );
      for (const button of closeButtons) {
        const isDisplayed = await button.isDisplayed();
        if (isDisplayed) {
          return button;
        }
      }
      throw new Error('No Close button to return');
    })();
  }

  get myAccount(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', { name: await browser.getTranslation('My Account') });
    })();
  }
  get wishList(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.drawer).getByRole('link', {
        name: await browser.getTranslation('Wish List'),
      });
    })();
  }
  get storeLocator(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Store Locator'),
      });
    })();
  }
}

export default new NavigationDrawer();
