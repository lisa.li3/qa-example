import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class NewsletterSignup extends Page {
  get thankYouDialog(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('dialog');
    })();
  }
  get thankYouDialogClose(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.thankYouDialog).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }
  get thankYou(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Thank you for subscribing.'));
    })();
  }
  get subscribe(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', { name: await browser.getTranslation('subscribe') });
    })();
  }
}

export default new NewsletterSignup();
