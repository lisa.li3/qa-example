import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class PopinBasket extends Page {
  get miniBasketButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await browser.getByRole('button', { name: await browser.getTranslation('Basket') });
    })();
  }

  get miniBagDrawer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByTestId(
        'MiniBagDrawer',
        {},
        { timeout: browser.config.waitforTimeout_clientMutation }
      );
    })();
  }

  get checkoutButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByRole('link', {
        name: await browser.getTranslation('Checkout'),
      });
    })();
  }

  get wishlistButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByRole('link', {
        name: await browser.getTranslation('Wish List'),
      });
    })();
  }

  get closeButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByRole('button', {
        name: await browser.getTranslation('Close'),
      });
    })();
  }

  // The following selectors are specific to the QATB testing under the ProductListingPage.feature file - Not entirely happy with these but will need to wait until we have a better selector strategy for the mini-basket
  // ToDo - Change these selectors when there is a better means of being able to grab items in the min-bag using testing library

  get oneSizeProductNameInBasket(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByRole('heading', {
        name: await browser.getTranslation('Product - 1Size'),
      });
    })();
  }

  get smallSizeInBasket(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const sizeInBasket = within(await this.miniBagDrawer).getByText(
        await browser.getTranslation('small')
      );
      return await sizeInBasket;
    })();
  }

  get incrementButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Increase quantity'),
      });
    })();
  }

  get removeButtonSingleItemInBasket(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getByRole('button', {
        name: await browser.getTranslation('remove'),
      });
    })();
  }

  get removeButtons(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.miniBagDrawer).getAllByRole('button', {
        name: /remove/i,
      });
    })();
  }

  get miniBagProductsInfoGridItems(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return $('.drawer').$$('.grid__item:not(.block__image)');
    })();
  }

  get miniBagFooter(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return $('section.bg--brand-three.p--lg');
    })();
  }

  get miniBag3for2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.miniBagFooter).getByText(await browser.getTranslation('3 for 2'));
    })();
  }

  get miniBag3for1999(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.miniBagFooter).getByText(
        await browser.getTranslation('3 for £19.99')
      );
    })();
  }

  get miniBag2for1499(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return within(await this.miniBagFooter).getByText(
        await browser.getTranslation('2 for £14.99')
      );
    })();
  }

  get itemsInFooterSection(): Promise<string> {
    return (async (): Promise<string> => {
      let textItemsToReturn = '';
      // @ts-ignore
      const textItemsInFooter = await (await $('section.bg--brand-three.p--lg')).$$('.text');
      for (const item of textItemsInFooter) {
        const itemText = await item.getText();
        let translatedItemText = await browser.getTranslation({ text: await itemText });
        if (typeof translatedItemText === 'boolean') {
          translatedItemText = await itemText;
        }
        textItemsToReturn = textItemsToReturn.concat(translatedItemText + '\n');
      }
      textItemsToReturn = textItemsToReturn.replace(/\n*$/, ''); // remove the last new line character
      return textItemsToReturn;
    })();
  }
}

export default new PopinBasket();
