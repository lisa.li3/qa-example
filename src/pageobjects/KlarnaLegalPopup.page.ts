import Page from './page';

export class KlarnaLegalPopup extends Page {
  get klarnaPaymentMethod(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('Pay later with Klarna.'));
    })();
  }
  get paymentTermsText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(await browser.getTranslation('For more information about'));
    })();
  }
  get paymentTermsLink(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('click here'),
      });
    })();
  }
  get sana(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.sana');
    })();
  }
}

export default new KlarnaLegalPopup();
