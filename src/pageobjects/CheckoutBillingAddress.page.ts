import { within } from '@project/testing-library-webdriverio';
import Page from './page';

export class CheckoutBillingAddress extends Page {
  get billingAddressSection(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('section.billing');
    })();
  }

  get navigationMain(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('navigation', {
        name: await browser.getTranslation('Navigation'),
      });
    })();
  }
  get billingAddressSameTick(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(
        await browser.getTranslation('My billing address is the same as my delivery address')
      );
    })();
  }

  get billingAddressCheckbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByTestId('isBillingSameAsDelivery-input');
    })();
  }
  get firstName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('First Name')
      );
    })();
  }
  get lastName(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('Last Name')
      );
    })();
  }
  get addressLine1(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('Address 1')
      );
    })();
  }
  get addressLine2(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('Address 2')
      );
    })();
  }
  get city(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      const cities = await within(await this.billingAddressSection).getAllByText(
        await browser.getTranslation('City')
      );
      return cities[0];
    })();
  }
  get postcode(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('ZIP/POSTAL CODE')
      );
    })();
  }
  get county(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText(
        await browser.getTranslation('STATE/ PROVINCE/ REGION')
      );
    })();
  }
  get manualAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('button', {
        name: await browser.getTranslation('Manually enter your address'),
      });
    })();
  }
  get billingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('heading', {
        name: await browser.getTranslation('Billing Address'),
      });
    })();
  }
  get country(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByPlaceholderText(
        await browser.getTranslation('Country')
      );
    })();
  }
  get UKbillingAddress(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByText('United Kingdom');
    })();
  }
  get stateTextbox(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('textbox', {
        name: await browser.getTranslation('STATE/ PROVINCE/ REGION'),
      });
    })();
  }
  get stateSelect(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.billingAddressSection).getByRole('combobox', {
        name: await browser.getTranslation('STATE/ PROVINCE/ REGION*'),
      });
    })();
  }

  get getStates(): Promise<WebdriverIO.Element[]> {
    return (async (): Promise<WebdriverIO.Element[]> => {
      // @ts-ignore
      return within(await this.stateSelect).getAllByRole('option', {});
    })();
  }
  get states(): Promise<string> {
    return (async (): Promise<string> => {
      let states = '';
      let stateText;
      for await (const state of await this.getStates) {
        stateText = await state.getText();
        states = states.concat((await stateText) + '\n');
      }
      states = states.replace(/\n*$/, ''); // remove the last new line character
      return states;
    })();
  }
  get stateErrorAlert(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('alert', {
        name: await browser.getTranslation('Too long'),
      });
    })();
  }
}

export default new CheckoutBillingAddress();
