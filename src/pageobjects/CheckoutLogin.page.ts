import Page from './page';

export class CheckoutLogin extends Page {
  get continueAsGuest(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return browser.getByText(await browser.getTranslation('Continue as guest'));
    })();
  }
  get paypalCheckoutBtn(): Promise<WebdriverIO.Element> {
    return $('[id^="zoid-paypal-buttons"]');
  }
}

export default new CheckoutLogin();
