import Page from './page';
import { within } from '@project/testing-library-webdriverio';

export class ShoppingBag extends Page {
  get shoppingBagGrid(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('grid');
    })();
  }
  get subtotalContainer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $$('.summary-row')[0];
    })();
  }
  get totalContainer(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await $('.summary-row.total');
    })();
  }
  get subtotal(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.subtotalContainer).getByText(
        await browser.getTranslation('Subtotal')
      );
    })();
  }
  get total(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.totalContainer).getByText(
        await browser.getTranslation('Total')
      );
    })();
  }

  // TODO: SAM - This does not look right, we should not be hard-coding item names in selectors like this, FE will need to resolve
  get name(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.shoppingBagGrid).getByText(
        await browser.getTranslation('Awesome Blue Hoodie')
      );
    })();
  }
  get colour(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.shoppingBagGrid).getByText(
        await browser.getTranslation('Colour')
      );
    })();
  }
  get size(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      // @ts-ignore
      return await within(await this.shoppingBagGrid).getByText(
        await browser.getTranslation('Size')
      );
    })();
  }
  get checkoutButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('link', {
        name: await browser.getTranslation('Checkout Now'),
      });
    })();
  }
  get getImage(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('img', {
        name: await browser.getTranslation('Awesome Blue Hoodie'),
      });
    })();
  }
  get removeButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Remove'),
      });
    })();
  }
  get decrementButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Decrease quantity'),
      });
    })();
  }
  get incrementButton(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('button', {
        name: await browser.getTranslation('Increase quantity'),
      });
    })();
  }
  get quantityText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByRole('slider');
    })();
  }
  get noItemsPresentText(): Promise<WebdriverIO.Element> {
    return (async (): Promise<WebdriverIO.Element> => {
      return await browser.getByText(
        await browser.getTranslation('No items presently in your bag.')
      );
    })();
  }

  // Using a WDIO class selector in this case as a testing library one wasn't suitable

  get itemsInOrderSummary(): Promise<string> {
    return (async (): Promise<string> => {
      let textItemsToReturn = '';
      const textItems = await $$('.summary-row');
      for (const item of await textItems) {
        const itemText = await item.getText();
        let translatedItemText = await browser.getTranslation({ text: itemText });
        if (typeof translatedItemText === 'boolean') {
          translatedItemText = itemText;
        }
        textItemsToReturn = textItemsToReturn.concat(translatedItemText + '\n');
      }
      textItemsToReturn = textItemsToReturn.replace(/\n*$/, ''); // remove the last new line character
      return textItemsToReturn;
    })();
  }
}

export default new ShoppingBag();
