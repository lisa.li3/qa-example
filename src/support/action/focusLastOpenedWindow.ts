/**
 * Focus the last opened window
 * @param  {String}   obsolete Type of object to focus to (window or tab)
 */
/* eslint-disable no-unused-vars */
export default async (obsolete: never): Promise<void> => {
  /* eslint-enable no-unused-vars */
  /**
   * The last opened window
   * @type {Object}
   */

  console.log(await browser.getWindowHandles());
  const lastWindowHandle = (await browser.getWindowHandles()).slice(-1)[0];

  console.log('Last window: ', lastWindowHandle);

  await browser.switchToWindow(lastWindowHandle);
};
