import { getState } from '../functions/state';

/**
 * Set a given local storage key to a given value. When the local storage key does not exist it will
 * be created
 * @param  {String}   key The local storage key to set
 * @param  {String}   value The value
 */
export const setLocalStorage = async (key: string, value: string): Promise<boolean> => {
  const state = await getState();

  await browser.execute(
    (key: string, value: string) => {
      if (key === 'wishlist') {
        value = JSON.stringify({ [value]: 1 });
      }
      localStorage.setItem(key, value);
    },
    `${state.siteConfig.canonicalDomain.replace('www', '').replace(/\W/g, '')}_${key}`,
    value
  );
  return true;
};

/**
 * Delete local storage
 * @param  {String}   keys The local storage keys to delete
 */
export const deleteLocalStorage = async (keys?: string): Promise<boolean> => {
  if (keys === null || keys === undefined) {
    await browser.execute(() => {
      localStorage.clear();
    });
    return true;
  }

  const keysArray = keys.indexOf(',') > -1 ? keys.split(',') : [keys];
  for (const key of keysArray) {
    await browser.execute((key: string) => {
      localStorage.removeItem(key);
    }, key);
  }
  return true;
};
