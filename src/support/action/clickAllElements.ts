import getElement from '../check/getElement';

/**
 * Perform an click action on the given element
 * @param  {String}   action  The action to perform (click or doubleClick)
 * @param  {String}   selector Element selector
 * @param  {String}   parentSelector The selector of the parent element
 */
export default async (action: 'click' | 'doubleClick', selector: string): Promise<void> => {
  /**
   * Get all things to click
   */
  let clickables;
  try {
    if (typeof selector === 'string') {
      clickables = await getElement({ selector });
    }
  } catch (err) {
    clickables = await $(selector);
  }

  const method = action === 'click' ? 'click' : 'doubleClick';

  try {
    Promise.all(clickables.map((clickable) => clickable[method]())).catch((err) => {
      console.log('error: ', err);
      throw err;
    });
  } catch (err) {
    console.log('error: ', err);
    throw err;
  }
};
