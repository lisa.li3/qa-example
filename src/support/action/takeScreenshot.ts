/**
 * Takes a screenshot and adds it to the report
 */
import cucumberJson from 'wdio-cucumberjs-json-reporter';

export default async (): Promise<void> => {
  cucumberJson.attach(await browser.takeScreenshot(), 'image/png');
};
