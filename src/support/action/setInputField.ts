import getElement from '../check/getElement';
import { definedCustomerValue } from '../functions/definedCustomer';

/**
 * Set the value of the given input field to a new value or add a value to the
 * current selector value
 * @param  {String}   method  The method to use (add or set)
 * @param  {String}   value   The value to set the selector to
 * @param  {String}   selector Element selector
 */
export default async (method: string, value: string, selector: string): Promise<void> => {
  const command = method === 'add' ? 'addValue' : 'setValue';
  let checkValue = value;
  if (!value) {
    checkValue = '';
  }

  checkValue = (await definedCustomerValue(checkValue)) || checkValue;

  // try {
  const element = await getElement({ selector });
  await element[command](checkValue);
  // } catch (err) {
  //   await checkIfElementExists(selector, false, 1);
  //   await $(selector)[command](checkValue);
  // }
};
