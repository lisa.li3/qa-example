/**
 * Complete a payment
 * @param  {String}   paymentType Type of payment to process
 */
import waitOnElement from './waitFor';
import scroll from './scroll';
import switchToChildFrame from './switchToChildFrame';
import clickElement from './clickElement';
import setInputField from './setInputField';
import switchToParentFrame from './switchToParentFrame';
import focusLastOpenedWindow from './focusLastOpenedWindow';
import setPaypalCookies from './setPaypalCookies';
import switchToWindow from './switchToWindow';

export default async (
  paymentType:
    | 'CREDIT_CARD'
    | 'BANK_TRANSFER'
    | 'KLARNA'
    | 'SOFORT'
    | 'CLEAR_PAY'
    | 'PAYPAL_EXPRESS'
): Promise<void | Error> => {
  // eslint-disable-next-line sonarjs/no-small-switch
  switch (paymentType) {
    case 'CREDIT_CARD':
      await waitOnElement('Checkout/PaymentDetails.cardNumber', '20000', false, '');
      await scroll('Checkout/PaymentDetails.cardNumber');
      await switchToChildFrame('Checkout/PaymentDetails.cardNumber');
      await clickElement('click', 'the', 'selector', 'Checkout/PaymentDetails.input');
      await setInputField('set', '4444 3333 2222 1111', 'Checkout/PaymentDetails.input');
      await switchToParentFrame();
      await switchToChildFrame('Checkout/PaymentDetails.expiryDate');
      await setInputField('set', '0330', 'Checkout/PaymentDetails.input');
      await switchToParentFrame();
      await switchToChildFrame('Checkout/PaymentDetails.cvc');
      await setInputField('set', '737', 'Checkout/PaymentDetails.input');
      await switchToParentFrame();
      await setInputField('set', 'Test User', 'Checkout/PaymentDetails.name');
      break;
    case 'PAYPAL_EXPRESS':
      await clickElement('click', 'the', 'button', 'CheckoutPayment.payPalExpressButton');
      await browser.pause(5000);
      await focusLastOpenedWindow(null);
      console.log('Setting cookies');
      await setPaypalCookies();
      console.log('Refreshing Browser');
      await browser.refresh();
      await setInputField(
        'set',
        'rob.wi_1344499904_per@supergroup.co.uk',
        'PaypalExpress.emailAddressInput'
      );
      await setInputField('set', '344499834', 'PaypalExpress.passwordInput');
      await clickElement('click', 'the', 'button', 'PaypalExpress.loginBtn');
      // And I expect that element "PaypalExpress.welcomeMessage" is displayed
      await clickElement('click', 'the', 'button', 'PaypalExpress.cookieBtn');
      // And I scroll to element "PaypalExpress.payNowBtn"
      await clickElement('click', 'the', 'button', 'PaypalExpress.visaCheckbox');
      await browser.pause(1000);
      await clickElement('click', 'the', 'button', 'PaypalExpress.payNowBtn');
      await browser.pause(5000);
      await switchToWindow('Checkout - project');
      await setInputField('set', '01234 123 123', 'CheckoutPayment.phoneInput');

      break;
    default:
      return Error('Payment method not implemented in framework');
  }
  await browser.pause(5000);
  // Click Buy Now
  await clickElement('click', 'the', 'selector', 'CheckoutPayment.buyNowButton');
};
