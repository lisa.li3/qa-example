/**
 * Browser back to the previous page
 */

export default async (): Promise<void> => {
  await browser.back();
};
