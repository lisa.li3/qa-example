import browserNavAction from '../functions/browserNavAction';
import { replace } from 'lodash';
import { getState, setShortLivedState } from '../functions/state';
import { getAsSeedUser } from '../functions/definedCustomer';

/**
 * Open the given URL
 * @param openType
 * @param  {String}   type Type of navigation (getUrl or site or page)
 * @param  {String}   page The URL to navigate to
 * @param pageLoad
 */
export default async (
  openType: 'open' | 'am on',
  type: 'url' | 'site' | 'page' | 'siteId' | 'domain',
  page: string,
  pageLoad?: string
): Promise<boolean> => {
  const forceOpen = openType === 'open';
  const { constants } = getState();
  const userSeed = await getAsSeedUser();

  let pageURL = page;

  if (type === 'page') {
    // TODO: Sam - DO NOT LIKE THIS, PLEASE REFACTOR ASAP
    const evalStrings = page.match(/{{\s?([^}]*)\s?}}/gi);
    if (evalStrings) {
      for (const evalString of evalStrings) {
        const codeToRun = evalString.replace('{{', '').replace('}}', '');
        const response = eval(codeToRun);
        pageURL = pageURL.replace(evalString, response);
      }
    }
  }

  if (type === 'siteId' || type === 'domain') {
    await browser.setSiteContext(page);
    await browserNavAction.openBaseURL();
    return Promise.resolve(true);
  } else if (type === 'url') {
    if (!forceOpen) {
      const currentUrl = await browser.getUrl();
      if (pageURL === currentUrl) {
        return Promise.resolve(true);
      } else {
        await setShortLivedState('runLoadingDetection', true);
        await browser.url(pageURL);
      }
    } else {
      await browser.url(pageURL);
    }
  } else {
    if (!forceOpen) {
      const currentPage = replace(await browser.getUrl(), constants.BASE_URL, '');
      if (pageURL === currentPage) {
        return Promise.resolve(true);
      } else {
        await setShortLivedState('runLoadingDetection', true);
        await browserNavAction.openPage(pageURL);
      }
    } else {
      await browserNavAction.openPage(pageURL);
    }
  }

  return Promise.resolve(true);
};
