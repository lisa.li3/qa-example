import getElement from '../check/getElement';

/**
 * Perform an click action on the given element
 * @param  {String}   action  The action to perform (click or doubleClick)
 * @param  {String}   ifIframe
 * @param  {String}   type    Type of the element (link or selector)
 * @param  {String}   selector Element selector
 * @param  {String}   withinSelector
 */
export default async (
  action: 'click' | 'doubleClick',
  ifIframe: 'the' | 'the iframe',
  type: 'link' | 'selector' | 'button',
  selector: string,
  withinSelector?: string
): Promise<void> => {
  /**
   * Element to perform the action on
   * @type {String}
   */
  const selector2 = type === 'link' ? `=${selector}` : selector;

  /**
   * The method to call on the browser object
   * @type {String}
   */
  const method = action === 'click' ? 'click' : 'doubleClick';

  const element = await getElement({ selector: selector2, withinSelector });
  await element[method]();
};
