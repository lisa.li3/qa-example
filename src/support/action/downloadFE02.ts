import { LambdaClient, InvokeCommand } from '@aws-sdk/client-lambda';
import { getState, updateState } from '../functions/state';
import { getDynamicStage } from '../hooks';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { downloadFromS3 } from '../functions/S3actions';
import { DOMParser } from 'xmldom';
import xpath from 'xpath';

const state = getState();
const serviceStage = getDynamicStage(state.constants.BASE_URL, true);

export default async (): Promise<void> => {
  await browser.pause(5000);
  /**
   * Trigger the order-exporter
   */
  const lambda = new LambdaClient({ region: 'eu-west-1' });
  const command = new InvokeCommand({
    FunctionName: `order-exporter-${serviceStage}-timerHandler`,
    LogType: 'Tail',
  });
  const response = await lambda.send(command);
  cucumberJson.attach(
    `Lambda Response: ${Buffer.from(response.LogResult || '', 'base64').toString()}`,
    'text/plain'
  );

  /**
   * Get OrderNumber from Page
   */
  // TODO: pull order number prefix from site config
  const orderContainer = await browser.findByText(new RegExp(`^project_`, 'i'));

  const orderNumber = await orderContainer.getText();

  /**
   * Download FE02 From S3
   */

  const FE02 = await downloadFromS3(`fe02-orders-data-${serviceStage}`, orderNumber);

  /**
   * Get order out of FE02
   */

  const XMLDOC = new DOMParser().parseFromString(FE02.toString());
  const select = xpath.useNamespaces({ oms: 'http://www.supergroup.com/ecom-orders-to-oms/' });
  const nodes = select(`//oms:CustomerOrder/oms:Order[@OrderNo = '${orderNumber}']`, XMLDOC);

  cucumberJson.attach(`Full FE02 XML: \n${formatXml(FE02.toString())}\n\n`, 'text/plain');
  if (nodes.length === 1) {
    cucumberJson.attach(`Our Order XML: \n${formatXml(nodes[0].toString())}`, 'text/plain');
    await updateState('FE_02_ORDER', nodes[0].toString());
  } else {
    throw Error(
      `The FE02 did not include our order number, or it had more than 1 order that matched. Check the Attached FE02 on the report`
    );
  }
};

const formatXml = (xml) => {
  // tab = optional indent value, default is tab (\t)
  let formatted = '',
    indent = '';
  const tab = '  ';
  xml.split(/>\s*</).forEach(function (node) {
    if (node.match(/^\/\w/)) indent = indent.substring(tab.length); // decrease indent by one 'tab'
    formatted += indent + '<' + node + '>\r\n';
    if (node.match(/^<?\w[^>]*[^\/]$/)) indent += tab; // increase indent
  });
  return formatted.substring(1, formatted.length - 3);
};
