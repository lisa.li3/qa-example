import getElement from '../check/getElement';

/**
 * Switches to a nested iFrame if one is on the page
 * @param  {String}   iFrameSelector?  Element selector
 */

export default async (iFrameSelector?: string): Promise<void> => {
  try {
    let frame;
    await browser.waitUntil(
      async () => {
        if (iFrameSelector) {
          frame = await getElement({ selector: iFrameSelector });
        } else {
          frame = await $('iframe');
        }
        return await (await frame).isExisting();
      },
      { timeout: 20000 }
    );
    await browser.switchToFrame(frame);
  } catch (err) {
    console.log(`No frames to switch to: ${err} \n\nMoving on`);
  }
};
