import { getState } from '../functions/state';
import { camelCase, truncate, tail } from 'lodash';
import debug from 'debug';
import openWebsite from './openWebsite';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

/**
 * Open the PDP for the given product
 * @param product
 */
export default async (pathToProduct: string): Promise<void> => {
  const namespace = 'openPDP';
  const _warn = debug(`warn:${namespace}`);

  const { feature } = await getState();

  console.log('FEATURE.PRODUCT: ', feature.product);

  const path = pathToProduct.split('.');

  const featureId = getFeatureId();
  const productFilePath = `${path[0]}.${path[1]}`;
  const productIndex = getIndex(path[2]);
  const state = getState();

  console.log({ productIndex });

  const idParts = [
    camelCase(state.siteConfig.siteId),
    camelCase(featureId),
    camelCase(tail(productFilePath.split('.')).join('.')),
    productIndex,
  ];
  const productId = idParts.join('-');

  console.log({ productId });

  const matchedProduct = feature.product.find((p) => p.id === productId);
  if (!matchedProduct)
    _warn(
      `There is no seeded product matching this ID: ${productId} Trying to open page anyway...`
    );

  console.log({ matchedProduct });
  cucumberJson.attach(`Opening PDP for: ${productId}`, 'text/plain');
  await openWebsite('open', 'page', `/products?sku=${productId}-s`);
};

const getFeatureId = (): string => {
  const { scenario } = getState();
  if (!Object.keys(scenario).length) throw Error('No scenario set in state');
  const uri = scenario.uri.split('/');
  return uri[uri.length - 1].replace('.feature', '');
};

const getIndex = (str): string => {
  if (!isNaN(parseInt(str, 10))) {
    return leftPad(parseInt(str, 10));
  }
  switch (str) {
    case 'first':
      return '000';
    case 'second':
      return '001';
    case 'third':
      return '002';
    case 'forth':
      return '003';
    default:
      return '000';
  }
};

const leftPad = (value): string => {
  const zeroes = new Array(3 + 1).join('0');
  return (zeroes + value).slice(-3);
};

// const stringifyNumber = (n) => {
//   var special = [
//     'zeroth',
//     'first',
//     'second',
//     'third',
//     'fourth',
//     'fifth',
//     'sixth',
//     'seventh',
//     'eighth',
//     'ninth',
//     'tenth',
//     'eleventh',
//     'twelfth',
//     'thirteenth',
//     'fourteenth',
//     'fifteenth',
//     'sixteenth',
//     'seventeenth',
//     'eighteenth',
//     'nineteenth',
//   ];
//   var deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet'];
//   if (n < 20) return special[n];
//   if (n % 10 === 0) return deca[Math.floor(n / 10) - 2] + 'ieth';
//   return deca[Math.floor(n / 10) - 2] + 'y-' + special[n % 10];
// };
