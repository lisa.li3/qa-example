import { Selector } from 'webdriverio';
import getElement from '../check/getElement';

/**
 * Scroll the page to the given element
 * @param  {String}   selector Element selector
 */
export default async (selector: Selector): Promise<void> => {
  try {
    if (typeof selector === 'string') {
      const element = await getElement({ selector });
      await element.scrollIntoView();
    }
  } catch (err) {
    console.log(err);
    await $(selector).scrollIntoView();
  }
};
