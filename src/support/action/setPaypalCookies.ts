import moment from 'moment';

/**
 * Sets the cookies for paypal
 */
export default async (): Promise<void> => {
  const cookie = {
    path: '/',
    domain: 'paypal.com',
    secure: true,
    httpOnly: true,
    // expiry: moment().add(6, 'hours').unix(),
  };

  await browser.setCookies([
    { ...cookie, name: 'LANG', value: 'en_GB%3BGB' },
    {
      ...cookie,
      name: '-1ILhdyICORs4hS4xTUr41S8iP0',
      value: 'BjcyF6SmxwxrW5Jo6EnyCxnR5kVmVZgx_1BTn2BZdqjMRCsWQ0i9fX6-z4qRV8DqADD_wm9_RGaYqM0j',
      expiry: moment().add(6, 'hours').unix(),
    },
    {
      ...cookie,
      name: 'KHcl0EuY7AKSMgfvHl7J5E7hPtK',
      value: 'JfWnq40jMy27Gk4D_asVNgkmcTqo4SxBzuMOX13ZfxRyE2T1B7NOVu2zPnyssj9HNbuF5S-mrPPk_U2f',
    },
    { ...cookie, name: 'X-PP-ADS', value: 'AToBoPHfYf-iHTGFT3Liggu4jW4EVO0' },
    { ...cookie, name: 'cookie_check', value: 'yes' },
    { ...cookie, name: 'd_id', value: '29806a9be179412c985a21d187a341261642065030734' },
    { ...cookie, name: 'enforce_policy', value: 'gdpr_v2.1', httpOnly: undefined },
    { ...cookie, name: 'fn_dt', value: '4CE503651T151105A' },
    {
      ...cookie,
      name: 'rmuc',
      value:
        'ppzrF6rjv3PRJFEzW22sDXOzHYcrJVlq2Wxsy1I01uOi8gCNbw8-8arW6p8fEYIvZmlQAaUiB9GBN2E8fdqeTpy5D7opLYC0Ff8g6MiGHkjXCUwpQtQCLCbroB2HjhF2VZzUGIOltrX1_BS28n2cs4wfnOwIgqY1saxu9I8qoaOGwJo30aQbK137GK4FkKmYxoysXMZVb8GD7K2kL5kTGhnFvly',
    },
    {
      ...cookie,
      name: 'ts',
      value:
        'vreXpYrS%3D1736760986%26vteXpYrS%3D1642068386%26vr%3D52b3ec7417e0a602122bcc4fd58f7fbf%26vt%3D52b3ec7417e0a602122bcc4fd58f7fbe%26vtyp%3Dnew',
      expiry: moment().add(6, 'hours').unix(),
    },
    {
      ...cookie,
      name: 'ts_c',
      value: 'vr%3D52b3ec7417e0a602122bcc4fd58f7fbf%26vt%3D52b3ec7417e0a602122bcc4fd58f7fbe',
      httpOnly: undefined,
      expiry: moment().add(6, 'hours').unix(),
    },
    {
      ...cookie,
      name: 'tsrce',
      value: 'unifiedloginnodeweb',
      expiry: moment().add(6, 'hours').unix(),
    },
    { ...cookie, name: 'ui_experience', value: 'login_type%3DEMAIL_PASSWORD%26home%3D2' },
    { ...cookie, name: 'x-cdn', value: 'fastly:LHR', httpOnly: undefined },
    { ...cookie, name: 'x-pp-s', value: 'eyJ0IjoiMTY0MjA2NjU4NzQ1OCIsImwiOiIwIiwibSI6IjAifQ' },
    {
      ...cookie,
      name: 'nsid',
      value:
        's%3AEflUjzep6Y3kSa_3cFazFfc3B1JFtBCn.0C%2FTC%2FVdjU0FhcyGDA10CnbCre6B93W%2FLnNgNWeHCTc',
    },
  ]);
};
