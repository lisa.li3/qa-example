import openWebsite from './openWebsite';
import setInputField from './setInputField';
import clickElement from './clickElement';
import waitFor from './waitFor';
import { getState } from '../functions/state';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
/**
 * Login with an email and password
 * @param email
 * @param password
 */
export const loginWithUsernameAndPassword = async (
  email: string,
  password: string
): Promise<void> => {
  await openWebsite('am on', 'page', '/login');
  await waitFor('Login.emailAddress', '10000', '', 'be displayed');
  await setInputField('set', email, 'Login.emailAddress');
  await setInputField('set', password, 'Login.password');
  await clickElement('click', 'the', 'button', 'Login.signInButton');
  await waitFor('Login.loginSuccess', '10000', null, 'be displayed');
};

/**
 * Login with a defined customer
 * @param customer
 */
export const loginWithDefinedCustomer = async (customer?: string | null): Promise<void> => {
  const customerNumber = parseInt(customer || '1');
  const customerIndex = customerNumber - 1 || 0;
  const state = getState();
  const definedCustomers = state.customerDetails;

  if (customerNumber > definedCustomers.length) {
    throw new Error(
      `Trying to use defined customer at index ${customerIndex} undefined, there are only ${definedCustomers.length} defined customers`
    );
  }

  const selectedCustomer = definedCustomers[customerIndex];
  const email = selectedCustomer.Email;
  const password = selectedCustomer.Password;
  cucumberJson.attach(JSON.stringify(selectedCustomer), 'application/json');
  await loginWithUsernameAndPassword(email, password);
};
