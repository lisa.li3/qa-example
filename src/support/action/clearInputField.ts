import getElement from '../check/getElement';

/**
 * Clear a given input field (placeholder for WDIO's clearElement)
 * @param  {String}   selector Element selector
 */
export default async (selector: string): Promise<void> => {
  const element = await getElement({ selector });
  await element.clearValue();
};
