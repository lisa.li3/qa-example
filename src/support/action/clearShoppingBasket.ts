import getElement from '../check/getElement';
import openWebsite from './openWebsite';

/**
 * Clear the shopping basket
 */
export default async (returnPage: string): Promise<void> => {
  const bagButton = (await getElement({
    selector: 'Navigation.basketButton',
  })) as WebdriverIO.Element;
  await bagButton.click();
  await browser.pause(3000);

  let clickables: WebdriverIO.Element[];

  try {
    clickables = (await getElement({
      selector: 'PopinBasket.removeButtons',
    })) as WebdriverIO.Element[];
  } catch (err) {
    return; // Probably not found, so just return
  }

  // Need to be removed in reverse order
  for await (const clickable of clickables.reverse()) {
    await clickable.click();
    await clickable.waitForDisplayed({
      timeout: 3000,
      reverse: true,
    });
  }

  returnPage && (await openWebsite('open', 'url', returnPage));
};
