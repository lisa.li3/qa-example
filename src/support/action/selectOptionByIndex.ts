import { Selector } from 'webdriverio';
import getElement from '../check/getElement';

/**
 * Select a option from a select element by it's index OR select an element from an array of elements by it's index
 * @param  {String}   index      The index of the option
 * @param  {String}   obsolete   The ordinal indicator of the index (unused)
 * @param  {String}   selector Element selector
 *
 * @todo  merge with selectOption
 */
export default async (index: string, obsolete: never, selector: Selector): Promise<void> => {
  /**
   * The index of the option to select
   * @type {Int}
   */
  const optionIndex = parseInt(index, 10);

  try {
    if (typeof selector === 'string') {
      const element = await getElement({ selector });
      if (Array.isArray(element)) {
        element[optionIndex - 1].click();
      } else {
        await element.selectByIndex(optionIndex);
      }
    }
  } catch (err) {
    await $(selector).selectByIndex(optionIndex);
  }
};
