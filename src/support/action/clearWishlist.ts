import getElement from '../check/getElement';
import openWebsite from './openWebsite';

/**
 * Clear the wishlist
 */
export default async (returnPage: string): Promise<void> => {
  await openWebsite('am on', 'url', '/my-account/wish-list');

  let clickables: Array<WebdriverIO.Element>;
  try {
    clickables = await getElement({ selector: 'Wishlist.RemoveFromWishlistButtons' });
  } catch (err) {
    return; // Probably not found, so return
  }

  // Need to be removed in reverse order
  for await (const clickable of clickables.reverse()) {
    await clickable.click();
    await clickable.waitForDisplayed({
      timeout: 3000,
      reverse: true,
    });
  }

  returnPage && (await openWebsite('open', 'url', returnPage));
};
