/**
 * Wait for a period of time
 */
export default async (): Promise<unknown> => {
  return browser.pause(5000); // TODO: This is a placeholder for something more inteligent
};
