import getElement from '../check/getElement';
const namespace = 'waitFor';
import debug from 'debug';

type WaitForCommands = 'waitForClickable' | 'waitForDisplayed' | 'waitForEnabled' | 'waitForExist';

const _info = debug(`info:${namespace}`);

/**
 * Wait for the given element to be enabled, displayed, or to exist
 * @param  {String}   selector                  Element selector
 * @param  {String}   ms                       Wait duration (optional)
 * @param  {String}   falseState               Check for opposite state
 * @param  {String}   state                    State to check for (default
 *                                             existence)
 */
export default async (
  selector: string,
  ms: string,
  falseState: string | null | boolean,
  state: string
): Promise<true | string | unknown> => {
  _info(`Started`, selector, ms, falseState, state);

  /**
   * Maximum number of milliseconds to wait, default browser.config.waitforTimeout
   * @type {Number}
   */
  const intMs = parseInt(ms, 10) || browser.config.waitforTimeout;

  /**
   * Command to perform on the browser object
   * @type {String}
   */
  let command: WaitForCommands = 'waitForExist';

  /**
   * Boolean interpretation of the false state
   * @type {Boolean}
   */
  let boolFalseState = !!falseState;

  /**
   * Parsed interpretation of the state
   * @type {String}
   */
  let parsedState = '';

  if (falseState || state) {
    parsedState = state.indexOf(' ') > -1 ? state.split(/\s/)[state.split(/\s/).length - 1] : state;

    if (parsedState) {
      command = (`waitFor${parsedState[0].toUpperCase()}` +
        `${parsedState.slice(1)}`) as WaitForCommands;
    }
  }

  if (typeof falseState === 'undefined') {
    boolFalseState = false;
  }

  try {
    const element = await getElement({ selector, timeout: boolFalseState ? 1000 : intMs });
    if (element && !boolFalseState) return;
    await element[command]({
      timeout: intMs,
      reverse: boolFalseState,
    });
    _info('Finished: Element was found:', selector, ms, falseState, state);
  } catch (err) {
    if (boolFalseState) {
      if (err.toString().includes('still displayed')) {
        // In the event that the element is an item in a list,
        // it may still be found (by child number) but not be the same element
        // In this case, we do one final check to see if the element is still
        // present.
        try {
          await getElement({ selector, timeout: 1000 });
        } catch (err) {
          _info('Finished: Element was found, then not found:', selector, ms, falseState, state);
          return;
        }
        _info(
          'Finished: Element was found and never disappeared:',
          selector,
          ms,
          falseState,
          state
        );
        throw err;
      }
      _info('Finished: Element was never found:', selector, ms, falseState, state);
      return; // If element not found, return - as this is what we're expecting
    }
    throw err;
  }
};
