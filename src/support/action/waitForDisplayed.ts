import waitFor from './waitFor';

/**
 * Wait for the given element to become visible
 * @param  {String}   selector      Element selector
 * @param  {String}   falseCase     Whether or not to expect a visible or hidden state
 *
 */
export default async (selector: string, falseCase: boolean): Promise<void> => {
  await waitFor(selector, '', falseCase, 'be displayed');
};
