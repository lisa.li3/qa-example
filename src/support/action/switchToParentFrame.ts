/**
 * Switches to the parent frame if you are within a child
 */
export default async (): Promise<void> => {
  try {
    await browser.switchToParentFrame();
  } catch (err) {
    console.log(`No parent frames to switch to: ${err} \n\nMoving on`);
  }
};
