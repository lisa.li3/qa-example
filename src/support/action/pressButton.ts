/**
 * Perform a key press
 * @param  {String}   key  The key to press
 */
export default async (key: string | string[]): Promise<void> => {
  if (typeof key === 'string') {
    switch (key) {
      case 'Tab':
        key = '\uE004';
        break;
      case 'Enter':
        key = '\uE007';
        break;
      case 'Backspace':
        key = '\uE003';
        break;
    }
  }
  await browser.keys(key);
};
