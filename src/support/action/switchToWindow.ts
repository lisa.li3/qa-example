/**
 * Switches to a window with a specified name
 * @param  {String}   windowName  Name of the window
 */
export default async (windowName: string): Promise<void> => {
  try {
    await browser.switchWindow(windowName);
  } catch (err) {
    throw new Error(`No window with the name: ${windowName}`);
  }
};
