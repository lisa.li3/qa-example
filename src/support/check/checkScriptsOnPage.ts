/**
 * Check if script exists on page by checking for the src URL
 * @param  {Type}     falseCase     Whether to check if the page contains the expected script or not
 * @param  {string}   htmlSection   Check in the head or body of the HTML
 * @param  {String}   searchText    Text to search for in the src URL
 */

export default async (
  falseCase: boolean,
  htmlSection: string,
  searchText: string
): Promise<void> => {
  const filteredResultsArray: string[] = [];
  const allScriptsInSection = await $$(`${htmlSection} > script[src]`);

  for (const item of allScriptsInSection) {
    const itemHTML = await item.getHTML(true);
    if (itemHTML.includes(searchText)) {
      filteredResultsArray.push(itemHTML);
    }
  }

  if (falseCase) {
    expect(filteredResultsArray.length).toEqual(0);
  } else {
    expect(filteredResultsArray.length).toBeGreaterThan(0);
  }
};
