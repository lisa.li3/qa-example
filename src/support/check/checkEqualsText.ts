import type { Selector } from 'webdriverio';
import getElement from './getElement';
import { definedCustomerValue } from '../functions/definedCustomer';
import moment from 'moment';

/**
 * Check if the given elements text is the same as the given text
 * @param  {String}   elementType   Element type (element or button)
 * @param  {String}   selector      Element selector
 * @param  {String}   falseCase     Whether to check if the content equals the
 *                                  given text or not
 * @param  {String}   expectedText  The text to validate against
 */
export default async ({
  elementType,
  selector,
  falseCase,
  expectedText,
}: {
  elementType: string;
  selector: string;
  falseCase: boolean;
  expectedText: string;
}): Promise<void> => {
  let element;

  if (elementType !== 'element' && elementType !== 'button') {
    const elementNumber = elementType.slice(4, elementType.indexOf('element') - 2);
    const optionIndex = parseInt(elementNumber, 10);
    try {
      element = await getElement({ selector, multipleSelectors: true });
    } catch (err) {
      element = await $$(selector);
    }
    element = element[optionIndex - 1];
  } else {
    try {
      element = await getElement({ selector });
    } catch (err) {
      element = await $(selector);
    }
  }

  if (!element) {
    throw Error(`No element found in checkEqualsText for selector: ${selector}`);
  }

  /**
   * The command to execute on the browser object
   * @type {String}
   */
  let command: 'getText' | 'getValue' = 'getValue';

  if (element && (elementType === 'button' || (await element.getAttribute('value')) === null)) {
    command = 'getText';
  }

  /**
   * The expected text to validate against
   * @type {String}
   */
  let parsedExpectedText = expectedText;

  /**
   * Whether to check if the content equals the given text or not
   * @type {Boolean}
   */
  let boolFalseCase = !!falseCase;

  // Check for empty element
  if (typeof parsedExpectedText === 'function') {
    parsedExpectedText = '';

    boolFalseCase = !boolFalseCase;
  }

  if (parsedExpectedText === undefined && falseCase === undefined) {
    parsedExpectedText = '';
    boolFalseCase = true;
  }

  const text = await element[command]();

  if (parsedExpectedText.includes('date+')) {
    const expectedDays = parseInt(parsedExpectedText.split('+')[1]);
    parsedExpectedText = moment().add(expectedDays, 'days').format('YYYY-MM-DD');
  } else if (parsedExpectedText.includes('date-')) {
    const expectedDays = parseInt(parsedExpectedText.split('-')[1]);
    parsedExpectedText = moment().subtract(expectedDays, 'days').format('YYYY-MM-DD');
  }

  const customerValue = (await definedCustomerValue(parsedExpectedText)) || parsedExpectedText;
  if (boolFalseCase) {
    if (parsedExpectedText === customerValue) {
      expect(parsedExpectedText).not.toBe(text);
    } else {
      expect(customerValue.toLowerCase()).not.toBe(text.toLowerCase());
    }
  } else {
    if (parsedExpectedText === customerValue) {
      expect(text).toBe(parsedExpectedText);
    } else {
      expect(text.toLowerCase()).toBe(customerValue.toLowerCase());
    }
  }
};
