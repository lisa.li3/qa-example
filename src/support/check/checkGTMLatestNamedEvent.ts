/**
 * Check the GTM Event property values
 * @param  {String}   eventName     The GTM event to check
 * @param  {String}   property      The GTM event property to check
 * @param  {String}   value         The value of the property to check
 */
export default async (
  eventName: string,
  property: string,
  value: string | number
): Promise<void> => {
  const dataLayer = await browser.execute('return window.dataLayer');

  // @ts-ignore
  const foundEvents = await dataLayer.filter((item) => {
    return item.event === eventName;
  });

  if (foundEvents.length === 0) {
    throw Error(`No ${eventName} events were found in the dataLayer`);
  }
  const lastEventEntry = foundEvents[foundEvents.length - 1];

  if (lastEventEntry.hasOwnProperty(property)) {
    expect(lastEventEntry[property].toString()).toEqual(value);
  } else {
    throw Error(`Property ${property} does not exist in the latest ${eventName} dataLayer event`);
  }
};
