import getElement from './getElement';
import { isArray } from 'lodash';

/**
 * Check if the given element exists in the current DOM
 * @param  {String}   selector  Element selector
 * @param  {String}   falseCase Whether to check if the element exists or not
 */
export default async (selector: string, falseCase: boolean): Promise<boolean | void> => {
  let elements: WebdriverIO.Element[] = [];
  let element: WebdriverIO.Element;

  const timeout = falseCase ? 2000 : undefined;

  try {
    console.log(`isExisting::Try getElement...`);
    element = await getElement({ selector, timeout });
    console.log(`isExisting::Try ...done, isArray check`);
    elements = isArray(element) ? element : [element];
    console.log('isExisting::Try ...done, Moving on');
  } catch (err) {
    console.log(`isExisting:: catch`);
    console.log(`falseCase: ${falseCase}`);
    if (falseCase) {
      return true;
    }
    elements = await $$(selector);
  }
  /**
   * Elements found in the DOM
   * @type {Object}
   */

  if (falseCase) {
    expect(elements).toHaveLength(
      0,
      // @ts-expect-error
      `Expected element "${selector}" not to exist`
    );
  } else {
    expect(elements.length).toBeGreaterThan(
      0,
      // @ts-expect-error
      `Expected element "${selector}" to exist`
    );
  }
};
