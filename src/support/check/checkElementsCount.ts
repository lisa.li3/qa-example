import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Check if the given elements contains text
 * @param  {String}   parentSelector Parent selector
 * @param  {String}   checkType      Whether to check if the length is equal, <, >, <=, >=
 * @param  {Number}   expectedCount   The number to check against
 */
export default async (
  parentSelector: Selector,
  checkType:
    | 'equal'
    | 'be less than'
    | 'be greater than'
    | 'be less than or equal to'
    | 'be greater than or equal to',
  expectedCount: number
): Promise<void> => {
  if (expectedCount === undefined || isNaN(expectedCount)) {
    throw new Error('expectedCount is a required argument and must be a number');
  }

  /**
   * Create array of all parents
   */
  let parents;
  try {
    if (typeof parentSelector === 'string') {
      parents = await getElement({ selector: parentSelector });
    }
  } catch (err) {
    parents = await $(parentSelector);
  }

  switch (checkType) {
    case 'equal': {
      if ((expectedCount === 0 && !parents) || !parents.length) {
        return;
      }
      expect(parents.length).toEqual(expectedCount);
      break;
    }
    case 'be less than':
      expect(parents.length).toBeLessThan(expectedCount);
      break;
    case 'be less than or equal to':
      expect(parents.length).toBeLessThanOrEqual(expectedCount);
      break;
    case 'be greater than':
      expect(parents.length).toBeGreaterThan(expectedCount);
      break;
    case 'be greater than or equal to':
      expect(parents.length).toBeGreaterThanOrEqual(expectedCount);
      break;
    default:
      throw new Error('comparison value is not recognised');
  }
};
