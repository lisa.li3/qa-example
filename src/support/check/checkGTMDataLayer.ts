/**
 * Check the GTM Event property values
 * @param  {String}   index         This is the index of the GTM data layer item to check
 * @param  {String}   property      The GTM datalayer property to check
 * @param  {String}   value         The value of the property to check
 */
export default async (index: string, property: string, value: string | number): Promise<void> => {
  /**
   * The index of the option to select
   * @type {Int}
   */

  const optionIndex = parseInt(index, 10) | 0; // Default to 0 if index isn't defined

  const dataLayer = await browser.execute('return window.dataLayer');
  // @ts-ignore
  const dataLayerItem = dataLayer[optionIndex - 1];

  const hasProp = dataLayerItem.hasOwnProperty(property);

  expect(hasProp).toEqual(true);

  if (value) {
    expect(dataLayerItem[property].toString()).toEqual(value);
  }
};
