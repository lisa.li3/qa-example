/**
 * Check the GTM Event property array values
 * @param  {String}   eventName     The GTM event to check
 * @param  {String}   property      The GTM event property to check
 * @param  {String}   index         Index of the item in the array for the GTM event property value
 * @param  {string}   arrayProperty The property of the item contained in the array (at index position)
 * @param  {String}   value         The value of the array property to check
 */
export default async (
  eventName: string,
  property: string,
  index: string,
  arrayProperty: string,
  value: string | number
): Promise<void> => {
  const dataLayer = await browser.execute('return window.dataLayer');

  /**
   * The index of the option to select
   * @type {Int}
   */
  const optionIndex = parseInt(index, 10) | 0; // Default to 0 if index isn't defined

  // @ts-ignore
  const foundEvents = await dataLayer.filter((item) => {
    return item.event === eventName;
  });

  if (foundEvents.length === 0) {
    throw Error(`No ${eventName} events were found in the dataLayer`);
  }
  const lastEventEntry = foundEvents[foundEvents.length - 1];

  if (!lastEventEntry.hasOwnProperty(property)) {
    throw Error(`Property ${property} does not exist in the latest ${eventName} dataLayer event`);
  }
  const returnedValueArray = lastEventEntry[property];

  const itemInArray = returnedValueArray[optionIndex - 1];

  if (itemInArray.hasOwnProperty(arrayProperty)) {
    expect(itemInArray[arrayProperty].toString()).toEqual(value);
  } else {
    throw Error(
      `Property ${arrayProperty} does not exist in associated array value for the latest ${eventName} dataLayer event`
    );
  }
};
