import debug from 'debug';
import getElement from './getElement';
import { isArray } from 'lodash';

//
// Debug
//
const namespace = 'waitForTextToChange';
const _debug = debug(`debug:${namespace}`);
const _info = debug(`info:${namespace}`);
/**
 * Compare the contents of two elements with each other
 * @param  {String}   selector  Element selector for the first element
 *                              elements match or not
 */
export default async (selector: string): Promise<void> => {
  const element = await getElement({ selector });
  _debug(`Started: ${selector}`);
  let text = '';
  if (element && !isArray(element) && typeof element !== 'string') {
    text = await element.getText();
    await browser.waitUntil(
      async () => {
        const elementText = await element.getText();
        _info(`---\n${elementText} <- Now\n${text} <- Before\n---`);
        return elementText !== text;
      },
      {
        timeout: browser.config.waitforTimeout || 7000,
        timeoutMsg: `Element's (${selector}) text did not change within the timeout`,
      }
    );
    _debug(`Complete! ${selector}`);
  } else {
    throw Error(`element returned from pageObject was either undefined or an array`);
  }
};
