import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Check if the given element is visible inside the current viewport
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Whether to check if the element is visible
 *                              within the current viewport or not
 */
export default async (selector: Selector, falseCase: boolean): Promise<void> => {
  let element;
  try {
    if (typeof selector === 'string') {
      element = await getElement({ selector });
    }
  } catch (err) {
    if (falseCase) {
      return;
    }
    element = await $(selector);
  }
  /**
   * The state of visibility of the given element inside the viewport
   * @type {Boolean}
   */
  const isDisplayed = await element.isDisplayedInViewport();

  if (falseCase) {
    expect(isDisplayed).not.toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" to be outside the viewport`
    );
  } else {
    expect(isDisplayed).toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" to be inside the viewport`
    );
  }
};
