import getElement from './getElement';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

const CONTENT_TYPE = 'text/plain';

/**
 * Check if the given element is (not) visible
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Check for a visible or a hidden element
 */
export default async (selector: string, falseCase: boolean): Promise<void> => {
  let element;
  try {
    element = await getElement({ selector, timeout: falseCase ? 1000 : undefined });
    cucumberJson.attach(
      `isDisplayed::getElement: Found element ${JSON.stringify(element)} `,
      CONTENT_TYPE
    );
  } catch (err) {
    if (falseCase) {
      cucumberJson.attach(
        `isDisplayed::getElement: Ignoring error due to falseCase being true`,
        CONTENT_TYPE
      );
      return;
    }
    throw err;
  }
  /**
   * Visible state of the give element
   * @type {String}
   */
  let isDisplayed;

  if (element === undefined) {
    isDisplayed = false;
  } else {
    isDisplayed = element.isDisplayed !== undefined && (await element.isDisplayed());
  }

  cucumberJson.attach(
    `isDisplayed::isElementDisplayed: Element Displayed State: ${isDisplayed}, falseCase: ${falseCase}`,
    CONTENT_TYPE
  );

  if (falseCase) {
    expect(isDisplayed).not.toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" not to be displayed`
    );
  } else {
    expect(isDisplayed).toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" to be displayed`
    );
  }
};
