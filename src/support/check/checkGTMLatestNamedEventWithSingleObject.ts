/**
 * Check the GTM Event property array values
 * @param  {String}   eventName     The GTM event to check
 * @param  {String}   property      The GTM event property to check
 * @param  {string}   objectProperty The property of the item contained in the Object
 * @param  {String}   objectValue         The value of the Object property to check
 */
export default async (
  eventName: string,
  property: string,
  objectProperty: string,
  objectValue: string | number
): Promise<void> => {
  const dataLayer = await browser.execute('return window.dataLayer');

  // @ts-ignore
  const foundEvents = await dataLayer.filter((item) => {
    return item.event === eventName;
  });

  if (foundEvents.length === 0) {
    throw Error(`No ${eventName} events were found in the dataLayer`);
  }
  const lastEventEntry = foundEvents[foundEvents.length - 1];

  if (!lastEventEntry.hasOwnProperty(property)) {
    throw Error(`Property ${property} does not exist in the latest ${eventName} dataLayer event`);
  }
  const returnedValueObject = lastEventEntry[property];

  console.log('Returned Value Object', returnedValueObject);

  if (returnedValueObject.hasOwnProperty(objectProperty)) {
    expect(returnedValueObject[objectProperty].toString()).toEqual(objectValue);
  } else {
    throw Error(
      `Property ${objectProperty} does not exist in associated Object value for the latest ${eventName} dataLayer event`
    );
  }
};
