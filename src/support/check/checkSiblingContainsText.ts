import type { Selector } from 'webdriverio';
import getElement from './getElement';
import { within } from '@project/testing-library-webdriverio';

/**
 * Check if the given elements contains text
 * @param  {String}   parentSelector Parent selector
 * @param  {String}   matchingText   Matching text
 * @param  {String}   checkType      Whether to check if the content ALSO contains
 *                                   the given text or NOT
 * @param  {String}   expectedText   The text to check against
 * @param  {String}   checkDisplay   Whether to check if expectedText is displayed
 */
export default async (
  parentSelector: Selector,
  matchingText: string,
  checkType: 'also' | 'not',
  expectedText: string,
  checkDisplay: ' and is displayed' | ' and is not displayed'
): Promise<void> => {
  if (expectedText === undefined || expectedText.length === 0) {
    throw new Error('expectedText is a required argument and must have a value');
  }

  /**
   * False case
   * @type {Boolean}
   */
  const boolFalseCase = checkType === 'not';

  /**
   * checkDisplay
   */
  let boolCheckDisplay = false;
  let boolExpectDisplayed;
  if (checkDisplay === ' and is displayed') {
    if (boolFalseCase) {
      throw new Error('Cannot expect text which does not exist to be displayed');
    }
    boolCheckDisplay = true;
    boolExpectDisplayed = true;
  } else if (checkDisplay === ' and is not displayed') {
    boolCheckDisplay = true;
    boolExpectDisplayed = false;
  }

  /**
   * Create array of all parents
   */
  let possibleParents: WebdriverIO.Element[] = [];
  try {
    if (typeof parentSelector === 'string') {
      possibleParents = (await getElement({
        selector: parentSelector,
        multipleSelectors: true,
      })) as WebdriverIO.Element[];
    }
  } catch (err) {
    const possible = await $(parentSelector);
    possibleParents = [possible];
  }

  /**
   * Step over each parent until the matching text is found
   */
  let matchedParent, matchedChild;
  for (const possibleParent of possibleParents) {
    if (!matchedParent) {
      try {
        const returnedElement = await within(possibleParent).findByText(
          new RegExp(matchingText, 'i'),
          {},
          { timeout: 1000 }
        );
        if (!!returnedElement && !matchedChild && !matchedParent) {
          matchedChild = returnedElement;
          matchedParent = possibleParent;
        }
      } catch {}
    }
  }

  if (!matchedParent) {
    throw new Error(`No element ${parentSelector} found containing the text ${matchingText}`);
  }

  /**
   * Expect the matched parent to also/not contain expectedText
   */
  if (boolFalseCase) {
    expect(await matchedParent.getText()).not.toContain(expectedText);
  } else {
    if (boolCheckDisplay) {
      const parentText = await matchedParent.getText();
      const boolTextFound = parentText.indexOf(expectedText) > -1;
      let isDisplayed = false;

      if (!boolTextFound) {
        throw new Error(
          `No element ${parentSelector} found containing the text ${matchingText} and ${expectedText}`
        );
      }

      if (
        boolTextFound &&
        matchedChild.isDisplayed !== undefined &&
        (await matchedChild.isDisplayed())
      ) {
        isDisplayed = true;
      }

      if (boolExpectDisplayed) {
        expect(isDisplayed).toEqual(true);
      } else {
        expect(isDisplayed).toEqual(false);
      }
    }
    expect(await matchedParent.getText()).toContain(expectedText);
  }
};
