import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Check the given property of the given element
 * @param  {String}   isCSS         Whether to check for a CSS property or an
 *                                  attribute
 * @param  {String}   attrName      The name of the attribute to check
 * @param  {String}   selector          Element selector
 * @param  {String}   falseCase     Whether to check if the value of the
 *                                  attribute matches or not
 * @param  {String}   expectedValue The value to match against
 */
export default async (
  isCSS: boolean,
  attrName: string,
  selector: Selector,
  falseCase: boolean,
  expectedValue: number | string
): Promise<void> => {
  let element;
  try {
    if (typeof selector === 'string') {
      element = await getElement({ selector });
    }
  } catch (err) {
    element = await $(selector);
  }
  /**
   * The command to use for fetching the expected value
   * @type {String}
   */
  const command = isCSS ? 'getCSSProperty' : 'getAttribute';

  /**
   * Te label to identify the attribute by
   * @type {String}
   */
  const attrType = isCSS ? 'CSS attribute' : 'Attribute';

  /**
   * The actual attribute value
   * @type {Mixed}
   */
  let attributeValue = await element[command](attrName);

  // eslint-disable-next-line
  expectedValue = isFinite(expectedValue as number)
    ? parseFloat(expectedValue as string)
    : expectedValue;

  /**
   * when getting something with such as a colour or font-weight WebdriverIO returns a
   * object but we want to assert against a string
   */
  if (attrName.match(/(color|font-weight|grid-template-columns)/)) {
    attributeValue = attributeValue.value;
  }

  /**
   * Special case : For checking the number of products in a row on the PLP, we just want the count of the PX values returned in the CSS property
   */
  if (attrName.match('grid-template-columns')) {
    attributeValue = (attributeValue.match(/px/g) || []).length;
  }

  if (falseCase) {
    expect(attributeValue).not.toEqual(
      expectedValue,
      // @ts-expect-error
      `${attrType}: ${attrName} of element "${selector}" should ` +
        `not contain "${attributeValue}"`
    );
  } else {
    expect(attributeValue).toEqual(
      expectedValue,
      // @ts-expect-error
      `${attrType}: ${attrName} of element "${selector}" should ` +
        `contain "${attributeValue}", but "${expectedValue}"`
    );
  }
};
