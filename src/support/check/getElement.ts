import { definedCustomerValue } from '../functions/definedCustomer';
import debug from 'debug';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { startsWith, head, includes, get, isNil } from 'lodash';
import { within } from '@project/testing-library-webdriverio';
import { getPageObject } from '../functions/pageObjects';

const namespace = 'getElement';
const _debug = debug(`debug:${namespace}`);
const _info = debug(`info:${namespace}`);
const _warn = debug(`warn:${namespace}`);
const attachmentType = 'text/plain';
const priceMatcher =
  /(USD|GBP|EUR|€|\$|£)\s?(\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2}))|(\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})?)\s?(USD|EUR|GBP|€|\$|£)/;

/**
 * Check the selector passed in for a variety of states
 * @param  {String}  selector   path of the Page Object
 * @param {Number} timeout Timeout in ms that you want to override the default pageObject one with
 * @param {boolean} multipleSelectors Flag to determine if multiple selectors need to be returned
 * @param withinSelector
 */
export default async ({
  selector,
  timeout = browser.config.waitforTimeout || 10000,
  multipleSelectors,
  withinSelector,
}: {
  selector: string;
  timeout?: number;
  multipleSelectors?: boolean;
  withinSelector?: string;
}): Promise<WebdriverIO.Element | WebdriverIO.Element[] | string | undefined> => {
  _info('Started', { selector, timeout });
  const isPrice = selector.match(priceMatcher);
  const isDefinedCustomer = startsWith(selector, 'definedCustomer.');
  const isTextElement = !includes(selector, '.') || isPrice || isDefinedCustomer;

  let lastElementError, element, textSearch;

  if (isDefinedCustomer) {
    textSearch = new RegExp((await definedCustomerValue(selector)) || selector, 'i');
  } else if (isTextElement) {
    textSearch = await browser.getTranslation(selector);
  }

  for (let timesRun = 0; timesRun < timeout / 1000; timesRun++) {
    try {
      if (isTextElement) {
        _debug('textSearch', textSearch);
        element = await textElement({ textSearch, multipleSelectors, withinSelector });
      } else {
        _debug('pageObjectPath', selector);
        element = await getPageObject({ selector });
      }
      _info('Finished', selector, get(element, 'selector'));
      return element;
    } catch (elementError) {
      await browser.pause(1000);
      lastElementError = elementError;
    }
  }

  if (!element) {
    return Promise.reject(
      `getElement::timeout: ${selector} did not find an element in ${timeout} ms, final Error: ${lastElementError}`
    );
  }
};

const textElement = async ({
  textSearch,
  multipleSelectors,
  withinSelector,
}: {
  textSearch: string;
  multipleSelectors?: boolean;
  withinSelector?: string;
}): Promise<WebdriverIO.Element | WebdriverIO.Element[] | undefined> => {
  const parentObject = !isNil(withinSelector)
    ? // @ts-ignore
      within(await getPageObject({ selector: withinSelector }))
    : browser;
  // eslint-disable-next-line testing-library/no-await-sync-query
  const elements = await parentObject.getAllByText(textSearch, {});

  console.log(`${elements.length} elements found`);

  if (!multipleSelectors && elements.length > 1) {
    _warn(
      `textSearch: Text search (${textSearch}) returned more than one element (${elements.length} found)! Continuing with first one...`
    );
  }

  const element = head(elements);
  cucumberJson.attach(
    `getElement::textElement: ${textSearch} used, ${
      elements.length
    } Elements found, Returning with ${JSON.stringify(element)}`,
    attachmentType
  );
  if (multipleSelectors) {
    return elements;
  } else {
    return element;
  }
};
