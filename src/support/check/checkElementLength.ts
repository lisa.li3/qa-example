import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Check the text length of the given element
 * @param  {String}   parentSelector Parent selector
 * @param  {String}   checkType      Whether to check if the length is equal, <, >, <=, >=
 * @param  {Number}   expectedCount   The number to check against
 */
export default async (
  type: 'text' | 'value',
  selector: Selector,
  checkType:
    | 'equal'
    | 'be less than'
    | 'be greater than'
    | 'be less than or equal to'
    | 'be greater than or equal to',
  expectedLength: number
): Promise<void> => {
  if (expectedLength === undefined || isNaN(expectedLength)) {
    throw new Error('expectedLength is a required argument and must be a number');
  }

  let element;
  try {
    if (typeof selector === 'string') {
      element = await getElement({ selector: selector });
    }
  } catch (err) {
    element = await $(selector);
  }

  let command;
  if (type === 'value') {
    command = 'getValue';
  } else {
    command = 'getText';
  }

  const value = await element[command]();
  const valueLength = value.length;

  switch (checkType) {
    case 'equal':
      expect(valueLength).toEqual(expectedLength);
      break;
    case 'be less than':
      expect(valueLength).toBeLessThan(expectedLength);
      break;
    case 'be less than or equal to':
      expect(valueLength).toBeLessThanOrEqual(expectedLength);
      break;
    case 'be greater than':
      expect(valueLength).toBeGreaterThan(expectedLength);
      break;
    case 'be greater than or equal to':
      expect(valueLength).toBeGreaterThanOrEqual(expectedLength);
      break;
    default:
      throw new Error('comparison value is not recognised');
  }
};
