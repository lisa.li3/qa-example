import type { Selector } from 'webdriverio';
import getElement from './getElement';
import { definedCustomerValue } from '../functions/definedCustomer';

/**
 * Check if the given elements contains text
 * @param  {String}   elementType   Element type (element or button)
 * @param  {String}   selector      Element selector
 * @param  {String}   falseCase     Whether to check if the content contains
 *                                  the given text or not
 * @param  {String}   expectedText  The text to check against
 */
export default async (
  elementType: 'element' | 'button',
  selector: Selector,
  falseCase: ' not',
  expectedText: string
): Promise<void> => {
  let element;
  if (typeof selector === 'string') {
    element = await getElement({ selector });
  }
  /**
   * The command to perform on the browser object
   * @type {String}
   */
  let command: 'getValue' | 'getText' = 'getValue';

  if (
    ['button', 'container'].includes(elementType) ||
    (await element.getProperty('value')) === null
  ) {
    command = 'getText';
  }

  /**
   * False case
   * @type {Boolean}
   */
  let boolFalseCase;

  /**
   * The expected text
   * @type {String}
   */
  let stringExpectedText = expectedText;

  /**
   * The text of the element
   * @type {String}
   */
  const elem = await element;
  await elem.waitForDisplayed();
  const text = await elem[command]();

  if (typeof expectedText === 'undefined') {
    stringExpectedText = falseCase;
    boolFalseCase = false;
  } else {
    boolFalseCase = falseCase === ' not';
  }

  const customerValue = (await definedCustomerValue(stringExpectedText)) || stringExpectedText;

  if (boolFalseCase) {
    if (stringExpectedText === customerValue) {
      expect(text).not.toContain(stringExpectedText);
    } else {
      expect(text.toLowerCase()).not.toContain(customerValue.toLowerCase());
    }
  } else {
    if (stringExpectedText === customerValue) {
      expect(text).toContain(stringExpectedText);
    } else {
      expect(text.toLowerCase()).toContain(customerValue.toLowerCase());
    }
  }
};
