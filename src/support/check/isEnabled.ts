import getElement from './getElement';

/**
 * Check if the given selector is enabled
 * @param  {String}   index     The index of the option - This is optional and used when passing in an array of elements rather than a single element
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Whether to check if the given selector
 *                              is enabled or not
 */
export default async (index: string, selector: string, falseCase: boolean): Promise<void> => {
  /**
   * The index of the option to select
   * @type {Int}
   */
  const optionIndex = parseInt(index, 10) | 0; // Default to 0 if index isn't defined

  let element;
  try {
    element = await getElement({ selector });
  } catch (err) {
    if (falseCase) {
      return;
    }
    throw err;
  }
  /**
   * The enabled state of the given selector
   * @type {Boolean}
   */

  let isEnabled;
  if (Array.isArray(element)) {
    isEnabled = await element[optionIndex - 1].isEnabled();
  } else {
    isEnabled = await element.isEnabled();
  }

  if (falseCase) {
    expect(isEnabled).not.toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" not to be enabled`
    );
  } else {
    expect(isEnabled).toEqual(
      true,
      // @ts-expect-error
      `Expected element "${selector}" to be enabled`
    );
  }
};
