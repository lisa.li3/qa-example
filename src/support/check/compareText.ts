import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Compare the contents of two elements with each other
 * @param  {String}   selector1  Element selector for the first element
 * @param  {String}   falseCase Whether to check if the contents of both
 *                              elements match or not
 * @param  {String}   selector2  Element selector for the second element
 */
export default async (
  selector1: Selector,
  falseCase: boolean,
  selector2: Selector
): Promise<void> => {
  let element;
  let element2;
  try {
    if (typeof selector1 === 'string' && typeof selector2 === 'string') {
      element = await getElement({ selector: selector1 });
      element2 = await getElement({ selector: selector2 });
    }
  } catch (err) {
    element = await $(selector1);
    element2 = await $(selector2);
  }
  /**
   * The text of the first element
   * @type {String}
   */
  const text1 = await element.getText();

  /**
   * The text of the second element
   * @type {String}
   */
  const text2 = await element2.getText();

  if (falseCase) {
    expect(text1).not.toEqual(
      text2,
      // @ts-expect-error
      `Expected text not to be "${text1}"`
    );
  } else {
    expect(text1).toEqual(
      text2,
      // @ts-expect-error
      `Expected text to be "${text1}" but found "${text2}"`
    );
  }
};
