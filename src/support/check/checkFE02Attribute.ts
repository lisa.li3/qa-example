import { getState } from '../functions/state';
import { DOMParser } from 'xmldom';
import xpath from 'xpath';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { definedCustomerValue } from '../functions/definedCustomer';
import { get } from 'lodash';

/**
 * Check the stored FE02 for attributes and their values
 */
export default async (path: string, attribute: string, value: string) => {
  const state = getState();
  const FE02 = state.FE_02_ORDER;
  const feature = state.feature;

  const XMLDOC = new DOMParser().parseFromString(FE02);
  const select = xpath.useNamespaces({ oms: 'http://www.supergroup.com/ecom-orders-to-oms/' });

  const pathCheck = select(`${path}`, XMLDOC);
  if (pathCheck.length === 0) {
    throw Error(
      `Path ${path} does not exist in FE02 Order. Make sure XPath is from the oms:Order level`
    );
  }

  const attributeCheck = select(`${path}[@${attribute}]`, XMLDOC);
  if (attributeCheck.length === 0) {
    throw Error(`Path ${path} does not have attribute ${attribute}!`);
  }

  console.log('Value: ', value);

  const splitPath = value.split('.');
  console.log('splitPath', splitPath);

  if (splitPath) {
    const definedType = splitPath[0];

    console.log('definedType', definedType);

    // eslint-disable-next-line sonarjs/no-small-switch
    switch (definedType) {
      case 'user':
        const user = feature.user[0];
        if (!user) throw Error('No user defined in feature');
        value = get(user, value.split(`user.`)[1]);
      default:
        break;
    }
  }

  const expectedValue = (await definedCustomerValue(value)) || value;

  console.log('expectedValue', expectedValue);

  const fullCheck = select(`${path}[@${attribute} = "${expectedValue}"]`, XMLDOC);
  if (fullCheck.length === 0) {
    throw Error(
      `The attribute(s) ${attribute} value was not ${expectedValue}! please check report for XML`
    );
  }
  cucumberJson.attach(`Attribute Tested: \n\n${fullCheck[0].toString()}\n`, 'text/plain');
};
