import type { Selector } from 'webdriverio';

import checkContainsAnyText from './checkContainsAnyText';

export default async (
  elementType: 'button' | 'element',
  element: Selector,
  falseCase: ' not'
): Promise<void> => {
  let newFalseCase = true;

  if (typeof falseCase === 'function' || falseCase === ' not') {
    newFalseCase = false;
  }

  await checkContainsAnyText(elementType, element, newFalseCase);
};
