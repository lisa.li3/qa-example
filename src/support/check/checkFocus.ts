import type { Selector } from 'webdriverio';
import getElement from './getElement';

/**
 * Check if the given element has the focus
 * @param  {String}   selector  Element selector
 * @param  {String}   falseCase Whether to check if the given element has focus
 *                              or not
 */
export default async (selector: Selector, falseCase: boolean): Promise<void> => {
  let element;
  try {
    if (typeof selector === 'string') {
      element = await getElement({ selector });
    }
  } catch (err) {
    element = await $$(selector);
  }
  /**
   * Value of the hasFocus function for the given element
   * @type {Boolean}
   */
  const hasFocus = await element.isFocused();

  if (falseCase) {
    expect(hasFocus).not.toBe(
      true,
      // @ts-expect-error
      'Expected element to not be focused, but it is'
    );
  } else {
    expect(hasFocus).toBe(
      true,
      // @ts-expect-error
      'Expected element to be focused, but it is not'
    );
  }
};
