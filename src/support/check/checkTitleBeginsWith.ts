/**
 * Check the title of the current browser window contains expected text/title
 * @param  {Type}     falseCase     Whether to check if the title contains the
 *                                  expected value or not
 * @param  {Type}     expectedTitle The expected title
 */
export default async (falseCase: boolean, expectedMatch: string): Promise<void> => {
  /**
   * The actual title of the current browser window
   * @type {String}
   */
  const title = await browser.getTitle();
  const titleMatch = title.slice(0, expectedMatch.length);

  if (falseCase) {
    expect(titleMatch).not.toEqual(
      expectedMatch,
      // @ts-expect-error
      `Expected title not to begin with "${expectedMatch}"`
    );
  } else {
    expect(titleMatch).toEqual(
      expectedMatch,
      // @ts-expect-error
      `Expected title to not begin with "${expectedMatch}"`
    );
  }
};
