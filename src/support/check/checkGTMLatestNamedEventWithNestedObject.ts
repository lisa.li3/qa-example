/**
 * Check the GTM Event property array values
 * @param  {String}   eventName       The GTM event to check
 * @param  {String}   property        The GTM event parent property to check
 * @param  {string}   childProperty   The child property to check
 * @param  {string}   objectProperty  The property of the item contained in the Object
 * @param  {String}   objectValue     The value of the Object property to check
 */
export default async (
  eventName: string,
  property: string,
  childProperty: string,
  objectProperty: string,
  objectValue: string | number
): Promise<void> => {
  const dataLayer = await browser.execute('return window.dataLayer');

  // @ts-ignore
  const foundEvents = await dataLayer.filter((item) => {
    return item.event === eventName;
  });

  if (foundEvents.length === 0) {
    throw Error(`No ${eventName} events were found in the dataLayer`);
  }
  const lastEventEntry = foundEvents[foundEvents.length - 1];

  if (!lastEventEntry.hasOwnProperty(property)) {
    throw Error(`Property ${property} does not exist in the latest ${eventName} dataLayer event`);
  }
  const returnedChildItem = lastEventEntry[property][childProperty];

  console.log('Returned Value Object', returnedChildItem);

  if (returnedChildItem.hasOwnProperty(objectProperty)) {
    expect(returnedChildItem[objectProperty].toString()).toEqual(objectValue);
  } else {
    throw Error(
      `Property ${objectProperty} does not exist in associated item ${property}  for the latest ${eventName} dataLayer event`
    );
  }
};
