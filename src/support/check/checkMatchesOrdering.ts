import getElement from './getElement';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

/**
 * Check if the given elements returned text values are in the same order as the expected text values
 * @param  {String}   position        Check the beginning or end of the string (first or last)
 * @param  {String}   quantityToCheck Number of text items to check returned by the element (assumes separate by a newline character)
 * @param  {String}   selector        Element selector
 * @param  {String}   expectedText    The text to validate against
 */

export default async (
  position: string,
  quantityToCheck: number,
  selector: string,
  expectedText: string
): Promise<void> => {
  const elementText = await getElement({ selector, multipleSelectors: false });
  let elementTextParsed;

  if (elementText && typeof elementText === 'string' && quantityToCheck) {
    let elementTextSplitArray;
    if (position !== 'first') {
      elementTextSplitArray = elementText.split(/\r?\n/).slice(-quantityToCheck);
    } else {
      elementTextSplitArray = elementText.split(/\r?\n/).slice(0, quantityToCheck);
    }
    elementTextParsed = '';
    for (const item of elementTextSplitArray) {
      elementTextParsed += `${item}\n`;
    }
    elementTextParsed = elementTextParsed.replace(/\n*$/, ''); // remove the last new line character
  }

  if (!elementTextParsed) {
    elementTextParsed = elementText;
  }

  cucumberJson.attach(`checkMatchesOrdering: Element Text:\n${elementTextParsed}`, 'text/plain');
  cucumberJson.attach(`checkMatchesOrdering: Expected Text:\n${expectedText}`, 'text/plain');
  expect(elementTextParsed).toEqual(expectedText);
};
