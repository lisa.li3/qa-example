/* eslint-disable @typescript-eslint/no-explicit-any */
import { WebdriverIOQueries } from '@project/testing-library-webdriverio';

export interface GetTranslationFromPathOptions {
  page: string;
  item: string;
  locale?: string;
}

export interface GetTranslation {
  (options: string): Promise<RegExp>;
  (options: { text: string; contains: true; locale?: string }): Promise<RegExp>;
  (options: { text: string; startsWith: true; locale?: string }): Promise<RegExp>;
  (options: { text: string; endsWith: true; locale?: string }): Promise<RegExp>;
  (options: {
    text: string;
    contains?: false;
    startsWith?: false;
    endsWith?: false;
    locale?: string;
  }): Promise<string>;
  (options: {
    page: string;
    item: string;
    contains?: boolean;
    startsWith?: boolean;
    endsWith?: boolean;
    locale?: string;
  }): Promise<string | RegExp | false>;
}

export interface WDIOConfig extends WebdriverIO.Config {
  waitforTimeout_get: number;
  waitforTimeout_find: number;
  waitforTimeout_clientMutation: number;
  waitforTimeout_serverMutation: number;
}

declare global {
  namespace WebdriverIO {
    interface Browser extends WebdriverIOQueries {
      addContext: (...args: string[]) => void;
      getTranslation: GetTranslation;
      seed: (
        payload: any,
        siteId?: string,
        seedValue?: number,
        locale?: string
      ) => Promise<{ [serviceName: string]: any }>;
      configureEnv: (...ops: any[]) => Promise<any>;
      commitEnvConfig: (configArtifacts: any[]) => Promise<void>;
      setSiteContext: (urlPrefix: string) => Promise<void>;
      waitForPath: (path: string | RegExp) => Promise<void>;
      capabilities: {
        platform: any;
        browserName: string;
        platformName: string;
      };
      isTablet: boolean;
      config: WDIOConfig;
    }
    // eslint-disable-next-line
    interface Element extends WebdriverIOQueries {}
  }
}
