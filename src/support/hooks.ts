/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import debug from 'debug';
import { get, includes, isEmpty, isEqual } from 'lodash';
import findChromeVersion from 'find-chrome-version';
import dotenv from 'dotenv';

dotenv.config();

import os from 'os';
import fs, { existsSync, readdirSync, readFileSync } from 'fs';
import { v5 } from 'uuid';
import path from 'path';
import { generate } from 'multiple-cucumber-html-reporter';
import { setupBrowser, onInject } from '@project/testing-library-webdriverio';
import {
  getState,
  resetState,
  setShortLivedState,
  resetShortLivedState,
  setState,
  setFeatureInState,
  setScenarioInState,
} from './functions/state';
import { translatedTerms } from './functions/translatedSearchTerms';
import { IConstants } from './types/IConstants';
import addCommands from './wdioHelpers/addCommands';
import overrideCommands from './wdioHelpers/overrideCommands';
import moment from 'moment';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import { makeBadge } from 'badge-maker';
import PopinBasket from '../pageobjects/PopinBasket.page';
import CheckoutOrderSummary from '../pageobjects/CheckoutOrderSummary.page';
import CheckoutStaffDiscountTerms from '../pageobjects/CheckoutStaffDiscountTerms.page';
import Path from 'path';

const branch = process.env.BRANCH || 'develop';
const device = process.env.DEVICE || 'desktop';
const tagExpression = process.env.TAG_EXPRESSION;
const tag = process.env.TAG || '@REGRESSION';
const featureBranchPrefix = process.env.featureBranch || '/B2C2-123';
const username = os.userInfo().username || 'unknown';

const TEST_AUTOMATION_REPORTING = 'test-automation-reporting';
const TEXT_PLAIN = 'text/plain';

const uuidSalt = `${branch}/${device}/${tagExpression}/${username}/${featureBranchPrefix}`;
//
// console.log(
//   `Branch: ${branch}, Device: ${device}, tagExpression: ${tagExpression}. This will generate ${uuidSalt}`
// );

//
// =====
// Hooks
// =====
// WebdriverIO provides a several hooks you can use to interfere the test process in order to
// enhance it and build services around it. You can either apply a single function to it or
// an array of methods. If one of them returns with a promise,
// WebdriverIO will wait until that promise is resolved to continue.
//

const BASE_URL =
  process.env.baseUrl && process.env.baseUrl?.endsWith('/')
    ? process.env.baseUrl.slice(0, -1)
    : process.env.baseUrl || 'https://com.qa.prometheus.sd.co.uk';

export const BUILD_ID = process.env.buildID || v5(uuidSalt, `228761fe-d29a-4b25-8693-fb63b286da46`);
const COUNTRY_SITE = getCountrySite(process.env.COUNTRYSITE);
const SITE_ID = process.env.SITE_ID || 'site-uk';

export const IS_DYNAMIC_ENV = BASE_URL?.includes('dynamic');

const { SERVICE_STAGE, STAGE } = getBaseConstants(BASE_URL, IS_DYNAMIC_ENV);

const anyLoading = async (testText): Promise<boolean> => {
  const loadersFound = 0;
  let loadingCounts = {
    ariaBusy: 0,
    testIdLoading: 0,
    skeleton: 0,
    spinner: 0,
    impliedForms: 0,
    total: 0,
  };

  const namespace = 'afterHook:loading';
  const _info = debug(`info:${namespace}`);

  let timesWithoutMinibag = 0;

  try {
    // Implied forms
    const fieldsRequiredHelperText = ((await $$('sup.form__required')) || []).length > 0;
    const forms = !fieldsRequiredHelperText
      ? false
      : ((await $$('form:not(.navigation__search-bar)')) || []).length > 0;
    const impliedForms = fieldsRequiredHelperText && !forms ? 1 : 0;

    // Halt loading for open modal
    if ((await browser.getUrl()).includes('/checkout')) {
      try {
        const outOfStockModal = await CheckoutOrderSummary.outOfStockModal;
        cucumberJson.attach(
          `Out of Stock Modal Shown: ${outOfStockModal ? 'yes' : 'no'}`,
          TEXT_PLAIN
        );
        _info('Skipping (OOS Modal shown)', testText);
        return false;
      } catch (modalError) {
        // Ignore Error
      }
      try {
        const staffDiscountModal = await CheckoutStaffDiscountTerms.termsDialog;
        cucumberJson.attach(
          `Staff Discount Modal Shown: ${staffDiscountModal ? 'yes' : 'no'}`,
          TEXT_PLAIN
        );
        _info('Skipping (Staff Discount Modal shown)', testText);
        return false;
      } catch (modalError) {
        // Ignore Error
      }
    }

    // Halt loading for open toast
    const toastShown = ((await $$('.Toastify__toast-body')) || []).length > 0;
    if (toastShown) {
      _info('Skipping loading check due to toast shown');
      return false;
    }

    // Halt loading for open minibag
    try {
      const miniBag = await PopinBasket.miniBagDrawer;
      cucumberJson.attach(`MiniBag Shown: ${miniBag ? 'yes' : 'no'}`, TEXT_PLAIN);
      _info('Skipping (miniBag shown)', testText);
      if (timesWithoutMinibag > 0) {
        return false;
      }
    } catch (miniBagError) {
      timesWithoutMinibag++;
      // Ignore the error thrown as it might not be shown
    }

    loadingCounts = {
      ...loadingCounts,
      ariaBusy: ((await $$('[aria-busy="true"]')) || []).length,
      testIdLoading: ((await $$('[data-testid="loading"]')) || []).length,
      skeleton: ((await $$('.react-loading-skeleton')) || []).length,
      spinner: ((await $$('.spinner')) || []).length,
      impliedForms,
    };
    loadingCounts.total = Object.values(loadingCounts).reduce((a, b) => a + b);
  } catch {
    return true;
  }
  return loadingCounts.total > 0;
};

export const hooks = {
  /**
   * Gets executed once before all workers get launched.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   */
  onPrepare: async (config, capabilities): Promise<void> => {
    console.log(`Running Tests on Env: ${BASE_URL}`);
  },
  /**
   * Gets executed before a worker process is spawned & can be used to initialize specific service
   * for that worker as well as modify runtime environments in an async fashion.
   * @param  {String} cid    capability id (e.g 0-0)
   * @param  {[type]} caps   object containing capabilities for session
   * @param  {[type]} specs  specs to be run in the worker process
   * @param  {[type]} args   object that will be merged with the main
   *                         configuration once worker is initialized
   * @param  {[type]} execArgv list of string arguments passed to the worker process
   */
  // onWorkerStart: function (cid, caps, specs, args, execArgv) {
  // },
  /**
   * Gets executed just before initializing the webdriver session and test framework.
   * It allows you to manipulate configurations depending on the capability or spec.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that are to be run
   */
  // beforeSession: function (config, capabilities, specs) {
  // },
  /**
   * Gets executed before test execution begins. At this point you can access to all global
   * variables like `browser`. It is the perfect place to define custom commands.
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that are to be run
   */
  before: async (capabilities, specs): Promise<void> => {
    const logLevel = parseInt(process.env.LOG_LEVEL || '0', 10);

    switch (logLevel) {
      case 0:
        debug.enable(
          'debug:*,info:*,log:*,warn:*,ERROR:*,-*:getTranslation*,-*:pageObjects*,-*:state::*'
        );
        break;
      case 1:
        debug.enable('info:*,log:*,warn:*,ERROR:*');
        break;
      case 2:
        debug.enable('log:*,warn:*,ERROR:*');
        break;
      default:
        debug.enable('warn:*,ERROR:*');
        break;
    }

    const namespace = 'global';
    console.debug = debug(`debug:${namespace}`);
    console.info = debug(`info:${namespace}`);
    console.log = debug(`log:${namespace}`);
    console.warn = debug(`warn:${namespace}`);
    console.error = debug(`ERROR:${namespace}`);

    //Set up AWS Credentials
    loadAwsAuth();

    // specName variable used in jiraRef
    const specString = String(specs);
    const specNameParts = specString.split('\\');

    // @ts-ignore
    const browserName = capabilities.browserName || '';
    // @ts-ignore
    const platformName = capabilities.platformName || 'unknown';

    // Manage site config
    const constants: IConstants = {
      BASE_URL,
      BROWSER_NAME: browserName,
      COUNTRY_SITE,
      EMAIL_ADDRESS: `projecttesting+${moment().unix()}+${COUNTRY_SITE}@gmail.com`,
      SEARCH_TERMS: translatedTerms[COUNTRY_SITE],
      STAGE,
      SERVICE_STAGE,
    };
    const { siteConfig } = await setState(constants);

    // Retrieve translations
    const locale = siteConfig.defaultLocale;

    // Add any WDIO overrides and commands to browser object
    addCommands(browserName, platformName, locale, STAGE, SITE_ID, SERVICE_STAGE);
    overrideCommands();

    // Set up Testing Library (https://testing-library.com/) for use in the tests
    // Queries defined globally in ./src/global.ts
    // @ts-ignore
    setupBrowser(browser);
    onInject(() => {
      // @ts-ignore
      window.Simmer = window.Simmer.configure({
        depth: 20,
      });
    });
    if (browser.isIOS) {
      // we're in an Appium context; JSON-WP methods available
      await browser.setImplicitTimeout(20000);
      await browser.setAsyncTimeout(60000);
    }

    const format = {
      label: `${branch} / ${device} / ${tag}`,
      message: 'Running',
      color: 'blue',
    };

    const svg = makeBadge(format);

    const s3Client = new S3Client(loadAwsAuth() || {});

    await s3Client.send(
      new PutObjectCommand({
        Bucket: TEST_AUTOMATION_REPORTING,
        Key: `${BUILD_ID}/badge/run.svg`,
        Body: svg,
        ContentType: 'image/svg+xml',
      })
    );
  },
  /**
   * Gets executed before the suite starts.
   * @param {Object} suite suite details
   */
  beforeSuite(suite): void {
    resetState();
  },
  /**
   * This hook gets executed _before_ every hook within the suite starts.
   * (For example, this runs before calling `before`, `beforeEach`, `after`)
   *
   * (`stepData` and `world` are Cucumber-specific.)
   *
   */
  // beforeHook: async (test, context, stepData, world) => {},
  /**
   * Hook that gets executed _after_ every hook within the suite ends.
   * (For example, this runs after calling `before`, `beforeEach`, `after`, `afterEach` in Mocha.)
   *
   * (`stepData` and `world` are Cucumber-specific.)
   */
  afterHook: async (
    test,
    context,
    { error, result, duration, passed, retries },
    stepData,
    world
  ): Promise<void> => {
    const namespace = 'afterHook:loading';
    const _debug = debug(`debug:${namespace}`);
    const _info = debug(`info:${namespace}`);

    const testText = test.text;
    const { shortLived } = getState();

    const isOpen =
      (includes(testText, 'open the') && !includes(testText, 'without waiting for page load')) ||
      includes(testText, 'refresh the page') ||
      includes(testText, 'login with');
    const isClick = includes(testText, 'click') && !includes(testText, 'on the iframe');
    const isForced = get(shortLived, 'runLoadingDetection', false);

    _debug(testText, { isOpen }, { isClick }, { isForced });
    if (isOpen || isClick || isForced) {
      _info('Started', testText);
      await setShortLivedState('runLoadingDetection', false);

      const startTimestamp = moment();
      const pauseDurationMs = 100;
      const minimumCleanChecks = isClick ? 2 : 5;
      const maximumResets = Math.ceil((browser?.config?.waitforTimeout || 10000) / pauseDurationMs);

      const pause = (ms?: number): Promise<void> => {
        return new Promise((resolve) => setTimeout(resolve, ms || pauseDurationMs));
      };

      let cleanChecks = 0;
      let resets = 0;
      while (cleanChecks <= minimumCleanChecks && resets <= maximumResets) {
        if (await anyLoading(testText)) {
          // Anything loading check
          _debug('...');
          cleanChecks = 0;
          resets++;
          await pause();
        } else {
          // All clear
          cleanChecks++;
        }
      }

      const durationSeconds = moment().diff(startTimestamp, 'seconds', true);
      _info('Finished', testText, `Page took ${durationSeconds} seconds to load`);
      cucumberJson.attach(`Page took ${durationSeconds} seconds to load`, TEXT_PLAIN);
    } else if (process.env.SLOW_MO) {
      // TODO: finish me!
      const delay = (ms?: number): Promise<void> => {
        return new Promise((resolve) => setTimeout(resolve, ms));
      };
      await delay(parseInt(process.env.SLOW_MO));
    }
  },
  /**
   * Function to be executed before a test (in Mocha/Jasmine) starts.
   */
  // beforeTest: function (test, context) {},
  /**
   * Runs before a WebdriverIO command is executed.
   * @param {String} commandName hook command name
   * @param {Array} args arguments that the command would receive
   */
  beforeCommand: async (commandName, args): Promise<void> => {
    // await browser.waitUntil(() => browser.execute(() => document.readyState === 'complete'), {
    //   timeout: 60 * 1000, // 60 seconds
    //   timeoutMsg: 'Message on failure',
    // });
  },
  /**
   * Runs after a WebdriverIO command gets executed
   * @param {String} commandName hook command name
   * @param {Array} args arguments that command would receive
   * @param {Number} result 0 - command success, 1 - command error
   * @param {Object} error error object, if any
   */
  // afterCommand: async (commandName, args, result, error) => {},
  /**
   * Function to be executed after a test (in Mocha/Jasmine)
   */
  // afterTest: async (test, context, { error, result, duration, passed, retries }) => {
  // },
  /**
   * Hook that gets executed after the suite has ended.
   * @param {Object} suite suite details
   */
  // afterSuite: function (suite) {
  // },
  /**
   * Gets executed after all tests are done. You still have access to all global variables from
   * the test.
   * @param {Number} result 0 - test pass, 1 - test fail
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that ran
   */
  // after: function (result, capabilities, specs) {
  // },
  /**
   * Gets executed right after terminating the webdriver session.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that ran
   */
  // afterSession: function (config, capabilities, specs) {
  // },
  /**
   * Gets executed after all workers have shut down and the process is about to exit.
   * An error thrown in the `onComplete` hook will result in the test run failing.
   * @param {Object} exitCode 0 - success, 1 - fail
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {<Object>} results object containing test results
   */
  onComplete: async (exitCode, config, capabilities, results): Promise<void> => {
    // Run Info
    let runner, browserType, windowSize;

    // Runner
    if (!isEmpty(get(capabilities, `[0]['sauce:options']`, {}))) {
      runner = 'SauceLabs';
    } else if (!isEmpty(get(capabilities, `[0]['LT:Options']`, {}))) {
      runner = 'LambdaTest';
    } else {
      runner = 'local';
    }

    // Browser Type
    if (isEqual(runner, 'SauceLabs')) {
      browserType = get(config, `['framework:meta'].browserType`, 'unknown');
    } else if (isEqual(runner, 'LambdaTest')) {
      browserType = get(config, `['config-path']`, '').split('.')[2] || 'unknown';
    } else {
      browserType = 'unknown';
    }

    // Window Size
    if (isEqual(runner, 'SauceLabs')) {
      windowSize = get(config, `['framework:meta'].windowSize`, 'unknown');
    } else if (isEqual(runner, 'LambdaTest')) {
      windowSize = get(capabilities, `[0]['LT:Options'].resolution`, 'unknown');
    } else {
      windowSize = 'unknown';
    }

    const tagExpression = config.cucumberOpts.tagExpression || 'unknown';
    const chromeOptionsKey = 'goog:chromeOptions';
    const emulationDevice =
      (capabilities[0][chromeOptionsKey] &&
        capabilities[0][chromeOptionsKey].mobileEmulation &&
        capabilities[0][chromeOptionsKey].mobileEmulation.deviceName) ||
      'no emulation';
    const browserInfo = `${browserType} (${emulationDevice})`;
    const chromeVersion =
      capabilities[0].browserVersion ||
      (typeof findChromeVersion === 'function' ? await findChromeVersion() : 'unknown') ||
      'unknown';
    const browserName = capabilities[0].browserName || 'unknown';
    const osType = capabilities[0].platformName || os.type() || 'unknown';

    // console.log({
    //   runner,
    //   browserType,
    //   browserName,
    //   osType,
    //   chromeVersion,
    //   browserInfo,
    //   emulationDevice,
    //   tagExpression,
    //   windowSize,
    // });

    // Send the report to S3
    try {
      generate({
        // Required
        // This part needs to be the same path where you store the JSON files
        // default = '.tmp/json/'
        jsonDir: '.tmp/json/',
        reportPath: `.tmp/report/${BUILD_ID}`,
        removeFolders: true,
        pageTitle: `${runner} ${tagExpression}`,
        reportName: `Build: ${BUILD_ID}`,
        useCDN: true,
        staticFilePath: `.tmp/report/${BUILD_ID}/features`,
        displayDuration: true,
        customData: {
          title: 'Run info',
          data: [
            { label: 'Run at', value: moment().format('dddd, MMMM Do YYYY, h:mm a') },
            { label: 'Run on', value: BASE_URL },
            { label: 'Runner', value: runner },
            { label: 'Tag Expression', value: tagExpression },
            { label: 'Browser Info', value: browserInfo },
            { label: 'Browser Name', value: browserName },
            { label: 'Window Size', value: windowSize },
            { label: 'Username', value: username },
          ],
        },
        // for more options see https://github.com/wswebcreation/multiple-cucumber-html-reporter#options
      });

      const params = {
        Bucket: TEST_AUTOMATION_REPORTING,
        Key: `${BUILD_ID}/report.html`,
        Body: readFileSync(path.join(__dirname, `../../.tmp/report/${BUILD_ID}/index.html`)),
        ContentType: 'text/html',
      };
      const s3Client = new S3Client(loadAwsAuth() || {});
      await s3Client.send(new PutObjectCommand(params));

      const features = readdirSync(path.join(__dirname, `../../.tmp/report/${BUILD_ID}/features`));
      for (const feature of features) {
        const featParams = {
          Bucket: TEST_AUTOMATION_REPORTING,
          Key: `${BUILD_ID}/features/${feature}`,
          Body: readFileSync(
            path.join(__dirname, `../../.tmp/report/${BUILD_ID}/features/${feature}`)
          ),
          ContentType: 'text/html',
        };
        await s3Client.send(new PutObjectCommand(featParams));
      }

      fs.writeFileSync(
        Path.join(__dirname, `../../${process.env.reportFileName || 'AT-Branch'}.txt`),
        `http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/${params.Key}`
      );

      console.log(
        `--------------------\n\nSuccessfully created report, and uploaded to: http://test-automation-reporting.s3-website-eu-west-1.amazonaws.com/${params.Key}\n\n--------------------`
      );

      const format = {
        label: `${branch} / ${device} / ${tag}`,
        message: exitCode !== 0 ? 'Failed' : 'Passed',
        color: exitCode !== 0 ? 'red' : 'green',
      };

      const svg = makeBadge(format);

      await s3Client.send(
        new PutObjectCommand({
          Bucket: TEST_AUTOMATION_REPORTING,
          Key: `${BUILD_ID}/badge/run.svg`,
          Body: svg,
          ContentType: 'image/svg+xml',
        })
      );
    } catch (err) {
      console.log(`Error pushing report to S3: ${err}`);
    }
  },
  /**
   * Gets executed when a refresh happens.
   * @param {String} oldSessionId session ID of the old session
   * @param {String} newSessionId session ID of the new session
   */
  // onReload: function (oldSessionId, newSessionId) {
  // },
  /**
   * Cucumber-specific hooks
   */
  beforeFeature: async (uri, feature, scenarios): Promise<void> => {
    await setFeatureInState(feature);
    await browser.executeScript(`lambda-name=${feature.name}`, []);
  },
  beforeScenario: async (feature): Promise<void> => {
    await setScenarioInState(feature.pickle);
  },
  // beforeStep: function ({ uri, feature, step }, context) {
  // },
  afterStep: async (
    { uri, feature, step },
    context,
    { error, result, duration, passed }
  ): Promise<void> => {
    if (error) {
      // @ts-ignore
      cucumberJson.attach(`Failed test on URL: ${await browser.getUrl()}`, 'text/plain');
      cucumberJson.attach(await browser.takeScreenshot(), 'image/png');
    }
  },
  afterScenario: async (uri, feature, scenario, result, sourceLocation): Promise<void> => {
    await resetShortLivedState();
  },
  // afterFeature: function (uri, feature, scenarios) {
  // }
};

/**
 * Helpers
 */
export function getBaseConstants(baseUrl: string, isDynamicEnv: boolean): Promise<void> {
  const STAGE = isDynamicEnv ? getDynamicStage(baseUrl) : getStage(baseUrl);
  const SERVICE_STAGE = isDynamicEnv ? getDynamicStage(baseUrl, true) : STAGE;

  return {
    SERVICE_STAGE,
    STAGE,
  };
}

function getStage(baseUrl: string): string {
  const baseStage = baseUrl.split('.')[1];
  if (!['qa', 'test', 'devlocal', 'uat', 'stage'].includes(baseStage)) {
    return 'qa';
  }

  return baseStage;
}

export function getDynamicStage(dynamicBaseUrl: string, isServiceStage = false): string {
  const urlArray = dynamicBaseUrl.split('.');
  return isServiceStage ? `${urlArray[2]}-${urlArray[1]}` : `${urlArray[1]}.${urlArray[2]}`;
}

function getCountrySite(countrySite: string | undefined): string {
  const countrySiteList = [
    'uk',
    'us',
    'ca-fr',
    'ca-en',
    'ch-fr',
    'ch-de',
    'dk',
    'be-nl',
    'be-fr',
    'de',
    'es',
    'fi',
    'fr',
    'ie',
    'it',
    'nl',
    'hk-en',
    'no',
    'pl',
    'se',
    'tw',
  ];

  if (!countrySite || !countrySiteList.includes(countrySite)) {
    return 'uk';
  }

  return countrySite;
}

export function loadAwsAuth(): {
  AWS_ACCESS_KEY_ID: string;
  AWS_SECRET_ACCESS_KEY: string;
  AWS_SESSION_TOKEN: string;
  AWS_REGION: string;
} | void {
  if (existsSync(path.join(__dirname, './../../aws-mfa-cache'))) {
    try {
      const TokenFile = readFileSync(path.join(__dirname, './../../aws-mfa-cache'), {
        encoding: 'utf-8',
      });
      const Token = JSON.parse(TokenFile);
      process.env.AWS_ACCESS_KEY_ID = Token.AccessKeyId;
      process.env.AWS_SECRET_ACCESS_KEY = Token.SecretAccessKey;
      process.env.AWS_SESSION_TOKEN = Token.SessionToken;
      return {
        AWS_ACCESS_KEY_ID: Token.AccessKeyId,
        AWS_SECRET_ACCESS_KEY: Token.SecretAccessKey,
        AWS_SESSION_TOKEN: Token.SessionToken,
        AWS_REGION: process.env.AWS_REGION || 'eu-west-1',
      };
    } catch (e) {
      console.warn('Failed to setup AWS MFA!', e);
    }
  }
}
