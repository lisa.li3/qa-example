import JwtDecode from 'jwt-decode';

export const getUserToken = async (): Promise<string | null> => {
  return await new Promise(async (resolve) => {
    try {
      await browser
        .execute(() => {
          return localStorage.getItem('auth');
        })
        .then((response) => {
          if (response === null || response === '') {
            resolve(null);
          } else {
            resolve(response);
          }
        });
    } catch (error) {
      console.log(error);
      resolve(null);
    }
  });
};

export const getRefreshToken = async (): Promise<string | null> => {
  return await browser.execute(() => {
    return localStorage.getItem('refreshToken');
  });
};

export const decodeToken = async (): Promise<unknown> => {
  const token = await getUserToken();
  if (token) {
    return JwtDecode(token);
  } else {
    console.log('No token exists - Guest user!!');
  }
};
