import axios, { AxiosRequestConfig } from 'axios';
import { getUserToken, decodeToken, getRefreshToken } from './authService';
import { getState } from './state';

import { IUserResponse } from '../types/IUser';
import { ITokenResponse } from '../types/IToken';

const apiVersion = 'v1';

const CONTENT_TYPE = 'application/json';
const ACCEPT_LANGUAGE = 'en-US,en;q=0.9';
const ERROR_STRING = 'The following error occurred : ';

const userLogout = async (): Promise<void> => {
  await browser.execute(() => localStorage.clear());
  await browser.pause(2000);
  const authValue = await browser.execute(() => localStorage.getItem('auth'));

  if (!authValue) {
    console.log('User logged out successfully!!');
  }

  expect([null, '']).toContain(authValue);
};

const getAddressList = async (emailAddress: string, password: string): Promise<void> => {
  const { constants, siteConfig } = getState();

  if (emailAddress && password) {
    const initialToken = await browser.call(() => {
      return getUserToken();
    });

    await userLogin(emailAddress, password);

    await browser.waitUntil(async () => (await getUserToken()) !== initialToken);
  }

  const token = browser.call(() => {
    return getUserToken();
  });

  const config: AxiosRequestConfig = {
    method: 'GET',
    url: `${siteConfig.services.user}/${apiVersion}/addresses/`,
    headers: {
      pragma: 'no-cache',
      'cache-control': 'no-cache',
      authorization: 'Bearer ' + token,
      'user-agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
      'content-type': CONTENT_TYPE,
      origin: constants.BASE_URL,
      'sec-fetch-site': 'cross-site',
      'sec-fetch-mode': 'cors',
      'sec-fetch-dest': 'empty',
      'accept-language': ACCEPT_LANGUAGE,
      referrerPolicy: 'no-referrer-when-downgrade',
      body: null,
    },
  };

  return await new Promise(async function (resolve, reject) {
    await browser.call(() => {
      return axios(config)
        .then(function (response) {
          resolve(response.data);
        })
        .catch(function (error) {
          console.log(error);
          reject(error);
        });
    });
  });
};

const getUserDetails = async (emailAddress: string, password: string): Promise<void> => {
  const { constants, siteConfig } = getState();

  if (emailAddress && password) {
    const initialToken = await browser.call(() => {
      return getUserToken();
    });

    await userLogin(emailAddress, password);
    await browser.waitUntil(async () => (await getUserToken()) !== initialToken);
  }

  const token = await getUserToken();

  const config: AxiosRequestConfig = {
    method: 'GET',
    url: `${siteConfig.services.user}/${apiVersion}`,
    headers: {
      authority: siteConfig.domain,
      pragma: 'no-cache',
      'cache-control': 'no-cache',
      accept: CONTENT_TYPE,
      authorization: 'Bearer ' + token,
      'user-agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
      'content-type': CONTENT_TYPE,
      origin: constants.BASE_URL,
      'sec-fetch-site': 'cross-site',
      'sec-fetch-mode': 'cors',
      'sec-fetch-dest': 'empty',
      referer: constants.BASE_URL,
      'accept-language': ACCEPT_LANGUAGE,
    },
  };

  await new Promise(async function (resolve, reject): Promise<void> {
    return browser.call(async function () {
      return axios(config)
        .then(function (response) {
          resolve(response.data);
        })
        .catch(function (error) {
          console.dir(ERROR_STRING + error.message);
          console.dir(error.response.data.message);
          reject(error.response.data.message);
        });
    });
  });
};

const registerDefinedUser = async (): Promise<string> => {
  const { constants, customerDetails, siteConfig } = getState();
  const Email = customerDetails.Email;

  const data = JSON.stringify({
    emailAddress: Email,
    password: customerDetails.Password,
    gender: customerDetails.Gender.toLowerCase(),
    subscriptionApproval: true,
    defaultAddress: {
      nickname: customerDetails.NickName,
      firstName: customerDetails.FirstName,
      lastName: customerDetails.LastName,
      addressLine1: customerDetails.Address1,
      addressLine2: customerDetails.Address2,
      contactNumber: {
        phone: customerDetails.Phone,
        mobile: customerDetails.Mobile,
      },
      city: customerDetails.City,
      county: customerDetails.County,
      country: customerDetails.CountryA3,
      postcode: customerDetails.Postcode,
      isDefault: true,
    },
    reCaptchaToken: 'ignore',
    locale: siteConfig.defaultLocale,
    siteId: siteConfig.siteId,
  });

  const config: AxiosRequestConfig = {
    method: 'post',
    url: `${siteConfig.services.user}/${apiVersion}/register`,
    headers: {
      accept: CONTENT_TYPE,
      'accept-language': ACCEPT_LANGUAGE,
      'cache-control': 'no-cache',
      pragma: 'no-cache',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-site',
      authority: siteConfig.domain,
      'content-type': CONTENT_TYPE,
      referer: constants.BASE_URL,
      referrerPolicy: 'strict-origin-when-cross-origin',
      mode: 'cors',
    },
    data,
  };

  await new Promise(async function (resolve, reject) {
    return axios(config)
      .then(function (response) {
        const status = response.status;
        console.log(`RDU: User "${Email}" has been registered successfully`);
        console.log(`Response Status code : ${status}`);
        resolve(response);
      })
      .catch(function (error) {
        console.log('returned error ', error);
        console.log(Email);
        console.dir(ERROR_STRING + error.message);
        console.log('User may already be registered');
        reject(error);
      });
  });
  await browser.refresh();
  return customerDetails.Email;
};

const refreshUserToken = async (): Promise<IUserResponse> => {
  const { constants, siteConfig } = getState();
  const token = await getRefreshToken();

  const config: AxiosRequestConfig = {
    method: 'GET',
    url: `${siteConfig.services.user}/${apiVersion}/refresh`,
    headers: {
      accept: CONTENT_TYPE,
      authorization: 'Bearer ' + token,
      referrer: constants.BASE_URL,
    },
  };

  return await axios(config)
    .then(async function (response) {
      await browser.execute(
        function (key1, value1, key2, value2) {
          this.localStorage.setItem(key1, value1);
          this.localStorage.setItem(key2, value2);
        },
        'auth',
        response.data.userToken,
        'refreshToken',
        response.data.refreshToken
      );
      return response.data;
    })
    .catch(function (error) {
      console.dir(ERROR_STRING + error);
    });
};

const userLogin = async (
  emailAddress: string,
  password: string
): Promise<ITokenResponse | unknown> => {
  const { constants, siteConfig } = getState();
  const token = await getUserToken();

  if ((token === undefined || token === null || token === '') && emailAddress && password) {
    const data = JSON.stringify({
      emailAddress,
      password: password,
      siteId: siteConfig.siteId,
      reCaptchaToken: 'ignore',
      bypassReCaptchaToken: 'ignore',
    });

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${siteConfig.services.user}/${apiVersion}/login`,
      headers: {
        accept: CONTENT_TYPE,
        'content-type': CONTENT_TYPE,
        referer: constants.BASE_URL,
      },
      data,
    };
    console.log('Attempting to authenticate user...');
    return await new Promise(async function (resolve, reject) {
      try {
        const authenticateUser = await axios(config);
        const { userToken, refreshToken } = (await authenticateUser).data;
        await (async function (): Promise<void> {
          console.log(userToken);
          console.log('Successfully authenticated user :  ' + emailAddress);
          await browser.execute(
            function (key1, value1, key2, value2) {
              this.localStorage.setItem(key1, value1);
              this.localStorage.setItem(key2, value2);
            },
            'auth',
            userToken,
            'refreshToken',
            refreshToken
          );
        })();
        await browser.refresh();
        resolve(userToken);
      } catch (error) {
        console.log(error);
        console.log(
          `Could not log in the user with email address "${emailAddress}". They may not be registered.`
        );
        console.log('The server returned the following response code : ' + error.response.status);
        return reject(error.response.status);
      }
    });
  } else if (token) {
    const currentLoggedInUser = await decodeToken();
    const currentLoggedInUserEmailAddress = await currentLoggedInUser.emailAddress;
    console.log(
      `The user with email address: ${currentLoggedInUserEmailAddress} is currently logged in`
    );
    if (emailAddress != null && currentLoggedInUserEmailAddress != emailAddress) {
      console.log(
        `Please log out user : ${currentLoggedInUserEmailAddress} if you want to log in user : ${emailAddress}`
      );
    }
    await refreshUserToken();
    return await getUserToken();
  } else {
    console.log(
      `A user is not currently logged in. You need to provide an email address in order to login!!!`
    );
  }
};

export {
  userLogin,
  registerDefinedUser,
  getUserDetails,
  getAddressList,
  userLogout,
  refreshUserToken,
};
