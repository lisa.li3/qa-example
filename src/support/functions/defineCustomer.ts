import faker from 'faker';
import moment from 'moment';
import { random, toLower } from 'lodash';

import { ISiteConfig } from '../types/ISiteConfig';
import { ICustomerDetails } from '../types/ICustomerDetails';

const locationBlock: { city: string; countryA3: string; county: string; postcode: string }[] = [
  { countryA3: 'GBR', city: 'Cheltenham', county: 'Gloucestershire', postcode: 'GL50 3JL' },
  { countryA3: 'DEU', city: 'Berlin', county: 'Leipziger', postcode: '10117' },
  { countryA3: 'USA', city: 'New York', county: 'NY', postcode: '10001' },
  { countryA3: 'CHE', city: 'Ebikon', county: 'Ebikon', postcode: '6030' },
  { countryA3: 'SWE', city: 'Göteborg', county: 'Göteborg', postcode: '411 07' },
  { countryA3: 'ESP', city: 'Madrid', county: 'Madrid', postcode: '28001' },
  { countryA3: 'PRT', city: 'Berlin', county: 'Leipziger', postcode: '10117' },
  { countryA3: 'POL', city: 'Berlin', county: 'Leipziger', postcode: '10117' },
  { countryA3: 'NOR', city: 'Strømmen', county: 'Støperiveien', postcode: '2010' },
  { countryA3: 'NLD', city: 'Amsterdam', county: 'Amsterdam', postcode: '1012 XG' },
  { countryA3: 'LUX', city: 'Luxembourg', county: 'Luxembourg', postcode: '54290' },
  { countryA3: 'ITA', city: 'Roma', county: 'Roma', postcode: '00128' },
  { countryA3: 'IRL', city: 'Dublin', county: 'Dublin', postcode: 'D02 X854' },
  { countryA3: 'HKG', city: 'Hong Kong', county: 'Hong Kong', postcode: '1013217' },
  { countryA3: 'GRC', city: 'Androutsou', county: 'Piraeus', postcode: 'AT4123' },
  { countryA3: 'FRA', city: 'Paris', county: 'Paris', postcode: '75004' },
  { countryA3: 'FIN', city: 'Helsinki', county: 'Firdonkatu', postcode: '00520' },
  { countryA3: 'DNK', city: 'København', county: 'København', postcode: '1150' },
  { countryA3: 'CAN', city: 'Ottawa', county: 'Ottawa', postcode: 'K1N322' },
  { countryA3: 'BEL', city: 'Brugge', county: 'Brugge', postcode: '8000' },
  { countryA3: 'AUT', city: 'Salzburg', county: 'Salzburg', postcode: '5020' },
  { countryA3: 'AUS', city: 'Melbourne', county: 'Victoria', postcode: 'MB1264' },
];

const defineCustomer = async (siteConfig: ISiteConfig): Promise<ICustomerDetails> => {
  faker.setLocale(process.env.locale || 'en_GB');

  const genders = ['Female', 'Male'];
  const gender = faker.random.arrayElement(genders);
  const countryA3 = siteConfig.defaultCountryCode;
  const addressBreakdown = locationBlock.filter((item) => item.countryA3 === countryA3);
  const city = addressBreakdown[0].city;
  const county = addressBreakdown[0].county;
  const postcode = addressBreakdown[0].postcode;
  const countriesToFilter = siteConfig.deliveryCountries;
  const country = countriesToFilter.filter((item) => item.code === countryA3);
  const email = toLower(
    `projecttesting+${moment().unix()}+${random(1000, 9999)}+${siteConfig.defaultLocale}@gmail.com`
  );

  return {
    Email: email,
    Password: 'Sum3rt1me!',
    Gender: gender,
    FirstName: faker.name.firstName(),
    LastName: faker.name.lastName(),
    Phone: faker.phone.phoneNumber(),
    Mobile: faker.phone.phoneNumber(),
    NickName: faker.lorem.word(),
    Address1: faker.address.streetAddress(),
    Address2: faker.lorem.word(),
    City: city,
    County: county,
    Postcode: postcode,
    CountryA3: countryA3,
    Country: country[0].name,
    Marketing: 'true',
  };
};

export { defineCustomer };
