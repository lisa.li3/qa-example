import { mkdirSync, PathLike } from 'fs';

export const mkDir = (dirPath: PathLike): void => {
  try {
    return mkdirSync(dirPath);
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
};
