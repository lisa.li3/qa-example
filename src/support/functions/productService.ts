import axios, { AxiosRequestConfig } from 'axios';
import { WDIOIProduct } from '../types/IProduct';
import { IProductsResults } from '../types/IProductService';
import { getState } from './state';

const apiVersion = 'v2';

const productSearch = async (
  searchTerm: string,
  numberToReturn: number
): Promise<WDIOIProduct[]> => {
  const { siteConfig } = getState();
  let searchResultCount;

  const url = `${siteConfig.services.product}/${apiVersion}/search/?searchTerm=${searchTerm}&siteId=${siteConfig.siteId}&locale=${siteConfig.pimLocale}&shopperGroupId=${siteConfig.shopperGroupId}`;

  const productsListing = <WDIOIProduct[]>[];

  const config: AxiosRequestConfig = {
    method: 'get',
    url,
    headers: {},
  };

  await new Promise<void>(function (resolve, reject) {
    return axios(config)
      .then(function (response) {
        const results: IProductsResults = response.data;
        searchResultCount = Object.keys(results.products).length;
        for (let i = 0; i < numberToReturn && i < searchResultCount; i++) {
          productsListing.push(results.products[i]);
        }
        resolve();
      })
      .catch(function (error) {
        console.log(error);
        reject(error);
      });
  });

  await browser.waitUntil(
    () => productsListing.length === numberToReturn || productsListing.length === searchResultCount,
    {
      timeout: 50000,
    }
  );

  productsListing.forEach((item) => {
    item.searchTerm = searchTerm;
  });
  return productsListing;
};

const getProductByID = async (id: string): Promise<WDIOIProduct> => {
  const { siteConfig } = getState();
  const url = `${siteConfig.services.product}/${apiVersion}/${id}?siteId=${siteConfig.siteId}`;

  return getProduct(url);
};

const getProductBySKU = async (sku: string): Promise<WDIOIProduct> => {
  const { siteConfig } = getState();
  const url = `${siteConfig.services.product}/${apiVersion}/sku/${sku}?siteId=${siteConfig.siteId}`;

  console.log('url', url);

  return getProduct(url);
};

export { productSearch, getProductByID, getProductBySKU };

////////////////////////////////////////////////////////////////
// Shared functions
////////////////////////////////////////////////////////////////

async function getProduct(url: string): Promise<void> {
  const { constants } = getState();
  const config: AxiosRequestConfig = {
    method: 'get',
    headers: {
      accept: 'application/json',
      'accept-language': 'en-US,en;q=0.9',
      'cache-control': 'no-cache',
      pragma: 'no-cache',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-site',
      referer: constants.BASE_URL,
    },
    url,
  };

  return axios(config)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      throw Error(error);
    });
}
