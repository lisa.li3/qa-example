import axios, { AxiosRequestConfig } from 'axios';

import { getState } from './state';

interface IInventory {
  [productSku: string]: number;
}

const apiVersion = 'v2';

const getConfigHeaders = (): unknown => {
  const { constants, siteConfig } = getState();
  return {
    authority: siteConfig.domain,
    pragma: 'no-cache',
    'cache-control': 'no-cache',
    accept: 'application/json',
    'user-agent':
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
    'content-type': 'application/json',
    origin: constants.BASE_URL,
    'sec-fetch-site': 'cross-site',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    referer: constants.BASE_URL,
    'accept-language': 'en-US,en;q=0.9',
  };
};

/*
  Takes the availabe size ids from the product search and the country code (e.g "GBR")
  and checks if the sizes are actually in inventory (check by sku).
*/
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const checkInventory = async (
  sizes: Array<{ sku: string }>,
  country: string
): Promise<IInventory> => {
  const { siteConfig } = getState();
  const skus = sizes.map((item) => item.sku);

  let url = `${siteConfig.services.inventory}/${apiVersion}/?siteShortName=${siteConfig.shortName}&siteId=${siteConfig.siteId}&country=${country}&skus=`;

  skus.forEach((item) => {
    url = url.concat(item + '%2C');
  });

  url = url.substring(0, url.length - 3);

  const config: AxiosRequestConfig = {
    method: 'GET',
    url: url,
    headers: getConfigHeaders(),
  };

  try {
    const response = await axios(config);
    const result = (await response).data;
    console.log(`Raw data from Inventory Service: ${JSON.stringify(result)}`);
    // Merge the two together
    const defaultInv = {};
    for (const sku of skus) {
      defaultInv[sku] = 0;
    }
    return { ...defaultInv, ...result };
  } catch (error) {
    console.log(error);
    throw Error(error);
  }
};

const checkConsignment = async (
  item: string | undefined,
  country: string | undefined
): Promise<IInventory> => {
  const { siteConfig } = getState();
  console.log('Items : ');
  console.log(item);
  const data: { basket?: string; country?: string } = {};

  const url = `${siteConfig.services.inventory}/${apiVersion}/consignment`;

  if (typeof item === 'string') {
    const converted = JSON.parse(item);
    data.basket = converted;
  } else {
    data.basket = item;
  }
  data.country = country;

  const config: AxiosRequestConfig = {
    method: 'POST',
    url: url,
    headers: {
      ...getConfigHeaders(),
      accept: 'application/jwt',
    },
    data,
  };

  try {
    const response = await axios(config);
    return (await response).data;
  } catch (error) {
    console.log(error);
    throw Error(error);
  }
};

export { checkInventory, checkConsignment };
