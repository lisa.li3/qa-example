import { WDIOIProduct } from '../types/IProduct';
import { getState } from './state';

const openBaseURL = async (): Promise<void> => {
  const { constants } = getState();
  await browser.url(constants.BASE_URL);
  return Promise.resolve();
};

const openPage = async (path: string): Promise<void> => {
  const { constants } = getState();
  await browser.url(constants.BASE_URL + path);
  // await checkMainLoaded();
};

// const checkMainLoaded = async () => {
//   let selector;
//   await browser.waitUntil(
//     async () => {
//       selector = await browser.queryAllByRole('generic');
//       return selector.length !== 0;
//     },
//     {
//       timeout: 5000,
//       timeoutMsg: `[browserNavAction][checkMainLoaded] generic queryByRole not found after 5s`,
//     }
//   );
//   return selector;
// };

const goToSearchProductPage = async (product: WDIOIProduct): Promise<void> => {
  const { constants } = getState();
  // This take an argument of a product object which is returned from a the product search API
  const productName = product.name.trim().replace(/ /g, '-').toLowerCase();
  const colour = product.colour.name.trim().replace(/ /g, '-').toLowerCase();
  console.log(productName);
  console.log(colour);
  const productId = product.id;
  const urlString = `/search/${product.searchTerm}/details/${productId}/${productName}-${colour}`;
  console.log(`${constants.BASE_URL}${urlString}`);
  await browser.url(`${constants.BASE_URL}${urlString}`);
};

const goToCategoryProductPage = async (product: WDIOIProduct): Promise<string> => {
  const { constants } = getState();
  // This take an argument of a product object which is returned from a the product search API
  const productName = product.name.trim().replace(/ /g, '-').toLowerCase();
  const colour = product.colour.name.trim().replace(/ /g, '-').toLowerCase();
  console.log(productName);
  console.log(colour);
  const productId = product.id;
  const urlString = `/${product.gender}/${product.searchTerm}/details/${productId}/${productName}-${colour}`;
  console.log(urlString);
  await browser.url(`${constants.BASE_URL}${urlString}`);
  return urlString;
};

const goBack = async (): Promise<void> => {
  // This function goes back to the previous page in the browser
  if (browser.config['framework:meta'].browserType === 'Desktop') {
    await browser.back();
  } else {
    await driver.back(); // for Android and IOS on Real Devices
  }
};

export default {
  openBaseURL,
  openPage,
  goToSearchProductPage,
  goToCategoryProductPage,
  goBack,
};
