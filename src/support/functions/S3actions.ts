import fs from 'fs';
import AWS from 'aws-sdk';

const API_VERSION = '2006-03-01';

const updateCredentials = async function (): Promise<void> {
  AWS.config.update({
    region: 'eu-west-1',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sessionToken: process.env.AWS_SESSION_TOKEN,
  });
};

const uploadToS3 = async function (
  bucketName: string,
  bucketPath: string,
  filePath: string
): Promise<AWS.S3.ManagedUpload.SendData> {
  if (!bucketName || !filePath) {
    throw new Error('bucketName and filePath are required inputs for this function');
  }

  try {
    await updateCredentials();
    const s3 = new AWS.S3({ apiVersion: API_VERSION });
    const fileStream = fs.createReadStream(filePath);

    fileStream.on('error', function (err) {
      console.log('File Error', err);
    });
    console.log('uploadToS3:: ', bucketName, bucketPath);
    const uploadParams = { Bucket: bucketName, Key: bucketPath, Body: fileStream };
    return await s3.upload(uploadParams).promise();
  } catch (error) {
    throw new Error(error);
  }
};

const downloadFromS3 = async function (bucketName: string, key: string): Promise<any> {
  try {
    await updateCredentials();

    const s3 = new AWS.S3();
    const params = {
      Bucket: bucketName,
      Key: key,
    };
    const download = await s3.getObject(params).promise();

    return download.Body;
  } catch (error) {
    console.log(`[S3Actions > downloadFromS3] ${error}`);
    return Promise.reject(error);
  }
};

const listBuckets = async function (): Promise<unknown> {
  try {
    await updateCredentials();

    const s3 = new AWS.S3({ apiVersion: API_VERSION });
    return await s3.listBuckets().promise();
  } catch (error) {
    return Promise.reject(error);
  }
};

const listBucketContents = async function (bucketName: string, prefix = ''): Promise<unknown> {
  if (!bucketName) {
    throw new Error('bucketName is a required input for this function');
  }

  try {
    await updateCredentials();

    const s3 = new AWS.S3({ apiVersion: API_VERSION });
    const bucketParams = {
      Bucket: bucketName,
      Prefix: prefix,
    };
    return s3.listObjects(bucketParams).promise();
  } catch (error) {
    throw new Error(error);
  }
};

const readingS3Bucket = async function (bucket: string, key: string): Promise<unknown> {
  // Create a S3 Object with S3 User ID and Key
  const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sessionToken: process.env.AWS_SESSION_TOKEN,
  });

  try {
    const getParams = {
      Bucket: bucket,
      Key: key,
    };
    return s3.getObject(getParams).promise();
  } catch (error) {
    throw new Error(error);
  }
};

export { listBucketContents, downloadFromS3, uploadToS3, listBuckets, readingS3Bucket };
