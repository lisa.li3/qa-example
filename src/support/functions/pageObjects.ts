//
// Imports
//
import { _Error } from '@aws-sdk/client-s3';
import debug from 'debug';
import { includes, union, isArray, startsWith } from 'lodash';

/**
 * List: first-delivery | 12th-delivery | last-delivery
 * Thing in List: first-delivery.removeProduct
 * List in List: last-delivery.first-product
 * Thing in List, in List: last-delivery.first-product.removeProduct
 */

//
// Types
//
type GetPageObject = {
  (options: { selector: string; multiple: true }): Promise<WebdriverIO.Element[]>;
  (options: { selector: string; multiple: false }): Promise<WebdriverIO.Element | string>;
  (options: { selector: string; multiple?: undefined }): Promise<
    WebdriverIO.Element | WebdriverIO.Element[] | string
  >;
};

type WrapResponse = {
  (options: {
    response: WebdriverIO.Element[] | WebdriverIO.Element;
    multiple: true;
    functionName?: string;
  }): WebdriverIO.Element[];
  (options: {
    response: WebdriverIO.Element[] | WebdriverIO.Element;
    multiple: false;
    functionName?: string;
  }): WebdriverIO.Element;
  (options: {
    response: WebdriverIO.Element | WebdriverIO.Element[] | string;
    multiple?: boolean;
  }): WebdriverIO.Element | WebdriverIO.Element[] | string;
};

type PositionInList = {
  (options: {
    modifier: string;
    selector: string;
    pageItems: {
      [key: string]: (options: {
        index: number;
        parent?: unknown;
      }) => Promise<WebdriverIO.Element[] | WebdriverIO.Element | undefined>;
    };
    parent?: () => Promise<WebdriverIO.Element[] | WebdriverIO.Element | undefined>;
  }): Promise<unknown>;
};

function isElementArray(thing): thing is Element[] {
  return isArray(thing) && (isElement(thing[0]) || thing.length === 0);
}

function isElement(thing): thing is Element {
  return (
    thing.parent !== undefined && thing.selector !== undefined && thing.elementId !== undefined
  );
}

//
// Debug
//
const namespace = 'pageObjects';
const _debug = debug(`debug:${namespace}`);
const _info = debug(`info:${namespace}`);
// const _warn = debug(`warn:${namespace}`);

//
// Constants
//
// prettier-ignore
const positionWords = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth', 'twentieth'];
// prettier-ignore
const positionShort = ['1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th', '11th', '12th', '13th', '14th', '15th', '16th', '17th', '18th', '19th', '20th'];
const listKeywords = union(positionWords, positionShort, ['last']);

//
// Helpers
//
// @ts-ignore
const wrapResponse: WrapResponse = ({ response, multiple }) => {
  // String input, string output
  if (typeof response === 'string') {
    return response;
  }

  // Multiple is undefined
  if (multiple === undefined && (isElement(response) || isElementArray(response))) {
    return response;
  }

  if (isElement(response)) {
    if (multiple) {
      return [response];
    } else {
      return response;
    }
  }

  if (isElementArray(response)) {
    if (multiple) {
      return response;
    } else {
      return response[0];
    }
  }
  throw new Error('response was not an element, an element array or a string');
};

const positionInList: PositionInList = async ({ modifier, selector, pageItems, parent }) => {
  const listSelector = `${selector}__LIST`;
  const index =
    modifier === 'last'
      ? -1
      : includes(positionWords, modifier)
      ? positionWords.indexOf(modifier)
      : positionShort.indexOf(modifier) || 0;

  _debug('positionInList: Item in LIST', {
    selector,
    listSelector,
    modifier,
    index,
    parent,
  });
  const response = await pageItems[listSelector]({ index, parent });
  if (response) {
    return response;
  } else {
    throw new Error('No element(s) returned');
  }
};

// @ts-ignore
export const getPageObject: GetPageObject = async ({ selector, multiple }) => {
  // Go get it from the page object
  _info('Started', { selector });
  const [filePath, ...functions] = selector.split('.');
  const fileClass = filePath.split('/').join('_');
  const pageObjectPath = `../../pageobjects/${filePath}.page.ts`;
  const pageObject = await import(pageObjectPath);
  const pageItems = new pageObject[fileClass]();

  _debug({
    selector,
    filePath,
    functions,
    fileClass,
    pageObjectPath,
    pageObject,
    pageItems,
  });

  let currentResponse;
  for await (const func of functions) {
    if (includes(func, '-')) {
      const [modifier, selector] = func.split('-');

      // LIST function?
      if (includes(listKeywords, modifier)) {
        currentResponse = await positionInList({
          modifier: modifier,
          selector: selector,
          parent: currentResponse,
          pageItems,
        });
      }
    } else if (startsWith(func, '$')) {
      currentResponse = await pageItems[func]({ parent: currentResponse });
    }
  }

  if (!!currentResponse) {
    _info('Finished: Advanced', { selector });
    return wrapResponse({
      response: currentResponse,
      multiple,
    });
  } else {
    _info('Finished: Normal', { selector });
    return wrapResponse({
      response: await pageItems[functions[0]],
      multiple,
      functionName: functions[0],
    });
  }
};
