import { IMenuData, Menu, MenuItemList, MenuItemText } from '../types/IMenuData';

interface footerData {
  categoriesText: string[];
  categoriesUrl: string[];
}

// Get all Text and Url for Main from Menu JSON
const extractMainJson = function (mainObj: Menu): footerData {
  const listOfElements: string[] = [];
  const listOfUrls: string[] = [];

  mainObj.forEach((mainCategories: MenuItemList) => {
    return mainCategories?.items.forEach((categories: MenuItemList) => {
      if (categories && categories.items) {
        return categories.items.forEach((list: MenuItemText) => {
          if (list && list.text && list.url) {
            listOfElements.push(list.text);
            listOfUrls.push(list.url);
          }
          if (list && list.text) {
            listOfElements.push(list.text);
          }
        });
      } else if (categories && categories.text && !categories.items && categories.url) {
        listOfElements.push(categories.text);
        listOfUrls.push(categories.url);
      }
    });
  });

  return { categoriesText: listOfElements, categoriesUrl: listOfUrls };
};

// Get all Main JSON
const getMainJson = function (data: IMenuData): Menu {
  return data.main ?? [];
};

export { getMainJson, extractMainJson };
