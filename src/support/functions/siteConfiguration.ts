import fetch from 'node-fetch';

import { ISiteConfig } from '../types/ISiteConfig';

export const getSiteConfig = async (stage: string, baseUrl: string): Promise<ISiteConfig> => {
  const siteConfigName = baseUrl.replace(/^http(s?):/i, '').replace(/[^a-zA-Z0-9]/g, '');

  let configUrl: string;

  if (baseUrl.includes('localhost')) {
    configUrl = `${baseUrl}/local-config/domain/${siteConfigName}.json`;
  } else {
    configUrl = `https://${stage}.ms-site-config.prometheus.sd.co.uk/domain/${siteConfigName}.json`;
  }

  // console.log('***** configUrl ***** ' + configUrl);

  const retrievedSiteConfig: ISiteConfig = await fetch(configUrl, { method: 'GET' })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.error(err));

  return retrievedSiteConfig;
};
