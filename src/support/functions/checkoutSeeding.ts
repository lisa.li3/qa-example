/* eslint-disable @typescript-eslint/no-explicit-any */
import moment from 'moment';
import { GenerateConfig } from '@project/site-config-transformer';

export const seedSingleQtyProduct = async (
  siteId: string,
  ticketNumber: number
): Promise<{ [serviceName: string]: any }> => {
  return await browser.seed(
    {
      product: [
        {
          quantity: 1,
          sizes: [12],
          attributes: {
            name: 'Single Line name',
            colour: {
              id: 'foobar',
              name: 'Single Line colour name',
              description: 'Single Line colour description',
            },
            pricing: [
              {
                shopperGroupId: `shoppergroup-${ticketNumber}`,
                price: 99.99,
                currency: 'GBP',
              },
            ],
          },
        },
      ],
      inventory: [
        {
          sizes: [12],
          productQuantity: 1,
          fulfilmentCentres: [`fc-${ticketNumber}-1`],
          stock: 1,
        },
      ],
    },
    siteId,
    ticketNumber
  );
};

export const seedMultiQtyProduct = async (
  siteId: string,
  ticketNumber: number,
  quantity: number
): Promise<{ [p: string]: any }> => {
  return await browser.seed(
    {
      product: [
        {
          quantity: 1,
          sizes: [12],
          attributes: {
            name: 'Single Line Multi Quantity name',
            colour: {
              id: 'foobar',
              name: 'Single Line Multi Quantity colour name',
              description: 'Single Line Multi Quantity colour description',
            },
            pricing: [
              {
                shopperGroupId: `shoppergroup-${ticketNumber}`,
                price: 99.99,
                currency: 'GBP',
              },
            ],
          },
        },
      ],
      inventory: [
        {
          sizes: [12],
          productQuantity: 1,
          fulfilmentCentres: [`fc-${ticketNumber}-1`],
          stock: quantity,
        },
      ],
    },
    siteId,
    ticketNumber
  );
};

export const seedMultiLines = async (siteId: string, ticketNumber: number): Promise<any> => {
  return await browser.seed(
    {
      product: [
        {
          quantity: 1,
          sizes: [11, 12, 13],
          attributes: {
            name: 'Multi Line name',
            colour: {
              id: 'foobar',
              name: 'Multi Line name',
              description: 'Multi Line description',
            },
            pricing: [
              {
                shopperGroupId: `shoppergroup-${ticketNumber}`,
                price: 49.99,
                currency: 'GBP',
              },
            ],
          },
        },
      ],
      inventory: [
        {
          sizes: [11, 12, 13],
          productQuantity: 1,
          fulfilmentCentres: [`fc-${ticketNumber}-1`],
          stock: 1,
        },
      ],
    },
    siteId,
    ticketNumber
  );
};

export const seedSplitBasket = async (siteId: string, ticketNumber: number): Promise<any> => {
  return await browser.seed(
    {
      product: [
        {
          quantity: 1,
          sizes: [11],
          attributes: {
            pricing: [
              {
                shopperGroupId: `shoppergroup-${ticketNumber}`,
                price: 99.99,
                currency: 'GBP',
              },
            ],
          },
        },
        {
          quantity: 1,
          sizes: [12],
          attributes: {
            pricing: [
              {
                shopperGroupId: `shoppergroup-${ticketNumber}`,
                price: 99.99,
                currency: 'GBP',
              },
            ],
          },
        },
      ],
      inventory: [
        {
          sizes: [11],
          productQuantity: 1,
          fulfilmentCentres: [`fc-${ticketNumber}-1`],
          stock: 1,
        },
        {
          sizes: [12],
          productQuantity: 1,
          fulfilmentCentres: [`fc-${ticketNumber}-2`],
          stock: 1,
        },
      ],
    },
    siteId,
    ticketNumber
  );
};

export const seedUser = async (
  siteId: string,
  seedValue: number,
  country: string
): Promise<any> => {
  let address = [
    {
      id: '2d2e91',
      firstName: 'Foo1',
      lastName: 'Bar1',
      contactNumber: {
        phone: '2342342342234',
        mobile: '123124123123',
      },
      addressLine1: 'Flat 1',
      addressLine2: 'Bar Road 1',
      city: 'Foobar',
      county: 'London',
      country: 'GBR',
      postcode: 'W11 1BA',
      isDefault: true,
      nickname: 'C Home1',
    },
  ];
  if (country === 'DEU') {
    address = [
      {
        id: '2d2e91',
        firstName: 'Foo1',
        lastName: 'Bar1',
        contactNumber: {
          phone: '2342342342234',
          mobile: '123124123123',
        },
        addressLine1: 'Flat 1',
        addressLine2: 'Bar Road 1',
        city: 'Foobar',
        county: 'London',
        country: 'DEU',
        postcode: '91232',
        isDefault: true,
        nickname: 'DE home',
      },
    ];
  }
  return await browser.seed(
    {
      user: [
        {
          attributes: {
            emailAddress: `projecttesting+${moment().unix()}@gmail.com`,
            password: 'its-a-secret',
            firstName: 'Foo',
            lastName: 'Bar',
            gender: 'male',
            dateOfBirth: '2000-01-01',
            lastVisitDate: '2020-01-01T00:00:00Z',
            addresses: address,
            tokensValidFromDate: '2020-01-01T00:00:00Z',
          },
        },
      ],
    },
    siteId,
    seedValue
  );
};

export const seedNewFulfilmentCentre = async (
  ticketNumber: number,
  currency: string,
  countryCode: string,
  locale: string
): Promise<any> => {
  const siteConfig = await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `shoppergroup-${ticketNumber}`, {
      currency: currency,
      defaultCountryCode: countryCode,
      defaultLocale: locale,
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'DEU', name: 'Germany' },
        { code: 'USA', name: 'United States' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-${ticketNumber}-1`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-${ticketNumber}-2`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-${ticketNumber}`, {
      name: 'Very fast delivery',
      message: 'Delivery in 2 days',
      description: 'Very fast delivery description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.site('site-uk', `site-${ticketNumber}`, `b2c2${ticketNumber}`, {
      shopperGroupId: `shoppergroup-${ticketNumber}`,
      fulfilmentCentres: [
        {
          fulfilmentCentreId: `fc-${ticketNumber}-1`,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-${ticketNumber}-2`,
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: `Delivery-${ticketNumber}`,
          fulfilmentCentres: [`fc-${ticketNumber}-1`, `fc-${ticketNumber}-2`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              DEU: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              USA: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
      ],
    })
  );
  await browser.commitEnvConfig(siteConfig);
};

export const seedMultipleDeliveryCountries = async (ticketNumber: number): Promise<any> => {
  const siteConfig = await browser.configureEnv(
    GenerateConfig.shopperGroup('site-uk-shoppergroup', `shoppergroup-${ticketNumber}`, {
      deliveryCountries: [
        { code: 'GBR', name: 'United Kingdom' },
        { code: 'DEU', name: 'Germany' },
        { code: 'USA', name: 'United States' },
      ],
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-${ticketNumber}-1`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.fulfilmentCentre('fc-uk-1', `fc-${ticketNumber}-2`, {
      isActive: true,
      buffer: 0,
    }),
    GenerateConfig.deliveryMethod('INTND', `Delivery-${ticketNumber}`, {
      name: 'Very fast delivery',
      message: 'Delivery in 2 days',
      description: 'Very fast delivery description',
      type: 'ToAddress',
      displayOrder: 0,
    }),
    GenerateConfig.site('site-uk', `site-${ticketNumber}`, `b2c2${ticketNumber}`, {
      shopperGroupId: `shoppergroup-${ticketNumber}`,
      fulfilmentCentres: [
        {
          fulfilmentCentreId: `fc-${ticketNumber}-1`,
          priority: 1,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
        {
          fulfilmentCentreId: `fc-${ticketNumber}-2`,
          priority: 2,
          isDistributionCentre: true,
          // @ts-ignore-next-line
          buffer: 0,
        },
      ],
      deliveryMethods: [
        {
          id: `Delivery-${ticketNumber}`,
          fulfilmentCentres: [`fc-${ticketNumber}-1`, `fc-${ticketNumber}-2`],
          extensions: {
            enabled: true,
            destinationConfig: {
              GBR: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              GER: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
              USA: {
                default: {
                  price: 2.5,
                  transitTime: 10,
                },
              },
            },
          },
        },
      ],
    })
  );
  await browser.commitEnvConfig(siteConfig);
};
