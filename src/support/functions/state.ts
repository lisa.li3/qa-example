import { IState } from '../types/IState';
import { defineCustomer } from './defineCustomer';
import { getSiteConfig } from './siteConfiguration';
import { IConstants } from '../types/IConstants';
import { ICustomerDetails } from '../types/ICustomerDetails';
import debug from 'debug';

let state: IState;
let initialState: IState;

const namespace = 'state::';
const _debug = debug(`debug:${namespace}`);

export const setState = async (constants: IConstants): Promise<IState> => {
  const { BASE_URL, STAGE } = constants;
  const siteConfig = await getSiteConfig(STAGE, BASE_URL);
  const scenario = {};
  const feature = {};
  const shortLived = {};

  const customerDetails: ICustomerDetails[] = [];
  for (let i = 0; i < 5; i++) {
    customerDetails.push(await defineCustomer(siteConfig));
  }

  state = {
    constants,
    customerDetails,
    siteConfig,
    scenario,
    feature,
    shortLived,
  };
  initialState = {
    ...state,
  };

  return state;
};

export const updateState = (key: string, value: any): void => {
  if (key === 'constants') {
    return;
  }
  state = {
    ...state,
    [key]: value,
  };
};

export const setFeatureInState = async (feature: Record<string, unknown>): Promise<IState> => {
  _debug('Adding feature to state');
  _debug(feature);
  state = {
    ...state,
    feature: {
      ...feature,
      product: [],
      inventory: [],
      user: [],
    },
  };
  return state;
};

export const setScenarioInState = async (scenario: Record<string, unknown>): Promise<IState> => {
  _debug('Adding scenario to state');
  _debug(scenario);
  state = {
    ...state,
    scenario,
  };
  return state;
};

export const addProductsToState = async ({ product, inventory }): Promise<IState> => {
  state = {
    ...state,
    feature: {
      ...state.feature,
      product: [...state.feature.product, ...product],
      inventory: [...state.feature.inventory, ...inventory],
    },
  };
  return state;
};

export const addUsersToState = async ({ user }): Promise<IState> => {
  state = {
    ...state,
    feature: {
      ...state.feature,
      user: [...state.feature.user, ...user],
    },
  };
  return state;
};

export const setShortLivedState = async (key: string, value: unknown): Promise<IState> => {
  state = {
    ...state,
    shortLived: {
      ...state.shortLived,
      [key]: value,
    },
  };
  return state;
};

export const resetShortLivedState = async (): Promise<IState> => {
  state = {
    ...state,
    shortLived: {
      ...initialState.shortLived,
    },
  };
  return state;
};

export const resetState = (): void => {
  state = {
    ...initialState,
  };
};

export const updateStateSiteConfig = async (STAGE: string, BASE_URL: string): Promise<IState> => {
  const siteConfig = await getSiteConfig(STAGE, BASE_URL);

  state = {
    ...state,
    siteConfig,
  };

  return state;
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
export const udpateStateConstants = (key: string, value: any): void => {
  if (!Object.keys(state.constants).includes(key)) return;

  state = {
    ...state,
    constants: {
      ...state.constants,
      [key]: value,
    },
  };
};

export const getState = (): IState => state;
