import { IMenuData, MenuItem, MenuItemList, MenuItemText } from '../types/IMenuData';

interface IFooterJsonElements {
  text: string;
  url: string;
}

const extractFooterMenu = function (footerJson: MenuItemList['items']): IFooterJsonElements[] {
  return footerJson.flatMap((jsonElements: MenuItemList) =>
    jsonElements.items.map((item: MenuItemText) => ({
      text: item.text.toLowerCase() || '',
      url:
        (item.url && item.url.includes('mailto')
          ? `/${item.url.toLowerCase()}`
          : item.url && item.url.toLowerCase()) || '',
    }))
  );
};

const getFooterJson = function (data: IMenuData): MenuItem[] {
  return data.footer ?? [];
};

export { extractFooterMenu, getFooterJson };
