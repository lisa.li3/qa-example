import { getState } from './state';

export const addToLocalStorage = async (key: string, value: unknown): Promise<void> => {
  const state = await getState();

  await browser.execute(
    function (key: string, value: string) {
      this.localStorage.setItem(key, value);
    },
    `${state.siteConfig.canonicalDomain.replace('www', '').replace(/\W/g, '')}_${key}`,
    JSON.stringify(value)
  );
};
