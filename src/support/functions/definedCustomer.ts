import IUser, { UserGender } from '../types/IUser';
import { getState } from './state';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

export const getAsSeedUser = (customerNumber?: number): IUser => {
  const state = getState();
  const definedCustomers = state.customerDetails;
  const index = (customerNumber || 1) - 1;

  if (index + 1 > definedCustomers.length) {
    throw new Error(
      `Trying to use defined customer at index ${index} undefined, there are only ${definedCustomers.length} defined customers`
    );
  }

  const selectedCustomer = definedCustomers[index];

  const customerId = selectedCustomer.Email.split('+')[1] || selectedCustomer.Email;
  return {
    attributes: {
      id: `AT-DefinedUser-${customerId}`,
      emailAddress: selectedCustomer.Email,
      password: selectedCustomer.Password,
      firstName: selectedCustomer.FirstName,
      lastName: selectedCustomer.LastName,
      addresses: [
        {
          id: `AT-DefinedUser-${customerId}-address`,
          firstName: selectedCustomer.FirstName,
          lastName: selectedCustomer.LastName,
          contactNumber: {
            phone: selectedCustomer.Phone,
            mobile: selectedCustomer.Mobile,
          },
          addressLine1: selectedCustomer.Address1,
          addressLine2: selectedCustomer.Address2,
          city: selectedCustomer.City,
          county: selectedCustomer.County,
          country: selectedCustomer.CountryA3,
          postcode: selectedCustomer.Postcode,
          isDefault: true,
          nickname: selectedCustomer.Address1,
        },
      ],
    },
  };
};

export const definedCustomerValue = (inputValue: string): string | false => {
  if (inputValue.indexOf('definedCustomer.') === -1) {
    return false;
  }

  const definedCustomerParts = inputValue.split('.');

  let customerIndex = 0;
  let property;
  if (definedCustomerParts.length === 3) {
    customerIndex = parseInt(definedCustomerParts[1]) - 1;
    property = definedCustomerParts[2];
  } else {
    property = definedCustomerParts[1];
  }

  const state = getState();
  const definedCustomers = state.customerDetails;

  if (customerIndex > definedCustomers.length - 1) {
    throw new Error(
      `Trying to use defined customer at index ${customerIndex} undefined, there are only ${definedCustomers.length} defined customers`
    );
  }
  cucumberJson.attach(definedCustomers[customerIndex][property], 'text/plain');
  return definedCustomers[customerIndex][property] || false;
};
