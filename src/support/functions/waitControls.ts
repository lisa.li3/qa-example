const click = async (input: WebdriverIO.Element): Promise<void> => {
  await browser.pause(250);
  await input.waitForExist({ timeout: 5000 });
  await input.waitForDisplayed({ timeout: 5000 });
  await input.scrollIntoView(false);
  await input.waitForClickable({ timeout: 5000 });
  await input.click();
  await browser.pause(1000);
};

const clickRandom = async (input: Array<WebdriverIO.Element>): Promise<void> => {
  await input[0].waitForExist({ timeout: 5000 });
  await input[0].waitForDisplayed({ timeout: 5000 });
  const numberOfInputs = input.length;
  const randomNumber = Math.floor(Math.random() * numberOfInputs - 1);
  await input[randomNumber].scrollIntoView(false);
  await input[randomNumber].waitForClickable({ timeout: 5000 });
  await input[randomNumber].click();
};

const expectElement = async (input: WebdriverIO.Element): Promise<void> => {
  await input.waitForExist({ timeout: 5000 });
  await input.waitForDisplayed({ timeout: 5000 });
  await browser.pause(250);
};

const scrollTo = async (input: WebdriverIO.Element): Promise<void> => {
  await input.waitForExist({ timeout: 5000 });
  await input.waitForDisplayed({ timeout: 5000 });
  await input.scrollIntoView(false);
  await input.isDisplayedInViewport();
};

const browserPause = async (): Promise<void> => {
  await browser.pause(3000);
};

export { click, clickRandom, expectElement, scrollTo, browserPause };
