import { addToLocalStorage } from './addToLocalStorage';
import { getProductBySKU } from './productService';

interface InventoryResponse {
  [key: string]: number;
}

export interface IBasketItem {
  id: string;
  sku: string;
  quantity: number;
  kind: 'product' | 'giftcard'; // This might change once gift cards are added
}

export const addProductsToBasket = async (inventoryResponse: InventoryResponse): Promise<void> => {
  const basketArray: IBasketItem[] = await Promise.all(
    Object.keys(inventoryResponse).map(async (sku): Promise<IBasketItem> => {
      const productModel = await getProductBySKU(sku);
      return {
        id: productModel.id,
        sku,
        quantity: inventoryResponse[sku],
        kind: 'product',
      };
    })
  );
  await addToLocalStorage('basket', basketArray);
};
