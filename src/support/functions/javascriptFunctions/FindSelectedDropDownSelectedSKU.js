//////////////////////////////////////////////////////////////////////
// * * DO NOT CONVERT THIS PAGE TO TYPESCRIPT * *
//////////////////////////////////////////////////////////////////////

class FindSelectedDropDownSelectedSKU {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  async grabSelectedProductSize() {
    return browser.execute(() => {
      const selectedElement = document.querySelector("[data-testid = 'select-input']");
      skuText = selectedElement.options[selectedElement.selectedIndex].value;
      return skuText;
    });
  }
}
export default new FindSelectedDropDownSelectedSKU();
