export const waitForElement = async function (
  text: string,
  functionType = 'queryByText',
  selOpts: unknown = undefined,
  opts: {
    timeout: number;
    interval?: number;
    message?: string;
  },
  throwErrorIfNotFound = true
): Promise<void> {
  console.log(`Waiting for element to appear: ${text}, ${functionType}`);
  let element;
  try {
    await browser.waitUntil(
      async () => {
        if ((await browser[functionType](text, selOpts)) !== null) {
          element = await browser[functionType](text, selOpts);
          return true;
        } else {
          return false;
        }
      },
      {
        timeout: opts.timeout || browser.config.waitforTimeout,
        interval: opts.interval || browser.config.waitforInterval,
        timeoutMsg: opts.message,
      }
    );

    return element;
  } catch (err) {
    if (throwErrorIfNotFound) {
      throw err;
    }
  }
};
