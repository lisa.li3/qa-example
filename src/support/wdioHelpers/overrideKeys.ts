/**
 * This command sends keys to the browser, but doesnt work on some older versions of them, so this uses performActions instead
 */
const overridekeys = async function (origKeysFunction: () => void, keys: string[]): Promise<void> {
  try {
    const keysToType =
      keys.length === 1 && keys[0].length === 1 ? keys : keys.length > 1 ? keys : keys[0].split('');

    const actions: { type: string; value: string }[] = [];
    for (const key of keysToType) {
      actions.push({ type: 'keyDown', value: key }, { type: 'keyUp', value: key });
    }

    await browser.performActions([
      {
        type: 'key',
        id: 'keyboard',
        actions: [...actions],
      },
    ]);

    return;
  } catch (err) {
    //console.log(`[functions][wdio_overrides][keys] Error: ${err}`);
    return origKeysFunction();
  }
};

export default overridekeys;
