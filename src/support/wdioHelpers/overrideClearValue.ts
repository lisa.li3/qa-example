/**
 * This command clears a value (have to use backspace)
 */
const overrideClearValue = async function (origClearValueFunction: () => void): Promise<void> {
  //console.log('[functions][wdio_overrides][clearValue] Inside Function');
  try {
    // attempt to clearValue
    const elementValue = await this.getAttribute('value');
    const elementLength = elementValue ? elementValue.length : 0;
    const backspace = '\uE003';
    const deleteKey = '\uE017';
    const backspaceString: string[] = [];
    let i = 0;
    while (i < elementLength) {
      backspaceString.push(backspace);
      backspaceString.push(deleteKey);
      i++;
    }
    return await this.addValue(backspaceString);
  } catch (err) {
    // console.warn(
    //   '[functions][wdio_overrides][clearValue] WARN: Element',
    //   this.selector,
    //   'could not be cleared, falling back to default'
    // );
    // console.log(err);
    // scroll to element and click again
    await this.click();
    return origClearValueFunction();
  }
};

export default overrideClearValue;
