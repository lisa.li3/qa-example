import { getTranslation } from './getTranslation';
import { saltedSeeder } from './saltedSeeder';
import { configureEnv, commitEnvConfig, setSiteContext } from './dynamicConfiguration';
import { waitForElement } from './waitForElement';
import { waitForPath } from './waitForPath';

const addCommands = (
  browserName: string,
  platformName: string,
  locale: string,
  stage: string,
  siteId: string,
  serviceStage: string
): void => {
  browser.addCommand('getTranslation', getTranslation(locale, serviceStage));
  browser.addCommand('seed', saltedSeeder(browserName, platformName, serviceStage, siteId, 'en')); // Will update locale at another time, en-gb doesnt work
  browser.addCommand('configureEnv', configureEnv(stage));
  browser.addCommand('commitEnvConfig', commitEnvConfig(stage));
  browser.addCommand('setSiteContext', setSiteContext(stage));
  // eslint-disable-next-line testing-library/await-async-utils
  browser.addCommand('waitForElement', waitForElement);
  browser.addCommand('waitForPath', waitForPath);
};

export default addCommands;
