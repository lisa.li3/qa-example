export const seedValueSalt = (browserName: string, platformName: string): number => {
  // Create a salt for seeding to top the multi-browser
  let salt = 0;
  switch (browserName.toLowerCase()) {
    case 'chrome':
      salt = salt + 100000;
    case 'firefox':
      salt = salt + 200000;
    case 'safari':
      salt = salt + 300000;
    default:
      salt = salt + 400000;
  }

  switch (platformName.toLowerCase()) {
    case 'osx':
      salt = salt + 10000;
    case 'windows 10':
      salt = salt + 20000;
    case 'ios':
      salt = salt + 30000;
    case 'android':
      salt = salt + 40000;
    default:
      salt = salt + 50000;
  }

  return salt;
};

export const getSalt = (): number =>
  seedValueSalt(
    browser.capabilities.browserName || 'unknown',
    browser.capabilities.platformName || 'unknown'
  );
