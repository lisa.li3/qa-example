import { getState } from '../functions/state';

export const waitForPath = async function (
  toMatch: string | RegExp,
  opts: {
    timeout?: number;
    interval?: number;
    message?: string;
  }
): Promise<void> {
  console.log(`Waiting for URL to match: ${toMatch.toString()}`);
  const baseUrl = await getState().constants.BASE_URL;
  const waitOpts = {
    ...{
      timeout: 10000,
      interval: 500,
    },
    ...opts,
  };
  try {
    await browser.waitUntil(
      async () => {
        const path = (await browser.getUrl()).replace(baseUrl, '');
        if (toMatch instanceof RegExp) {
          return path.search(toMatch) > -1;
        }
        return path === toMatch;
      },
      {
        timeout: waitOpts.timeout,
        interval: waitOpts.interval,
        timeoutMsg: waitOpts.message,
      }
    );
  } catch (err) {
    const path = (await browser.getUrl()).replace(baseUrl, '');
    console.error(`Path failed to match expectations: ${path} vs ${toMatch.toString()}`);
    throw err;
  }
};
