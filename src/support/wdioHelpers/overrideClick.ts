/**
 * This command allows Safari to Click on things that its missed
 */
const overrideClick = (browserName: string) =>
  async function (origClickFunction: () => void): Promise<void> {
    if (browserName === 'safari') {
      // clicking with js
      // console.warn(
      //   '[functions][wdio_overrides][click] WARN: Using force click for',
      //   this.selector,
      //   `as we have detected its on Safari (OS X)`
      // );
      try {
        //console.log(`[functions][wdio_overrides][click] Waiting for clickable...`);
        await this.waitForClickable({ timeout: 5000 });
        //console.log(`[functions][wdio_overrides][click] DONE! Element Clickable`);
        //console.log(`[functions][wdio_overrides][click] Executing JS....`);
        await browser.execute(function (selector) {
          function eventFire(el, etype): void {
            if (el.fireEvent) {
              el.fireEvent('on' + etype);
            } else {
              const evObj = document.createEvent('Events');
              evObj.initEvent(etype, true, false);
              el.dispatchEvent(evObj);
            }
          }
          eventFire(document.querySelector(`${selector}`), 'click');
        }, this.selector);
      } catch (err) {
        //console.log(`[functions][wdio_overrides][click] JS ERROR: ${err}`);
        //console.log(`[functions][wdio_overrides][click] Going with Different Click`);
        try {
          await browser.execute(function (selector) {
            const item = document.evaluate(`${selector}`, document.body, null, 9, null);
            if (item.singleNodeValue !== null) {
              // @ts-ignore
              return item.singleNodeValue.click();
            } else {
              return Error('XPath didnt match anything on Safari for some reason');
            }
          }, this.selector);
        } catch (error) {
          //console.log(
          //   `[functions][wdio_overrides][click] That FAILED too! \n\n ${error} \n\nDoing original click`
          // );
          return origClickFunction();
        }
      }
      //console.log(`[functions][wdio_overrides][click] DONE! Returning...`);
      return;
    }

    try {
      // attempt to click
      await this.waitForClickable({ timeout: 5000 });
      await origClickFunction();
      return;
    } catch (err) {
      try {
        //console.log(
        //   `[functions][wdio_overrides][click] Failed on ${browserName} clicking/scrolling to ${this.selector}, so going to try a JS click...`
        // );
        await browser.execute(function (selector) {
          const elm = document.querySelector(`${selector}`);
          // @ts-ignore
          elm.click();
        }, this.selector);
      } catch (error) {
        //console.log(
        //   `[functions][wdio_overrides][click] That FAILED too! \n\n ${error} \n\nDoing original click`
        // );
        return origClickFunction();
      }
    }
  };

export default overrideClick;
