import { downloadTranslations } from './downloadTranslations';
import {
  chain,
  forEach,
  head,
  isEmpty,
  get,
  set,
  findKey,
  isEqual,
  isNull,
  isString,
  tail,
  size,
  escapeRegExp,
} from 'lodash';
import { GetTranslation, GetTranslationFromPathOptions } from '../global';
import { getState } from '../functions/state';
import { promises as fsPromises } from 'fs';
import { definedCustomerValue } from '../functions/definedCustomer';
import { getBaseConstants, IS_DYNAMIC_ENV } from '../hooks';
import flatten from 'flat';
import path from 'path';
import debug from 'debug';

//
// Constants
const namespace = 'getTranslation';
const _debug = debug(`debug:${namespace}`);
const _info = debug(`info:${namespace}`);
const _log = debug(`log:${namespace}`);
const _warn = debug(`warn:${namespace}`);
const masterLocale = 'en-GB';
const cachedLocales = {};
const basePath = path.join(__dirname, `../../localTranslations`);

//
// Helpers
//

/**
 * Returns a cleaned version of a string
 * @param inputText
 */
const cleanText = (inputText: string): string => {
  // TODO: {{x}} and {{y}}
  return chain(inputText)
    .deburr()
    .toLower()
    .replace(/[^a-z0-9 ]/g, '')
    .split(' ')
    .without('', ' ')
    .join(' ')
    .trim()
    .value();
};

/**
 * Not all locales are supported this returns the best match
 * @param locale
 */
const getBestLocaleMatch = (locale: string): string => {
  switch (locale) {
    case 'de-CH':
      return 'de-DE';
    case 'fr-BE':
    case 'fr-CA':
    case 'fr-CH':
      return 'fr-FR';
    case 'nl-BE':
      return 'nl-NL';
    default:
      return locale;
  }
};

/**
 * Returns true when cache includes values from a given locale
 * @param locale
 */
const cacheIncludesLocale = (locale: string): boolean => {
  return !isEmpty(get(cachedLocales, locale, null));
};

/**
 * Download and cache translations for a given locale
 * @param locale
 * @param stage
 */
const populateCacheForLocale = async (locale: string, stage: string): Promise<void> => {
  if (!cacheIncludesLocale(locale)) {
    _debug(`Cache is empty for ${locale}`);
    try {
      await downloadTranslations(locale, stage);
      _debug('... downloaded translations');

      const localDirectoryPath = `${basePath}/${locale}`;

      const localDirectoryFiles = await fsPromises.readdir(localDirectoryPath);
      _debug(`Found ${localDirectoryFiles.length} files in ${localDirectoryPath}`);

      for (const fullFilename of localDirectoryFiles) {
        const fileName = head(fullFilename.split('.'));
        if (fileName) {
          const fileContents = JSON.parse(
            await fsPromises.readFile(`${localDirectoryPath}/${fileName}.json`, {
              encoding: 'utf-8',
            })
          );
          const flatContents = flatten(fileContents);
          _debug(
            `${locale}.${fileName} top: ${size(fileContents)}, nested: ${
              size(flatContents) - size(fileContents)
            }`
          );
          set(cachedLocales, [locale, fileName], flatContents);
        }
      }
    } catch (err) {
      _warn(`Failed to populate cache for ${locale} on ${stage}`);
    }
  } else {
    _debug(`Cache already populated for`, { locale });
  }
};

/**
 * Return translation from path
 * @param options
 */
const getTranslationFromPath = async (
  options: GetTranslationFromPathOptions
): Promise<string | false> => {
  const { siteConfig } = getState();
  const localeToUse = getBestLocaleMatch(options.locale || siteConfig.defaultLocale);
  const foundTranslation = get(cachedLocales, [localeToUse, options.page, options.item], null);
  _debug(`Translation "${foundTranslation}" found with path`, [
    localeToUse,
    options.page,
    options.item,
  ]);

  //
  // No translation found
  //
  if (isNull(foundTranslation)) {
    _warn(`No translation found for:`, [localeToUse, options.page, options.item]);
    return false;
  }

  //
  // Default return
  //
  return foundTranslation;
};

/**
 * Get translation from either a text search or a path
 * @param defaultLocale
 * @param stage
 */
export const getTranslation =
  (defaultLocale: string, stage: string): GetTranslation =>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async (options): Promise<any> => {
    _info('Started', options);
    //
    // Setup
    //
    let optionsToUse: {
      text?: string;
      page?: string;
      item?: string;
      contains?: boolean;
      startsWith?: boolean;
      endsWith?: boolean;
      locale?: string;
    };

    if (isString(options)) {
      optionsToUse = { text: options, contains: true };
    } else {
      optionsToUse = options;
    }

    const { siteConfig, constants } = getState();
    const { SERVICE_STAGE } = getBaseConstants(constants.BASE_URL, IS_DYNAMIC_ENV);
    const localeToUse = getBestLocaleMatch(
      optionsToUse.locale || siteConfig.defaultLocale || defaultLocale
    );
    const stageToUse = SERVICE_STAGE || stage;
    const inputText = optionsToUse.text;
    const missingReturnsOriginal = !!inputText;
    const fallbackResponse = missingReturnsOriginal && inputText ? inputText : false;

    let searchText;
    let pageToUse = optionsToUse.page;
    let itemToUse = optionsToUse.item;

    //
    // Load cache for master locale
    //
    if (!cacheIncludesLocale(masterLocale)) {
      await populateCacheForLocale(masterLocale, stageToUse);
    }

    //
    // Load cache for requested locale
    //
    if (localeToUse !== masterLocale && !cacheIncludesLocale(localeToUse)) {
      await populateCacheForLocale(localeToUse, stageToUse);
    }

    if (inputText) {
      //
      // Translate based on text
      //

      // Defined Customer Value?
      const customerValue = await definedCustomerValue(inputText);
      if (customerValue) {
        _debug('Search text is a Defined Customer Value', inputText);
        return customerValue;
      }

      // Basic text lookup
      searchText = cleanText(inputText);

      let outputPath: string[] = [];
      forEach(get(cachedLocales, [localeToUse]), (translations, fileName) => {
        if (isEmpty(outputPath)) {
          const foundWithKey = findKey(translations, (value) => {
            return isEqual(cleanText(value), searchText);
          });
          if (foundWithKey) {
            outputPath = [fileName, ...foundWithKey.split('.')];
          }
        }
      });

      if (size(outputPath) > 1) {
        _debug(`Path found for input "${inputText}"`, outputPath);
        pageToUse = head(outputPath);
        itemToUse = tail(outputPath).join('.');
      } else {
        _log(`No path found for "${inputText}" using search text "${searchText}"`);
      }
    }

    //
    // Create response
    //
    let response;
    if (pageToUse && itemToUse) {
      // Get translation from path
      response = await getTranslationFromPath({
        ...optionsToUse,
        page: pageToUse,
        item: itemToUse,
        locale: localeToUse,
      });
      response = response || fallbackResponse;
    } else {
      // Handle failures
      _log(
        `A path could not be generated for input`,
        { optionsToUse },
        { pageToUse },
        { itemToUse },
        { fallbackResponse }
      );
      response = fallbackResponse;
    }

    //
    // Return Response
    //
    if (response !== false && optionsToUse.contains) {
      _debug('... returning contains regex');
      response = new RegExp(escapeRegExp(response), 'i');
    } else if (response !== false && optionsToUse.startsWith) {
      _debug('... returning startsWith regex');
      response = new RegExp(`^${escapeRegExp(response)}`, 'i');
    } else if (response !== false && optionsToUse.endsWith) {
      _debug('... returning endsWith regex');
      response = new RegExp(`${escapeRegExp(response)}$`, 'i');
    }
    _info('Finished', options, response);
    return response;
  };
