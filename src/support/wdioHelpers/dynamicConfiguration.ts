import { InputConfigFile, UnwrittenFile } from '@project/site-config-transformer/dist';
import { mergeToAws, GenerateConfig } from '@project/site-config-transformer';

import { getState, udpateStateConstants, updateStateSiteConfig } from '../functions/state';

interface ConfigOp {
  // eslint-disable-next-line @typescript-eslint/ban-types
  op: Function;
}

function getDynamicStage(stage: string): string {
  // assume stage is in the form `{something}.dynamic`
  const stageElements = stage.split('.');
  if (stageElements.length === 2) {
    return stageElements[0];
  }
  return stage;
}

export const configureEnv =
  (stage: string) =>
  async (
    ...params: (ConfigOp | ((file: InputConfigFile) => ConfigOp))[]
  ): Promise<UnwrittenFile[]> => {
    const dynamicStage = getDynamicStage(stage.toLowerCase());
    return GenerateConfig.defineConfig(dynamicStage, ...params);
  };

export const commitEnvConfig =
  (stage: string) =>
  async (configFiles: UnwrittenFile[]): Promise<void> => {
    const dynamicStage = getDynamicStage(stage.toLowerCase());
    return mergeToAws.default(dynamicStage, configFiles);
  };

export const setSiteContext =
  (stage: string) =>
  async (urlPrefix: string): Promise<void> => {
    const { constants } = getState();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const [schema, hostname] = constants.BASE_URL!.split('://');
    const domainElements = hostname.split('.');
    domainElements.shift();

    const prefixedUrl = `${schema}://${urlPrefix.toLowerCase()}.${domainElements.join('.')}`;

    udpateStateConstants('BASE_URL', prefixedUrl);
    await updateStateSiteConfig(stage.toLowerCase(), prefixedUrl.toLowerCase());
  };
