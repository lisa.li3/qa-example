import cucumberJson from 'wdio-cucumberjs-json-reporter';

/**
 * This command moves to an element, allowing changes in different browsers
 */
const overrideMoveTo = async function (origMoveToFunction: () => void): Promise<void> {
  try {
    await browser.execute(function () {
      // eslint-disable-next-line no-var
      const div = document.createElement('div');
      document.body.appendChild(div);
      div.id = 'wdioMouseReset';
      div.style.position = 'absolute';
      div.style.top = '0';
      div.style.left = '0';
      div.style.width = '1px';
      div.style.height = '1px';
      div.style.zIndex = '9999999';
      // eslint-disable-next-line func-names, prefer-arrow-callback
      div.addEventListener('click', function () {
        document.body.removeChild(div);
      });
    });
    await (await $('#wdioMouseReset')).click();

    await this.scrollIntoView({ block: 'center' });
    const { x, y } = await (await $(this.selector)).getLocation();
    await browser.performActions([
      {
        type: 'pointer',
        id: 'finger1',
        parameters: { pointerType: 'mouse' },
        // @ts-ignore
        actions: [{ type: 'pointerMove', duration: 0, x: parseInt(x) + 2, y: parseInt(y) + 2 }],
      },
    ]);
    await browser.releaseActions();
    cucumberJson.attach(
      `overrideMoveTo::MoveTo: Moved to element ${this.selector}, located at ${x}, ${y}`,
      'text/plain'
    );
  } catch (err) {
    cucumberJson.attach(
      `overrideMoveTo::Error: Error when doing moveTo: ${JSON.stringify(err)}`,
      'text/plain'
    );
    // console.warn(
    //   '[functions][wdio_overrides][moveTo] WARN: Element',
    //   this.selector,
    //   `could not be moved to, falling back to default \n \n ${err}`
    // );

    // scroll to element and click again
    return origMoveToFunction();
  }
};

export default overrideMoveTo;
