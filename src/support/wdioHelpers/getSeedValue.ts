import { getState } from '../functions/state';

export default async (): Promise<number> => {
  const state = await getState();

  const scenarioName = state.scenario.name;
  const featureName = state.scenario.uri.substring(
    state.scenario.uri.lastIndexOf('/') + 1,
    state.scenario.uri.length - 9
  );

  let scenarioNumber = 0;
  for (const char of scenarioName.split('')) {
    scenarioNumber = scenarioNumber + char.charCodeAt(0);
  }
  let featureNumber = 0;
  for (const char of featureName.split('')) {
    featureNumber = featureNumber + char.charCodeAt(0);
  }

  return scenarioNumber + featureNumber;
};
