import seeder from '@project/seeder-orchestrator';

import { seedValueSalt } from './seedValueSalt';

interface SeedEventMap {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: { [key: string]: any }[];
}

type SaltedSeeder = (
  seedEventMap: SeedEventMap,
  siteId: string,
  seedValue: number,
  locale?: string | undefined
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) => Promise<any>;

export const saltedSeeder = (
  browserName: string,
  platformName: string,
  serviceStage: string,
  siteId: string,
  defaultLocale: string
): SaltedSeeder => {
  const salt = seedValueSalt(browserName, platformName);
  const seed = seeder(serviceStage, siteId, defaultLocale);
  return async (
    seedEventMap: SeedEventMap,
    siteId: string,
    seedValue: number,
    locale?: string | undefined
  ): Promise<unknown> => seed(seedEventMap, siteId, seedValue + salt, locale);
};
