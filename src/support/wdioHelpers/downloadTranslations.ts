import { existsSync, readFileSync, writeFileSync, mkdirSync } from 'fs';
import path from 'path';

import { downloadFromS3, listBucketContents } from '../functions/S3actions';
import ITranslations from '../types/ITranslations';

const extractTranslationFile = async (bucket: string, key: string): Promise<[string, string]> => {
  const translationFile = <string>await downloadFromS3(bucket, key) || '';
  return [key, translationFile];
};

export const downloadTranslations = async (
  locale: string,
  stage: string
): Promise<Array<[string, ITranslations]>> => {
  // Get Page Content Translations
  // The following function will read in the translations json file for the specified page and return the translation for the item specified
  // Examples of pages that can be read in are CategoryPage, HelpCentre, WishListPage, Stores etc
  // When the site loads, inspect the network tab and you will find the various translation files that get loaded in
  try {
    // Hard-coded to eu-west-1 ( we might need to change this in the future )
    const bucket = `project-react-static-${stage}-eu-west-1`;
    const prefix = `translations/${locale}/`;

    //console.log(`[wdio.conf][getTranslation][downloadTranslation] Looking in ${bucket}/${prefix}`);
    const pagesWithTranslations = <{ Contents: { Key: string }[] }>(
      await listBucketContents(bucket, prefix)
    );

    const downloadedPageTranslations = await Promise.all(
      pagesWithTranslations.Contents.map((page) => extractTranslationFile(bucket, page.Key))
    );

    const baseLocalTranslationsRelativePath = `../../localTranslations/${locale}`;
    const baseLocalTranslationsPath = path.join(__dirname, baseLocalTranslationsRelativePath);

    return downloadedPageTranslations.map(([pageKey, translationFile]) => {
      const pageName = pageKey.substring(pageKey.lastIndexOf('/') + 1, pageKey.length - 5);
      // Check if the local translations locale folder exists and create if not
      mkdirSync(baseLocalTranslationsPath, { recursive: true });

      const baseLocalTranslationsPageRelativePath = `${baseLocalTranslationsRelativePath}/${pageName}.json`;
      const baseLocalTranslationsPagePath = path.join(
        __dirname,
        baseLocalTranslationsPageRelativePath
      );

      // Check if local page translation file already exists and create and return it if not
      if (!existsSync(baseLocalTranslationsPagePath)) {
        writeFileSync(baseLocalTranslationsPagePath, translationFile);

        return [pageName, JSON.parse(translationFile)];
      }

      const localTranslationFile = readFileSync(baseLocalTranslationsPagePath);

      if (localTranslationFile.length !== translationFile.length) {
        //console.log(
        //   `[wdio.conf][getTranslation]  /${locale}/${pageName}.json Has been updated, so will change the local copy\n Old Length: ${localTranslationFile.length}\n New Length: ${translationFile.length}`
        // );
        writeFileSync(baseLocalTranslationsPagePath, translationFile);
      }
      return [pageName, JSON.parse(translationFile)];
    });
  } catch (err) {
    console.error('[wdio.conf][retrieveTranslations]', err);
    throw Error(
      `[wdio.conf][retrieveTranslations] Error when getting Translations from S3:\n ${err}`
    );
  }
};
