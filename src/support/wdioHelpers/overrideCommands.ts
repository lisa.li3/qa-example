import overrideClearValue from './overrideClearValue';
import overrideMoveTo from './overrideMoveTo';
import overrideKeys from './overrideKeys';

const overrideCommands = (): void => {
  //browser.overwriteCommand('click', overrideClick(browserName.toLowerCase()), true);
  browser.overwriteCommand('clearValue', overrideClearValue, true);
  browser.overwriteCommand('moveTo', overrideMoveTo, true);
  browser.overwriteCommand('keys', overrideKeys, false);
};

export default overrideCommands;
