import { getState } from '../functions/state';
import Path from 'path';
import { uploadToS3 } from '../functions/S3actions';

/**
 * Add Items into S3, using the DYNAMIC_STAGE name
 * @param  {string}   fileName  File name under /data/
 * @param  {string}   bucketFriendlyName this is the s3 bucket name without the dynamic stage on it
 * @param  {string}   key  S3 file key/path to put the file

 */

export default async (fileName: string, bucketFriendlyName: string, key: string): Promise<void> => {
  const { constants } = await getState();
  const bucket = `${bucketFriendlyName}-${constants.SERVICE_STAGE}`;
  const filePath = Path.join(__dirname, `../../data/${fileName}`);

  await uploadToS3(bucket, key, filePath);
};
