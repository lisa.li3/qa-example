import browserNavAction from '../functions/browserNavAction';
import { isEqual, toLower } from 'lodash';
import openWebsite from '../action/openWebsite';
import { getState, updateStateSiteConfig } from '../functions/state';

//
// Function
//
export const spawnSite = async ({
  newSite,
  baseSite,
}: {
  newSite: string;
  baseSite: string;
}): Promise<void> => {
  const newName = toLower(newSite);
  const baseName = toLower(baseSite);

  if (isEqual(newName, baseName)) {
    throw new Error(
      `newSite name "${newName}" must be different from baseConfig name "${baseName}"`
    );
  }

  if (newName.length > 20) {
    throw new Error(`newSite name "${newName}" must be 20 characters or less.`);
  }

  const { getSiteConfig } = await import(`../../data/baseConfigs/${baseName}.config.seed.ts`);
  const newConfig = await getSiteConfig({
    newSite: toLower(newName),
  });
  await browser.commitEnvConfig(newConfig);
  await browser.setSiteContext(newName);
  await openWebsite('open', 'siteId', newName);
  await openWebsite('open', 'page', '/');
};

//
// Step Invoker
//
export default async (newSite: string, baseSite: string): Promise<void> => {
  return await spawnSite({
    newSite,
    baseSite,
  });
};
