/**
 * Perform data seeding, based on a specified file
 * @param  {string}   file  File path to seeding partner
 * @param  {string}   fromDifferentSiteConfig  If you dont want to create config based from site-uk
 * @param  {boolean}   switchToNewSite  If you want to open the new site straight away
 */
import browserNavAction from '../functions/browserNavAction';

export default async (
  file: string,
  fromDifferentSiteConfig?: string,
  switchToNewSite?: boolean
): Promise<boolean> => {
  console.log(`Site Config Seeding: Started`);

  const { getSiteConfig } = await import(`../../data/seedingPayloads/${file}.config.seed.ts`);

  if (fromDifferentSiteConfig !== null) {
    await browser.commitEnvConfig(await getSiteConfig(file.toLowerCase(), fromDifferentSiteConfig));
  } else {
    await browser.commitEnvConfig(await getSiteConfig(file.toLowerCase()));
  }

  if (switchToNewSite !== null) {
    await browser.setSiteContext(file.toLowerCase());
    await browserNavAction.openBaseURL();
  }

  console.log(`Site Config Seeding: COMPLETE`);
  return Promise.resolve(true);
};
