import { addToLocalStorage } from '../functions/addToLocalStorage';
import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { getState } from '../functions/state';
import seedValue from '../wdioHelpers/getSeedValue';
import { addProductsToBasket } from '../functions/addProductsToBasket';

/**
 * Perform data seeding, based on a specified file
 * @param  {string}   file  File path to seeding partner
 * @param  {string}   items Either " the items" or " one of each item" or " custom quantity"
 * @param  {string}   localStorageKey  Key to add to local storage
 * @param  {string}   locale  locale to pass, defaults to en
 * @param  {string}   quantities  quantity to pass for " custom quantity"
 *

 */

export default async (
  file: string,
  items?: string,
  localStorageKey?: string,
  locale?: string,
  quantities?: string
): Promise<void> => {
  const onlyOneItem = items === ' one of each item';
  const payload = await import(
    `../../data/seedingPayloads/${file.split('.').join('/')}.data.seed.ts`
  );
  const customQuantity = items === ' custom quantity';
  const state = await getState();
  //console.debug('Sending Seed Payload', JSON.stringify(await payload.default()));
  const seedResponse = await browser.seed(
    await payload.default(),
    state.siteConfig.siteId,
    await seedValue(),
    locale ? locale : 'en'
  );
  cucumberJson.attach(JSON.stringify(seedResponse), 'application/json');
  if (localStorageKey) {
    const inventory = seedResponse.inventory;
    if (onlyOneItem) {
      for (const key of Object.keys(inventory)) {
        inventory[key] = 1;
      }
    }
    if (customQuantity) {
      const inventoryArray = quantities ? quantities.split(',').map(Number) : [];
      for (const [index1, quantity] of inventoryArray.entries()) {
        let keyIndex = 0;
        for (const [key] of Object.entries(inventory)) {
          if (index1 === keyIndex) {
            inventory[key] = quantity;
          } else {
            if (keyIndex >= inventoryArray.length) {
              delete inventory[key];
            }
          }
          keyIndex++;
        }
      }
    }
    console.log(inventory);
    if (localStorageKey === 'basket') {
      await addProductsToBasket(inventory);
    } else {
      await addToLocalStorage(localStorageKey, inventory);
    }
  }
};
