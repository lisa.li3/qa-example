import cucumberJson from 'wdio-cucumberjs-json-reporter';
import { getState, addProductsToState, addUsersToState } from '../functions/state';
import seedValue from '../wdioHelpers/getSeedValue';

/**
 * Perform data seeding, based on a specified file
 * @param  {string}   file  File path to seeding partner
 */

export default async (type: string, file: string): Promise<void> => {
  const seedingData = await import(
    `../../data/seedingPayloads/seedingData/${file.split('.').join('/')}.data.seed.ts`
  );
  const payload = await seedingData.default();

  const state = await getState();
  //console.debug('Sending Seed Payload', JSON.stringify(await payload.default()));

  if (type === 'seed') {
    const seedResponse = await browser.seed(
      payload,
      state.siteConfig.siteId,
      await seedValue(),
      'en'
    );
    cucumberJson.attach(JSON.stringify(seedResponse), 'application/json');
  }

  console.log('payload', payload);
  switch (file.split('.')[0]) {
    case 'products':
      addProductsToState(payload);
      break;
    case 'users':
      addUsersToState(payload);
      break;
    default:
  }
};
