import getElement from '../check/getElement';
import { isArray } from 'lodash';

/**
 * Check if the given element exists in the DOM one or more times
 * @param  {String}  selector  Element selector
 * @param  {Boolean} falseCase Check if the element (does not) exists
 * @param  {Number}  exactly   Check if the element exists exactly this number
 *                             of times
 */
export default async (
  selector: string,
  falseCase?: boolean,
  exactly?: string | number
): Promise<boolean> => {
  let elements: WebdriverIO.Element[] = [];
  let element: WebdriverIO.Element;
  if (typeof exactly === 'string') {
    exactly = parseInt(exactly);
  }
  const timeout = falseCase ? 2000 : undefined;

  try {
    console.log(`checkElementExists::Try getElement`);
    element = await getElement({
      selector,
      timeout,
      multipleSelectors: true,
    });
    elements = isArray(element) ? element : [element];
  } catch (err) {
    console.log(`checkElementExists:: catch Error: ${err}`);
    console.log(`falseCase: ${falseCase}`);
    if (falseCase) {
      return true;
    }
    elements = await $$(selector);
  }
  /**
   * The number of elements found in the DOM
   * @type {Int}
   */
  const nrOfElements = elements;

  if (falseCase === true) {
    expect(nrOfElements).toHaveLength(
      0,
      // @ts-expect-error
      `Element with selector "${selector}" should not exist on the page`
    );
  } else if (exactly) {
    expect(nrOfElements).toHaveLength(
      exactly,
      // @ts-expect-error
      `Element with selector "${selector}" should exist exactly ${exactly} time(s)`
    );
  } else {
    expect(nrOfElements.length).toBeGreaterThanOrEqual(
      1,
      // @ts-expect-error
      `Element with selector "${selector}" should exist on the page`
    );
  }
};
