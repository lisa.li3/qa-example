export type IToken = {
  siteId: string;
  isStaff: boolean;
  userId: string;
  emailAddress: string;
  iat: number;
  exp: number;
};

export type ITokenResponse = {
  userToken: string;
  refreshToken: string;
};
