export interface IInventory {
  [productSku: string]: number;
}

export interface IInventorySeeder {
  sizes?: number[] | string[];
  productQuantity?: number;
  fulfilmentCentres?: string[];
  stock?: number;
  skus?: string[];
  attributes?: {
    location: Record<string, unknown>;
  };
}
