export default interface IOrderSeeder {
  products?: Array<unknown>;
  attributes: {
    id: string;
    createdAt: string;
    status: string;
    pricing?: { totals: { shipping: number; total: number } };
    userId?: string;
  };
}
