export default interface ITranslations {
  [key: string]: string;
}
