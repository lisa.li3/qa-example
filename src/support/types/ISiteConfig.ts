import { IServiceName } from './IServiceName';

export interface ISiteConfig {
  defaultLocale: string;
  defaultCountryCode: string;
  canonicalDomain: string;
  deliveryCountries: Record<string, never>[];
  pimLocale: string;
  siteId: string;
  shortName?: string;
  domain?: string;
  services: {
    [key in IServiceName]: string;
  };
  shopperGroupId: string;
}
