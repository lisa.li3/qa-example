export interface ICustomerDetails {
  Email: string;
  Password: string;
  Gender: string;
  FirstName: string;
  LastName: string;
  Phone: string;
  Mobile: string;
  NickName: string;
  Address1: string;
  Address2: string;
  City: string;
  County: string;
  Postcode: string;
  CountryA3: string;
  Country: string;
  Marketing: string;
}
