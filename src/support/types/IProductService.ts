import IProduct from './IProduct';
import IFacets from './IFacets';
import IInventory from './IInventory';
import IProductRatings from './IProductRatings';

export enum ProductsResponseType {
  RESULTS = 'results',
  REDIRECT = 'redirect',
}

export interface IProductsRedirect {
  responseType: ProductsResponseType.REDIRECT;
  url: string;
}

export interface IProductsResults<T extends IProduct = IProduct> {
  responseType: ProductsResponseType.RESULTS;
  products: T[];
  facets: IFacets;
  s3Key?: string;
  inventory?: IInventory;
  ratings?: IProductRatings;
}

export type IProductsResponse<T extends IProduct = IProduct> =
  | IProductsRedirect
  | IProductsResults<T>;
