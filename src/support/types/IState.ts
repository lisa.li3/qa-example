/* eslint-disable @typescript-eslint/no-explicit-any */
import { IConstants } from './IConstants';
import { ICustomerDetails } from './ICustomerDetails';
import { ISiteConfig } from './ISiteConfig';

export interface IState {
  constants: IConstants;
  customerDetails: ICustomerDetails[];
  siteConfig: ISiteConfig;
  scenario: any;
  feature: any;
  shortLived: {
    [key: string]: any;
  };
}
