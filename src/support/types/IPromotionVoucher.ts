export default interface IPromotion {
  type: 'PercentOffSpendAmountPromotion' | 'AmountOffPromotion';
  status: 'active' | 'expired' | 'singleUse' | 'singleUseUsed';
  code: string;
  parameters: {
    valueX?: number;
    valueY?: number;
  };
}
