import { IIcon } from './IIcon';

/**
 * A menu item that is just a plain text link
 */
export type MenuItemText = {
  id?: string;

  /**
   * Optional css class to help apply styling
   */
  className?: string;

  /**
   * Custom text style overrides.
   * Generally we should opt for creating a class and applying via className.
   * https://reactjs.org/docs/dom-elements.html#style
   */
  styles?: {
    [key: string]: string;
  };

  /**
   * The text to render
   */
  text: string;

  /**
   * The url to link to
   */
  url?: string;

  /**
   * An icon to show
   */
  icon?: IIcon.IProps['name'];
};

/**
 * A menu item that has an image
 */
export type MenuItemImage = MenuItemText & {
  /**
   * Mobile (default) image
   */
  srcMobile: string;

  /**
   * Specific alternative image for desktop, if this is specified then `srcMobile` image will only show on mobile.
   */
  srcDesktop?: string;

  /**
   * Alt text for images
   */
  alt: string;

  /**
   * Text is optional on images
   */
  text?: string;
};

/**
 * A menu item that has nested child items, but also shows a link/image
 */
export type MenuItemList = (MenuItemText | MenuItemImage) & {
  /**
   * The nested child items of this list
   */
  items: MenuItem[];
};

/**
 * Group (never any text or link) is only used to group lists but doesn't constitute a menu tier on mobile
 */
export type MenuItemGroup = {
  /**
   * Optional css class to help apply styling
   */
  className?: string;

  /**
   * The nested child items of this group
   */
  items: MenuItem[];
};

/**
 * A union of all menu item types
 */
export type MenuItem = MenuItemList | MenuItemGroup | MenuItemText | MenuItemImage;

/**
 * The outer menu item array
 */
export type Menu = MenuItem[];

export interface IMenuData {
  main: Menu;
  footer: MenuItemList[];
  banner: MenuItemText[];
}
