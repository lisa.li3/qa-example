import IReview from './IReview';

export interface IReviewsResponse {
  productReviews: Array<IReview>;
  links: Array<{
    href: string;
    method: string;
    rel: string;
  }>;
}
