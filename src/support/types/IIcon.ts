export type Size = 'XSmall' | 'Small' | 'Medium' | 'Large' | 'XLarge' | 'XXLarge';
export type UIState = 'Success' | 'Warning' | 'Error' | 'Info';

/* eslint-disable @typescript-eslint/no-namespace */
export namespace IIcon {
  export interface IProps {
    /**
     * An optional extra CSS class string
     */
    className?: string;

    /**
     * The icon name
     */
    name:
      | 'Arrow-left'
      | 'Arrow-right'
      | 'Bag'
      | 'Caution'
      | 'Chevron-down'
      | 'Chevron-left'
      | 'Chevron-right'
      | 'Chevron-up'
      | 'Circle'
      | 'Clock'
      | 'Cogs'
      | 'Cross'
      | 'Crosshair'
      | 'Delivery'
      | 'Delivery-alert'
      | 'Facebook'
      | 'Hamburger'
      | 'Heart'
      | 'Instagram'
      | 'Loader'
      | 'Marker-collectplus'
      | 'Marker-default'
      | 'Marker-project'
      | 'Minus'
      | 'Pencil'
      | 'Pinterest'
      | 'Phone'
      | 'Plus'
      | 'Return'
      | 'Search'
      | 'Star'
      | 'Tick'
      | 'Twitter'
      | 'User'
      | 'Youtube';

    /**
     * The icon size
     */
    size?: Size | 'XXXLarge';

    /**
     * Some icons have an "active" state, e.g a filled heart for wishlist. Set this on/off
     */
    active?: boolean;

    /**
     * The icon colour
     */
    colour?: 'Inherit' | 'Dark' | 'Inverse' | UIState;

    /**
     * Any inner HTML that needs to be defined
     */
    // children?: React.ReactNode;

    /**
     * Percentage to clip "Star" icons (svgs) by
     */
    clipPercentage?: number;
  }

  export type IIconTypes = { [key in UIState]: IIcon.IProps['name'] };
}
