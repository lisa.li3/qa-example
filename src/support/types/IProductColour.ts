export interface IProductColour {
  description: string;
  name: string;
  image?: string;
  id: string;
  skus: string[];
}
