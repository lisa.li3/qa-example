export default interface IReview {
  id: string;
  createdAt: string;
  stars: number;
  content: string;
  consumer: {
    id: string;
    displayName?: string;
    name?: string;
  };
  language: string;
  attributeRatings: Array<{
    attributeId: string;
    attributeName: string;
    rating: number;
  }>;
  attachments: Array<unknown>;
  firstCompanyComment: null;
}

export interface IReviewSeeder {
  productQuantity?: number;
  reviewQuantity?: number;
  attributes?: {
    siteId?: string;
    productId?: string;
    averageRating?: {
      averageRating?: number;
      totalRatings?: number;
    };
    reviews?: Array<{
      name?: string;
      content?: string;
    }>;
  };
}
