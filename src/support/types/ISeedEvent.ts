import { IProductSeeder } from './IProduct';
import { IContentSeeder } from './IContent';
import { IInventorySeeder } from './IInventory';
import { IReviewSeeder } from './IReview';
import IOrderSeeder from '../../support/types/IOrder';
import IUser from './IUser';
import IPaymentSeeder from './IPayment';

export interface ISeedEvent {
  content?: IContentSeeder[];
  product?: IProductSeeder[];
  inventory?: IInventorySeeder[];
  user?: IUser[];
  review?: IReviewSeeder[];
  order?: IOrderSeeder[];
  payment?: IPaymentSeeder[];
}
