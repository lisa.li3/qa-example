export interface IServiceGroup {
  ServiceGroup: {
    promises: Array<{
      day: string;
      cutoff: string;
      transit_time: string;
      specific_days: {
        day: string[];
      };
    }>;
  };
}
