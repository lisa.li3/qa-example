export interface IContentSeeder {
  type: string;
  attributes: {
    path: string;
    categoryId?: string;
    sanaId?: string;
    head?: {
      title?: string;
      meta?: {
        content?: string;
        name?: string;
        property?: string;
      }[];
    };
    body?: string;
  };
}
