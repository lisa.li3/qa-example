import { IProductColour } from './IProductColour';
import IPromotion from './IPromotion';
import IPromotionVoucher from './IPromotionVoucher';

// sorry about the name, we have ISize & IProductSize
export interface ISizeOnProduct {
  id: string;
  name: string;
  sku: string;
  ean: string;
}

export interface IModelData {
  height?: string;
  chest?: string;
  wears?: string;
}

export interface IProductToBasketAndNotify {
  id: string;
  availableSizes: ISizeOnProduct[];
  mainImage: string;
  name: string;
  price: number;
}

/**
 * Product entity as returned from API
 */
export interface IProductBase {
  id: string;
  defaultCategoryId: string;
  gender?: 'mens' | 'womens';
  name: string;
  colour: Omit<IProductColour, 'image' | 'skus'>;
  image: string;
  coloursAvailable: number;
  // rating: number; //  removed
  sizeName?: string;
  sizes: ISizeOnProduct[];
  promotion?: IPromotion;

  // new fields
  siteId?: string;
  sku?: string;
  colours?: string[];
  family: string[];
  type: string;
  upcCode?: string;
  style?: string;
  rating?: number;

  /**
   * Current price. Can be lower than listPrice if product is discounted.
   */
  price: number;

  /**
   * Price without discounts.
   */
  listPrice: number;
}

/**
 * Product entity decorated with additional properties
 */
export interface IEnhancedProduct extends IProductBase {
  /**
   * The path corresponding to the defaultCategoryId
   */
  categoryPath?: string;
}

type IProduct = IProductBase | IEnhancedProduct;

export default IProduct;

// DETAIL - todo separate into another file

export interface IProductDetail {
  id: string;
  sku?: string;
  defaultCategoryId: string;
  name: string;
  promotion?: IPromotion;
  colour: IProductColour;
  mainImage: string;
  media: string[];
  rating: number;
  productLineId: string;
  availableSizes: ISizeOnProduct[];
  description: string;
  specification: {
    sizing: {
      name: string;
      measurements: {
        location: string;
        imperial: number;
        metric: number;
      }[];
    }[];
    materials: {
      part: string;
      fabrics: {
        percentage: number;
        fabric: string;
      }[];
    }[];
    cleaning: {
      wash: string;
      bleach: string;
      dry: string;
      iron: string;
      dryClean: string;
    };
  };
  family: IProductColour[];
  style: string;
  sizeGuide?: {
    /**
     * If there is only one size guide image, I believe b/e stick it in the `desktop` prop.
     */
    desktop: string;

    /**
     * If there is only one size guide image, it will be in `desktop`, and this property will be `undefined`.
     */
    mobile?: string;
  };

  /**
   * Current price. Can be lower than listPrice if product is discounted.
   */
  price: number;

  plu: string;

  /**
   * Price without discounts.
   */
  listPrice: number;

  model?: IModelData;
}

export interface WDIOIProduct extends IProductBase {
  searchTerm?: string;
}

export interface IProductSeeder {
  quantity?: number;
  sizes?: number[] | string[];
  id?: string;
  attributes?: {
    id?: string;
    sku?: string;
    ean?: string;
    plu?: string;
    name?: string;
    style?: string;
    defaultCategoryId?: string;
    type?: string;
    staffDiscountAvailable?: boolean;
    description?: string;
    model?: {
      wears?: string;
      height?: string;
    };
    colour?: {
      id?: string;
      name?: string;
      description?: string;
    };
    family?: {
      name?: string;
      image?: string;
      description?: string;
      id?: string;
    }[];
    gender?: string;
    media?: string[];
    mainImage?: string;
    availableSizes?: {
      name?: string;
      id?: string;
      ean?: string;
      sku?: string;
    }[];
    pricing?: {
      shopperGroupId: string;
      price?: number;
      currency?: string;
    }[];
  };
  promotions?: { product?: IPromotion; markdown?: IPromotion; vouchers?: IPromotionVoucher[] };
}
