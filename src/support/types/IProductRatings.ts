export default interface IProductRatings {
  [productId: string]: { averageRating: number; totalRatings: number };
}
