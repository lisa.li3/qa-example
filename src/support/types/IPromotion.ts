export default interface IPromotion {
  id: string;
  type:
    | 'XPercentOffProductPromotion'
    | 'XAmountOffProductPromotion'
    | 'PercentOffSpendAmountPromotion'
    | 'AmountOffPromotion'
    | 'XForYAmountPromotion'
    | 'XForYPromotion';
  status: string;
  slug: string;
  parameters: {
    valueX?: number;
    valueY?: number;
  };
}
