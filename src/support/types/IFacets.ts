export type FacetName = 'sizes' | 'colours' | 'types' | 'genders' | 'categories';

export interface IFacetOption {
  name: string;
  products: string[];
}

type IFacets = Partial<Record<FacetName, IFacetOption[]>>;
export default IFacets;
