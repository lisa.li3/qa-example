export interface IConstants {
  BASE_URL: string;
  BROWSER_NAME: string;
  COUNTRY_SITE: string;
  EMAIL_ADDRESS: string;
  SEARCH_TERMS: string[];
  STAGE: string;
  FE_02_ORDER?: any;
  SERVICE_STAGE: string;
}
