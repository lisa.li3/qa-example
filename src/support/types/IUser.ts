export type UserGender = 'male' | 'female' | 'prefer_not_to_say';

export default interface IUser {
  subscriptionApproval?: boolean;
  credit?: number;
  staffDiscountRate?: number;
  attributes?: {
    id: string;
    emailAddress: string;
    password: string;
    firstName?: string;
    lastName?: string;
    gender?: UserGender;
    dateOfBirth?: string;
    addresses?: {
      id?: string;
      firstName?: string;
      lastName?: string;
      contactNumber?: {
        phone?: string;
        mobile?: string;
      };
      addressLine1?: string;
      addressLine2?: string;
      city?: string;
      county?: string;
      country?: string;
      postcode?: string;
      isDefault?: boolean;
      nickname?: string;
    }[];
  };
}

export interface IUserResponse {
  id: string;
  userToken: string;
  refreshToken: string;
  attributes: {
    emailAddress: string;
    password: string;
    firstName?: string;
    lastName?: string;
    gender?: UserGender;
    dateOfBirth?: string;
  };
}
