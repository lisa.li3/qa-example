import { IServiceGroup } from './IServiceGroup';

export interface IShipping {
  storeIds?: string[];
  destinationCountries: string[];
  fulfilmentCentres: string[];
  methodIds: string[];
  attributes: IServiceGroup;
}
