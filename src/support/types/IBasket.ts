export interface IBasket {
  [key: string]: number;
}
