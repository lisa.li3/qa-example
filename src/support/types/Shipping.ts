export enum DeliveryType {
  ToAddress = 'ToAddress',
  ToStore = 'ToStore',
}

export enum Day {
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6,
  Sunday = 7,
}

export enum DayName {
  Monday = 'Monday',
  Tuesday = 'Tuesday',
  Wednesday = 'Wednesday',
  Thursday = 'Thursday',
  Friday = 'Friday',
  Saturday = 'Saturday',
  Sunday = 'Sunday',
}

export interface IServiceGroupAttributes {
  ServiceGroup: {
    promises: IDayPromise[];
  };
}

export interface IDayPromise {
  day: string;
  cutoff: string;
  transit_time: string;
  specific_days?: {
    day: string[];
  };
}

export interface IServiceGroup {
  storeIds: string[];
  destinationCountries: string[];
  fulfilmentCentres: string[];
  methodIds: string[];
  attributes?: IServiceGroupAttributes;
}
