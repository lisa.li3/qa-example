export default interface IPaymentSeeder {
  attributes: {
    emailAddress?: string;
    shopperGroupId?: string;
    credits: Array<{
      disabled: boolean;
      endDate: string;
      id: string;
      initialValue: number;
      name: string;
      remainingValue: number;
      startDate: string;
      transactions: Array<unknown>;
    }>;
  };
}
