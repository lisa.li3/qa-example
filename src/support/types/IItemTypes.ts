export interface IItemTypes {
  country: string;
  keywords: string;
  redirect: string;
}
