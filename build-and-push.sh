echo "Logging into ECR ( aws-mfa should have been run before now )"\
&&\
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin aws-account-id.dkr.ecr.eu-west-1.amazonaws.com\
&&\
echo "Starting docker build"\
&&\
docker buildx create --use\
&&\
echo "Building image and pushing"\
&&\
docker buildx build --platform linux/amd64 . -f docker/ci-runner.dockerfile --push -t aws-account-id.dkr.ecr.eu-west-1.amazonaws.com/sd-ci-testrunner:latest\
&&\
echo "Complete!"