#!/bin/bash

DEFAULT_RUNNER="lambdatest"
DEFAULT_CONFIG="desktop"
DEFAULT_TAG="@REGRESSION"

if [ $IS_ACID ]; then
  echo "Updating to run acid test pack"
  DEFAULT_TAG="@ACID"
fi

if [[ "$@" == *"local"* ]]; then
  echo "Updating to run on local"
  RUNNER="local"
fi

if [[ "$@" == *"saucelabs"* ]]; then
  echo "Updating to run on saucelabs"
  RUNNER="saucelabs"
fi

if [[ "$@" == *"mobile"* ]]; then
  echo "Updating browser to mobile"
  CONFIG="mobile"
fi

if [[ "$@" == *"tablet"* ]]; then
  echo "Updating browser to tablet"
  CONFIG="tablet"
fi

if [[ "$@" == *"headless"* ]]; then
  echo "Updating chrome to be headless"
  HEADLESS="true"
fi

if [[ "$@" == *"@"* ]]; then
  TAG='@'$(echo $@ | cut -d'@' -f 2 | cut -d' ' -f 1)
  echo "Updating tag to be $TAG"
fi

if [ "$1" = "--help" ]; then
  echo "Running the Automation";
  echo "-------------------------------";
  echo "";
  echo "Usage:";
  echo "yarn test [ lambda* | saucelabs | local ] [ desktop* | tablet | mobile ] [ @REGRESSION* | tag ]";
  echo "* Default"
  echo "";
  echo "-------------------------------";
  echo "-------------------------------";
  echo "";
  echo "Example Commands:";
  echo ""
  echo "# Running SauceLabs Mobile Tests on Basket";
  echo "yarn test mobile @BASKET";
  echo "";
  echo "# Running Local Tablet Tests on Basket";
  echo "yarn test local tablet @BASKET";
  echo "";
  echo "-------------------------------";
  echo "-------------------------------";
exit 0; fi

if [ "$RUNNER" = "" ]; then RUNNER="$DEFAULT_RUNNER"; fi
if [ "$CONFIG" = "" ]; then CONFIG="$DEFAULT_CONFIG"; fi
if [ "$TAG" = "" ]; then TAG="$DEFAULT_TAG"; fi

if [ $SKIP_DEFECTS ]; then
  TAG_EXTENSION=" or @DEFECT"
else
  TAG_EXTENSION=""
fi

if [ "$HEADLESS" != "" ]; then
  TAG_EXTENSION+=" or @SKIP_HEADLESS"
fi

if [ "$RUNNER" = "saucelabs" ]; then
  TAG_EXTENSION+=" or @SKIP_SAUCELABS"
fi

if [ "$RUNNER" = "lambdatest" ]; then
  TAG_EXTENSION+=" or @SKIP_LAMBDA"
fi

TAG_EXPRESSION="$TAG"
if [ "$CONFIG" = "desktop" ]; then
  TAG_EXPRESSION="${TAG_EXPRESSION} and not (@SKIP_DESKTOP${TAG_EXTENSION})"
elif [ "$CONFIG" = "tablet" ]; then
  TAG_EXPRESSION="${TAG_EXPRESSION} and not (@SKIP_TABLET${TAG_EXTENSION})"
elif [ "$CONFIG" = "mobile" ]; then
  TAG_EXPRESSION="${TAG_EXPRESSION} and not (@SKIP_MOBILE${TAG_EXTENSION})"
fi

export TAG_EXPRESSION

 if [ $BITBUCKET_BRANCH ]; then
        BRANCH=$BITBUCKET_BRANCH
  else
    if git branch --show-current ; then
        BRANCH=$(git branch --show-current)
    else
        BRANCH="develop"
    fi
  fi

export BRANCH
export DEVICE=$CONFIG
export TAG
export HEADLESS
export RUNNER
export IS_ACID=$IS_ACID

echo "Running "$TAG_EXPRESSION

echo "wdio run ./config/"$RUNNER/$RUNNER"."$CONFIG.conf.ts" --cucumberOpts.tagExpression=""$TAG_EXPRESSION"

wdio run ./config/$RUNNER/$RUNNER.$CONFIG.conf.ts --cucumberOpts.tagExpression="$TAG_EXPRESSION"

